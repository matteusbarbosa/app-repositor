<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class UserSalesChannel extends Pivot
{
    protected $table = 'user_sales_channel';
    public $incrementing = true;

    public function sales_goals(){
        return $this->belongsToMany('App\SalesGoal', 'sales_channel_goal')->withPivot('id');
    }


}
