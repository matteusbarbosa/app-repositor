<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use App\Tag;
class Search extends Model
{
    public $timestamps = false, $stocks, $tags, $query;
    protected $fillable = ['session_id', 'term', 'result_count', 'created_at'];
    protected $table = 'search';
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function hasFilters(){

        if(!is_array($this->query))
        return false;

        if(is_array($this->query) && count($this->query) == 1 && isset($this->query['term']) && ($this->query['term'] == null))
        return false; 

        return true;
    }
    public function getClassifiedStocks($wholesale_only = false, $filter_displayed = false){

        $t = $this->term;
        if(!$this->hasFilters() && empty($t)){
            //não carregue todos os resultados do banco
            $this->stocks = collect([]);
            return Tag::withStocks($this->stocks, $filter_displayed);
        }
        $this->stocks = Stock::join('product', 'product.id', '=', 'stock.product_id')
        ->select('stock.*')
        ->whereNull('product.disabled_at')
        ->whereNull('stock.disabled_at');
        if(!empty($t)){
            $this->stocks = $this->stocks->where(function($q) use ($t){
                $q->orWhere('product.title', 'LIKE', '%'.$t.'%')
                ->orWhere('product.slug', 'LIKE', '%'.$t.'%')
                ->orWhere('product.details', 'LIKE', '%'.$t.'%')
                ->orWhere('stock.details', 'LIKE', '%'.$t.'%');
            }) ;     
        }

        if($this->hasFilters()){
            $this->stocks = $this->stocks
            ->leftJoin('stock_field', 'stock_field.stock_id', '=', 'stock.id')
            ->leftJoin('field', 'field.id', '=', 'stock_field.field_id');
            $filters = $this->query;
            $this->stocks = $this->stocks->where(function($q) use ($filters){
                foreach($filters as $slug => $values){
                    if($slug == 'term' || $slug == 'tag')
                    continue;
                    if($slug == 'price_unit' || $slug == 'price_total'){
                        $range = explode('-',$values);
                        $range_start = $range[0];
                        $range_end = $range[1];
                        $q->where(function($q) use ($slug, $range_start, $range_end){
                            $q->where('field.slug', $slug)
                            ->where('stock_field.content', '>=', $range_start)
                            ->where('stock_field.content', '<=', $range_end);
                        });
                        continue;
                    }
                    if(is_array($values)){
                        foreach($values as $v){                          
                            $q->where(function($q) use ($v, $slug){
                                $q->where('field.slug', $slug)
                                ->where('stock_field.content', 'LIKE', '%'.$v.'%');
                            });
                        }      
                    } else {
                        $q->where('field.slug', $slug)
                        ->where('stock_field.content', 'LIKE', '%'.$values.'%');                   
                    }
                }
            });
            foreach($this->query as $slug => $values){
                
                if($slug == 'tag'){

                    if(is_array($values)){
                        $tags = $values;
                    } else {
                        $tags = [$values];
                    }
            
                    $this->stocks = $this->stocks
                    ->join('tag_stock', 'tag_stock.stock_id', '=', 'stock.id')
                    ->join('tag', 'tag.id', '=', 'tag_stock.tag_id')
                    ->whereIn('tag.slug', $tags)
                    ->whereNull('tag.disabled_at');
                 
                } else {
                    if(!is_array($values))
                    continue;
    
                    foreach($values as $v){
                        $this->stocks = $this->stocks->where(function($q) use ($v, $slug){
                            $q->where('field.slug', $slug)
                            ->where('stock_field.content', 'LIKE', '%'.$v.'%');
                        });
                    } 
                }

                         
            }
        }
        if($wholesale_only){
            $unit_type = StockType::where('slug', 'unit')->first();
            $this->stocks = $this->stocks->where('stock.type_id', '!=', $unit_type->id);
        }
        $this->stocks = $this->stocks->get();
        return Tag::withStocks($this->stocks, $filter_displayed);
    }
}
