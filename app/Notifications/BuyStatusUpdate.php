<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use NotificationChannels\WebPush\WebPushMessage;
use NotificationChannels\WebPush\WebPushChannel;
use App\Buy;
use App\Status;

class BuyStatusUpdate extends Notification
{
    
    private $status, $buy;

    public function __construct($buy_id, $status_id){
        $this->buy = Buy::find($buy_id);
        $this->status = Status::find($status_id);
    }    
    
    private function persist(){
        $n = new \App\Notification();
        $n->type = 'log';
        $n->notifiable_type = 'App\Buy';
        $n->notifiable_id = $this->buy->id;
        $n->data = $this->price;
        $n->save();
    }


    public function via($notifiable)
    {
        return [WebPushChannel::class];
    }

    public function toWebPush($notifiable, $notification)
    {
        return (new WebPushMessage)
            ->title(config('app.name').' informa – Pedido '.$this->buy->id.' atualizado!')
            ->icon('/img/notification-icon.png')
            ->body('Novo status: '.$this->status->title)
            ->action('Acompanhar pedido', 'buy-status')
            ->tag('BuyStatusUpdate');
            
            //->vibrate()
            // ->data(['id' => $notification->id])
            // ->badge()
            // ->dir()
            // ->image()
            // ->lang()
            // ->renotify()
            // ->requireInteraction()
            // ->tag()
    }
}