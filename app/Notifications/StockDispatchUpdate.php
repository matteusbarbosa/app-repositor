<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use NotificationChannels\WebPush\WebPushMessage;
use NotificationChannels\WebPush\WebPushChannel;
use App\ItemStatus;
use App\Stock;
use App\Status;

class StockDispatchUpdate extends Notification
{
    
    private $stock, $status;

    public function __construct($stock_id, $status_id, $details = null){
        $this->stock = Stock::find($stock_id);
        $this->status = Status::find($status_id);
        $this->details = $details;
    }

    public function via($notifiable)
    {
        return [WebPushChannel::class];
    }

    public function toWebPush($notifiable, $notification)
    {
        return (new WebPushMessage)
            ->title(config('app.name').' informa: Status do item '.$this->stock->product->title.' no buy foi atualizado para '.$this->status->title)
            ->icon('/notification-icon.png')
            ->body($this->details)
            ->action('Acompanhar pedido', '/')
            ->tag('StockDispatchUpdate');
            //->vibrate()
            // ->data(['id' => $notification->id])
            // ->badge()
            // ->dir()
            // ->image()
            // ->lang()
            // ->renotify()
            // ->requireInteraction()
            // ->tag()
    }
}