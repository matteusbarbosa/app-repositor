<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use NotificationChannels\WebPush\WebPushMessage;
use NotificationChannels\WebPush\WebPushChannel;
use App\Buy;

class BuyPriceInform extends Notification
{

    private $buy, $price;
    public function __construct($id){
        $this->buy = Buy::find($id);
        $this->price = !empty($this->buy->items) ? 'R$'.$this->buy->items->sum('price_total') : __('legend.contact-seller', ['contact' => config('app.admin_phone')]);
    }

    public function via($notifiable)
    {
        return [WebPushChannel::class];
    }
    
    private function persist(){
        $n = new \App\Notification();
        $n->type = 'log';
        $n->notifiable_type = 'App\Buy';
        $n->notifiable_id = $this->buy->id;
        $n->data = $this->price;
        $n->save();
    }

    public function toWebPush($notifiable, $notification)
    {

        $this->persist();

        return (new WebPushMessage)
            ->title(config('app.name').' – '.__('object.buy').' #'.$this->buy->id)
            ->icon('/notification-icon.png')
            ->body(__('legend.price-total').' : '.$this->price)
            ->action('Confira', 'buy')
            ->tag('BuyPriceInform');
            //->vibrate()
            // ->data(['id' => $notification->id])
            // ->badge()
            // ->dir()
            // ->image()
            // ->lang()
            // ->renotify()
            // ->requireInteraction()
            // ->tag()
    }
}