<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use NotificationChannels\WebPush\WebPushMessage;
use NotificationChannels\WebPush\WebPushChannel;
use App\ItemStatus;

class ItemStatusUpdate extends Notification
{
    
    private $item_status;

    public function __construct($id){
        $this->item_status = ItemStatus::find($id);
    }


    public function via($nifiable)
    {
        return [WebPushChannel::class];
    }

    public function toWebPush($notifiable, $notification)
    {
        return (new WebPushMessage)
            ->title(config('app.name').' informa – Pedido '.$this->item_status->item->cart->buy->id.' atualizado!')
            ->icon('/notification-icon.png')
            ->body('O status do item '.$this->item_status->item->object->product->title.' no buy #'.$this->item_status->item->cart->buy->id.' foi atualizado: '.$this->item_status->status->title)
            ->action('Acompanhar buy', 'item-status')
            ->data(['buy_id' => $this->item_status->item->cart->buy->id])
            ->tag('ItemStatusUpdate');
            //->vibrate(1)
            // ->badge()
            // ->dir()
            // ->image()
            // ->lang()
            // ->renotify()
            // ->requireInteraction()
            // ->tag()
    }
}