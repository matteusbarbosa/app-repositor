<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use NotificationChannels\WebPush\WebPushMessage;
use NotificationChannels\WebPush\WebPushChannel;
use App\Promotion;

class PromotionAdvertise extends Notification
{

    private $promotion;
    public function __construct($id){
        $this->promotion = Promotion::find($id);
    }

    public function via($notifiable)
    {
        return [WebPushChannel::class];
    }

    public function toWebPush($notifiable, $notification)
    {

        $notify = (new WebPushMessage)
            ->title(config('app.name').' – '.$this->promotion->title)
            ->icon('/notification-icon.png')
            ->body($this->promotion->details)
            ->action('Confira', 'promotion')
            ->requireInteraction()
            ->data(['id' => $this->promotion->id])
            ->tag('PromotionAdvertise');

            if($this->promotion->file_id != null)
            $notify = $notify->image(url($this->promotion->image->url));

        return $notify;
            //->vibrate()
            // ->badge()
            // ->dir()
            // ->image()
            // ->lang()
            // ->renotify()
            // ->requireInteraction()
            // ->tag()
    }
}