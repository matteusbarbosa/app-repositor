<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockField extends Model
{
    protected $table = 'stock_field';
}
