<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesRouteStatus extends Model
{
    protected $table = 'sales_route_status';

    
    public function route(){
        return $this->hasMany('App\SalesRoute');
    }
}
