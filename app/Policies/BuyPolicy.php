<?php

namespace App\Policies;

use App\User;
use App\Buy;
use Illuminate\Auth\Access\HandlesAuthorization;

class BuyPolicy
{
    use HandlesAuthorization;

        /**
     * Acesso à fila
     *
     * @param  \App\User  $user
     * @param  \App\Buy  $buy
     * @return mixed
     */
    public function queue(User $user)
    {
        return $user->role->slug === 'attendant' || $user->role->slug === 'admin';
    }

    /**
     * Determine whether the user can view the buy.
     *
     * @param  \App\User  $user
     * @param  \App\Buy  $buy
     * @return mixed
     */
    public function view(User $user, Buy $buy)
    {
        //
    }

    /**
     * Determine whether the user can create buys.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the buy.
     *
     * @param  \App\User  $user
     * @param  \App\Buy  $buy
     * @return mixed
     */
    public function update(User $user, Buy $buy)
    {
        //
    }

    /**
     * Determine whether the user can delete the buy.
     *
     * @param  \App\User  $user
     * @param  \App\Buy  $buy
     * @return mixed
     */
    public function delete(User $user, Buy $buy)
    {
        //
    }

    /**
     * Determine whether the user can restore the buy.
     *
     * @param  \App\User  $user
     * @param  \App\Buy  $buy
     * @return mixed
     */
    public function restore(User $user, Buy $buy)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the buy.
     *
     * @param  \App\User  $user
     * @param  \App\Buy  $buy
     * @return mixed
     */
    public function forceDelete(User $user, Buy $buy)
    {
        //
    }
}
