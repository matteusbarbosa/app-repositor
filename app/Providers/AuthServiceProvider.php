<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\BuyPolicy'
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('sell', function ($user, $object = null) {
            return $user->role->slug === 'seller' || $user->role->slug === 'admin';
        });

        Gate::define('manage', function ($user, $object = null) {
            return $user->role->slug === 'attendant' || $user->role->slug === 'admin';
        });

        Gate::define('deliver', function ($user, $object = null) {
            return $user->role->slug === 'deliver';
        });

        Gate::define('login:phone', function ($user, $object = null) {
            return $user->role->slug !== 'admin' && $user->role->slug !== 'attendant';
        });

        //Gate::define('buy.queue', 'App\Policies\BuyPolicy@queue');
        //Gate::define('buy.queue', 'App\Policies\BuyPolicy@queue');
       // Gate::resource('buy', 'App\Policies\BuyPolicy');

    }
}
