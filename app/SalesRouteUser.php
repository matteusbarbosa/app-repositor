<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class SalesRouteUser extends Pivot
{
    
    protected $table = 'sales_route_user';
    public $incrementing = true;

    public function user(){
        return $this->belongsTo('App\User');
    }
}
