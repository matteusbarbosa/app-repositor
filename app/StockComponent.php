<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class StockComponent extends Pivot
{
    protected $table = 'stock_component';
    public $incrementing = true;
}
