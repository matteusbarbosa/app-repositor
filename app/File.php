<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class File extends Model
{
    protected $table = 'file';

    public function owner(){
        return $this->belongsTo('App\User', 'id', 'user_id');
    }

    public function delete(){
        Storage::delete($this->url);
        parent::delete();
    }

}
