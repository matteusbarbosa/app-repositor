<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buy extends Model
{
    protected $table = 'buy';

    public function user(){
        return $this->belongsTo('App\User');
    }
    
    public function addresses(){
        return $this->belongsToMany('App\Address', 'buy_address');
    }

    public function cart(){
        return $this->belongsTo('App\Cart');
    }
    
    public function statuses(){
        $data = collect([]);
        foreach($this->items as $k => $item){
            $data->push($item->getLastStatus());
        }
        return $data;
    }

    public function getLastStatus(){
       $status_last = $this->statuses()->sortByDesc('updated_at')->first();

       if(!empty($status_last))
       return $status_last;

    }

    public function getFirstStatus(){

        $status_first = $this->statuses()->sortBy('item_status.created_at')->first();
 
        if(!empty($status_first))
        return $status_first;
 
     }

    public function items(){
        return $this->hasMany('App\Item', 'cart_id', 'cart_id');
    }
}
