<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    public $dates = ['disabled_at'];
    protected $table = 'field';

    public function stock(){
        return $this->belongsToMany('App\Stock', 'stock_field');
    }

    public function getContentFormatted(){
        $method_name = $this->type.'Display';
        $has_method = method_exists(new \Helpers\Format, $method_name);

        if(!$has_method)
        return $this->pivot->content;

        return \Helpers\Format::$method_name($this->pivot->content);
    }

}
