<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuyAddress extends Model
{
    protected $table = 'buy_address';
}
