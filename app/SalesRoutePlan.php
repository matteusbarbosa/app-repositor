<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use App\SalesRouteProgress;
class SalesRoutePlan extends Model
{
    protected $table = 'sales_route_plan';
    public function sales_route(){
        return $this->belongsTo('App\SalesRoute');
    }
    public function plan_addresses(){
        return $this->belongsToMany('App\Address', 'sales_route_plan_address')->withPivot('id');
    }
    public function getProgress(){
        $data = [];
       foreach($this->plan_addresses as $pa){
        $pivot_id = $pa->pivot->id;
        $list_progress = SalesRouteProgress::where('sales_route_plan_address_id', $pivot_id)->get();
        $data[$pa->id] = new \stdClass();
        $data[$pa->id]->address = $pa;
        $data[$pa->id]->progress = collect([]);
        foreach($list_progress as $p){
            if(empty($p->user))
            continue;
            if(empty($p->user->user))
            continue;
            if(empty($p->status))
            continue;
            $p_obj = new \stdClass();
            $p_obj->id = $p->id;
            $p_obj->user_name = $p->user->user->name;
            $p_obj->sales_status = $p->status->title;
            $p_obj->status = $p->status;
            $data[$pa->id]->progress->push($p_obj);
        }
    }
       return $data;
    }
}
