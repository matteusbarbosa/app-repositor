<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = 'cart';

    public function user(){
        return $this->belongsTo('\App\User');
    }

    public function buy(){
        return $this->hasOne('\App\Buy');
    }

    public function items(){
        return $this->hasMany('\App\Item');
    }
}
