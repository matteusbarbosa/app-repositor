<?php

function env_read($env_config){
    return explode(',', $env_config);
}

function is_enabled($key, $cfg){
  if(empty(config('instance.'.$cfg)))
  return false;

  $items_count = substr_count(config('instance.'.$cfg), ',');

  if($items_count > 0){
    $cfg_items = explode(',', config('instance.'.$cfg));
    if(in_array($key, $cfg_items))
    return true;
  } else {
    if(config('instance.'.$cfg) == $key)
    return true;
  }

  return false;
}

function last_segment(){
  $segments = Request::segments();
  return array_pop($segments);
}
function get_breadcrumbs($data){
  $b_obj = is_array($data) && count($data) > 0 ? current($data) : null;
  $model = ($b_obj instanceof \Illuminate\Database\Eloquent\Model) ? $b_obj : null;
  $breadcrumbs = Breadcrumbs::generate(null, $model);
  return $breadcrumbs;
}
function get_datatable_lang($lang_object){
  return json_encode([
    'sEmptyTable'=>  __('datatable.sEmptyTable', ['lang_object' => trans_choice('object.'.$lang_object, 1)]),
    'sInfo'=>  __('datatable.sInfo', ['lang_object' => trans_choice('object.'.$lang_object,2)]),
    'sInfoEmpty'=>  __('datatable.sInfoEmpty', ['lang_object' => trans_choice('object.'.$lang_object,2)]),
    'sInfoFiltered'=>  __('datatable.sInfoFiltered', ['lang_object' => trans_choice('object.'.$lang_object, 1)]),
    'sInfoPostFix'=>  __('datatable.sInfoPostFix'),
    'sInfoThousands'=>  __('datatable.sInfoThousands'),
    'sLengthMenu'=>  __('datatable.sLengthMenu', ['lang_object' => trans_choice('object.'.$lang_object,2)]),
    'sLoadingRecords'=>  __('datatable.sLoadingRecords'),
    'sProcessing'=>  __('datatable.sProcessing'),
    'sZeroRecords'=>  __('datatable.sZeroRecords', ['lang_object' => trans_choice('object.'.$lang_object, 1)]),
    'sSearch'=>  __('datatable.sSearch'),
    'oPaginate'=> [
      'sNext'=>  __('datatable.sNext'),
      'sPrevious'=>  __('datatable.sPrevious'),
      'sFirst'=>  __('datatable.sFirst'),
      'sLast'=>  __('datatable.sLast')
    ],
    'oAria'=> [
      'sSortAscending'=>  __('datatable.sSortAscending'),
      'sSortDescending'=>  __('datatable.sSortDescending')
      ]
    ], 1);
  }
  function money_input_parse($money){
    return str_replace(',', '.', $money);
  }
  function path_admin($path = null){
    return config('app.url_admin').'/'.$path;
  }
  function path_main(){
    return env('APP_URL');
  }
  function flush_buffers(){
    ob_end_flush();
    ob_flush();
    flush();
    ob_start();
  }
  function flash($flash, $class, $prefix, $text){
    session()->flash('flash' , $flash);
    session()->flash('flash-class' ,  $class);
    session()->flash( 'flash-prefix', $prefix);
    session()->flash('flash-text', $text);
  }
  /*
  * Percorra todos os índices até retornar nulo
  */
  function find_deep($array, $element){
    foreach($array as $k => $v){
      if(is_array($v)){
        if(isset($v[$element]))
        return $v[$element];
        find_deep($v, $element);
      }
    }
  }
  function field_type($table_name, $field_name){
    $field_describe = DB::select('describe '.$table_name.' '.$field_name);
    if(strpos($field_describe[0]->Type, 'varchar') !== false || strpos($field_describe[0]->Type, 'text') !== false)
    return 'string';
    if(strpos($field_describe[0]->Type, 'integer') !== false )
    return 'number';
    return null;
  }
  function text_normalize($text, $limit){
    return \Helpers\Format::replaceAccents(str_replace(",","", str_pad(substr($text, 0, $limit), $limit, " ")));
  }
  function number_normalize($number, $limit){
    return str_pad(substr($number, -$limit), $limit, "0", STR_PAD_LEFT);
  }

  function upload_field($file){
    if (!file_exists( public_path().'/storage/field' )) {
      mkdir(public_path().'/storage/field', 0755, true);
    }
    $file->move(public_path('storage/field'), $file->getClientOriginalName());
    $file_path = 'storage/field/'.$file->getClientOriginalName();
    $file_id = \App\File::insertGetId([ 'label' => $file->getClientOriginalName(), 'user_id' => Auth::user()->id, 'url' => $file_path ]);  
    return $file_id;
  }
  function upload_tag($file){
    if (!file_exists( public_path().'/storage/tag' )) {
      mkdir(public_path().'/storage/tag', 0755, true);
    }
    upload($file, 'storage/tag/1280', 1280);
    return upload($file, 'storage/tag/240', 240);
  }
  function upload_product($file){
    if (!file_exists( public_path().'/storage/product' )) {
      mkdir(public_path().'/storage/product', 0755, true);
    }
    upload($file, 'storage/product/1280', 1280);
    return upload($file, 'storage/product/240', 240);
  }
  function upload_stock($file){
    if (!file_exists( public_path().'/storage/stock' )) {
      mkdir(public_path().'/storage/stock', 0755, true);
    }
    upload($file, 'storage/stock/1280', 1280);
    return upload($file, 'storage/stock/240' , 240);
  }
  function upload($file, $path, $size){
    $resize = Image::make($file)->widen($size);
    $file_path = $path.'/'.$file->getClientOriginalName();
    $final_path = public_path().'/'.$file_path;
    if (!file_exists($path)) {
      mkdir($path, 0755, true);
    }
    $resize->save($final_path, 75);
    // $url    =  'storage/'.str_replace('public/', '', Storage::url($store));
    // $store = Storage::putFile('public/storage/stock/'.$size.'/'.$file->getClientOriginalName(), $resize->__toString());
    $file_id = \App\File::insertGetId([ 'label' => $file->getClientOriginalName(), 'user_id' => Auth::user()->id, 'url' => $file_path ]);  
    return $file_id;
  }
  