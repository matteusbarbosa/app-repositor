<?php

namespace Helpers;

class Visual {

    public static function flash($icon, $title, $tag, $args = []) {
        return '<div class="notify-icon-wrapper"><i class="icon-' . $icon . '"></i></div><div class="notify-message-wrapper"><h3>' . trans('cognition.' . $title) . '</h3><p>' . trans('notify.' . $tag, $args) . '</p></div>';
    }

    //can access $field->pivot
    public static function displayStockField($field){
        
        $method_name = $field->slug.'DisplayStockField';
        $has_method = method_exists(new self, $method_name);

        if(!$has_method)
        return sprintf('<span class="stock-option field-content %s">%s</span>', $field->slug, $field->getContentFormatted());

        return self::$method_name($field);
    }

    public static function colorDisplayStockField($field){

        if(!isset(config('instance-colors')[$field->pivot->content]))
        return null;

        return sprintf('<span class="stock-option field-content color" style="color:%s">%s</span>', $field->pivot->content, config('instance-colors')[$field->pivot->content]);

    }

    public static function priceRangeDisplay($stock_fields){
        $range_list = [];

        $range_possible = collect([50, 100, 250, 500, 750, 1000, 2500, 5000]);

        foreach($stock_fields as $sf){
            $range = new \stdClass();
            $price = $sf->content;
         
            $range->start_at = $range_possible->last(function ($item, $key) use ($price){
                return $price >= $item;
            });  
            
            $range_start = $range->start_at;
            
            $range->end_at = $range_possible->last(function ($item, $key) use ($price){
                return $item >= $price;
            });

            if($range->start_at != $range->end_at)
            $range_list[$range->start_at.'-'.$range->end_at] = __('product.price-from-to',['from' => $range->start_at, 'to' => $range->end_at]);
        }

        return $range_list;
    }


   
}