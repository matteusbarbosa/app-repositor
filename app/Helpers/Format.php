<?php
namespace Helpers;
class Format {
    public static function flash($icon, $title, $tag, $args = []) {
        return '<div class="notify-icon-wrapper"><i class="icon-' . $icon . '"></i></div><div class="notify-message-wrapper"><h3>' . trans('cognition.' . $title) . '</h3><p>' . trans('notify.' . $tag, $args) . '</p></div>';
    }
    final public static function dateYmd($date) {
        return date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $date)));
    }
    final public static function dateDmy($date) {
        return date('d/m/Y H:i:s', strtotime(str_replace('-', '/', $date)));
    }
    final public static function dateTimestamp($date) {
        return strtotime(self::dateYmd($date));
    }
    /**
    * @param dom : html de entrada da aplicação
    * @param format : formato do arquivo de saída
    * @param name : nome do arquivo de saída
    */
    final public static function replaceAccents($text) {
        $table = array(
            'Š' => 'S', 'š' => 's', 'Đ' => 'Dj', 'đ' => 'dj', 'Ž' => 'Z', 'ž' => 'z', 'Č' => 'C', 'č' => 'c', 'Ć' => 'C', 'ć' => 'c',
            'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E',
            'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O',
            'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss',
            'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e',
            'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o',
            'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ý' => 'y', 'ý' => 'y', 'þ' => 'b',
            'ÿ' => 'y', 'Ŕ' => 'R', 'ŕ' => 'r',
        );
        return strtr($text, $table);
    }
    /*
    * 
    */
    final public static function redirectLink($url, $params = null) {
        global $CFG;
        $urlValues = '?redir=1';
        if ($params) {
            if (is_array($params)) {
                foreach ($params as $field => $value) {
                    $urlValues .= '&' . $field . '=' . $value;
                }
            } else {
                $urlValues .= '&' . $field . '=' . $value;
            }
        }
        echo '<script>location.href="' . $CFG->wwwroot . '/' . $url . $urlValues . '";</script>;';
    }
    /**
    * @param $data formato BR ou EUA
    */
    final public static function urlFriendly($string) {
        $modified = str_replace(' ', '-', strtolower(self::replaceAccents($string)));
        return $modified;
    }
    public static function priceDisplay($price){
        return 'R$'.$price;
    }
    public static function weightDisplay($weight){
        if($weight > 1000){
            $display = intval($weight/1000);
            if($weight%1000 > 0)
            $display .= ','.($weight%1000) / 100;
            return $display.__('legend.kilograms');
        }
        return $weight.__('legend.grams');
    }
    public static function colorDisplay($rgb){
        if(!isset(config('instance-colors')[$rgb]))
        return null;
        return config('instance-colors')[$rgb];
    }
    public static function sizeDisplay($size){
        return __('product.size-abbr').$size;
    }
    /*
    * 
    */
    public static function format($string, $type) {
        $string = preg_replace("[^0-9]", "", $string);
        if (!$type) {
            switch (strlen($string)) {
                case 10: $type = 'fone';
                break;
                case 8: $type = 'cep';
                break;
                case 11: $type = 'cpf';
                break;
                case 14: $type = 'cnpj';
                break;
            }
        }
        switch ($type) {
            case 'fone':
            $string = '(' . substr($string, 0, 2) . ') ' . substr($string, 2, 4) .
            '-' . substr($string, 6);
            break;
            case 'cep':
            $string = substr($string, 0, 5) . '-' . substr($string, 5, 3);
            break;
            case 'cpf':
            $string = substr($string, 0, 3) . '.' . substr($string, 3, 3) .
            '.' . substr($string, 6, 3) . '-' . substr($string, 9, 2);
            break;
            case 'cnpj':
            $string = substr($string, 0, 2) . '.' . substr($string, 2, 3) .
            '.' . substr($string, 5, 3) . '/' .
            substr($string, 8, 4) . '-' . substr($string, 12, 2);
            break;
            case 'rg':
            $string = substr($string, 0, 2) . '.' . substr($string, 2, 3) .
            '.' . substr($string, 5, 3);
            break;
            case 'money':
            $string = substr($string, 0, -2) . ',' . substr($string, -2);
            //$string = substr($string, 0, -2). ',' . substr($string, -2);
            break;
        }
        return $string;
    }
    /*
    * 
    */
    public static function filterNumbers($string) {
        preg_match_all('/\d+/', $string, $matches);
        return implode('', $matches[0]);
    }
    /*
    @param text mensagem a ser exibida
    @param type success/fail para sucesso ou fracasso
    */
    final public static function bsMessage($text, $type, $hasPopup = null) {
        //emite alerta de erro
        if ($hasPopup) {
            echo '<script>alert("' . $text->title . '");</script>';
        }
        echo '<div class="alert alert-' . $type . '">
        <i class="fa fa-exclamation"></i>
        <strong>' . $text->title . '</strong>. ' . $text->details . '
        </div>';
    }
    /* Validação de CPF (sem pontos e traço)
    * Otimização pendente 
    * 
    */
    public static function validateCPF($string) {
        //DÍGITO 1
        $numbersVector = str_split($string);
        $validationPair = 0;
        $positionCounter = 0;
        $sum = 0;
        //Calcule apenas os 9 números, obtenha o décimo
        for ($c = 10; $c != 1; $c--) {
            $sum += $numbersVector[$positionCounter] * $c;
            $positionCounter++;
        }
        $mod1 = $sum % 11;
        if ($mod1 < 2) {
            $validationPair = 0;
        } else {
            $validationPair = (int) (11 - $mod1);
        }
        //DÍGITO 2
        $positionCounter = 0;
        $sum2 = 0;
        //Calcule com os 10 números, obtenha o 11
        for ($c = 11; $c != 1; $c--) {
            $sum2+= $numbersVector[$positionCounter] * $c;
            $positionCounter++;
        }
        $mod2 = $sum2 % 11;
        if ($mod2 < 2) {
            $validationPair .= 0;
        } else {
            $validationPair .= (int) (11 - $mod2);
        }
        //últimos 2 dígitos conferem?
        if (substr($string, -2) == $validationPair) {
            return true;
        }
        //não conferem
        else {
            return false;
        }
    }
    /*
    * Entra 31
    * Sai: Trigésima Primeira
    * Verifica se number > 10 (Constata execução dupla)
    * Obtém parte decimal (Executa o método)
    * Obtém parte de unidades (permite a continuação)
    * 
    *      * 
    */
    public static function verbalNumberOrder($number, $lastchar) {
        //30 // passa direto
        //31,12,25 // intercepta, obtém parte decimal e aplica método. Obtém parte unitária e aplica método.
        //8 // passa direto
        if($number > 100 or $number == 0){
            throw new \Exception('Número fornecido não suportado.');
        }
        if ($number > 10 and $number % 10 > 0) {
            $unit = $number % 10;
            $decimal = $number - $unit;  
            return self::verbalNumberOrder($decimal, $lastchar).' '.self::verbalNumberOrder($unit, $lastchar);
        } else {
            switch ($number) {
                case 1: $verb = 'Primeir';
                break;
                case 2: $verb = 'Segund';
                break;
                case 3: $verb = 'Terceir';
                break;
                case 4: $verb = 'Quart';
                break;
                case 5: $verb = 'Quint';
                break;
                case 6: $verb = 'Sext';
                break;
                case 7: $verb = 'Sétim';
                break;
                case 8: $verb = 'Oitav';
                break;
                case 9: $verb = 'Non';
                break;
                case 10: $verb = 'Décim';
                break;
                case 20: $verb = 'Vigésim';
                break;
                case 30: $verb = 'Trigésim';
                break;
                case 40: $verb = 'Quadragésim';
                break;
                case 50: $verb = 'Quinquagésim';
                break;
                case 60: $verb = 'Sexagésim';
                break;
                case 70: $verb = 'Septagésim';
                break;
                case 80: $verb = 'Octagésim';
                break;
                case 90: $verb = 'Nonagésim';
                break;
            }
            $verb .= $lastchar;
            return $verb;
        }
    }
    /*
    * Recebe a string via GET e exibe os erros
    */
    public static function notify($notify) {
        global $DB;
        $alerts = \explode('|', $notify);
        array_pop($alerts);
        foreach ($alerts as $alert) {
            $message = AccessControl::getNotification($alert);
            if ($message) {
                echo '<div class="col-sm-12">';
                self::bsMessage($message, $message->class);
                echo '</div>';
            }
        }
    }
    /*
    * Filtro padrão simples para entradas de dados ao Banco
    * Deve ser aplicado a cada input
    * Não deve ser aplicado a queries inteiras
    * Remova comandos SQL
    * Remova aspas simples e duplas
    */
    public static function sanitize($input) {
        $sanitize = trim($input);
        $replaces = array("'", "\\", "DROP", "SELECT", "INSERT", "UPDATE", "WHERE", '"', "SUBSTRING", ";");
        $sanitize = str_replace($replaces, '', $sanitize);
        return $sanitize;
    }
    public static function getRealSize($size) {
        $unit = array('b', 'kb', 'mb', 'gb', 'tb', 'pb');
        return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
    }
    /*
    * Deve retornar a referência da parcela. EX: [start]01/02/2015 - [end]01/03/2015
    * @param $datestart date('Y-m-d)
    * @param $numberintervals integer
    */
    public static function calculateIntervals($datestart, $numberintervals) {
        $intervals = [];
        $date = new \DateTime($datestart);
        echo '<pre>';
        for ($c = 0; $c < $numberintervals; $c++) {
            $intervals[$c]['start'] = \strtotime($date->format('Y-m-d'));
            $date->add(new \DateInterval('P1M'));
            $intervals[$c]['end'] = \strtotime($date->format('Y-m-d'));
        }
        if(count($intervals) == 0){
            throw new \Exception('date-intervals-invalid|');
        }
        return $intervals;
    }

    public static function is_rgb($hex) {
        return preg_match('/^#?(([a-f0-9]{3}){1,2})$/i', $hex);
    }
}
