<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use App\UserLog;

class RegisterActionLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

       self::register_input_log($request);
       self::register_access_log($request);

        return $next($request);
    }

    private static function register_access_log($request){
        //exclude from log
        $routes = ['user', 'search', 'address'];
        if(Request::getMethod() == 'GET' && !in_array(Request::segment(1), $routes)){
            $log = new UserLog();
            $log->session_id = $request->session()->getId();

            if(Auth::check())
            $log->user_id = Auth::user()->id;
            
            $log->action = Request::getMethod();
            $log->path = $request->path();
            $log->save();
    }
    }

        private static function register_input_log($request){
        if(Request::getMethod() == 'POST' || Request::getMethod() == 'PUT' 
        || Request::getMethod() == 'PATCH' || Request::getMethod() == 'DELETE'){
            $log = new UserLog();
            $log->session_id = $request->session()->getId();

            if(Auth::check())
            $log->user_id = Auth::user()->id;
            
            $log->action = Request::getMethod();
            $log->path = $request->path();
            $log->save();
    }
    }
}
