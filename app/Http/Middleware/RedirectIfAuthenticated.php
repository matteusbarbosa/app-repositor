<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Auth;
use App\UserLog;
use App\User;
use App\Role;

class RedirectIfAuthenticated
{
    /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next
    * @param  string|null  $guard
    * @return mixed
    */
    public function handle($request, Closure $next, $guard = null)
    {
        if(false == Auth::check()){            
        $user_log = Userlog::where('session_id', session()->getId())->orderBy('created_at', 'DESC')->first();

            if(!empty($user_log)){
                $user = $user_log->user;
            }   
        }   
        
        return $next($request);
    }
}
