<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Address;
use App\BuyAddress;

class AddressController extends Controller
{
    public function store(Request $request){
        $data = [];

        $data['address'] = new Address();
        $data['address']->user_id = $request->input('user_id');
        $data['address']->title = $request->input('title');
        $data['address']->zipcode = $request->input('zipcode');
        $data['address']->street = $request->input('street');
        $data['address']->number = $request->input('number');
        $data['address']->district = $request->input('district');
        $data['address']->city = $request->input('city');
        $data['address']->state = $request->input('state');
        $data['address']->save();
        
        return response()->json(['address_id' => $data['address']->id]);
    }

    public function bindToBuy(Request $request){
        $data = [];

        $data['buy_address'] = new BuyAddress();
        $data['buy_address']->buy_id = $request->input('buy_id');
        $data['buy_address']->address_id = $request->input('address_id');
        $data['buy_address']->save();
        
    }
    
    public function unbindFromBuy(Request $request){

        $result = BuyAddress::where('buy_id', $request->input('buy_id'))->where('address_id', $request->input('address_id'))->delete();
        
        return response()->json($result);
    }
}