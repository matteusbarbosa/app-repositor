<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \Symfony\Component\HttpFoundation\StreamedResponse;
use App\Notifications\ItemStatusUpdate;
use \Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\DB;
use App\Buy;
use App\Status;
use App\Stock;
use App\ItemStatus;
use App\User;
use App\Item;
class ItemStatusController extends Controller
{
    public function fetchQueueData(){
        $data = [];       
        $item_list = DB::select(sprintf("select `app_stock`.`id`, count(`app_stock_component`.`id`) AS `component_count`, `app_stock`.`quantity` as `stock_quantity`,`i1`.`quantity` as `item_quantity`,  
        `app_product`.`title`, COUNT(`i1`.`id`) as total_requests, max(`app_item_status`.`updated_at`) AS `last_updated_at`,
        group_concat(DISTINCT `app_users`.`name`) AS `users_name` from `app_item`  as `i1`
        inner join `app_item_status` on `app_item_status`.`item_id` = `i1`.`id` and `app_item_status`.`id` = 
        (
	      select `is2`.`id` from `app_item_status` as `is2` 
	        where `is2`.`item_id` = `i1`.`id`
	        group by `is2`.`item_id` 
	        having max(`is2`. `updated_at`)
        )
        inner join `app_status` on `app_status`.`id` = `app_item_status`.`status_id` and `app_status`.`in_queue` = %d
        inner join `app_cart` on `app_cart`.`id` = `i1`.`cart_id` 
        inner join `app_stock` on `app_stock`.`id` = `i1`.`object_id` and `i1`.`object_type` = '%s'
        left join `app_stock_component` on `app_stock_component`.`parent_id` = `app_stock`.`id`
        inner join `app_product` on `app_product`.`id` = `app_stock`.`product_id`
        inner join `app_users` on `app_users`.`id` = `app_cart`.`user_id`
        group by `app_stock`.`id`, `app_stock`.`quantity`, `i1`.`quantity`,`app_product`. `title`
        order by `last_updated_at` ASC", 1, addslashes('App\Stock')));


        foreach($item_list as $stock){
            $stock_find = Stock::find($stock->id);
            $data[$stock->id]['users_name'] = !empty($stock->users_name) ? \str_limit($stock->users_name, 60) : '–';
            $data[$stock->id]['updated_at'] = $stock->last_updated_at;
            $data[$stock->id]['created_at'] = $stock->last_updated_at;
            $data[$stock->id]['created_at_formatted'] = \DateTime::createFromFormat('Y-m-d H:i:s', $stock->last_updated_at)->format('d/m/Y H:i');
            $data[$stock->id]['id'] = $stock->id;
            $data[$stock->id]['component_count'] = $stock->component_count;
            $data[$stock->id]['stock_quantity'] = $stock->stock_quantity;
            $data[$stock->id]['item_quantity'] = $stock->item_quantity;
            $data[$stock->id]['title'] = $stock->title;
            $data[$stock->id]['total_requests'] = $stock->total_requests;
            $data[$stock->id]['details'] = $stock_find->fieldsToString(null, explode(',', config('instance.STOCK_FIELD_DEFAULT_DISPLAY')));
        }

        $order_parameter = array_column($data, config('instance.QUEUE_ITEM_SORT'));

        array_multisort($order_parameter, SORT_ASC, $data);

        
        return $data;
    }
    /* 
    /item/dispatch
    Agrupe itens de todos os buys
    */
    public function queueUpdate(Request $request){
        return $response = new StreamedResponse(function() use ($request) {
            $last_update = true;
            $date_flag = date('Y-m-d H:i:s');
            $data = [];
            while(true) {
                $data['queue'] = $this->fetchQueueData();
                krsort($data['queue']);
                echo 'data: ' . json_encode($data) . "\n\n";
                ob_flush();
                flush();
                usleep(10000000);
            }
        }, Response::HTTP_OK, [
            'Content-Type' => 'text/event-stream', 
            'Cache-Control' => 'no-cache',
            'X-Accel-Buffering', 'no'
            ]);
        }
        /**
        * Atualize status de itens que estão na fila de expedição
        * 
        *
        * @param  \Illuminate\Http\Request  $request
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
        public function stockItemsDispatch(Request $request, $stock_id)
        {
            $data = [];
            $stock = Stock::find($stock_id);
            $items = $stock->items()
            ->join('item_status', 'item_status.item_id', '=', 'item.id')
            ->join('status', 'status.id', '=', 'item_status.status_id')
            ->where(function($q){
                return $q->where('status.slug', 'solicitado')
                ->orWhere('status.slug', 'em-separacao');
            })
            ->whereNull('status.disabled_at')
            ->select('item.id')
            ->get();
            foreach($items as $item){
                $item_status = new ItemStatus();
                $item_status->item_id = $item->id;
                $item_status->attendant_id = $request->input('attendant_id');
                $item_status->status_id = $request->input('status_id');
                $item_status->details = $request->input('details');
                $item_status->updateStockQuantity();
                $item_status->save();
                $item->touch();

            }
            return response()->json(true, 201);
        }
        public function broadcastStatus(Request $request, $item_id){
            $item = Item::find($item_id);
            if($item == null){
                return 'data: '.json_encode(['error' => 404]);
                ob_flush(); 
                flush();
            }
            return $response = new StreamedResponse(function() use ($request, $item) {
                while(true) {
                    $data = [];                
                    $status_last = $item->statuses()->orderBy('item_status.updated_at', 'DESC')->first();
                    if(!empty($status_last)){
                        $data['id'] = $status_last->id;
                        $data['file_url'] = $status_last->status->image->url;
                        $data['status_id'] = $status_last->status_id;
                        $data['updated_at'] = $status_last->status->updated_at->format('d/m/Y H:i:s');
                        $data['title'] = $status_last->status->title;
                        $data['time_deadline'] = $status_last->status->time_deadline;
                    }
                    $time = time('r');
                    echo 'data: ' . json_encode($data) . "\n\n";
                    ob_flush();
                    flush();
                    usleep(10000000);
                }
            }, Response::HTTP_OK, [
                'Content-Type' => 'text/event-stream',
                'Cache-Control' => 'no-cache',
                'X-Accel-Buffering', 'no'
                ]);
            }
            /**
            * Atualize status de itens que estão no buy
            * 
            *
            * @param  \Illuminate\Http\Request  $request
            * @param  int  $id
            * @return \Illuminate\Http\Response
            */
            public function buyItemsDispatch(Request $request, $buy_id)
            {
                $data = [];
                $buy = Buy::find($buy_id);
                $items = $buy->items()
                ->join('item_status', 'item_status.item_id', '=', 'item.id')
                ->join('status', 'status.id', '=', 'item_status.status_id')
                ->where(function($q){
                    return $q->where('status.slug', 'solicitado')
                    ->orWhere('status.slug', 'em-separacao');
                })
                ->whereNull('status.disabled_at')
                ->select('item.id')
                ->get();
                foreach($items as $item){
                    $item_status = new ItemStatus();
                    $item_status->item_id = $item->id;
                    $item_status->attendant_id = $request->input('attendant_id');
                    $item_status->status_id = $request->input('status_id');
                    $item_status->details = $request->input('details');
                    $item_status->updateStockQuantity();
                    $item_status->save();
                    $item->touch();            
                }
                return response()->json(true, 201);
            }

            
       

        }