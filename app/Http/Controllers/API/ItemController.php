<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Item;
use App\ItemStatus;

class ItemController extends Controller
{
    public function show(Request $request, $id){
        return response()->json(Item::find($id), 201);
    }
    public function update(Request $request, $id){
        $item = Item::find($id);
        $item->fields()->delete();
        if(!empty($item->object->fields) && !empty($request->input('form'))){
            foreach($request->input('form') as $key => $field){
                switch($field['name']){
                    case "item_id":
                    continue;
                    break;
                    case "status_id":
                    $data['item_status'] = new ItemStatus();
                    $data['item_status']->item_id = $id;
                    $data['item_status']->status_id = $field['value'];
                    $data['item_status']->save();
                    break;
                    case "quantity":
                    $item->quantity = $field['value'];
                    break;
                    case "details":
                    $item->details = $field['value'];
                    break;
                    default:
                    $item->saveFieldContent($field['name'], $field['value']);
                }
            }
            $item->save();      
        }  
        return response()->json([], 201);   
    }
    /**
        * Remove the specified resource from storage.
        *
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
        public function destroy($id)
        {
            $data = [];
            Item::find($id)->delete();
            return response()->json(null, 204);
        }
}
