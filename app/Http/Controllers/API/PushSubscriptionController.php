<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\PushSubscription;
use App\User;
class PushSubscriptionController extends Controller
{
    use ValidatesRequests;
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        // $this->middleware('auth');
    }
    /**
    * Update user's subscription.
    *
    * @param  \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request)
    {
        $this->validate($request, ['endpoint' => 'required']);
        $user = User::find($request->input('user_id'));
        $user->updatePushSubscription(
            $request->endpoint,
            $request->key,
            $request->token
        );
        return response()->json(null, 204);
    }
    /**
    * Delete the specified subscription.
    *
    * @param  \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
    public function destroy(Request $request, $user_id)
    {
        $ps = PushSubscription::where('public_key', $request->input('key'))->first();
        $ps->user->deletePushSubscription($ps->endpoint);
        return response()->json(null, 204);
    }
}