<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \Symfony\Component\HttpFoundation\StreamedResponse;
use \Symfony\Component\HttpFoundation\Response;
use App\Stock;
use App\StockField;
class StockController extends Controller
{
    public function getAlternatives(Request $request, $id){
        $stock = Stock::find($id);
   
        $data = [];
        $stocks = $stock->product->stocks->reject(function($s) use ($stock){
            return $stock->id == $s->id;
        });

        $data['stocks'] = [];
        foreach($stocks as $s){
            if(empty($s->product))
            continue;

            $data['stocks'][$s->id] = $s->fieldsToString(null, null, true);
        }

        return response()->json($data['stocks'], 201);
    }
    public function availabilityCheck(Request $request){
        return $response = new StreamedResponse(function() use ($request) {
            while(true) {
                $data = [];    
                $start_flag = date("Y-m-d H:i:s", strtotime('now') - 700);      
                $stocks_updated = Stock::where('quantity', '<=', config('instance.LOW_STOCK_LIMIT'))
                ->orderBy('updated_at', 'ASC')->pluck('quantity', 'id');
                echo 'data: ' . json_encode($stocks_updated) . "\n\n";
                ob_flush();
                flush();
                usleep(10000000);
            }
        }, Response::HTTP_OK, [
            'Content-Type' => 'text/event-stream',
            'Cache-Control' => 'no-cache',
            'X-Accel-Buffering', 'no'
            ]);
        }
    }