<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Events\NotificationRead;
use App\Events\NotificationReadAll;
use App\Notifications\PromotionAdvertise;
use App\Notifications\BuyPriceInform;
use App\Notifications\BuyStatusUpdate;
use App\Notifications\ItemStatusUpdate;
use App\Notifications\StockDispatchUpdate;
use NotificationChannels\WebPush\PushSubscription;
use App\ItemStatus;
use App\Buy;
use App\Cart;
use App\Stock;
use App\Item;
use App\User;
use App\Promotion;
use App\UserSalesChannel;
class NotificationController extends Controller
{
    /**
    * Get user's notifications.
    *
    * @param  \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        $user = $request->user();
        // Limit the number of returned notifications, or return all
        $query = $user->unreadNotifications();
        $limit = (int) $request->input('limit', 0);
        if ($limit) {
            $query = $query->limit($limit);
        }
        $notifications = $query->get()->each(function ($n) {
            $n->created = $n->created_at->toIso8601String();
        });
        $total = $user->unreadNotifications->count();
        return compact('notifications', 'total');
    }
    /**
    * Create a new notification.
    *
    * @param  \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
    public function promotion(Request $request)
    {
        $promotion_id = $request->input('promotion_id');
        $sales_channel_id = !empty($request->input('sales_channel_id')) ? $request->input('sales_channel_id') : null;
        self::broadCastPromotion($promotion_id, $sales_channel_id);
        return response()->json('Promotion sent.', 201);
    }
    public static function broadCastPromotion($promotion_id, $sales_channel_id = null){
        $data = [];
        $data['promotion'] = Promotion::find($promotion_id);

        $users_list = UserSalesChannel::where('sales_channel_id', $sales_channel_id)->pluck('user_id')->toArray();

        if($users_list != null)
        $users = User::whereIn('id', $users_list)->get();
        else
        $users = User::all();
        foreach($users as $u){
            $u->notify(new PromotionAdvertise($data['promotion']->id));
        }
        $data['promotion']->notified_at = date('Y-m-d H:i:s');
        $data['promotion']->save();
    }
    public static function buyPriceInform($buy_id){
        $buy = Buy::find($buy_id);
        $u = $buy->user;
        $u->notify(new BuyPriceInform($buy_id));
    }
    /**
    * Create a new notification.
    *
    * @param  \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
    public function status(Request $request)
    {
        $data = [];
        $data['item_status'] = ItemStatus::find($request->input('item_status_id'));
        $data['user'] = $data['item_status']->cart->user;
        $data['user']->notify(new ItemStatusUpdate($data['item_status']->id));
        $data['item_status']->notified_at = date('Y-m-d H:i:s');
        $data['item_status']->save();
        return response()->json('Notification sent.', 201);
    }
    /**
    * Create a new notification.
    *
    * @param  \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
    public function buyStatusUpdate(Request $request, $buy_id)
    {
        $data = [];
        $buy = Buy::find($buy_id);
        foreach($buy->items as $item){
            $item_status = ItemStatus::where('item_id', $item->id)->orderBy('updated_at', 'DESC')->first();
            if(empty($item_status))
            continue;
            $item_status->notified_at = date('Y-m-d H:i:s');
            $item_status->save();
            $buy->user->notify(new BuyStatusUpdate($buy_id, $request->input('status_id')));
            $item_status->attendant->notify(new BuyStatusUpdate($buy_id, $request->input('status_id')));
        }
        return response()->json('Notification sent.', 201);
    }
    /**
    * 
    *
    * @param  \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
    public function stockDispatchStatusUpdate(Request $request, $stock_id)
    {
        $data = [];
        $data['stock'] = Stock::find($stock_id);
        $data['buy_list'] = $data['stock']->getCurrentBuyList();
        foreach($data['buy_list'] as $buy){
            $item = $buy->items()->where('item.object_id', $stock_id)->first();
            $item_status = $item->statuses()->orderBy('status.updated_at', 'DESC')->first();
            $item_status->notified_at = date('Y-m-d H:i:s');
            $item_status->save();
            $buy->user->notify(new StockDispatchUpdate($stock_id, $request->input('status_id'), $item_status->details));
            $item_status->attendant->notify(new StockDispatchUpdate($stock_id, $request->input('status_id'), $item_status->details));
        }
        return response()->json('Notification sent.', 201);
    }

    /**
    * Create a new notification.
    *
    * @param  \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
    public function cartSync(Request $request, $session_id)
    {
        $data = [];
        $item = Item::find($item_id);
        $item_status = $item->statuses()->orderBy('status.updated_at', 'DESC')->first();
        $item_status->notified_at = date('Y-m-d H:i:s');
        $item_status->save();
        $item_status->user->notify(new CartSync($item_status->id, $request->input('status_id'), $item_status->details));
        $item_status->attendant->notify(new CartSync($item_status->id, $request->input('status_id'), $item_status->details));
        return response()->json('Notification sent.', 201);
    }

    /**
    * Create a new notification.
    *
    * @param  \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
    public function itemUpdate(Request $request, $item_id)
    {
        $data = [];
        $item = Item::find($item_id);
        $item_status = $item->statuses()->orderBy('status.updated_at', 'DESC')->first();
        $item_status->notified_at = date('Y-m-d H:i:s');
        $item_status->save();
        $item_status->user->notify(new ItemStatusUpdate($item_status->id, $request->input('status_id'), $item_status->details));
        $item_status->attendant->notify(new ItemStatusUpdate($item_status->id, $request->input('status_id'), $item_status->details));
        return response()->json('Notification sent.', 201);
    }
    /**
    * Create a new notification.
    *
    * @param  \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $request->user()->notify(new ItemStatusUpdate);
        return response()->json('Notification sent.', 201);
    }
    /**
    * Mark user's notification as read.
    *
    * @param  \Illuminate\Http\Request $request
    * @param  int $id
    * @return \Illuminate\Http\Response
    */
    public function markAsRead(Request $request, $id)
    {
        $notification = $request->user()
        ->unreadNotifications()
        ->where('id', $id)
        ->first();
        if (is_null($notification)) {
            return response()->json('Notification not found.', 404);
        }
        $notification->markAsRead();
        event(new NotificationRead($request->user()->id, $id));
    }
    /**
    * Mark all user's notifications as read.
    *
    * @param  \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
    public function markAllRead(Request $request)
    {
        $request->user()
        ->unreadNotifications()
        ->get()->each(function ($n) {
            $n->markAsRead();
        });
        event(new NotificationReadAll($request->user()->id));
    }
    /**
    * Mark the notification as read and dismiss it from other devices.
    *
    * This method will be accessed by the service worker
    * so the user is not authenticated and it requires an endpoint.
    *
    * @param  \Illuminate\Http\Request $request
    * @param  int $id
    * @return \Illuminate\Http\Response
    */
    public function dismiss(Request $request, $id)
    {
        if (empty($request->endpoint)) {
            return response()->json('Endpoint missing.', 403);
        }
        $subscription = PushSubscription::findByEndpoint($request->endpoint);
        if (is_null($subscription)) {
            return response()->json('Subscription not found.', 404);
        }
        $notification = $subscription->user->notifications()->where('id', $id)->first();
        if (is_null($notification)) {
            return response()->json('Notification not found.', 404);
        }
        $notification->markAsRead();
        event(new NotificationRead($subscription->user->id, $id));
    }
}