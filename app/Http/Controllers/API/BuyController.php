<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\BuyController as BC;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use \Symfony\Component\HttpFoundation\StreamedResponse;
use \Symfony\Component\HttpFoundation\Response;
use App\Product;
use App\Stock;
use App\File;
use App\User;
use App\Buy;
use App\ItemStatus;
use App\Cart;
class BuyController extends Controller
{
    public function broadcastStatus(Request $request, $id){
        $buy = Buy::find($id);
        if($buy == null){
            echo 'data: '.json_encode(['error' => 404]);
            ob_flush();
            flush();
        }
        return $response = new StreamedResponse(function() use ($request, $buy) {
            while(true) {
                $data = [];                
                $status_last = $buy->getLastStatus();
                if(!empty($status_last)){
                    $data['id'] = $status_last->id;
                    $data['file_url'] = $status_last->image->url;
                    $data['status_id'] = $status_last->id;
                    $data['updated_at'] = \DateTime::createFromFormat('Y-m-d H:i:s', $status_last->pivot->updated_at)->format('d/m/Y H:i:s');
                    $data['status_title'] = $status_last->title;
                    $data['time_deadline'] = $status_last->time_deadline;
                }
                echo 'data: ' . json_encode($data) . "\n\n";
                ob_flush(); 
                flush();
                usleep(10000000); 
            }
        }, Response::HTTP_OK, [
            'Content-Type' => 'text/event-stream',
            'Cache-Control' => 'no-cache',
            'X-Accel-Buffering', 'no'
            ]);
        }
        private function fetchQueueData(){
            $buy_list = Buy::join('cart', 'cart.id', '=', 'buy.id')
            ->join('item', 'item.cart_id', '=', 'cart.id')
            ->join('item_status', 'item_status.item_id', '=', 'item.id')
            ->join('status', 'status.id', '=', 'item_status.status_id')
            ->join('users', 'users.id', '=', 'buy.user_id')
            ->select('buy.id', DB::raw('MAX(app_item_status.updated_at) AS updated_at'), 'item_status.attendant_id', 'users.name', DB::raw('MAX(app_item_status.attendant_id) AS attendant_id'), 'status.id as status_id', 'status.title', 'status.time_deadline')
            ->whereNotNull('status.in_queue')
            ->groupBy('buy.id', 'status.title', 'status.time_deadline', 'status.id', 'users.name', 'item_status.attendant_id')
            ->havingRaw('MAX(app_item_status.updated_at)')
            ->take(30)
            ->get();
            $data = [];
            foreach($buy_list as $v){
                $data[$v->id]['updated_at'] =  $v->updated_at;
                $data[$v->id]['created_at'] =  $v->updated_at;
                $data[$v->id]['created_at_formatted'] =\DateTime::createFromFormat('Y-m-d H:i:s', $v->updated_at)->format('d/m/Y H:i:s');
                $data[$v->id]['id'] = $v->id;
                $data[$v->id]['customer'] = $v->name != null ? $v->name : '–';
                if(!empty($v->attendant_id)){
                    $attendant = User::find($v->attendant_id);
                    $data[$v->id]['attendant'] = $attendant->name;
                } else {
                    $data[$v->id]['attendant'] = '–';
                }
                $data[$v->id]['status_id'] = $v->status_id;
                $data[$v->id]['status'] = $v->title;
                $data[$v->id]['title'] = $v->title;
                $data[$v->id]['time_deadline'] = $v->time_deadline;
            }

            $order_parameter = array_column($data, config('instance.QUEUE_BUY_SORT'));

            array_multisort($order_parameter, SORT_ASC, $data);

            return $data;
        }
        public function queueUpdate(Request $request){
            return $response = new StreamedResponse(function() use ($request) {
                $last_update = true;
                $date_flag = date('Y-m-d H:i:s');
                while(true) {
                    $data = [];   
                    $data['queue'] = $this->fetchQueueData();  
                    echo 'data: ' . json_encode($data) . "\n\n";
                    ob_flush();
                    flush();
                    usleep(10000000);
                }
            }, Response::HTTP_OK, [
                'Content-Type' => 'text/event-stream',
                'Cache-Control' => 'no-cache',
                'X-Accel-Buffering', 'no'
                ]);
            }
            /* 
            Itens de um buy específico
            Diferente do fetchQueueData, pega apenas itens do buy especificado
            Exibição em view é diferente da fila
            */
            public function fetchDispatchData($buy){
                $data = collect([]);
                $q =  sprintf("select distinct `app_item`.`id`, `app_item_status`. `status_id`, `app_item`.`object_id` as `stock_id`, `app_item`.`quantity`,
                `app_status`.`title` as `status_title`, `app_status`.`file_id` as `status_file_id`, max(`app_item_status`.`updated_at`) as `last_updated_at`
                from `app_item` inner join `app_item_status` on `app_item_status`.`item_id` = `app_item`.`id`
                inner join `app_status` on `app_status`.`id` = `app_item_status`.`status_id`
                inner join `app_cart` on `app_cart`.`id` = `app_item`.`cart_id`
                where `app_item`.`object_type` = '%s'
                and (`app_item`.`id`, `app_item_status`.`updated_at`) IN (
                    select `ai`.`id`, max(`ais`.`updated_at`) from `app_item` as `ai`
                    inner join `app_item_status` as `ais` on `ais`.`item_id` = `ai`.`id`
                    and `ai`.`cart_id` = %d
                    group by `ai`.`id`
                    )
                    group by `app_item`.`id`
                    order by `last_updated_at` desc
                    ", addslashes('App\Stock'), $buy->cart_id);
                    $item_list = DB::select($q);
                    //file,product,stock
                    foreach($item_list as $item){
                        $item_data = [];
                        $item_data['updated_at'] =  $item->last_updated_at;
                        $item_data['created_at'] =  $item->last_updated_at;
                        $item_data['stock_id'] =  $item->stock_id;
                        $item_data['created_at_formatted'] = \DateTime::createFromFormat('Y-m-d H:i:s', $item->last_updated_at)->format('d/m/Y H:i:s');
                        $item_data['id'] = $item->id;
                        $item_data['status_title'] = $item->status_title;
                        $item_data['status_id'] = $item->status_id;
                        $item_data['quantity'] = $item->quantity;

                 
                        $file = File::find($item->status_file_id);
                        $item_data['status_img'] = config('app.url').'/'.$file->url;
                        //stock
                        $stock = Stock::find($item->stock_id);

                        if(empty($stock)){
                            continue;
                        }

                        //product
                        $item_data['item_quantity_min'] = 1;
                        $item_data['item_quantity_max'] = $item_data['quantity'];

                     
                        if($stock->getContent('item_quantity_min') != null)
                        $item_data['item_quantity_min'] = $stock->getContent('item_quantity_min');

                        if($stock->getContent('item_quantity_max') != null)
                        $item_data['item_quantity_max'] = $item_data['quantity'] < $stock->getContent('item_quantity_max') ? $item_data['quantity'] : $stock->getContent('item_quantity_max');
                        $item_data['title'] = $stock->product->title;
                        $item_data['details'] = $stock->fieldsToString(null, explode(',', config('instance.STOCK_FIELD_DEFAULT_DISPLAY')));
                        //file
                        $item_data['file_url'] = !empty($stock->file_id) ? $stock->image->url : $stock->product->image->url;
                        $data->push($item_data);
                    }
                    return $data;
                }
                /* 
                Itens de um buy específico
                Diferente do ItemStatusController
                */
                public function dispatchUpdate(Request $request, $buy_id){
                    $buy = Buy::find($buy_id);
                    return $response = new StreamedResponse(function() use ($request, $buy) {
                        ob_implicit_flush(true);
                        while(true) {
                            $data = [];
                            $data['queue'] = $this->fetchDispatchData($buy);     
                            echo "data: ".json_encode($data)."\n\n";              
                            ob_flush();
                            flush();
                            usleep(10000000);
                        }
                    }, Response::HTTP_OK, [
                        'Content-Type' => 'text/event-stream',
                        'Cache-Control' => 'no-cache',
                        'X-Accel-Buffering', 'no'
                        ]);
                    }
                    /**
                    * Store a newly created resource in storage.
                    *
                    * @param  \Illuminate\Http\Request  $request
                    * @return \Illuminate\Http\Response
                    */
                    public function store(Request $request)
                    {
                        $data = [];
                        $request->input('cart');
                        $data['cart'] = 
                        $data['buy'] = new Buy();
                        $data['buy']->user_id = Auth::user()->id;
                        $data['buy']->cart_id = $data['cart']->id;
                        $data['buy']->save();
                        return response()->json($data);
                    }
                    /**
                    * Update the buy and it's items status
                    *
                    * @param  \Illuminate\Http\Request  $request
                    * @param  int  $id
                    * @return \Illuminate\Http\Response
                    */
                    public function update(Request $request, $id)
                    {
                        $data['buy'] = Buy::find($id);
                        if(empty($request->input('item')))
                        return response()->json($data, 201);
                        if(is_array($request->input('item'))){
                            foreach($request->input('item') as $id => $item){
                                $data['item_status'] = new ItemStatus();
                                $data['item_status']->item_id = $id;
                                $data['item_status']->status_id = $item['status_id'];
                                $data['item_status']->touch();
                                $data['item']->touch();
                            }
                        }
                        return response()->json($data, 201);
                    }
                    /**
                    * Remove the specified resource from storage.
                    *
                    * @param  int  $id
                    * @return \Illuminate\Http\Response
                    */
                    public function destroy($id)
                    {
                        $data = [];
                        Buy::find($id)->delete();
                        return response()->json(null, 204);
                    }
                    
                    public function searchByTerm(Request $request, $term){
                        
                        $data = DB::select(sprintf("                        
                        select concat('#', `app_buy`.`id`,' – ', `app_users`.`name`) as `name` , `app_buy`.`id`,
                        concat('/buy/', `app_buy`.`id`) as `link` from `app_buy`
                        inner join `app_users` on `app_users`.`id` = `app_buy`.`user_id`
                        inner join `app_cart` on `app_cart`.`id` = `app_buy`.`cart_id`
                        inner join `app_item` on `app_item`.`cart_id` = `app_cart`.`id`
                        where `app_buy`.`id` LIKE '%%%s%%'
                        or `app_users`.`name` LIKE '%%%s%%'
                        or `app_users`.`phone` LIKE '%%%s%%'
                        or `app_users`.`phone_whatsapp` LIKE '%%%s%%'
                        or `app_buy`.`details` LIKE '%%%s%%'
                        or `app_cart`.`details` LIKE '%%%s%%'
                        or `app_item`.`details` LIKE '%%%s%%'
                        group by `app_buy`.`id`
                        ", $term, $term, $term, $term, $term, $term, $term));
                        
                        return response()->json($data, 201);
                    }

                    public function sync(Request $request){
                        foreach($request->input('buy_list') as $buy){
                            $r = new \Illuminate\Http\Request();  
                            $r->setMethod('POST');    
                            $r->replace($buy);            
                            BC::persist($r);

                            return response()->json([], 201);
                        }
                    }
                }