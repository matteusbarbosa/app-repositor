<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Promotion;
use App\Product;
use App\Stock;
class PromotionController extends Controller
{
    public function findProductOptions($promotion_id){
        $data = [];
        $promotion = Promotion::find($promotion_id);
        $object_class = $promotion->object_type;
        $obj_instance = new $object_class();
        $data['object'] = $obj_instance::find($promotion->object_id);
        //mostra pacotes do malão separadamente
        if(get_class($obj_instance) == 'App\Stock'){
            $components = $data['object']->components;
            $data['stocks'] = array_merge($data['object']->parents, $components);
        }
        return json_encode($data);
    }
}