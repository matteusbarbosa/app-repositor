<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
class UserController extends Controller
{
    public static function getBuyList($user_id){
        $data = collect([]);
        $carts = User::carts();
        foreach($carts as $c)
        $data->push($c->buy);
        return response()->json($data, 204);
    }

    public function getPJCaptcha(Request $request){
        $params = \JansenFelipe\CnpjGratis\CnpjGratis::getParams();

        return $params;
    }

    //cnpj, captcha, cookie
    public function getPJInfo( Request $request ){
        $dadosEmpresa = \JansenFelipe\CnpjGratis\CnpjGratis::consulta(
            $request->input('cnpj'),
            $request->input('captcha'),
            $request->input('cookie')
        );

        return $dadosEmpresa;
    }
}