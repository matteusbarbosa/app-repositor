<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Stock;
use App\Tag;
use App\User;
use App\Promotion;

class PromotionController extends Controller
{

    public function __construct()
    {
        view()->share('lang_object', 'promotion');
    }


    public const ROWS_LIMIT = 15;
    /*
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        $data = [];
        
        $now = new \DateTime();
        
        //todo: criar campo de "item destacado" p/ product
        $data['promotions'] = Promotion::whereNull('disabled_at')
        ->where('promotion.end_at', '>=', $now->format('Y-m-d H:i:s'))
        ->orWhereNull('promotion.end_at')
        ->limit(self::ROWS_LIMIT)
        ->get();
        
        $data['tags'] = collect([]);

       

        foreach($data['promotions'] as $k => $p){
            
            if(empty($p->object))
            continue;
            
            $data['tags'] = $data['tags']->concat($p->object->tags()->whereNull('disabled_at')->get());
            
        }

        $data['results'] = Tag::getClassifiedStocks($data['tags'], config('instance.WHOLESALE_ONLY'), true);


        if(Auth::check())
        $data['buy_last'] = User::getLastBuyBySession(session()->getId());
    
        return view('shop.promotion')->with('data', $data);
    }
    
    
}
