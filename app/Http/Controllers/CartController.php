<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Role;
use App\Field;
use App\Cart;
use App\Exceptions\ObjectNotFoundException;
class CartController extends Controller
{

    public function __construct()
    {
        view()->share('lang_object', 'cart');
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request){
        //$data = self::retrieve($request);
        $data = [];
        $data['fields'] = Field::whereIn('slug', explode(',', config('instance.ITEM_FIELD_INPUT')))->get();
        
        $role = Role::where('slug', 'customer')->select('id')->first();
        $data['customer_list'] = User::where('role_id', $role->id)->pluck('name', 'id');

        return view('crud.cart')->with('data', $data);
    }
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function repeat($cart_id)
    {
        $data = [];
        $data['cart'] = Cart::find($cart_id);
        $data['cart_new'] = $data['cart']->replicate();
        return view('crud.cart')->with('data', $data);
    }
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show(Request $request, $id)
    {
       $data = self::retrieve($request, $id);
        return view('crud.cart');
    }
 /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public static function persist($session_id)
    {
        $data = []; 
        $cart = Cart::where('session_id', $session_id)->orderBy('created_at', 'DESC')->first();
        if(empty($cart)){
            $cart = new Cart();
            $cart->session_id = $session_id;
            $cart->save();
        }
        return $cart;
    }

    private static function retrieve(Request $request, $id = null){
        $data = [];
        $data['cart'] = Cart::leftJoin('buy', 'buy.cart_id', '=', 'cart.id')
        ->select('cart.id', 'cart.user_id', 'cart.session_id')
        ->whereNull('buy.id');
        if($id)
        $data['cart'] = $data['cart']->where('cart.id', $id);
        else
        $data['cart'] = $data['cart']->where('cart.session_id', $request->session()->getId());
        $data['cart'] = $data['cart']->orderBy('cart.id', 'DESC')->first();
        if(empty($data['cart'])){
            $request['object_type'] = 'cart';
            $request['object_id'] = $id;
            throw new ObjectNotFoundException($request);
        }
        if(Auth::check() && Auth::user()->cannot('manage')){
            $data['cart']->user_id = Auth::user()->id;
            $data['cart']->save();
        }
        $data['fields'] = Field::whereIn('slug', explode(',', config('instance.ITEM_FIELD_INPUT')))->get();
        $data['buy_last'] = User::getLastBuyBySession($data['cart']->session_id);
        if(!empty($data['cart']->user))
        $data['addresses_available'] =  $data['cart']->user->addresses;
        return $data;
    } 
}
