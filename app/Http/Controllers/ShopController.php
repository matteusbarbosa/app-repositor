<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use \App\User;
use \App\Product;
use \App\Stock;
use \App\Promotion;
use \App\Tag;
use \App\File;

class ShopController extends Controller
{

    public function __construct()
    {
        view()->share('lang_object', 'shop');
    }

    public const ROWS_LIMIT = 15;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $data = [];
        
        $now = new \DateTime();

        //only displayed
        $data['results'] = Tag::getClassifiedStocks(config('instance.WHOLESALE_ONLY'), true);
        
        if(Auth::check())
        $data['buy_last'] = User::getLastBuyBySession(session()->getId());

        return view('shop.tags')->with('data', $data);
    }

       


  
}
