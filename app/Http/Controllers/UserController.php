<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;
use \Spatie\Activitylog\Models\Activity;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public function __construct()
    {
        view()->share('lang_object', 'user');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return $this->edit(Auth::user()->id);
    }

        /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'role_id' => 'required|integer',
            'phone' => 'required|string|email|max:14',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        self::persist($request);
        
        session()->flash([
            'flash' => true,
            'flash-class' => 'success',
            'flash-prefix' => trans('legend.success-prefix'),
            'flash-text' => trans('legend.store-success'),
        ]);
        
        return redirect()->action('Admin\UserController@index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];
        $data['user'] = User::find($id);
        
        $data['login_last'] = $data['user']->logs()->where('action', 'login')->orderBy('created_at', 'DESC')->first();

        return view('crud.user')->with('data', $data);
    }

    private static function persist(Request $request, $id = null){
        $user = $id != null ? User::find($id) : new User();
        $user->cnpj = \Helpers\Format::filterNumbers($request->input('cnpj'));
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->phone_whatsapp = $request->input('phone');

        $user->bank_data = Crypt::encrypt($request->input('bank_data'));
        $user->details = $request->input('details');

        if($request->input('password_new') != null)
        $user->password = Hash::make($request->input('password_new'));
        
        $user->save();

        return $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = self::persist($request, $id);
        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', trans('legend.success-prefix'));
        session()->flash('flash-text', trans('legend.update-success'));
        
        return redirect()->action('Admin\UserController@index');
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $title = __('object.user').' – '.$user->name;
        $user->delete();

        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.delete-success'));
        session()->flash('flash-text', $title);
        
        return redirect()->action('UserController@index');
    }


}
