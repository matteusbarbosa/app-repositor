<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Address;
use App\Role;
use App\Buy;
use App\Cart;
use App\Status;
use App\ItemStatus;
use App\Item;
use App\Notifications\ItemStatusUpdate;
use App\Exceptions\ObjectNotFoundException;
use App\Exceptions\PermissionDeniedException;
use App\Exceptions\LoginRequiredException;
use App\Http\Controllers\API\NotificationController;
class BuyController extends Controller
{

    public function __construct()
    {
        view()->share('lang_object', 'buy');
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        $data = [];
        $customer_role_id = Role::where('slug', 'customer')->first()->id;
        $data['user_sales_channels'] = Auth::user()->sales_channels;

        $data['customer_list'] = [];

        foreach($data['user_sales_channels'] as $usc){
            $users = $usc->users()->pluck('users.name', 'users.id');
            $data['customer_list'] = $data['customer_list']->merge($users);
        }

        if(Auth::check())
        $data['buy_list'] = Buy::where('user_id', Auth::user()->id)->orderBy('updated_at', 'DESC')->get();

        return view('table.buy-history')->with('data', $data);
    }
    public static function persist(Request $request, $id = null){
        $data = [];

        $data['buy'] = $id == null ? new Buy() : Buy::find($id);
        if($id != null && empty($data['buy'])){
            $request['object_type'] = 'buy';
            $request['object_id'] = $id;
            throw new ObjectNotFoundException($request);
        }


        if($id == null)
        $data['cart'] = CartController::persist($request->input('session_id'));

        $customer = User::find($request->input('user_id'));
     
        $data['customer'] = $customer == null ? new User() : $customer;
    
        $data['customer']->role_id = Role::where('slug', 'customer')->first()->id;
        $data['customer']->name = $request->input('name');
        $data['customer']->phone =  \Helpers\Format::filterNumbers($request->input('phone'));
        $data['customer']->save();

        if(!Auth::check())
        Auth::login($data['customer']);

        $data['buy']->user_id = $data['customer']->id;
        $data['buy']->cart_id = $id == null ? $data['cart']->id : $data['buy']->cart_id;
        $data['buy']->details = $request->input('details');
        $data['buy']->save();
        $data['buy']->cart->details = $request->input('details');
        $data['buy']->cart->save();


        $items = ItemController::persistFromCart($data['cart']->id, $request);

        foreach($items as $item){
            $last_status = $item->getLastStatus(); 
            if(!empty($last_status->pivot)){
                $last_status->pivot->updateStockQuantity();
                $data['buy']->user->notify(new ItemStatusUpdate($last_status->pivot->id));
            }
        }

        $address_list = $request->input('address_id');
        if($request->input('address_new') == 'true'){
            $add = new Address();
            $add->user_id = $request->input('user_id');
            $add->title = $request->input('title');
            $add->zipcode = $request->input('zipcode');
            $add->street = $request->input('street');
            $add->number = $request->input('number');
            $add->district = $request->input('district');
            $add->city = $request->input('city');
            $add->state = $request->input('state');
            $add->save();
            $address_list[] = $add->id;
        }
        DB::table('buy_address')->where('buy_id', $id)->delete();
        if(!empty($address_list)){
            foreach($address_list as $k => $address_id){
                $ba_id = DB::table('buy_address')
                ->insertGetId([
                    'buy_id' => $id,
                    'address_id' => $address_id
                    ]);
                }
            }                       
            session()->flash('flash', true);
            session()->flash('flash-class', 'success');
            session()->flash('flash-prefix', __('legend.success-prefix'));
            session()->flash('flash-text', __('legend.store-success'));
            return $data['buy'];
        }
        /**
        * Store a newly created resource in storage.
        *
        * @param  \Illuminate\Http\Request  $request
        * @return \Illuminate\Http\Response
        */
        public function store(Request $request)
        {
            $data = [];
            $data['buy'] = self::persist($request);
            return redirect('buy/'.$data['buy']->id);
        }
        /**
        * Display the specified resource.
        *
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
        public function show(Request $request, $id)
        {
            $data = [];
            $customer_role_id = Role::where('slug', 'customer')->first()->id;
            $data['user_sales_channels'] = Auth::user()->sales_channels;

            $data['customer_list'] = [];

            foreach($data['user_sales_channels'] as $usc){
                $users = $usc->users()->pluck('users.name', 'users.id');
                $data['customer_list'] = $data['customer_list']->merge($users);
            }
     
            $data['buy'] = Buy::find($id);
            if(!Auth::check()){
                $request['object_type'] = 'buy';
                $request['object_id'] = $id;
                throw new LoginRequiredException($request);
            }
            if($id != null && empty($data['buy'])){
                $request['object_type'] = 'buy';
                $request['object_id'] = $id;
                throw new ObjectNotFoundException($request);
            }
            if(Auth::user()->id != $data['buy']->user_id && Auth::user()->cannot('manage')){
                $request['object_type'] = 'buy';
                $request['object_id'] = $id;
                throw new PermissionDeniedException($request);
            }
            $data['cart'] = $data['buy']->cart;
            $data['customer'] = $data['buy']->user;
            $data['addresses'] = $data['buy']->addresses->pluck('id')->toArray();
            $data['buy_last'] = User::getLastBuyBySession($request->session()->getId());
            if(!empty($data['buy']->user))
            $data['addresses_available'] =  $data['buy']->user->addresses;
            $data['statuses'] = Status::pluck('title', 'id');
            return view('crud.buy')->with('data', $data);
        }
        /**
        * Update the specified resource in storage.
        *
        * @param  \Illuminate\Http\Request  $request
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
        public function update(Request $request, $id)
        {
            $data = [];
            $data['buy'] = self::persist($request, $id);
            return redirect('buy/'.$data['buy']->id);
        }
        /**
        * Remove the specified resource from storage.
        *
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
        public function destroy(Request $request, $id)
        {
            $buy = Buy::find($id);
            if(empty($buy)){
                $request['object_type'] = 'buy';
                $request['object_id'] = $id;
                throw new ObjectNotFoundException($request);
            }
            $buy->delete();
            session()->flash('flash', true);
            session()->flash('flash-class', 'success');
            session()->flash('flash-prefix', __('legend.success-prefix'));
            session()->flash('flash-text', __('legend.delete-success'));
            return redirect()->action('CartController@index');
        }
        public function repeat($buy_id){
            $data = [];
            $data['buy'] = Buy::find($buy_id);
            if($data['buy'] == null)
            throw new ObjectNotFoundException();
            $data['cart_old'] = Cart::find($data['buy']->cart_id);
            $data['cart'] = $data['cart_old']->replicate();
            $data['cart']->push();
            foreach($data['cart_old']->items as $k => $v){
                $v->cart_id = $data['cart']->id;
                $i = Item::create($v->toArray());
            }
            return redirect('cart/'.$data['cart']->id);
        } 
    }
    