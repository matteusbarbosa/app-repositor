<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\User;
use App\SalesChannel;
use App\Role;
use App\UserSalesChannel;
use App\UserSalesGoal;
use App\SalesGoal;

class UserController extends Controller
{

    public function __construct()
    {
        view()->share('lang_object', 'user');
    }

      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];

        $data['users'] = [];
        

        $users = User::all();

        foreach($users as $k => $u){
            $data['users'][$k] = $u;
            $data['users'][$k]->last_meaningful_activity = $u->logs()->orderBy('created_at', 'DESC')->take(1)->first();
        } 
          
        return view('table.user')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];

        $data['role_list'] = Role::pluck('title', 'id');

        $data['sales_channels'] = SalesChannel::pluck('title', 'id');
       
        $data['sales_goals'] = SalesGoal::pluck('title', 'id');


        return view('crud.user')->with('data', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [];

        $data['user'] = self::persist($request);

        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.store-success'));
        session()->flash('flash-text', $data['user']->name);

        return $this->show($data['user']->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];

        $data['user'] = User::find($id);
       // $data['user'] = User::getLastuser(Auth::user()->id);

       if(empty($data['user']))
       throw new \Exception();

        return view('table.user')->with('data', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];

        $data['user'] = User::find($id);

       $data['attendants'] = User::whereHas('role', function ($query) {
        $query->where('slug', 'attendant')->orWhere('slug', 'admin');
         })->get();

        $data['role_list'] = Role::pluck('title', 'id');

        $data['sales_channels'] = SalesChannel::pluck('title', 'id');

        $data['user_sales_channels'] = $data['user']->sales_channels()->pluck('sales_channel.id')->toArray();
       
        $data['sales_goals'] = SalesGoal::pluck('title', 'id');

        $data['user_sales_goals'] = $data['user']->sales_goals()->pluck('sales_goal.id')->toArray();

       if(empty($data['user']))
       throw new \Exception();

       if(Auth::user()->cannot('manage') && $data['user']->user_id != Auth::user()->id)
       throw new AccessForbiddenException();



        return view('crud.user')->with('data', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = [];

        $data['user'] = self::persist($request, $id);
        
        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.update-success'));
        session()->flash('flash-text', $data['user']->title);

        return back();
    }

    private static function persist(Request $request, $id = null){

        $user = $id != null ? User::find($id) : new User();

        $user->role_id = $request->input('role_id');
        $user->cnpj = \Helpers\Format::filterNumbers($request->input('cnpj'));
        $user->name = $request->input('name');
        $user->email = $request->input('email');

        if(empty($id))
        $user->phone = $request->input('phone');

        $user->phone_whatsapp = $request->input('phone_whatsapp');

        if($request->input('password_new') != null)
        $user->password = Hash::make($request->input('password_new'));

        $user->bank_data = Crypt::encrypt($request->input('bank_data'));
        $user->details = $request->input('details');

        $user->save();

        if(!empty($request->input('sales_channel_id'))){

            if($id != null)
            UserSalesChannel::where('user_id', $id)->delete();

            foreach($request->input('sales_channel_id') as $k => $id){
                if($id == null)
                continue;             
                
                $usc = new UserSalesChannel();
                $usc->sales_channel_id = $id;
                $usc->user_id = $user->id;
                $usc->save();
            }     
        }

        if(!empty($request->input('sales_goal_id'))){

            if($id != null)
            UserSalesGoal::where('user_id', $id)->delete();

            foreach($request->input('sales_goal_id') as $k => $id){
                if($id == null)
                continue;             
                
                $usc = new UserSalesGoal();
                $usc->sales_goal_id = $id;
                $usc->user_id = $user->id;
                $usc->save();
            }     
        }

        return $user;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $title = trans_choice('object.user', 1).' – '.$user->name;
        $user->delete();

        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.delete-success'));
        session()->flash('flash-text', $title);
        
        return redirect()->action('Admin\UserController@index');
    }
}
