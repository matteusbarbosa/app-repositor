<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Buy;
use App\Status;

class BuyController extends Controller
{

  public function __construct()
  {
      view()->share('lang_object', 'buy');
  }

  /* 
  * Apenas o atendente ou admin podem ver.
  * Fila de buys
  */
  public function queue(){

    $data = [];

    $data['statuses'] = Status::pluck('title', 'id');

    return view('queue.buy')->with('data', $data);

    }

      /* 
  * Apenas o atendente ou admin podem ver.
  * Fila de buys
  */
  public function index(){

    $data = [];

    $data['buy_list'] = Buy::orderBy('updated_at', 'ASC');

    return view('table.buy-history')->with('data', $data);

    }
    
    

  }
  