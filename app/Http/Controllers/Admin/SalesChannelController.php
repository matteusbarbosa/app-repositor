<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SalesChannel;
use App\SalesGoal;
class SalesChannelController extends Controller
{

    public function __construct()
    {
        view()->share('lang_object', 'sales_channel');
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $data = [];
        $data['sales_channels'] = SalesChannel::all();
        return view('table.sales_channel')->with('data', $data);
    }
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $data = [];
        $data['sales_channel_list'] = SalesChannel::orderBy('slug', 'ASC')->get();

        return view('crud.sales_channel')->with('data', $data);
    }
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $data = [];
        $data['sales_channel'] = self::persist($request);
        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.store-success'));
        session()->flash('flash-text', $data['sales_channel']->title);
        return redirect('/admin/sales_channel/'.$data['sales_channel']->id.'/edit');
    }
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $data = [];
        $data['sales_channel'] = SalesChannel::find($id);
        // $data['sales_channel'] = User::getLastsales_channel(Auth::user()->id);
        if(empty($data['sales_channel']))
        throw new \Exception();
        return view('table.sales_channel')->with('data', $data);
    }
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $data = [];
        $data['sales_channel'] = SalesChannel::find($id);
        
        if($data['sales_channel']->sales_goals != null)
        $data['sales_channel_goals'] = $data['sales_channel']->sales_goals->pluck('sales_goal.id')->toArray();

        if(empty($data['sales_channel']))
        throw new \Exception();
        return view('crud.sales_channel')->with('data', $data);
    }
    private static function persist(Request $request, $id = null){
        $sales_channel = $id != null ? SalesChannel::find($id) : new SalesChannel();
        $sales_channel->title = $request->input('title');
        $sales_channel->slug = \Helpers\Format::urlFriendly($request->input('title'));
        $sales_channel->details = $request->input('details');      
        $sales_channel->disabled_at = $request->input('active') == "1" ? null : date('Y-m-d H:i:s');
        $sales_channel->save();

        if(!empty($request->input('sales_goal_id'))){

            if($id != null)
            SalesChannelGoal::where('sales_channel_id', $id)->delete();

            foreach($request->input('sales_goal_id') as $k => $id){
                if($id == null)
                continue;             
                
                $scg = new SalesChannelGoal();
                $scg->sales_goal_id = $id;
                $scg->sales_channel_id = $sales_channel->id;
                $scg->save();
            }     
        }


        return $sales_channel;
    }
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $data = [];
        $data['sales_channel'] = self::persist($request, $id);
        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.update-success'));
        session()->flash('flash-text', $data['sales_channel']->title);
        return redirect('/admin/sales_channel/'.$id.'/edit');
    }
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $sales_channel = SalesChannel::find($id);
        $title = trans_choice('object.sales_channel',1).' – '.$sales_channel->title;
        $sales_channel->delete();
        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.delete-success'));
        session()->flash('flash-text', $title);
        return redirect()->action('Admin\SalesChannelController@index');
    }
}
