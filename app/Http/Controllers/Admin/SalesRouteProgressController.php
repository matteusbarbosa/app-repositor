<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\SalesRoute;
use App\Address;
use App\User;
use App\SalesRoutePlan;
use App\SalesStatus;
use App\SalesRouteProgress;

class SalesRouteProgressController extends Controller
{
    public function __construct()
    {
        view()->share('lang_object', 'sales_route_progress');
    }

    public function index(Request $request){

        $data = [];
        $data['sales_route_plan'] = SalesRoutePlan::find($request->query('sales_route_plan_id'));
        $data['sales_progress'] = $data['sales_route_plan']->getProgress();   
    

        return view('table.sales_route_progress')->with('data', $data);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create(Request $request)
    {
        $data = [];
        $data['user_list'] = User::whereIn('role_id', [4,5])->pluck('name', 'id');
        $data['plan_list'] = SalesRoutePlan::join('sales_route', 'sales_route.id', '=', 'sales_route_plan.sales_route_id')
        ->pluck('sales_route.title', 'sales_route_plan.id');
        $data['status_list'] = SalesStatus::pluck('title', 'id');
        return view('crud.sales_route_progress')->with('data', $data);
    }
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $data = [];
        $data['sales_route_progress'] = self::persist($request);
        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.store-success'));
        session()->flash('flash-text', $data['sales_route_progress']->status->title);
        return redirect('/admin/sales_route_progress/'.$data['sales_route_progress']->id.'/edit');
    }
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $data = [];
        $data['sales_route_progress'] = SalesRouteProgress::find($id);
        // $data['sales_route'] = User::getLastsales_route(Auth::user()->id);
        if(empty($data['sales_route_progress']))
        throw new \Exception();
        return view('table.sales_route_progress')->with('data', $data);
    }
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $data = [];
        $data['sales_route_progress'] = SalesRouteProgress::find($id);
        if(empty($data['sales_route_progress']))
        throw new \Exception();
        $data['plan_address_list'] = $data['sales_route_progress']->plan_addresses;
        $data['sales_routes'] = SalesRoute::whereNull('disabled_at')->pluck('title', 'id')->toArray();
        $data['supervisor_list'] = User::whereIn('role_id', [2,1])->pluck('name', 'id');
        $data['address_list'] = User::join('address', 'address.user_id', '=', 'users.id')
        ->select('address.id', 'address.user_id', 'users.name', 'address.title', 'address.street', 'address.number', 'address.district')
        ->orderBy('users.name', 'ASC')
        ->get();
        $data['client_list'] = $data['address_list']->pluck('name', 'user_id');
        return view('crud.sales_route_progress')->with('data', $data);
    }

    private static function persist(Request $request, $id = null){
        $sales_route_progress = $id != null ? SalesRouteProgress::find($id) : new SalesRouteProgress();
        $sales_route_progress->sales_route_id = $request->input('sales_route_id');
        $sales_route_progress->supervisor_id = $request->input('supervisor_id');
        $sales_route_progress->disabled_at = $request->input('active') == "1" ? null : date('Y-m-d H:i:s');
        $sales_route_progress->save();
        return $sales_route_progress;
    }
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $data = [];
        $data['sales_route_progress'] = self::persist($request, $id);
        $data['addresses'] = self::persistAddresses($request, $id);
        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.update-success'));
        session()->flash('flash-text', $data['sales_route_progress']->sales_route->title);
        return redirect('/admin/sales_route_progress/'.$id.'/edit');
    }

    private function persistAddresses(Request $request, $plan_id){
        $addresses = [];
        //rebuild binds
        DB::table('sales_route_progress_address')->where('sales_route_progress_id', $plan_id)
        ->delete();
        //get filled stock fields
        if(!empty($request->input('address_id'))){
            foreach($request->input('address_id') as $k => $id){
                if(empty($request->input('address_id')[$k]))
                continue;
                $addresses[$k]['sales_route_progress_id'] = $plan_id;
                $addresses[$k]['address_id'] = $id;
                $addresses[$k]['queue_position'] = $request->input('queue_position')[$k];
            }
        }
        $addresses_inserted = DB::table('sales_route_progress_address')->insert($addresses);
    }
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $sales_route_progress = SalesRouteProgress::find($id);
        $title = trans_choice('object.sales_route_progress',1).' – '.$sales_route_progress->sales_route->title;
        $sales_route_progress->delete();
        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.delete-success'));
        session()->flash('flash-text', $title);
        return redirect()->action('Admin\SalesRouteController@index');
    }
}
