<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Tag;
use App\Stock;
use App\Report\ViewMonthYearStockItemByTag;
use App\Report\ViewMonthYearStockItemByStatus;
use App\Report\ViewMonthYearCartAbandonCount;
use App\Report\ViewMonthYearUserJoinCount;
use App\Report\ViewStockNeverSold;
use App\Report\ViewMostActiveAttendant;
use App\Report\ViewBestSellingSet;
use App\Report\ViewTrimesterBestSellingProduct;
use App\Report\ViewTrimesterHighestQuantitySellingProduct;
use App\Report\ViewSearchEmptyResult;

class ReportController extends Controller
{

    public function __construct()
    {
        view()->share('lang_object', 'report');
    }

   public function index(){
       return view('reports.index');
   }

    public function show(Request $request, $kind){
        $from = new \DateTime($request->query('from'));
        $to = new \DateTime($request->query('to'));
        switch($kind){
            case "vendas-por-categoria":
            return $this->buyByTag();
            break;
            case "vendas-por-status";
            return $this->buyByStatus();
            break;
  
            case "usuarios-cadastrados";
            return $this->registeredCustomers();
            break;
            case "produtos-nunca-vendidos";
            return $this->stockNeverSold();
            break;
            case "atendente-mais-ativo";
            return $this->mostActiveAttendant();
            break;
            case "conjunto-mais-vendido";
            return $this->bestSellingSet();
            break;
            case "product-mais-vendido-por-trimestre";
            return $this->trimesterBestSellingProduct();
            break;
            case "product-vendido-em-maior-quantidade-por-trimestre";
            return $this->trimesterHighestQuantitySellingProduct();
            break;
            case "pesquisas-sem-resultado";
            return $this->searchesEmptyResult();
            break;
         
        }
    }

      /* 
        Pesquisas sem resultado
    */ 
    public function searchesEmptyResult(){
        //   view_trimester_best_selling_product
        $data = [];
    
          //report query results
          $data['rows'] = ViewSearchEmptyResult::all();
    
        return view('reports.view_search_empty_result')->with('data', $data);
        }

     /* 
        Produto vendido em maior quantidade por trimestre
    */ 
    public function trimesterHighestQuantitySellingProduct(){
        //   view_trimester_best_selling_product
        $data = [];
    
          //report query results
          $data['rows'] = ViewTrimesterHighestQuantitySellingProduct::all();
    
          foreach($data['rows'] as $k => $content){
              $stock = Stock::find($content['object_id']);
              $title = $stock->product->title;
         
              $data['rows'][$k]['product_name'] = !empty($stock) ? $title : __('legend.undefined');
              $data['rows'][$k]['details'] =  !empty($stock) ?  $stock->fieldsToString(null, ['weight', 'size', 'color']) : __('legend.undefined');
          }
    
        return view('reports.view_trimester_highest_quantity_selling_product')->with('data', $data);
        }

 /* 
        Produto mais vendido por trimestre
    */ 
    public function trimesterBestSellingProduct(){
    //   view_trimester_best_selling_product
    $data = [];

      //report query results
      $data['rows'] = ViewTrimesterBestSellingProduct::all();

      foreach($data['rows'] as $k => $content){
          $stock = Stock::find($content['object_id']);
          $title = $stock->product->title;
     
          $data['rows'][$k]['product_name'] = !empty($stock) ? $title : __('legend.undefined');
          $data['rows'][$k]['details'] =  !empty($stock) ?  $stock->fieldsToString(null, ['weight', 'size', 'color']) : __('legend.undefined');
      }

    return view('reports.view_trimester_best_selling_product')->with('data', $data);
    }

        
      /* 
        Conjunto mais vendido
    */ 
    public function bestSellingSet(){

        $data = [];

        //report query results
        $data['rows'] = ViewBestSellingSet::all();

        $data['combinations'] = [];

        foreach($data['rows'] as $k => $r){
            $numbers = explode(',', $r['combination']);

            sort($numbers);

            $stocks = Stock::whereIn('id', $numbers)->get();

            $product_names = [];
            foreach($stocks as $k => $s){
                $title = $s->product->title;

                $product_names[] = $title;

            }

            $str_combination = '';

            foreach($numbers as $k => $n){
                $str_combination .= $n;

                if(!isset($data['combinations'][$str_combination]['product_names'])){
                    $data['combinations'][$str_combination] = [];
                    $data['combinations'][$str_combination]['object_ids'] = $numbers;
                    $data['combinations'][$str_combination]['product_names'] = $product_names;
                    $data['combinations'][$str_combination]['times_requested'] = 0;
                }
                
                $data['combinations'][$str_combination]['times_requested']++;
                
                if(isset($numbers[$k+1]))
                $str_combination .= ','; 
            }
        }

  


                return view('reports.view_best_selling_set')->with('data', $data);
    }


    
      /* 
        Atendente mais ativo
    */ 
    public function mostActiveAttendant(){

        $data = [];

        $data['rows'] = ViewMostActiveAttendant::all();

        return view('reports.view_most_active_attendant')->with('data', $data);
    }

      /* 
        Produtos nunca vendidos
    */ 
    public function stockNeverSold(){

        $data = [];

        $data['rows'] = ViewStockNeverSold::all();

        return view('reports.view_stock_never_sold')->with('data', $data);
    }

    /* 
        Relatório de compras por tag
    */ 
    public function buyByTag(){

        $data = [];

        $data['rows'] = ViewMonthYearStockItemByTag::all();

        return view('reports.view_month_year_stock_item_by_tag')->with('data', $data);
    }
    public function buyByStatus(){

        $data = [];

        $data['rows'] = ViewMonthYearStockItemByStatus::all();

        return view('reports.view_month_year_stock_item_by_status')->with('data', $data);
    }

    public function registeredCustomers(){
        $data = [];

        $data['rows'] = ViewMonthYearUserJoinCount::all();

        return view('reports.view_month_year_user_join_count')->with('data', $data);
    }
    public function lowStockQuantity(){

    }
    public function notifications(){

    }

    public function stockCount(){

    }
 
}
