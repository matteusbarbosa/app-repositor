<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SalesChannel;
use App\SalesRoute;
use App\SalesRouteStatus;
class SalesRouteController extends Controller
{
    public function __construct()
    {
        view()->share('lang_object', 'sales_route');
    }
    public function createForChannel(Request $request, $channel_id){
        $data = [];
        $data['sales_channel'] = SalesChannel::find($channel_id);
        return view('crud.sales_route')->with('data', $data);
    }
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $data = [];
        $data['sales_routes'] = SalesRoute::all();
        return view('table.sales_route')->with('data', $data);
    }
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $data = [];

        $data['sales_route_statuses'] = SalesRouteStatus::pluck('title', 'id');

        
        return view('crud.sales_route')->with('data', $data);
    }
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $data = [];
        $data['sales_route'] = self::persist($request);
        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.store-success'));
        session()->flash('flash-text', $data['sales_route']->title);
        return redirect('/admin/sales_route/'.$data['sales_route']->id.'/edit');
    }
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $data = [];
        $data['sales_route'] = SalesRoute::find($id);
        // $data['sales_route'] = User::getLastsales_route(Auth::user()->id);
        if(empty($data['sales_route']))
        throw new \Exception();
        return view('table.sales_route')->with('data', $data);
    }
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $data = [];
        $data['sales_route'] = SalesRoute::find($id);
        if(empty($data['sales_route']))
        throw new \Exception();
        
        $data['sales_route_statuses'] = SalesRouteStatus::pluck('title', 'id');

        return view('crud.sales_route')->with('data', $data);
    }

    private static function persist(Request $request, $id = null){
        $sales_route = $id != null ? SalesRoute::find($id) : new SalesRoute();
        $sales_route->status_id = $request->input('status_id');
        $sales_route->title = $request->input('title');

        $sales_route->slug = \Helpers\Format::urlFriendly($request->input('title'));
        $sales_route->details = $request->input('details');   

        $sales_route->disabled_at = $request->input('active') == "1" ? null : date('Y-m-d H:i:s');
        $sales_route->save();
        return $sales_route;
    }
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $data = [];
        $data['sales_route'] = self::persist($request, $id);
        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.update-success'));
        session()->flash('flash-text', $data['sales_route']->title);
        return redirect('/admin/sales_route/'.$id.'/edit');
    }



    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $sales_route = SalesRoute::find($id);
        $title = trans_choice('object.sales_route',1).' – '.$sales_route->title;
        $sales_route->delete();
        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.delete-success'));
        session()->flash('flash-text', $title);
        return redirect()->action('Admin\SalesRouteController@index');
    }
}
