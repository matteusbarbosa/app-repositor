<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Product;
use \App\Role;
use \App\Stock;
use \App\User;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\ObjectNotFoundException;
class ProductController extends Controller
{

    public function __construct()
    {
        view()->share('lang_object', 'product');
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $data = [];
        $data['product_list'] = Product::all();
        return view('table.product')->with('data', $data);
    }
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit(Request $request, $id)
    {
        $data = [];
        $data['product'] = Product::find($id);
        if(empty($data['product'])){
            $request['object_type'] = 'product';
            $request['object_id'] = $id;
            throw new ObjectNotFoundException($request);
        }
        $data['stock_list'] = $data['product']->stocks;
        // $data['product'] = User::getLastproduct(Auth::user()->id);
        $data['attendants'] = User::whereHas('role', function ($query) {
            $query->where('slug', 'attendant')->orWhere('slug', 'admin');
        })->get();
        if(empty($data['product']))
        throw new \AccessForbiddenException();
        if(Auth::user()->cannot('manage') && $data['product']->user_id != Auth::user()->id)
        throw new AccessForbiddenException();
        return view('crud.product')->with('data', $data);
    }
    private function persist(Request $request, $id = null){
        $product = $id == null ? new Product() : Product::find($id);
        if(empty($product)){
            $request['object_type'] = 'product';
            $request['object_id'] = $id;
            throw new ObjectNotFoundException($request);
        }
        $product->user_id = Auth::user()->id;
        $product->slug = \Helpers\Format::urlFriendly($request->input('title'));
        $product->title = $request->input('title');
        $product->details = $request->input('details');

        $product->disabled_at = $request->input('active') == 1 ? null : date('Y-m-d H:i:s');
        if($request->file('upload_remove')){
            if(!empty($product->file_id))
            $product->image()->delete();
        }
        if($request->file('upload')){
            $up_tag = \upload_product($request->file('upload'));
            if(!empty($product->file_id))
            $product->image()->delete();
            $product->file_id  = $up_tag;
        }
        $product->save();
        return $product;
    }
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $data = [];
        $data['user'] = Auth::user();
        $data['attendants'] = Role::where('slug', 'attendant')->first()->users;
        return view('crud.product')->with('data', $data);
    }
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $data = [];
        $data['product'] = self::persist($request);
        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', trans('legend.success-prefix'));
        session()->flash('flash-text', trans('legend.store-success'));
        return redirect('/admin/product/'.$data['product']->id.'/edit');
    }
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $data = [];
        $data['product'] = self::persist($request, $id);
        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.update-success'));
        session()->flash('flash-text', $data['product']->title);
        return back();
    }
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy(Request $request, $id)
    {
        $product = Product::find($id);
        if(empty($product)){
            $request['object_type'] = 'product';
            $request['object_id'] = $id;
            throw new ObjectNotFoundException($request);
        }
        $title = __('object.product').' – '.$product->title;
        $product->delete();
        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.delete-success'));
        session()->flash('flash-text', $title);
        return redirect()->action('Admin\ProductController@index');
    }
}
