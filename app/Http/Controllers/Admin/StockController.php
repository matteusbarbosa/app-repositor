<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Cart;
use App\Status;
use App\Stock;
use App\Product;
use App\StockType;
use App\Tag;
use App\Field;
use App\TagStock;
use App\StockComponent;
use App\Item;
use App\Exceptions\AccessForbiddenException;
use App\Http\Controllers\Controller;
use Intervention\Image\ImageAdminStatic as Image;
use Carbon\Carbon;
class StockController extends Controller
{

    public function __construct()
    {
        view()->share('lang_object', 'stock');
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $data = [];
        $data['stocks'] = Stock::all();
        return view('table.stock')->with('data', $data);
    }
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $data = [];
        $data['product_list'] = Product::whereNull('disabled_at')->pluck('title', 'id');
        $data['type_list'] = StockType::all();
        $data['tag_list'] = Tag::whereNull('disabled_at')->pluck('title', 'id');
        $data['field_list'] = Field::whereNull('disabled_at')->pluck('title', 'id');
        $data['stock_list'] = Stock::join('product', 'product.id', '=', 'stock.product_id')
        ->select('stock.id', 'stock.type_id', 'product.disabled_at', 'stock.product_id')
        ->whereNull('product.disabled_at')
        ->get();
        return view('crud.stock')->with('data', $data);
    }
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $data = [];
        $stock =  $this->persist($request, new Stock());
        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.store-success'));
        session()->flash('flash-text', $stock->product->title);
        return back();
    }
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $data = [];
        $data['stock'] = Stock::find($id);
        // $data['stock'] = User::getLaststock(Auth::user()->id);
        $data['product_list'] = Product::whereNull('disabled_at')->pluck('title', 'id');
        $data['stock_components'] = $data['stock']->components;
        $data['stock_list'] = Stock::join('product', 'product.id', '=', 'stock.product_id')
        ->select('stock.id', 'stock.type_id', 'product.disabled_at', 'stock.product_id')
        ->whereNull('product.disabled_at')
        ->where('stock.id', '!=', $id)
        ->get();
        $data['type_list'] = StockType::all();
        $data['stock_tag_list'] = $data['stock']->tags()->pluck('tag.id')->toArray();
        $data['tag_list'] = Tag::whereNull('disabled_at')->pluck('title', 'id');
        $data['stock_fields'] = $data['stock']->fields;
        $data['field_list'] = Field::whereNull('disabled_at')->pluck('title', 'id');
        $data['attendants'] = User::whereHas('role', function ($query) {
            $query->where('slug', 'attendant')->orWhere('slug', 'admin');
        })->get();
        if(empty($data['stock']))
        throw new \Exception();
        if(Auth::user()->cannot('manage'))
        throw new AccessForbiddenException();
        return view('crud.stock')->with('data', $data);
    }
    private function persistFields(Request $request, $stock_id){
        $fields = [];
        $stock = Stock::find($stock_id);
        //rebuild binds
        DB::table('stock_field')->where('stock_id', $stock->id)
        ->delete();
        //get filled stock fields

        if(!empty($request->input('field_id'))){
            foreach($request->input('field_id') as $k => $id){
                if(empty($request->input('field_content')[$k]))
                continue;
                $fields[$k]['user_id'] = Auth::user()->id;
                $fields[$k]['stock_id'] = $stock->id;
                $fields[$k]['field_id'] = $id;

                if(isset($request->file('field_content')[$k])){
                    $fields[$k]['content'] = \upload_field($request->file('field_content')[$k]);
                } else {
                    $fields[$k]['content'] = $request->input('field_content')[$k];
                }
            }
        }
  

        $fields_inserted = DB::table('stock_field')->insert($fields);
    }
    private function persistComponents(Request $request, $stock_id){
         //rebuild binds
         StockComponent::where('parent_id', $stock_id)
         ->delete();
         if(!empty($request->input('component_id'))){
             foreach($request->input('component_id') as $k => $id){
                 if($id == null)
                 continue;
                 $bind = new StockComponent();
                 $bind->parent_id = $stock_id;
                 $bind->stock_id = $id;
                 $bind->quantity = $request->input('component_quantity')[$k];
                 $bind->save();
             }     
         }
    }
    private function persistTags(Request $request, $stock_id){
        TagStock::where('stock_id', $stock_id) ->delete();

        if(!empty($request->input('tag_id'))){
        
            foreach($request->input('tag_id') as $k => $id){
                if($id == null)
                continue;

                $tag_stock = new TagStock();
                $tag_stock->tag_id = $id;
                $tag_stock->stock_id = $stock_id;
                $tag_stock->save();
            }     
        }


    }
    private function persist(Request $request, $stock){
        $stock->sku = $request->input('sku');
        $stock->user_id = Auth::user()->id;
        $stock->type_id = $request->input('type_id');
        $stock->product_id = $request->input('product_id');
        $stock->quantity = $request->input('quantity');
        $stock->details = $request->input('details');

        $stock->shop_display_at = $request->input('shop_display') == "1" ? date('Y-m-d H:i:s') : null;

        $stock->disabled_at = $request->input('active') == 1 ? null : date('Y-m-d H:i:s');
        //save within public but return path without public
        if($request->file('upload')){
            if(!empty( $stock->file_id))
            $stock->image->delete();
            $stock->file_id = \upload_stock($request->file('upload'));
        }
        if($stock->save()){
            self::persistFields($request, $stock->id);
            self::persistComponents($request, $stock->id);
            self::persistTags($request, $stock->id);
            return $stock;
        } else {
            throw new \Exception();
        }     
    }
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $data = [];
        $stock = Stock::find($id);
        $stock = $this->persist($request, $stock);
        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.update-success'));
        session()->flash('flash-text', $stock->product->title);
        return back();
    }
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $stock = Stock::find($id);
        $title = __('object.stock').' – #'.$stock->id.' – '.$stock->product->title;
        $stock->delete();
        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.delete-success'));
        session()->flash('flash-text', $title);
        return redirect()->action('Admin\StockController@index');
    }
}
