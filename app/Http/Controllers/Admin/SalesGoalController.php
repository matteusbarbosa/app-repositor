<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SalesChannel;
use App\SalesGoal;
class SalesGoalController extends Controller
{
    public function __construct()
    {
        view()->share('lang_object', 'sales_goal');
    }
    public function createForChannel(Request $request, $channel_id){
        $data = [];
        $data['sales_channel'] = SalesChannel::find($channel_id);
        return view('crud.sales_goal')->with('data', $data);
    }
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $data = [];
        $data['sales_goals'] = SalesGoal::all();
        return view('table.sales_goal')->with('data', $data);
    }
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $data = [];
        return view('crud.sales_goal')->with('data', $data);
    }
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $data = [];
        $data['sales_goal'] = self::persist($request);
        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.store-success'));
        session()->flash('flash-text', $data['sales_goal']->title);
        return redirect('/admin/sales_goal/'.$data['sales_goal']->id.'/edit');
    }
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $data = [];
        $data['sales_goal'] = SalesGoal::find($id);
        // $data['sales_goal'] = User::getLastsales_goal(Auth::user()->id);
        if(empty($data['sales_goal']))
        throw new \Exception();
        return view('table.sales_goal')->with('data', $data);
    }
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $data = [];
        $data['sales_goal'] = SalesGoal::find($id);
        if(empty($data['sales_goal']))
        throw new \Exception();
        return view('crud.sales_goal')->with('data', $data);
    }
    private static function persist(Request $request, $id = null){
        $sales_goal = $id != null ? SalesGoal::find($id) : new SalesGoal();
        $sales_goal->title = $request->input('title');
        $sales_goal->goal_amount_min = $request->input('goal_amount_min');
        $sales_goal->goal_amount = $request->input('goal_amount');
        $sales_goal->goal_percent_max = $request->input('goal_percent_max');
        $sales_goal->slug = \Helpers\Format::urlFriendly($request->input('title'));
        $sales_goal->details = $request->input('details');   
        if(!empty($request->input('start_at')))
        $sales_goal->start_at = \DateTime::createFromFormat('d/m/Y', $request->input('start_at'));
        if(!empty($request->input('end_at')))
        $sales_goal->end_at = \DateTime::createFromFormat('d/m/Y', $request->input('end_at'));
        $sales_goal->disabled_at = $request->input('active') == "1" ? null : date('Y-m-d H:i:s');
        $sales_goal->save();
        return $sales_goal;
    }
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $data = [];
        $data['sales_goal'] = self::persist($request, $id);
        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.update-success'));
        session()->flash('flash-text', $data['sales_goal']->title);
        return redirect('/admin/sales_goal/'.$id.'/edit');
    }
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $sales_goal = SalesGoal::find($id);
        $title = trans_choice('object.sales_goal',1).' – '.$sales_goal->title;
        $sales_goal->delete();
        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.delete-success'));
        session()->flash('flash-text', $title);
        return redirect()->action('Admin\SalesGoalController@index');
    }
}
