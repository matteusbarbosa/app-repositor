<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Cart;
use App\Status;
use App\Stock;
use App\Promotion;
use App\SalesChannel;
use App\Item;
use App\Exceptions\AccessForbiddenException;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class PromotionController extends Controller
{

    public function __construct()
    {
        view()->share('lang_object', 'promotion');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];

        $data['promotions'] = Promotion::all()->reject(function($p){

            if($p->object == null)
            return true;
   
        });

        return view('table.promotion')->with('data', $data);
    }

    public function notificationIndex($id){
        $data = [];

        $data['promotion'] = Promotion::find($id);

       if(empty($data['promotion']))
       throw new \Exception();

       if(Auth::user()->cannot('manage'))
       throw new AccessForbiddenException();

        return view('table.promotion-notifications')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    $data = [];

    $data['sales_channels'] = SalesChannel::pluck('title', 'id');

    $data['stocks'] = Stock::where('quantity', '>', 0)->get();

    return view('crud.promotion')->with('data', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [];

        $data['promotion'] = self::persist($request);

        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.store-success'));
        session()->flash('flash-text', $data['promotion']->title);

        return $this->show($data['promotion']->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];

        $data['promotion'] = Promotion::find($id);

       if(empty($data['promotion']))
       throw new \Exception();

        return view('table.promotion')->with('data', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];

        $data['promotion'] = Promotion::find($id);

        $data['sales_channels'] = SalesChannel::pluck('title', 'id');

        $data['stocks'] = Stock::where('quantity', '>', 0)->get();

       if(empty($data['promotion']))
       throw new \Exception();

        return view('crud.promotion')->with('data', $data);
    }

    private static function persist(Request $request, $id = null){

        $promotion = $id == null ? new Promotion() : Promotion::find($id);
        $promotion->user_id = !empty($request->input('user_id')) ? $request->input('user_id') : Auth::user()->id;
        $promotion->title = $request->input('title');

        $promotion->object_type = 'App\Stock';
        $promotion->object_id = $request->input('stock_id');

        $promotion->discount_percent = $request->input('discount_percent');

        if(!empty($request->input('discount_start')))
        $promotion->discount_start = \DateTime::createFromFormat('d/m/Y', $request->input('discount_start'));

        if(!empty($request->input('discount_end')))
        $promotion->discount_end = \DateTime::createFromFormat('d/m/Y', $request->input('discount_end'));

        if(!empty($request->input('featured_until')))
        $promotion->featured_until = \DateTime::createFromFormat('d/m/Y', $request->input('featured_until'));
       
        $promotion->details = $request->input('details');
        $promotion->disabled_at = $request->input('active') == 1 ? null : date('Y-m-d H:i:s');

        $promotion->save();

        return $promotion;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = [];

        $data['promotion'] = self::persist($request, $id);

        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.update-success'));
        session()->flash('flash-text', $data['promotion']->title);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $promotion = Promotion::find($id);
        $title = __('object.promotion').' – '.$promotion->title;
        $promotion->delete();

        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.delete-success'));
        session()->flash('flash-text', $title);
        
        return redirect()->action('Admin\PromotionController@index');
    }

}
