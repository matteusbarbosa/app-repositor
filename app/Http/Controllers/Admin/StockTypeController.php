<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\StockType;
use App\Exceptions\AccessForbiddenException;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class StockTypeController extends Controller
{

    public function __construct()
    {
        view()->share('lang_object', 'stock_type');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];

        $data['stock_types'] = StockType::all();
          
        return view('table.stock_type')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];

        $data['stock_type_list'] = StockType::orderBy('slug', 'ASC')->get();

        return view('crud.stock_type')->with('data', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [];

        $data['stock_type'] = self::persist($request);

        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.store-success'));
        session()->flash('flash-text', $data['stock_type']->title);

        return redirect('/admin/stock_type/'.$data['stock_type']->id.'/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];

        $data['stock_type'] = StockType	::find($id);
       // $data['stock_type'] = User::getLaststock_type(Auth::user()->id);

       if(empty($data['stock_type']))
       throw new \Exception();

        return view('table.stock_type')->with('data', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];

        $data['stock_type'] = StockType::find($id);
       // $data['stock_type'] = User::getLaststock_type(Auth::user()->id);

       $data['stock_type_list'] = StockType::where('id', '!=', $id)->orderBy('slug', 'ASC')->get();

       if(empty($data['stock_type']))
       throw new \Exception();

        return view('crud.stock_type')->with('data', $data);
    }

    private static function persist(Request $request, $id = null){
      
        $stock_type = $id != null ? StockType::find($id) : new StockType();
        $stock_type->parent_id = $request->input('parent_id');
        $stock_type->title = $request->input('title');

        if($id == null)
        $stock_type->slug = \Helpers\Format::urlFriendly($request->input('title'));
        
        $stock_type->details = $request->input('details');
       
        $stock_type->disabled_at = $request->input('active') == 1 ? null : date('Y-m-d H:i:s');

        $stock_type->save();

        return $stock_type;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = [];

        $data['stock_type'] = self::persist($request, $id);

        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.update-success'));
        session()->flash('flash-text', $data['stock_type']->title);

        return redirect('/admin/stock_type/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $stock_type = StockType	::find($id);
        $title = trans_choice('object.stock_type', 1).' – '.$stock_type->title;
        $stock_type->delete();

        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.delete-success'));
        session()->flash('flash-text', $title);
        
        return redirect()->action('Admin\StockTypeController@index');
    }

}
