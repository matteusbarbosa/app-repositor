<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Field;
use App\Exceptions\ObjectNotFoundException;
use App\Exceptions\UniqueRegisterException;

class FieldController extends Controller
{
    public function __construct()
    {
        view()->share('lang_object', 'field');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];

        $data['fields'] = Field::all();
          
        return view('table.field')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];

        $data['field_list'] = Field::orderBy('slug', 'ASC')->get();

        return view('crud.field')->with('data', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [];

        $data['field'] = self::persist($request);

        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.store-success'));
        session()->flash('flash-text', $data['field']->title);

        return redirect('/admin/field/'.$data['field']->id.'/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];

        $data['field'] = Field::find($id);

       if(empty($data['field']))
       throw new \ObjectNotFoundException();

        return view('table.field')->with('data', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];

        $data['field'] = Field::find($id);

       $data['field_list'] = Field::orderBy('slug', 'ASC')->get();

       if(empty($data['field']))
       throw new \ObjectNotFoundException();

        return view('crud.field')->with('data', $data);
    }

    private static function persist(Request $request, $id = null){
      
        $field = $id != null ? Field::find($id) : new Field();
        $field->user_id = Auth::user()->id;
        $field->title = $request->input('title');
        if($id == null)
        $field->slug =  \Helpers\Format::urlFriendly($request->input('title'));
        
        $field->type = $request->input('type');
        $field->shop_display_at = $request->input('shop_display') == "1" ? date('Y-m-d H:i:s') : null;

        $field->disabled_at = $request->input('active') == "1" ? null : date('Y-m-d H:i:s');

        $field->save();

        return $field;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = [];

        $slug =  \Helpers\Format::urlFriendly($request->input('title'));
        
     
            $field = Field::where('id', '!=', $id)
            ->where('slug', $slug)
            ->where('type', $request->input('type'))
            ->first();
    
            if(!empty($field)){
                $request['object_type'] = 'field';
                $request['object_id'] = $id;
                $request['url_redirect'] = '/admin/field';
                throw new UniqueRegisterException($request);
            }
        

        $data['field'] = self::persist($request, $id);


        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.update-success'));
        session()->flash('flash-text', $data['field']->title);

        return redirect('/admin/field/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $field = Field::find($id);
        $title = __('object.field').' – '.$field->title;
        $field->delete();

        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.delete-success'));
        session()->flash('flash-text', $title);
        
        return redirect()->action('Admin\FieldController@index');
    }
}
