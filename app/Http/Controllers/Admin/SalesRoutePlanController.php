<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\SalesRoute;
use App\Address;
use App\User;
use App\SalesRoutePlan;

class SalesRoutePlanController extends Controller
{
    public function __construct()
    {
        view()->share('lang_object', 'sales_route_plan');
    }


    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $data = [];

        $data['sales_route'] = SalesRoute::find($request->query('sales_route_id'));


        $data['sales_route_plan'] = $data['sales_route']->plan;

        return view('table.sales_route_plan')->with('data', $data);
    }
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create(Request $request)
    {
        $data = [];

        $data['sales_routes'] = SalesRoute::whereNull('disabled_at')->pluck('title', 'id')->toArray();
        $data['supervisor_list'] = User::whereIn('role_id', [2,1])->pluck('name', 'id');
        $data['address_list'] = User::join('address', 'address.user_id', '=', 'users.id')
        ->select('address.id', 'address.user_id', 'users.name', 'address.title', 'address.street', 'address.number', 'address.district')
        ->orderBy('users.name', 'ASC')
        ->get();
        $data['client_list'] = $data['address_list']->pluck('name', 'user_id');

        return view('crud.sales_route_plan')->with('data', $data);
    }
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $data = [];
        $data['sales_route_plan'] = self::persist($request);
        $data['addresses'] = self::persistAddresses($request, $data['sales_route_plan']->id);
        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.store-success'));
        session()->flash('flash-text', $data['sales_route_plan']->sales_route->title);
        return redirect('/admin/sales_route_plan/'.$data['sales_route_plan']->id.'/edit');
    }
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $data = [];
        $data['sales_route_plan'] = SalesRoutePlan::find($id);
        // $data['sales_route'] = User::getLastsales_route(Auth::user()->id);
        if(empty($data['sales_route_plan']))
        throw new \Exception();
        return view('table.sales_route_plan')->with('data', $data);
    }
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $data = [];
        $data['sales_route_plan'] = SalesRoutePlan::find($id);
        $data['sales_route'] = $data['sales_route_plan']->sales_route;
        
        $data['sales_routes'] = SalesRoute::whereNull('disabled_at')->pluck('title', 'id')->toArray();
        $data['supervisor_list'] = User::whereIn('role_id', [2,1])->pluck('name', 'id');
        $data['address_list'] = User::join('address', 'address.user_id', '=', 'users.id')
        ->select('address.id', 'address.user_id', 'users.name', 'address.title', 'address.street', 'address.number', 'address.district')
        ->orderBy('users.name', 'ASC')
        ->get();
        $data['client_list'] = $data['address_list']->pluck('name', 'user_id');

        $data['plan_address_list'] = $data['sales_route_plan']->plan_addresses;


        if(empty($data['sales_route_plan']))
        throw new \Exception();
        return view('crud.sales_route_plan')->with('data', $data);
    }
    private static function persist(Request $request, $id = null){
        $sales_route_plan = $id != null ? SalesRoutePlan::find($id) : new SalesRoutePlan();
        $sales_route_plan->sales_route_id = $request->input('sales_route_id');
        $sales_route_plan->supervisor_id = $request->input('supervisor_id');
        $sales_route_plan->disabled_at = $request->input('active') == "1" ? null : date('Y-m-d H:i:s');
        $sales_route_plan->save();
        return $sales_route_plan;
    }
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $data = [];
        $data['sales_route_plan'] = self::persist($request, $id);
        $data['addresses'] = self::persistAddresses($request, $id);
        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.update-success'));
        session()->flash('flash-text', $data['sales_route_plan']->sales_route->title);
        return redirect('/admin/sales_route_plan/'.$id.'/edit');
    }

    
    private function persistAddresses(Request $request, $plan_id){
        $addresses = [];
        //rebuild binds
        DB::table('sales_route_plan_address')->where('sales_route_plan_id', $plan_id)
        ->delete();
        //get filled stock fields

        if(!empty($request->input('address_id'))){
            foreach($request->input('address_id') as $k => $id){
                if(empty($request->input('address_id')[$k]))
                continue;

                $addresses[$k]['sales_route_plan_id'] = $plan_id;
                $addresses[$k]['address_id'] = $id;
                $addresses[$k]['queue_position'] = $request->input('queue_position')[$k];
            }
        }
  

        $addresses_inserted = DB::table('sales_route_plan_address')->insert($addresses);
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $sales_route_plan = SalesRoutePlan::find($id);
        $title = trans_choice('object.sales_route_plan',1).' – '.$sales_route_plan->sales_route->title;
        $sales_route_plan->delete();
        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.delete-success'));
        session()->flash('flash-text', $title);
        return redirect()->action('Admin\SalesRouteController@index');
    }
}
