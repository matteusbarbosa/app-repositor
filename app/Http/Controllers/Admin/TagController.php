<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Cart;
use App\Stock;
use App\Status;
use App\Tag;
use App\Item;
use App\Exceptions\AccessForbiddenException;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class TagController extends Controller
{

    public function __construct()
    {
        view()->share('lang_object', 'tag');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];

        $data['tags'] = Tag	::all();
          
        return view('table.tag')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];

        $data['tag_list'] = Tag::orderBy('slug', 'ASC')->get();

        return view('crud.tag')->with('data', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [];

        $data['tag'] = self::persist($request);

        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.store-success'));
        session()->flash('flash-text', $data['tag']->title);

        return redirect('/admin/tag/'.$data['tag']->id.'/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];

        $data['tag'] = Tag	::find($id);
       // $data['tag'] = User::getLasttag(Auth::user()->id);

       if(empty($data['tag']))
       throw new \Exception();

        return view('table.tag')->with('data', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];

        $data['tag'] = Tag::find($id);
       // $data['tag'] = User::getLasttag(Auth::user()->id);
       
       $data['tag_list'] = Tag::where('id', '!=', $id)->orderBy('slug', 'ASC')->get();

       if(empty($data['tag']))
       throw new \Exception();

        return view('crud.tag')->with('data', $data);
    }

    private static function persist(Request $request, $id = null){
      
        $tag = $id != null ? Tag::find($id) : new Tag();
        $tag->parent_id = $request->input('parent_id');
        $tag->title = $request->input('title');
        $tag->slug = \Helpers\Format::urlFriendly($request->input('title'));
        
        $tag->details = $request->input('details');

        $tag->shop_display_at =  $request->input('shop_display') == "1" ? date('Y-m-d H:i:s') : null;
      
    
        $tag->disabled_at = $request->input('active') == "1" ? null : date('Y-m-d H:i:s');

            if($request->file('upload_remove')){
                if(!empty($tag->file_id))
                $tag->image()->delete();
            }

            if($request->file('upload')){
            $up_tag = \upload_tag($request->file('upload'));

            if(!empty($tag->file_id))
            $tag->image()->delete();
            
            $tag->file_id  = $up_tag;
        }

        $tag->save();

        return $tag;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = [];

        $data['tag'] = self::persist($request, $id);

        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.update-success'));
        session()->flash('flash-text', $data['tag']->title);

        return redirect('/admin/tag/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tag = Tag	::find($id);
        $title = __('object.tag').' – '.$tag->title;
        $tag->delete();

        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.delete-success'));
        session()->flash('flash-text', $title);
        
        return redirect()->action('Admin\TagController@index');
    }

}
