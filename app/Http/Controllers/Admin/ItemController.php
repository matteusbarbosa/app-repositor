<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Status;

class ItemController extends Controller
{
    public function __construct()
    {
        view()->share('lang_object', 'item');
    }

    public function index(Request $request){
        $data = [];

        $data['statuses'] = Status::pluck('title', 'id');

        return view('queue.item')->with('data', $data);
    }    
}