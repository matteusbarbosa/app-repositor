<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notification;

class NotificationController extends Controller
{

    public function __construct()
    {
        view()->share('lang_object', 'notification');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];

        $data['notifications'] = Notification::all();
          
        return view('table.notification')->with('data', $data);
    }

    private static function persist(Request $request, $id = null){
 
        $notification = $id != null ? (Notification::find($id)) : new Notification;
        $notification->type = $request->input('type');
        $notification->notifiable_type = $request->input('notifiable_type');
        $notification->notifiable_id = $request->input('notifiable_id');
        $notification->data = $request->input('data');
        $notification->read_at = $request->input('read_at');
        $notification->save();


        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.store-success'));
        session()->flash('flash-text', $notification->title);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];

        $data['notification_list'] = Notification::orderBy('type', 'ASC')->get();

        return view('crud.notification')->with('data', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [];

        self::persist($request);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];

        $data['notification'] = Notification::find($id);
       // $data['notification'] = User::getLastnotification(Auth::user()->id);

       if(empty($data['notification']))
       throw new \Exception();

        return view('table.notification')->with('data', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];

        $data['notification'] = Notification::find($id);
       // $data['notification'] = User::getLastnotification(Auth::user()->id);

       
       $data['notification_list'] = Notification::orderBy('type', 'ASC')->get();

       if(empty($data['notification']))
       throw new \Exception();

       if(Auth::user()->cannot('manage') && $data['notification']->user_id != Auth::user()->id)
       throw new AccessForbiddenException();

        return view('crud.notification')->with('data', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        self::persist($request, $id);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $notification = Notification::find($id);
        $title = __('object.notification');
        $notification->delete();

        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.delete-success'));
        session()->flash('flash-text', $title);
        
        return redirect()->action('Admin\NotificationController@index');
    }

}
