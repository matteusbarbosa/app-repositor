<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Stock;
use App\Role;
use App\Exceptions\ObjectNotFoundException;

class StockController extends Controller
{

    public function __construct()
    {
        view()->share('lang_object', 'stock');
    }

    public function index(){
        $data = [];

        $data['products'] = DB::select('SELECT p.id, s.id, p.user_id, p.file_id, p.title,f.url AS image, s.quantity
        FROM product AS p
        JOIN stock AS s ON p.id = s.product_id
        JOIN file AS f ON f.id = p.file_id
        GROUP BY p.id, s.id');

        $data['users'] = User::where('role_id', 1)->orWhere('role_id', 2)->get();

        $data['attendants'] = Role::where('slug', 'attendant')->first()->users;

        return view('stock')->with('data', $data);
    }

    public function edit(Request $request, $id){

        $data = [];

        $data['stock'] = Stock::find($id);

        
        if(empty($data['stock'])){
            $request['object_type'] = 'stock';
            $request['object_id'] = $id;
            throw new ObjectNotFoundException($request);
        }

        $data['product'] = $data['stock']->product;

        return view('crud.stock')->with('data', $data);
    }

    public function show(Request $request, $id){

        $data = [];

        $data['stock'] = Stock::find($id);

        if(empty($data['stock'])){
            $request['object_type'] = 'stock';
            $request['object_id'] = $id;
            throw new ObjectNotFoundException($request);
        }


        $data['stock']->component_quantity = $data['stock']->components()->sum('stock_component.quantity');
        
        $data['stock_tags'] = $data['stock']->tags()->pluck('tag.title', 'tag.id');

        $data['product'] = $data['stock']->product;

        return view('stock')->with('data', $data);
    }

}
