<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Item;
use App\ItemStatus;
use App\Buy;
use App\Cart;
use App\StockType;
use App\Notifications\ItemStatusUpdate;
class ItemController extends Controller
{

    public function __construct()
    {
        view()->share('lang_object', 'item');
    }

    public static function persistFromCart($cart_id, Request $request){
        $cart = Cart::find($cart_id);
        $data = [];
        $data['items'] = [];
        //user or attendant updated item
        foreach($request->input('items') as $index => $item){
            //skip model
            if(empty($item))
            continue;
            $fields = null;
            if(isset($item['item_fields'])){
                $fields = $item['item_fields'];
            }
            
            $data['items'][$index] = self::persist($cart_id, $item['object_id'], $item['object_type'], $item['quantity'], $fields);
        }
        return $data['items'];
    }
    private static function persist($cart_id, $object_id, $object_type, $quantity, $fields = null){
        $data = [];
        $data['item'] = new Item();
        $data['item']->cart_id = $cart_id;
        $data['item']->object_type = $object_type;
        $data['item']->object_id = $object_id;
        $data['item']->save();
        $data['item_status'] = new ItemStatus();
        $data['item_status']->item_id = $data['item']->id;
        $data['item_status']->status_id = config('instance.BUY_ITEM_INITIAL_STATUS');
        $data['item_status']->save();
 
        if(!empty($fields)){
            foreach($fields as $key => $content){
                $data['item']->saveFieldContent($key, $content);
            }      
        }        
        return $data['item'];
    }
    private static function store($request){ 
        $data = [];
        $object_type = $request->input('object_type');
        $object_id = $request->input('object_id');
        $data['object'] = (new $object_type)::find($object_id);
        if($data['object'] == null)
        throw new \Exception('something wrong'); 
        ItemController::persist($request->input('cart_id'), $request->input('object_id'), $request->input('object_type'), $request->input('quantity'), $request->input('fields'));
        return $data['item'];
    }
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */ 
    public function destroy($id)
    {
        $data = [];
        $data['item'] = Item::find($id)->delete();
        return response()->json($data);
    }
}