<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Search;
use App\Product;
use App\StockField;
use App\Field;
use App\Tag;
use App\Stock;
use Illuminate\Support\Facades\Auth;

class SearchController extends Controller
{

    public function __construct()
    {
        view()->share('lang_object', 'search');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $data = []; 
        $s = new Search();
        $s->session_id = $request->session()->getId();
        $s->term = $request->query('term');
        $s->query = $request->query();
        $data['results'] = $s->getClassifiedStocks();
        $s->result_count = $s->stocks->count();
        $s->save();
        
        $data['fields_available'] = self::getFilters($data['results']);
        
        return view('search.products')->with('data', $data);
    }

    private static function getFilters($results){
        $filters_available = [];
        $field_list = Field::whereNull('disabled_at')->get();

        $ignore = explode(',', config('instance.SEARCH_FILTER_IGNORE'));

        $field_list = $field_list->reject(function($field) use ($ignore){
            return in_array($field->slug, $ignore);
        });

        foreach($field_list as $index => $f){
            $stock_fields = StockField::where('field_id', $f->id)->groupBy('content')->get();

            if($stock_fields->count() == 0)
            continue;

            $filters_available[$index] = new \stdClass();
            $filters_available[$index]->slug = $f->slug;
            $filters_available[$index]->type = $f->type;
            $filters_available[$index]->title = $f->title;
            $filters_available[$index]->stock_fields = $stock_fields;
        }

        $tag_list = Tag::whereNull('disabled_at')->pluck('title', 'slug');
        //tags
        $filters_available['tag'] = new \stdClass();
        $filters_available['tag']->slug = 'tag';
        $filters_available['tag']->type = 'tag';
        $filters_available['tag']->title = trans_choice('object.tag',2);

        $filters_available['tag']->options = [];
        foreach($tag_list as $t_slug => $title){  
            $filters_available['tag']->options[$t_slug] = $title;
        }

        if(count($filters_available['tag']->options) == 0)
        unset($filters_available['tag']);

        return $filters_available;
    }

  
}
