<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Customer;
use Illuminate\Support\Facades\Auth;

class RoleController extends Controller
{

    public function __construct()
    {
        view()->share('lang_object', 'role');
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $data = [];

        $data['list_role'] = Role::orderBy('created_at', 'DESC')->take(30)->get();
        
        $data['users'] = User::where('disabled_at', null)->select('id', 'name')->get();

        return view('role')->with('data', $data);
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {



        $data = [];
        
        $data['users'] = User::where('disabled_at', null)->select('id', 'name')->get();

        return view('crud.role')->with('data', $data);
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {

        $role = new Role();
        $role->title = $request->input('user_id');
        $role->details = $request->input('customer_id');
        $role->list_permission = $request->input('list_permission');
        $role->save();
    
        session()->flash('response-success', 'save-success');
        
        return redirect()->action('RoleController@index');
    }
    
  
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        
        $data = [];
        $data['role'] = Role::find($id);
        
        $data['users'] = User::where('disabled_at', null)->select('id', 'name')->get();
        
        return view('crud.role')->with('data', $data);
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $role = Role::find($id);
        /*
        if($role && $role->user_id != Auth::check()->id)
        throw new Exception('Apenas o titular da entrada ou admin pode modificá-la.');
        */
        $role->title = $request->input('user_id');
        $role->details = $request->input('customer_id');
        $role->list_permission = $request->input('list_permission');

        $role->save();
        
        session()->flash('response-success', 'save-success');
        
        return redirect()->action('RoleController@index');
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $role = Role::find($id);
        $role->delete();

        session()->flash('response-success', 'delete-success');
        
        return redirect()->action('RoleController@index');
    }
}
