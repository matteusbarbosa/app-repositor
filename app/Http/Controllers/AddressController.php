<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Address;
use App\Cart;
use App\Status;
use App\AddressStatus;
use App\Item;
use App\Exceptions\AccessForbiddenException;

class AddressController extends Controller
{

    public function __construct()
    {
        view()->share('lang_object', 'address');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
     
        $data['address_list'] = Address::where('user_id', Auth::user()->id)->orderBy('updated_at', 'DESC')->get();

        return view('address')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    
        return view('crud.address');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [];

        $data['address'] = new Address();
        $data['address']->user_id = Auth::user()->id;
        $data['address']->title = $request->input('title');
        $data['address']->street = $request->input('street');
        $data['address']->number = $request->input('number');
        $data['address']->district = $request->input('district');
        $data['address']->city = $request->input('city');
        $data['address']->state = $request->input('state');
        $data['address']->save();


        session()->flash('flash', true);
        session()->flash('flash-class', 'store-success');
        session()->flash('flash-prefix', __('legend.store-success'));

        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];

        $data['address'] = Address::find($id);
       // $data['address'] = User::getLastAddress(Auth::user()->id);

       if(empty($data['address']))
       throw new \Exception();

        return view('table.address')->with('data', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];

        $data['address'] = Address::find($id);
       // $data['address'] = User::getLastAddress(Auth::user()->id);

       if(empty($data['address']))
       throw new \Exception();

       if(!Auth::user()->can('manage') && $data['address']->user_id != Auth::user()->id)
       throw new AccessForbiddenException();

        return view('crud.address')->with('data', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = [];

        $data['address'] = Address::find($id);
        $data['address']->title = $request->input('title');
        $data['address']->street = $request->input('street');
        $data['address']->number = $request->input('number');
        $data['address']->district = $request->input('district');
        $data['address']->city = $request->input('city');
        $data['address']->state = $request->input('state');
        $data['address']->save();


        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.success'));
        session()->flash('flash-text', __('legend.update-success'));

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Address::find($id);
        $role->delete();

        session()->flash('flash', true);
        session()->flash('flash-class', 'success');
        session()->flash('flash-prefix', __('legend.delete-success'));
        
        return redirect()->action('AddressController@index');
    }

       /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showFromUser($user_id)
    {
        $data = [];
     
        $data['address_list'] = Address::where('user_id', $user_id)->orderBy('updated_at', 'DESC')->get();

        return view('address')->with('data', $data);
    }

}
