<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Role;
use App\User;
use App\Exceptions\UserNotFoundException;
use App\Exceptions\InvalidPasswordException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected $redirectToAdmin = '/admin';

    protected $redirectToLogin = '/login';
    protected $redirectToAdminLogin = '/login?admin=1';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      //  $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'phone';
    }


      /**
     * Finaliza
     *
     * @return void
     */
    public function getLogout()
    {

       // Auth::logoutOtherDevices();

        Auth::logout();
        session()->flush();

        return redirect('/');
    }
    

    public function showLoginForm(Request $request){
        $request->session()->reflash();

        return view('auth.login');
    }


    public function login(Request $request){

        $user = User::where('phone', \Helpers\Format::filterNumbers($request->input('phone')))->first(); 

        Auth::logout();

        $credentials = $request->only('phone', 'password');

        if($user != null && $user->can('login:phone') == false && empty($request->input('password'))){
            $request['url_redirect'] = $this->redirectToAdminLogin;
            throw new UserNotFoundException($request);
        }

        if(config('instance.USER_REGISTER') && $user == null){
            $user = new User();
            $user->role_id = Role::where('slug', 'guest')->first()->id;
            $user->phone = \Helpers\Format::filterNumbers($request->input('phone'));
            $user->save(); 

        }
 
        if($user->can('login:phone') == true){
            Auth::login($user);
            return redirect()->intended($this->redirectTo);
        }
        
        if($user->can('login:phone') == false && Hash::check($request->input('password'), $user->password)){
            Auth::login($user);
            return redirect($this->redirectToAdmin);
        } else {
            throw new InvalidPasswordException();
        }
    }

}
