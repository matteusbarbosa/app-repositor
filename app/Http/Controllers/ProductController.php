<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Product;
use \App\Role;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\ObjectNotFoundException;

class ProductController extends Controller
{

    public function __construct()
    {
        view()->share('lang_object', 'product');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];

        $data['product_list'] = Product::select(
        'product.id', 'product.file_id', 'product.title',
        'stk.price_unit', 'stk.price_total', 'stk.quantity'
        )
        ->join('stock as stk', 'stk.product_id', '=', 'product.id')
        ->whereNull('disabled_at')->orderBy('title', 'ASC')->get();

        $data['attendants'] = Role::where('slug', 'attendant')->first()->users;

        return view('table.product')->with('data', $data);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $data = [];

        $data['product'] = Product::find($id);

        if(empty($data['product'])){
            $request['object_type'] = 'product';
            $request['object_id'] = $id;
            throw new ObjectNotFoundException($request);
        }


        if(!empty($data['product']->stocks)){
            $stocks = $data['product']->stocks;

            foreach($stocks as $k => $v){
                $data['stocks'][$v->id] = $v;
                $data['stocks'][$v->id]->component_quantity = $v->components()->sum('stock_component.quantity');
            }
        }

        if(Auth::check() && Auth::user()->can('manage')){
            $data['btn_dock'] = new \stdClass();
            $data['btn_dock']->id = $id;
            $data['btn_dock']->link = url('admin/product');
       }


        return view('product')->with('data', $data);
    }



 
}
