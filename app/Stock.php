<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Stock extends Model
{
    protected $table = 'stock';
    public $dates = ['shop_display_at', 'disabled_at'];
    public function image(){
        return $this->belongsTo('App\File', 'file_id')->withDefault(function ($file) {
            $file->url = asset('img/placeholder.jpg');
        });
    }
 
    public function promotions()
    {
        return $this->morphMany('App\Promotion', 'object');
    }
    public function weightDisplay(){
        return \Helpers\Format::weightDisplay($this->getContent('weight'));
    }

    public function getContent($slug){
        $fields = $this->fields()->where('slug', $slug)->get();

        if($fields->count() == 0)
        return null;

        if($fields->count() == 1 && !empty($fields->first()->pivot))
        return $fields->first()->pivot->content;

        $data = collect([]);
        foreach($fields as $k => $f){
            $data->push($f->pivot->content);
        }
        
        return $data;
    }
    public function fieldsToString($number = null, $slugs = null, $shop_display = false){
        $result_string = '';
        $fields = $this->fields(); 
        if($number != null)
        $fields = $fields->take($number);
        if($slugs != null){
            $fields = $fields->whereIn('slug', $slugs);
        }

        $fields = $fields->orderBy('field.updated_at', 'DESC');

        if($shop_display)
        $fields = $fields->whereNotNull('field.shop_display_at');
        
       $fields = $fields->get();

        foreach($fields as $k => $f){
            if($k > 0)
            $result_string .= ', ';

            $result_string .= $f->title.': '.$f->getContentFormatted();
        }
        
        return $result_string;
    }

    public function fields(){
        return $this->belongsToMany('App\Field', 'stock_field')->withPivot('id', 'content');
    }

    public function type(){
        return $this->belongsTo('App\StockType');
    }
  
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function product(){
        return $this->belongsTo('App\Product');
    }
    public function tags(){
        return $this->belongsToMany('App\Tag', 'tag_stock')->withPivot('id');
    }
    public function components(){
        return $this->belongsToMany('App\Stock', 'stock_component', 'parent_id', 'stock_id');
    }
    public function parents(){
        return $this->belongsToMany('App\Stock', 'stock_component', 'stock_id', 'parent_id');
    }
    public function items(){
        return $this->morphMany('App\Item', 'object');
    }
    public function getCurrentBuyList(){
        $items = $this->items();
        $buy_list = collect([]);
        foreach($items as $i){
            if(!empty($i->cart->buy))
            $buy_list->push($i->cart->buy);
        }
        return $buy_list;
    }
}
