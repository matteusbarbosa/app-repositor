<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'address';

    public function buy(){
        return $this->belongsTo('App\Buy')->withPivot('address_id', 'buy_id');
    }

}
