<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use App\Stock;
use App\StockType;
class Tag extends Model
{
    protected $table = 'tag';
    public function parent(){
        return $this->belongsTo('App\Tag', 'parent_id');
    }
    public function image(){
        return $this->belongsTo('App\File', 'file_id')->withDefault(function ($file) {
            $file->url = asset('img/placeholder.jpg');
        });
    }
    public function tags(){
        return $this->hasMany('App\Tag');
    }
    public function stocks(){
        return $this->belongsToMany('App\Stock', 'tag_stock');
    }
    public static function getClassifiedStocks($wholesale_only = false, $filter_displayed = false){
        if($wholesale_only){
            $unit_type = StockType::where('slug', 'unit')->first();
            $stocks = Stock::whereNull('stock.disabled_at')->where('stock.type_id', '!=', $unit_type->id)->get();
        }
        else
        $stocks = Stock::whereNull('stock.disabled_at')->get();
        return self::withStocks($stocks, true);
    }
    public static function withStocks($stocks, $filter_displayed = false){
        $results = collect([]);
        $tags = collect([]);
        foreach($stocks as $k => $s){
            $tags->concat($s->tags()->whereNull('tag.disabled_at')->whereNotNull('tag.shop_display_at')->get());
            
            foreach($s->tags as $t){

                if(!isset($results[$t->slug])){
                    $results[$t->slug] = new \stdClass();
                    $results[$t->slug]->tag = $t;
                    $results[$t->slug]->items = collect([]);
                }
                $results[$t->slug]->items->push($s);
            }    
        }


        if($filter_displayed)
        return self::filterDisplayedStocks($results);
        else
        return $results;
    }
    public static function filterDisplayedStocks($tags_items){
        return $tags_items->reject(function($tag_items, $slug){

            $tag_items->items = $tag_items->items->reject(function($item){
                //do not display on shop 
                if(empty($item->shop_display_at))
                return true;

                return false;
            });

            return $tag_items->items->count() == 0;          
        });
    }
}
