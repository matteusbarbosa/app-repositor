<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
class SalesRoutePlanAddress extends Pivot
{
    protected $table = 'sales_route_plan_address';
    public $incrementing = true;
    public function address(){
        return $this->belongsTo('App\Address');
    }
    public function progress(){
        return $this->hasMany('App\SalesRouteProgress', 'id', 'sales_route_plan_address_id');
    }
    public function plan(){
        return $this->belongsTo('App\SalesRoutePlan');
    }
}
