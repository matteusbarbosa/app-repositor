<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesGoal extends Model
{
    protected $table = 'sales_goal';
    public $dates = ['start_at', 'end_at', 'disabled_at'];

    public function sales_channels(){
        return $this->belongsToMany('App\SalesChannel', 'user_sales_channel')->withPivot('id');
    }
}
