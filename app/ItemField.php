<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Field;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ItemField extends Pivot
{
    protected $table = 'item_field';
    public $incrementing = true;
    
    public function getField(){
        return Field::where('slug', $this->slug)->first();
    }
}
