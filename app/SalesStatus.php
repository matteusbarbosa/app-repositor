<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesStatus extends Model
{
    protected $table = 'sales_status';
}
