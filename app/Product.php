<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';

    public function image(){
        return $this->belongsTo('App\File', 'file_id')->withDefault(function ($file) {
            $file->url = asset('img/placeholder.jpg');
        });
    }

    
    public function user(){
        return $this->belongsTo('App\User');
    }
    
  
    
    public function component_type(){
        return '\App\Product';
    }
    
    public function units(){
        return $this->hasMany('App\Unit');
    }
    
    public function tags(){
        return $this->hasManyThrough('App\Tag', 'App\ProductTag');
    }
    
    public function stocks(){
        return $this->hasMany('App\Stock');
    }

    
}
