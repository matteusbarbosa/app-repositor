<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesRouteProgress extends Model
{
    protected $table = 'sales_route_progress';

    public function user(){
        return $this->belongsTo('App\SalesRouteUser', 'sales_route_user_id', 'id');
    }

    public function plan_address(){
        return $this->belongsTo('App\Address');
    }

    public function plan(){
        return $this->belongsTo('App\SalesRoutePlan');
    }

    public function status(){
        return $this->belongsTo('App\SalesStatus', 'sales_status_id');
    }
}
