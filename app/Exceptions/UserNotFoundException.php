<?php

namespace App\Exceptions;

use Exception;

class UserNotFoundException extends Exception
{
 /**
     * Report the exception.
     *
     * @return void
     */
    public function report()
    {
        //
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {

        session()->flash('flash', true);
        session()->flash('flash-class', 'danger');
        session()->flash('flash-prefix', $request->input('phone'));
        session()->flash('flash-text', __('legend.login'));

        if(!empty($request->input('url_redirect')))
        return redirect($request->input('url_redirect'));
        else 
        return redirect('/');    
    }
}
