<?php

namespace App\Exceptions;

use Exception;

class InvalidPasswordException extends Exception
{
     /**
     * Report the exception.
     *
     * @return void
     */
    public function report()
    {
        //
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {

        session()->flash('flash', true);
        session()->flash('flash-class', 'danger');
        session()->flash('flash-prefix', __('exception.invalid-password'));
        session()->flash('flash-text', __('legend.login'));

        return redirect('/login');
        
    }
}
