<?php

namespace App\Exceptions;

use Exception;

class UniqueRegisterException extends Exception
{
     /**
     * Report the exception.
     *
     * @return void
     */
    public function report()
    {
        //
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {

        session()->flash('flash', true);
        session()->flash('flash-class', 'danger');
        session()->flash('flash-prefix', __('object.'.$request->input('object_type')).' '.$request->input('object_id'));
        session()->flash('flash-text', __('legend.not-found'));

        if(!empty($request->input('url_redirect')))
        return redirect($request->input('url_redirect'));
        else 
        return redirect('/');    
        
    }
}
