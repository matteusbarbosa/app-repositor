<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use NotificationChannels\WebPush\HasPushSubscriptions;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Cart;
use App\Buy;
use App\Item;

class User extends Authenticatable
{
    use Notifiable, HasPushSubscriptions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'role_id', 'email', 'password', 'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function findBySession($session_id){
        return User::where('session_id', $session_id)->orderBy('updated_at', 'DESC')->first();
    }

    public function cart(){

        $data = [];

        $cart_last = Cart::where('session_id', session()->getId())->first();

        if(!empty($cart_last))
        return $cart_last;

        $data['cart'] = $cart_last != null && $cart_last->buy == null ? $cart_last : new Cart();
        $data['cart']->session_id = session()->getId();
        $data['cart']->user_id = $this->id;
        $data['cart']->save();

        return $data['cart'];
    }

    public function role(){
        return $this->belongsTo('App\Role');
    }


    public function logs(){
        return $this->hasMany('App\UserLog');
    }

    public function addresses(){
        return $this->hasMany('App\Address');
    }

    public function searches(){
        return $this->hasMany('App\Search');
    }

    public function image(){
        return $this->belongsTo('App\File', 'file_id')->withDefault(function ($file) {
            $file->url = asset('img/placeholder.jpg');
        });
    }
    
    public static function getLastBuyBySession($session_id){

        $cart = Cart::where('session_id' , $session_id)->orderBy('id', 'DESC')->first();

        if(empty($cart))
        return null;

       return $cart->buy;
    }

    public static function getLastBuy($user_id){
        $buy = Buy::where('user_id', $user_id)->orderBy('created_at', 'DESC')->first();

        if(empty($buy))
        return null;

        return $buy;
    }

    public static function getLastItemStatus($user_id){
        $item_ids = Item::where('user_id', $user_id)->orderBy('created_at', 'DESC')->pluck('id');
        $status_updated = ItemStatus::whereIn($item_ids)->orderBy('updated_at', 'DESC')->first();
        return $status_updated;
    }

    public function getLastUsedAddress(){
        $buy = Buy::where('user_id', $user_id)->orderBy('created_at', 'DESC')->first();
        return $buy->address;
    }

    public function sales_channels(){
        return $this->belongsToMany('App\SalesChannel', 'user_sales_channel')->withPivot('id');
    }

    public function sales_goals(){
        return $this->belongsToMany('App\SalesGoal', 'user_sales_goal')->withPivot('id');
    }


}
