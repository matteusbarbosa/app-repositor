<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesRoute extends Model
{
    protected $table = 'sales_route';

    public function status(){
        return $this->belongsTo('App\SalesRouteStatus');
    }

    public function plan(){
        return $this->hasOne('App\SalesRoutePlan');
    }


}
