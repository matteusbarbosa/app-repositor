<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLog extends Model
{
    public $timestamps = false;
    protected $dates = ['created_at'];
    protected $table = 'user_log';

    public function user(){
        return $this->belongsTo('App\User');
    }
}
