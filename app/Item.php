<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ItemField;


class Item extends Model
{
    protected $table = 'item';

    protected $fillable = ['cart_id', 'title', 'object_type', 'object_id', 'price_unit', 'price_total', 'discount_percent', 'details'];

    public function statuses(){
        return $this->belongsToMany('App\Status')->using('App\ItemStatus')->withPivot('id', 'updated_at');
    }

    public function getLastStatus(){
        $status_last = $this->statuses()->orderBy('item_status.updated_at', 'DESC')->first();
 
        if(!empty($status_last))
        return $status_last;
 
     }
 
     public function getFirstStatus(){
 
         $status_first = $this->statuses()->orderBy('item_status.created_at', 'ASC')->first();
  
         if(!empty($status_first))
         return $status_first;
  
      }

    public function cart(){
        return $this->belongsTo('App\Cart');
    }

    public function object(){
        return $this->morphTo();
    }

    
    public function saveFieldContent($slug, $content, $sort_position = null){

        $item_field = new ItemField();
        $item_field->item_id = $this->id;
        $item_field->sort_position = $sort_position;
        $item_field->slug = $slug;
        $item_field->content = $content;
        $item_field->save();
        return $item_field;
    }

    //$format 'collection' or 'string'
    public function getContent($slug, $format = 'string'){
        $fields = $this->fields()->where('slug', $slug)->get();

        if($fields->count() == 0)
        return null;

        if($fields->count() == 1 && !empty($field))
        return $fields->first()->content;

        $data = collect([]);
        foreach($fields as $k => $f){
            if(!empty($field))
            $data->push($field->content);
        }

        if($format == 'collection')
        return $data;

        return $data->implode(config('instance.ITEM_MULTIPLE_CONTENT_IMPLODE_GLUE'));
    }

    private function getFormattedContent($fields){
        $fields = $fields->orderBy('field.updated_at', 'DESC')->get();
        foreach($fields as $k => $f){
            if($k > 0)
            $result_string .= ' | ';
            $title = !empty($f->getField()) ? $f->getField() : $f->slug;
            $result_string .= $title.': '.$this->getContent($f->slug);
        }
        return $result_string;
    }

    public function fieldsToString($number = null, $slugs = null){
        $result_string = '';
        $fields = $this->fields();
        if($number != null)
        $fields = $fields->take($number);
        if($slugs != null){
            $fields = $fields->whereIn('slug', $slugs);
        }

        return $this->getFormattedContent($fields);
    
    }
    public function fields(){
        return $this->hasMany('App\ItemField');
    }
    
}
