<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockType extends Model
{
    protected $table = 'stock_type';
    public $dates = ['disabled_at'];

   
    public function parent(){
        return $this->belongsTo('App\StockType', 'parent_id');
    }

    public function stock_types(){
        return $this->hasMany('App\StockType');
    }
}
