<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesChannel extends Model
{
    protected $table = 'sales_channel';

    public function users(){
        return $this->belongsToMany('App\User', 'user_sales_channel');
    }
}
