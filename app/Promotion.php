<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $table = 'promotion';
    protected $dates = ['discount_start', 'discount_end', 'disabled_at', 'end_at', 'featured_until', 'created_at', 'updated_at'];

    protected $fillable = ['user_id', 'file_id', 'title', 'object_type', 'object_id', 'object_type', 'object_id', 'discount_percent', 'discount_start', 'discount_end', 'disabled_at', 'details'];

    public function object(){
        return $this->morphTo();
    }

    public function image(){
        return $this->belongsTo('App\File', 'file_id');
    }

    public function product_target(){

        $data = [];
        
        $object_class = $this->object_type;

        $obj_instance = new $object_class();

        $object = $obj_instance::find($this->object_id);

        return $object->product_target();
    }

    public function sales_channel(){
        return $this->belongsTo('App\SalesChannel');
    }

    
}
