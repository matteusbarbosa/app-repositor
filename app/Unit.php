<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $table = 'product_unit';

    public function items(){
        return $this->morphMany('App\Item', 'object');
    }

    public function component_type(){
        return 'App\Unit';
    }

    public function product(){
        return $this->belongsTo('App\Product');
    }

    public function image(){
        return $this->belongsTo('App\File', 'file_id', 'id');
    }
    
}
