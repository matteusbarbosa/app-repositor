<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'status';

    public function image(){
        return $this->belongsTo('App\File', 'file_id')->withDefault(function ($file) {
            $file->url = asset('img/loading_2.gif');
        });
    }

    public function items(){
        return $this->belongsToMany('App\Item')->using('App\ItemStatus');
    }

}