<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class SalesChannelGoal extends Pivot
{
    protected $table = 'sales_channel_goal';
    public $incrementing = true;
}
