<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PushSubscription extends Model
{
    protected $table = 'push_subscriptions';

    public function user(){
        return $this->belongsTo('App\User');
    }
}
