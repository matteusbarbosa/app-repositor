<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class TagStock extends Pivot
{
    protected $table = 'tag_stock';
    public $incrementing = true;
}
