<?php

namespace App\Report;

use Illuminate\Database\Eloquent\Model;

class ViewSearchEmptyResult extends Model
{
    protected $table = 'view_search_empty_result'; 
}