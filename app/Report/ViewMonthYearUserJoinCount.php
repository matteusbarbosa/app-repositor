<?php

namespace App\Report;

use Illuminate\Database\Eloquent\Model;

class ViewMonthYearUserJoinCount extends Model
{
    protected $table = 'view_month_year_user_join_count';
}