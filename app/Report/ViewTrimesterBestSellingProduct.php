<?php

namespace App\Report;

use Illuminate\Database\Eloquent\Model;

class ViewTrimesterBestSellingProduct extends Model
{
    protected $table = 'view_trimester_best_selling_product';
}