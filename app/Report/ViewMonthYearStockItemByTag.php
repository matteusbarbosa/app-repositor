<?php

namespace App\Report;

use Illuminate\Database\Eloquent\Model;

class ViewMonthYearStockItemByTag extends Model
{
    protected $table = 'view_month_year_stock_item_by_tag';
}