<?php

namespace App\Report;

use Illuminate\Database\Eloquent\Model;

class ViewTrimesterHighestQuantitySellingProduct extends Model
{
    protected $table = 'view_trimester_highest_quantity_selling_product';
}