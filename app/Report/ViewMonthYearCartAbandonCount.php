<?php

namespace App\Report;

use Illuminate\Database\Eloquent\Model;

class ViewMonthYearCartAbandonCount extends Model
{
    protected $table = 'view_month_year_cart_abandon_count';
}