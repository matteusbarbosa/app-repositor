<?php

namespace App\Report;

use Illuminate\Database\Eloquent\Model;

class ViewBestSellingSet extends Model
{
    protected $table = 'view_best_selling_set';
}