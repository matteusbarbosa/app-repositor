<?php

namespace App\Report;

use Illuminate\Database\Eloquent\Model;

class ViewMonthYearStockItemByStatus extends Model
{
    protected $table = 'view_month_year_stock_item_by_status';
}