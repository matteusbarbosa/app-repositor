<?php

namespace App\Report;

use Illuminate\Database\Eloquent\Model;

class ViewStockNeverSold extends Model
{
    protected $table = 'view_stock_never_sold';
}