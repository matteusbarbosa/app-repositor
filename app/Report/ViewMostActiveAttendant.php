<?php

namespace App\Report;

use Illuminate\Database\Eloquent\Model;

class ViewMostActiveAttendant extends Model
{
    protected $table = 'view_most_active_attendant';
}