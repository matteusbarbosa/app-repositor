<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use ObjectNotFoundException;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ItemStatus extends Pivot
{
    protected $table = 'item_status';
    public $incrementing = true;
    public function item(){
        return $this->belongsTo('App\Item');
    }
    public function attendant(){
        return $this->belongsTo('App\User', 'attendant_id');
    }
    public function status(){
        return $this->belongsTo('App\Status');
    }
    //true/false
    private function undoLastStockOperation(){

        //se tiver realizado com operador +, remova a quantidade respectiva do item
        if($this->item->stock_quantity_operator == '+')
        $this->item->object->quantity -= $this->quantity;
        //se tiver realizado com operador -, acrescente a quantidade respectiva do item
        if($this->item->stock_quantity_operator == '-')
        $this->item->object->quantity += $this->quantity;
        $this->item->object->save();
        $this->item->stock_quantity_operator = null;
        $this->item->stock_updated_at = null;
        $this->item->save();
        return true;
    }

    //update stock quantity and return
    public function updateStockQuantity(){

        //return null if you try to restock a stock that has not been removed
        if($this->item->stock_quantity_operator == null
        && $this->status->stock_quantity_operator == '+'){
            return null;
        }

        if($this->item->stock_quantity_operator != null)
        $this->undoLastStockOperation();

        if($this->status->stock_quantity_operator == '+')
            $this->item->object->quantity += $this->item->quantity;

    
        if($this->status->stock_quantity_operator == '-')
            $this->item->object->quantity -= $this->item->quantity;

            $this->item->object->save();
            $this->item->stock_updated_at = date('Y-m-d H:i:s');
            $this->item->stock_quantity_operator = $this->status->stock_quantity_operator;
            $this->item->save();

            return $this->item->object->quantity;
        
    }
}
