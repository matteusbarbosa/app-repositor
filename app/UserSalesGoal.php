<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;


class UserSalesGoal extends Pivot
{
    protected $table = 'user_sales_goal';
    public $incrementing = true;
}
