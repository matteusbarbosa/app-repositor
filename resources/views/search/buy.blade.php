@section('header')
@parent
<link href="{{ url('vendor/EasyAutocomplete/dist/easy-autocomplete.min.css') }}" rel="stylesheet">
<link href="{{ url('vendor/EasyAutocomplete/dist/easy-autocomplete.themes.min.css') }}" rel="stylesheet">
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="{{ url('vendor/EasyAutocomplete/dist/jquery.easy-autocomplete.min.js') }}"></script>
@endsection
<div class="container my-4">
  <div class="row">
    <div class="col-12">
      <div class="form-group">
        <label>@lang('legend.search')</label>
        <div class="input-group">
          <input id="term-input" class="form-control" placeholder="@lang('tip.buy-search')">
        </div>
      </div>
    </div> 
  </div>
</div>
<script>
  var options = {
    url: function(term) {
      return api_url+"/buy-search/" + term + "?format=json";
    },
    getValue: "name",   
    template: {
      type: "links",
      fields: {
        description: "id",
        link: "link"
      }
    },
    list: {
      match: {
        enabled: true
      },
      maxNumberOfElements: 15
    }
  };
  $("#term-input").easyAutocomplete(options);
</script>