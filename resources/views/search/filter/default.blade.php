<li class="card filter my-3" data-type="{{ $f_contents->slug }}" data-format-function="filter_{{ $f_contents->slug }}">
    <h4 class="card-header title @if(!empty(Request::query($f_contents->slug))) active @endif">{{ $f_contents->title }}</h4>
    <ul class="card-body">
      @foreach($f_contents->stock_fields as $sf)
      <li>
        <label class="filter-field {{ $f_contents->slug }}">
          <input class="check" name="{{ $f_contents->slug }}" @if(!empty(Request::query($f_contents->slug)) && (is_array(Request::query($f_contents->slug)) && in_array($sf->content, Request::query($f_contents->slug))  !== false) || (Request::query($f_contents->slug) == $sf->content) ) checked @endif type="checkbox" value="{{ $sf->content }}"> {{ $sf->content }}
        </label>
      </li>
      @endforeach 
    </ul>
  </li>