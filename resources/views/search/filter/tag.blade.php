<li class="card filter my-3" data-type="{{ $f_contents->slug }}" data-format-function="filter_{{ $f_contents->slug }}">
        <h4 class="card-header title @if(!empty(Request::query($f_contents->slug))) active @endif">{{ $f_contents->title }}</h4>
        <ul class="card-body">
        @foreach($f_contents->options as $t_slug => $t_title)
          <li>
            <label class="filter-field {{ $t_slug }}">
              <input class="check" name="tag[]" @if(!empty(Request::query('tag')) && (is_array(Request::query('tag')) && in_array($t_slug, Request::query('tag')) !== false) || (Request::query('tag') == $t_slug) ) checked @endif type="checkbox" value="{{ $t_slug }}"> {{ $t_title }}
            </label>
          </li>
        @endforeach 
        </ul>
      </li>