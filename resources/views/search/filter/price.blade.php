<li class="card filter my-3" data-type="{{ $f_contents->slug }}" data-format-function="filter_{{ $f_contents->slug }}">
    <h4 class="card-header title @if(!empty(Request::query($f_contents->slug))) active @endif">{{ $f_contents->title }}</h4>
    <ul class="card-body">
      @php
        $range_span_list = \Helpers\Visual::priceRangeDisplay($f_contents->stock_fields);
      @endphp
      @foreach($range_span_list as $range => $range_label)
      <li>
        <label class="filter-field {{ $f_contents->slug }}">
          <input class="check" name="{{ $f_contents->slug }}" @if(Request::query($f_contents->slug) == $range) checked @endif type="radio" value="{{ $range }}">
          {{ $range_label }}
        </label>
      </li>
      @endforeach
    </ul>
  </li>