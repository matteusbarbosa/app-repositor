@extends('layouts.public')
@section('title', trans('object.search'))
@section('header')
@parent
<script src="{{ asset('dist/js/cart-min.js') }}" defer></script>
<script src="{{ asset('dist/js/buy-min.js') }}" defer></script>
<script src="{{ asset('dist/js/shop-min.js') }}" defer></script>
<script src="{{ asset('dist/js/search-min.js') }}" defer></script>

<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/search.css') }}">
@endsection
@section('content')
<div class="container mt-4">

  <div class="row">
    <div class="col-12 col-md-7">
      <div class="input-group wrap-search mt-2" style="min-width: 280px;">
        <input id="search-filter" class="form-control " type="search" name="term" placeholder="Busca de produtos" value="{{ Request::query('term') }}">
      </div>
    </div>
  </div>

  @if(\App\Stock::count() == 0)
  <div class="alert bg-light my-2">
    @lang('legend.empty-product-list')
  </div>
  @can('manage')
  <div class="text-center">
    <a href="{{ path_admin('product') }}">{{ __('crud.manage', ['object' => trans_choice('object.product', 2)]) }}</a>
  </div>
  @endcan
  @endif


  <div class="row">
    <div class="col-12">
        <ul id="filters-enabled" class="list-inline d-none">
        </ul>
    </div>
  </div>
    <div class="row">
    <div class="col-md-3 pt-2 @if(count($data['fields_available']) == 0) d-none @endif">
      <button data-toggle="collapse" data-target="#filters" id="btn-filters" class="btn btn-secondary">Filtros</button>
      <div class="collapse" id="filters">
        <ul>
          @foreach($data['fields_available'] as $index => $f_contents)
            @includeFirst(['search.filter.'.$f_contents->type, 'search.filter.default'])
          @endforeach
        </ul>
      </div>
    </div>
    <div class="col-12 @if(count($data['fields_available']) == 0) col-md-12 @else col-md-9 @endif  text-center">
      <div class="row">
        <div class="col-12 my-3">
          <p>{{ __('legend.search-results', [ 'term' => Request::query('term') ]) }}</p>
        </div>
      </div>
      @if(count($data['results']) > 0)
       @include('partials.showroom')
    @endif
    </div>
  </div>


  @include('partials.btn-fixed-share')

</div>

@endsection