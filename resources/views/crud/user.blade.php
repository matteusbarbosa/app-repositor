@extends('layouts.public')
@if(isset($data['user']))
@section('title', $data['user']->name)
@else
@section('title', __('crud.add').' '.trans_choice('object.user', 1))
@endif
@section('header')
@parent
@include('script.form')
<link rel="stylesheet" href="{{ asset('dist/css/user.css') }}" >
<script src="{{ asset('dist/js/user-min.js') }}"></script>
@endsection
@section('content')
<div class="container mb-4 mt-2">
    <div class="t-section bg-red mt-4">
        <h4>   
            @if(!empty($data['user']->name))
                {{ $data['user']->name }}
            @else
                {{ trans_choice('object.user', 1) }}
            @endif
        </h4>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="bs-component">
                @can('manage')
                @if(isset($data['user']))
                <form method="POST" action="{{ url('admin/user', ['id' => $data['user']->id]) }}">
                    {{ method_field('PUT') }}
                    <input type="hidden" name="user_id" value="{{ $data['user']->id }}">
                    @else
                    <form method="POST" action="{{ url('admin/user') }}">
                        @endif
                        @endcan
                        @cannot('manage')
                        @if(isset($data['user']))
                        <form method="POST" action="{{ url('user', ['id' => $data['user']->id]) }}">
                            {{ method_field('PUT') }}
                            <input type="hidden" name="user_id" value="{{ $data['user']->id }}">
                            @else
                            <form method="POST" action="{{ url('user') }}">
                                @endif
                                @endcan
                                @csrf
                                <div id="accordion" class="sections">
                                    <div class="card">
                                        <div class="card-header">
                                            <a class="collapsed card-link" data-toggle="collapse" href="#collapse-1">
                                                @lang('legend.user-data')
                                                <i class="fa fa-plus-square-o float-right fa-2x"></i>
                                            </a>
                                        </div>
                                        <div id="collapse-1" class="collapse" data-parent="#accordion">
                                            <div class="card-body">
                                        
                                                @if(isset($data['role_list']) && Auth::user()->can('manage'))
                                                <div class="form-group">
                                                    <label class="d-block">{{ trans_choice('object.role', 1) }}</label>
                                                    <select class="form-control custom-select" name="role_id">
                                                        @foreach($data['role_list'] as $id => $title)
                                                        <option value="{{ $id }}"  @if(isset($data['user']) && !empty($data['user']->role) && $id == $data['user']->role->id) selected @endif>{{ $title }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                @else
                                                <div class="form-group">
                                                    <label for="cpf">Função</label>
                                                    <input type="text" readonly class="form-control" value="@if(!empty($data['user']->role)){{ $data['user']->role->title }}@endif">
                                                </div>
                                                @endif
                                                @include('form.user')
                                            </div>
                                        </div>
                                    </div>
                                    @if(isset($data['user']) && $data['user']->id == Auth::user()->id)
                                    <div class="card">
                                        <div class="card-header">
                                            <a class="card-link" data-toggle="collapse" href="#collapse-2">
                                                {{ trans_choice('object.notification', 2) }}
                                                <i class="fa fa-plus-square-o float-right fa-2x"></i>
                                            </a>
                                        </div>
                                        <div id="collapse-2" class="collapse" data-parent="#accordion">
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label class="d-block">{{ trans_choice('object.notification', 2) }}</label>
                                                    <button id="toggle-push-enable" class="btn toggle-push btn-sm" type="button"></button>
                                                    <small id="emailHelp" class="form-text text-muted">Lembre-se de manter o aplicativo aberto em segundo plano.</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    @if(isset($data['user']))
                                    <div class="card">
                                        <div class="card-header">
                                            <a class="collapsed card-link" data-toggle="collapse" href="#collapse-3">
                                                {{ trans_choice('object.address', 2) }}
                                                <i class="fa fa-plus-square-o float-right fa-2x"></i>
                                            </a>
                                        </div>
                                        <div id="collapse-3" class="collapse" data-parent="#accordion">
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label class="d-block">{{ trans_choice('object.address', 2) }}</label>
                                                    @if(isset($data['user']) && $data['user']->addresses->count() > 0)
                                                    <ul>
                                                        @foreach($data['user']->addresses as $add)
                                                        <li>{{ $add->title }}</li>
                                                        @endforeach
                                                    </ul>
                                                    @else
                                                    <div class="alert bg-secondary">@lang('tip.insert-rows')</div>
                                                    @endif
                                                    <div class="text-right">
                                                        <a href="{{ url('user/'.$data['user']->id.'/enderecos') }}" class="btn btn-sm btn-secondary">@lang('crud.manage-addresses')</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @can('sell')
                                    <div class="card">
                                        <div class="card-header">
                                            <a class="collapsed card-link" data-toggle="collapse" href="#collapse-4">
                                                {{ trans_choice('object.sales_channel', 3) }}
                                                <i class="fa fa-plus-square-o float-right fa-2x"></i>
                                            </a>
                                        </div>
                                        <div id="collapse-4" class="collapse" data-parent="#accordion">
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label class="d-block">{{ trans_choice('object.sales_channel', 2) }}</label>
                                                    @if(isset($data['sales_channels']) && count($data['sales_channels']) > 0)
                                                    <select class="form-control" name="sales_channel_id[]" multiple>
                                                        @foreach($data['sales_channels'] as $id => $title)
                                                        <option value="{{ $id }}"  @if(isset($data['user']) && count($data['user_sales_channels']) > 0 && in_array($id, $data['user_sales_channels'])) selected @endif>{{ $title }}</option>
                                                        @endforeach
                                                    </select>
                                                    @else
                                                    <div class="alert bg-secondary">@lang('tip.insert-rows')</div>
                                                    @endif
                                                    <div class="text-right">
                                                        <a href="{{ path_admin('sales_channel') }}" class="btn btn-sm btn-secondary">@lang('crud.manage-sales_channel')</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header">
                                            <a class="collapsed card-link" data-toggle="collapse" href="#collapse-5">
                                                {{ trans_choice('object.sales_goal', 2) }}
                                                <i class="fa fa-plus-square-o float-right fa-2x"></i>
                                            </a>
                                        </div>
                                        <div id="collapse-5" class="collapse" data-parent="#accordion">
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label class="d-block">{{ trans_choice('object.sales_goal', 2) }}</label>
                                                    @if(isset($data['sales_goals']) && count($data['sales_goals']) > 0)
                                                    <select class="form-control" name="sales_goal_id[]" multiple>
                                                        @foreach($data['sales_goals'] as $id => $title)
                                                        <option value="{{ $id }}"  @if(isset($data['user_sales_goals']) && count($data['user_sales_goals']) > 0 && in_array($id, $data['user_sales_goals'])) selected @endif>{{ $title }}</option>
                                                        @endforeach
                                                    </select>
                                                    @else
                                                    <div class="alert bg-secondary">@lang('tip.insert-rows')</div>
                                                    @endif
                                                    <div class="text-right">
                                                        <a href="{{ path_admin('sales_goal') }}" class="btn btn-sm btn-secondary">@lang('crud.manage', ['object' => trans_choice('object.sales_goal', 2)])</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endcan
                                    @endif
                                </div> 
                                <div class="form-group row mt-4">
                                    <div class="col-12">
                                        <button type="submit" class="btn btn-secondary float-right"><i class="fa fa-floppy-o"></i> Salvar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                @if(isset($data['user']))
                @php
                $o_slug = 'user';
                $o_endpoint = 'user';
                @endphp
                @include('partials.modal-delete')
                @endif
                <!-- Modal RECEITA -->
                <div class="modal" id="modal-preview">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title">@lang('legend.data-confirm')</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <!-- Modal body -->
                            <div class="modal-body">
                                <ul id="data-preview"></ul>
                            </div>        
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" id="data-confirm">@lang('legend.confirm-use')</button>
                            </div>
                        </div>
                    </div>
                </div>
                @endsection