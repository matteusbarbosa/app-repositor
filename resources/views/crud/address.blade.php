@extends('layouts.public')

@section('title', trans('object.address'))

@section('header')
@parent
@include('script.form')
<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/addresses.css') }}">

@endsection


@section('content')


<div class="container my-4">
    <div class="row">
        <div class="col-12">
            <div class="bs-component">
                @if(isset($data['address']))
                   <div class="t-section bg-red mt-4">
                <h4>Editar endereço</h4>
                   </div>
                <form method="POST" action="{{ url('address', ['id' => $data['address']->id] ) }}">
                    {{ method_field('PATCH') }}
                    @else
                    <div class="t-section bg-red">
                    <h4 >Cadastrar endereço</h4>
                    </div>
                    <form method="POST" action="{{ url('address') }}" >
                        @endif
                        @csrf
                        @include('form.address')
                    </form>
                </div>
                
            </div>
        </div>
        
        
        @endsection