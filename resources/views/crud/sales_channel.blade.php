@extends('layouts.manage')
@if(isset($data['sales_channel']))
@section('title', __('crud.edit-sales_channel').' '.$data['sales_channel']->title)
@else
@section('title', __('crud.add-sales_channel'))
@endif
@section('header')
@parent
<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/sales_channels.css') }}">
@endsection
@section('content')
<div class="container my-4">
    <div class="t-section bg-red mt-4">
        @if(isset($data['sales_channel']))
        <h4>{{ $data['sales_channel']->title }}</h4>
        @else
        <h4>{{ __('crud.add-sales_channel') }}  </h4>
        @endif
    </div>

    <div class="row">
        <div class="col-12">
            <div class="bs-component">
                @include('form.sales_channel')
            </div>
        </div>
    </div>
    @endsection