@extends('layouts.public')
@section('title', 'Acompanhamento do buy '.$data['buy']->id)
@section('header')
@parent
@include('script.form')
<script src="{{ asset('dist/js/cart-min.js') }}"></script>
<script src="{{ asset('dist/js/buy-min.js') }}"></script>
<script src="{{ asset('dist/js/shop-min.js') }}"></script>
<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/buy.css') }}">
@include('script.datatable')
<script>
    $(document).ready( function () {
        var list = $('table#list').DataTable({
            "language": {!! get_datatable_lang($lang_object) !!},
            ordering: false,
            paging: false,
            searching: false,
            autoWidth: false,
            dom: 'Bfrtip',
            buttons: [
            {
                extend: 'print',
                text: 'Imprimir'
            },
            {
                extend: 'excel',
                text: 'Excel .xls'
            },
            'pdf'
            ]
        });
    } );
</script>
@endsection
@section('content')
@include('partials.buy-fixed')
<div class="container mb-4">
    <div class="t-section bg-red mt-4">
        <h4>{{ trans_choice('product.buy', 1) }} {{ $data['buy']->id }}</h4>
    </div>
    <div class="row buy-status-specific alert bg-light p-4" id="buy-status-specific" >
        <div class="col-md-2 text-center">
            <div class="loading d-none">
                <img src="{{ asset('img/loading_2.gif') }}" class="img-fluid m-auto d-block" style="width:30px; height: 30px;">
            </div>
            <div class="status d-none" data-id="{{ $data['buy']->id }}">
                @cannot('manage')
                <div class="display"></div>
                @endcan 
            </div>
        </div>
        <div class="col-md-7">
            @if(Auth::user()->can('manage'))
            <div>
                <div class="form-group">
                    <label>Status</label>
                    <select id="status-id" name="status_id" class="form-control custom-select" data-buy-id="{{ $data['buy']->id }}" data-current-status-id="">
                        @if(!empty($data['statuses']))
                        @foreach($data['statuses'] as $k => $v)
                        <option value="{{ $k }}">{{ $v }}</option>
                        @endforeach
                        @endif
                    </select> 
                </div>  
            </div>
            @endif
            <script>
            </script>
        </div>
        <div class="col-12 col-md-3 text-center">
            <strong>{{ trans('legend.updated_at') }}:</strong> <br> <span class="updated_at"></span>
        </div>
    </div>
    <form action="{{ url('buy', [$data['buy']->id]) }}" method="POST" class="validate" id="buy-data">
        {{ method_field('PATCH') }}
        @csrf
        @if(Auth::check())
        <input type="hidden" name="user_id" value="{{ $data['buy']->user_id }}">
        @else
        <input type="hidden" name="user_id" value="{{ $data['cart']->user_id }}">
        @endif
        <input type="hidden" name="cart_id" value="{{ $data['buy']->cart_id }}">
        <input type="hidden" name="buy_id" value="{{ $data['buy']->id }}">
        <div class="form-group">
            <label class="my-3">{{ trans('legend.items') }}</label>
            <div class="row">
                <div class="col-12">
                    <div id="export_buttons"></div>
                    <span class="text-unavailable d-none">@lang('legend.unavailable')</span>
                    <span class="text-available d-none">@lang('legend.available')</span>
                    <div class="table-responsive mb-4">
                        <table data-id="{{ $data['buy']->id }}" id="list" class="table buy table-striped table-bordered display">
                            <thead>
                                <tr>
                                    <th scope="col">Foto</th>
                                    <th scope="col">Identificação</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Ações</th>
                                    <!-- <th scope="col">Conteúdo</th> -->
                                    <th class="d-none"> </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="d-none item" id="row-model" data-stock-id="" data-item-id="" data-index-id="" data-stamp="">
                                    <td class="img">
                                        <img class="img-fluid img-view" src="">
                                    </td>
                                    <td class="title"></td>
                                    <td class="status"></td>
                                    <td class="actions">
                                        <button type="button" class="item-edit btn btn-secondary btn-sm"><i class="fa fa-edit"></i></button>
                                    </td>
                                    <!-- <td class="details"></td> -->
                                    <td class="d-none"><input type="hidden" name="item[]" value=""></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    @include('form.buy-customer')
                    @include('form.buy-address')
                    <!-- modal img view -->
                </div>
            </div>
        </div>
    </form>
    @include('partials.modal-gallery')
    @include('partials.modal-item')
</div>
@endsection