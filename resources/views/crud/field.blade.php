@extends('layouts.manage')

@if(isset($data['field']))
@section('title', trans('legend.edit').' '.trans_choice('product.field', 1).' "'.$data['field']->title.'"')
@else
@section('title', trans('legend.create').' '.trans_choice('product.field', 1))
@endif

@section('header')
@parent
<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/fields.css') }}">
@endsection

@section('content')


<div class="container my-4">
    <div class="t-section bg-red mt-4">
    @if(isset($data['field']))
    <h4>{{ trans('legend.edit').' '.trans_choice('product.field', 1).' "'.$data['field']->title.'"' }}</h4>
    
        @else
        <h4>{{ trans_choice('product.field', 1) }} </h4>
        @endif

    </div>
   
    <div class="row">
        <div class="col-12">
        <div class="bs-component">
          @include('form.field')
        </div>
 
    </div>
</div>





@endsection