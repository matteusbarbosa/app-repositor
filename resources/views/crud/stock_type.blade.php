@extends('layouts.public')

@if(isset($data['stock_type']))
@section('title', trans('legend.edit').' '.trans_choice('product.stock-type', 1).' "'.$data['stock_type']->title.'"')
@else
@section('title', trans('legend.create').' '.trans_choice('product.stock-type', 1))
@endif

@section('header')
@parent
<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/stock-type.css') }}">
@endsection

@section('content')


<div class="container my-4">
    <div class="t-section bg-red mt-4">
        <h4>
            @if(isset($data['stock_type']))
            {{ trans('legend.edit').' '.trans_choice('product.stock-type', 1).' "'.$data['stock_type']->title.'"' }} 
            @else
            {{ trans_choice('product.stock-type', 1) }}
            @endif
        </h4>
    </div>
    
    <div class="row">
        <div class="col-12">
            <div class="bs-component">
                @include('form.stock_type')
            </div>
            
        </div>
    </div>
    
    @php
    $o_slug = 'stock_type';
    $o_endpoint = 'stock_type';
    @endphp
    @include('partials.modal-delete')
    
    @endsection
    
    
    