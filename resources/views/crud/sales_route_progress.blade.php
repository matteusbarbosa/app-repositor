@extends('layouts.manage')
@if(isset($data['sales_route_progress']))
@section('title', __('crud.edit', ['object' => trans_choice('object.sales_route_progress',1).' – '.$data['sales_route_progress']->sales_route->title]))
@else
@section('title', __('crud.add', ['object' => trans_choice('object.sales_route_progress',1)]))
@endif
@section('header')
@parent
@include('script.form')
<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/sales_route.css') }}">
<script src="{{ asset('dist/js/route-plan-address-min.js') }}"></script>
@endsection
@section('content')
<div class="container my-4">
    <div class="row">
            @if(isset($data['sales_route'])) 
            <div class="col-12 col-md-3">
                <a href="{{ path_admin('sales_route/create') }}" class="btn btn-secondary mb-3">{{ __('crud.add', ['object' => trans_choice('object.sales_route',1)]) }}</a>
                 <h5><i class="fa fa-cogs text-muted"></i> {{ __('legend.object-options', ['object' => trans_choice('object.sales_route', 1)]) }}</h5>
                 <hr>
                 <ul class="list-group">
                    @if(!empty(Request::query('sales_route_id')))
                    <li class="list-group-item"><a href="{{ path_admin('sales_route/'.Request::query('sales_route_id')) }}"><i class="fa fa-map-marker text-muted"></i> {{ trans_choice('object.sales_route',1) }}</a></li>
                    @endif
                    @if(isset($data['sales_route_progress']) && !empty($data['sales_route_progress']->sales_route_id))
                        <li class="list-group-item"><a href="{{ path_admin('sales_route/'.$data['sales_route_progress']->sales_route_id) }}"><i class="fa fa-map-marker text-muted"></i> {{ trans_choice('object.sales_route',1) }}</a></li>
                        <li class="list-group-item"><a href="{{ path_admin('sales_route_progress?sales_route_progress='.$data['sales_route_progress']->id) }}"><i class="fa fa-toggle-on text-muted"></i> {{ __('crud.list', ['object' => trans_choice('object.sales_route_progress',2)]) }}</a></li>
                        @endif
                 </ul>
            </div>
            @endif
        <div class="col-12 mt-4 mt-md-0 @if(isset($data['sales_route'])) col-md-9 @else col-md-12 @endif">
            @include('form.sales_route_progress')
        </div>
    </div>
@endsection