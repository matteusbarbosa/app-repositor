@extends('layouts.manage')
@if(isset($data['sales_goal']))
@section('title', __('crud.edit-sales_goal').' '.$data['sales_goal']->title)
@else
@section('title', __('crud.add-sales_goal'))
@endif
@section('header')
@parent
@include('script.form')
<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/sales_goal.css') }}">
@endsection
@section('content')
<div class="container my-4">
    @if(isset($data['sales_goal']))
    <div class="row mb-3">
        <div class="col-md-12">
            <div class="btn-group float-right">
                <a href="{{ path_admin('sales_goal/create-channel/'.$data['sales_goal']->id) }}" class="btn btn-secondary">@lang('crud.add-sales_goal')</a>
            </div>
        </div>
    </div>
@endif

    <div class="row">
        <div class="col-12">
            <div class="bs-component">



                @include('form.sales_goal')
            </div>
        </div>
    </div>
    @endsection