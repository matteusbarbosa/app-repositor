@extends('layouts.manage')
@if(isset($data['sales_route']))
@section('title', __('crud.edit', ['object' => trans_choice('object.sales_route',1).' – '.$data['sales_route']->title]))
@else
@section('title', __('crud.add-sales_route'))
@endif
@section('header')
@parent
@include('script.form')
<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/sales_route.css') }}">
@endsection
@section('content')
<div class="container my-4">
    <div class="row">
            @if(isset($data['sales_route'])) 
            <div class="col-12 col-md-3">


                 <h5><i class="fa fa-cogs text-muted"></i> {{ __('legend.object-options', ['object' => trans_choice('object.sales_route', 1)]) }}</h5>
                 <hr>
                 <ul class="list-group">
                    
                    <li class="list-group-item"><a href="@if($data['sales_route']->plan == null){{ path_admin('sales_route_plan/create?sales_route_id='.$data['sales_route']->id) }} @else {{ path_admin('sales_route_plan/'.$data['sales_route']->plan->id).'/edit' }} @endif"><i class="fa fa-arrows-alt text-muted"></i> {{ trans_choice('object.sales_route_plan',1) }}</a></li>
                    
                </ul>
                 <h5 class="mt-3"><i class="fa fa-cogs text-muted"></i> {{ __('legend.extra-options') }}</h5>

                 <a href="{{ path_admin('sales_route/create') }}" class="btn btn-secondary mb-3">{{ __('crud.add', ['object' => trans_choice('object.sales_route',1)]) }}</a>

            </div>
            @endif
        <div class="col-12 mt-4 mt-md-0 @if(isset($data['sales_route'])) col-md-9 @else col-md-12 @endif">
            @include('form.sales_route')
        </div>
   
    </div>
</div>
    @endsection