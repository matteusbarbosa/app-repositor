@extends('layouts.manage')

@if(isset($data['tag']))
@section('title', trans('legend.edit').' '.trans('object.tag').' "'.$data['tag']->title.'"')
@else
@section('title', trans('legend.create').' '.trans_choice('object.tag',2))
@endif

@section('header')
@parent
<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/tags.css') }}">
@endsection

@section('content')


<div class="container my-4">
    <div class="t-section bg-red mt-4">
        @if(isset($data['tag']))
        <h4>{{ trans('legend.edit').' '.trans_choice('object.tag',1).' "'.$data['tag']->title.'"' }}    </h4>
        @else
        <h4>{{ trans_choice('object.tag',2) }}</h4>
        @endif
    </div>
    
    <div class="row">
        <div class="col-12">
            <div class="bs-component">
                @include('form.tag')
            </div>
            
        </div>
    </div>
    
    
    
    
    
    @endsection