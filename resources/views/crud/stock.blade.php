@extends('layouts.manage')
@section('title', trans('object.stock'))
@section('header')
@parent
@include('script.form')
<link rel="stylesheet" href="{{ asset('dist/css/stock.css') }}" >
<script src="{{ asset('dist/js/stock-min.js') }}"></script>
<script src="{{ asset('dist/js/stock-component-min.js') }}"></script>
<script src="{{ asset('dist/js/stock-field-min.js') }}"></script>
<script src="{{ asset('vendor/js/iro.min.js') }}"></script>
@endsection
@section('content')
<div class="container my-4">
    <div class="row my-3">
        <div class="col text-right">
            <a href="{{ path_admin('stock/create') }}" class="btn btn-secondary">{{ __('crud.add', ['object' => __('object.stock')]) }} </a>
        </div>
    </div>
    @if(isset($data['stock']))
    <div class="t-section bg-red mt-4">
        <h4>{{ $data['stock']->product['title'] }} </h4>
    </div>
    @else
    <div class="t-section bg-red mt-4">
        <h4>{{ trans('legend.create').' '.trans('object.stock') }} </h4>
    </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="bs-component">
                @include('form.stock')
            </div>
        </div>
    </div>
    @endsection