@extends('layouts.public')

@section('title', trans('object.product'))


@section('header')
@parent
@include('script.form')
<script src="{{ asset('dist/js/product-min.js') }}"></script>
<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/product.css') }}">
@endsection

@section('content')


<div class="container mb-4">
    <div class="row">
        <div class="col-12">
        <div class="bs-component">
          @include('form.product')
        </div>
 
    </div>
</div>

@endsection