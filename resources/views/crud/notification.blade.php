@extends('layouts.public')

@if(isset($data['notification']))
@section('title', trans('legend.edit').' '.trans('object.notification').' "'.$data['notification']->title.'"')
@else
@section('title', trans('legend.create').' '.trans('object.notification'))
@endif

@section('header')
@parent
<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/notifications.css') }}">
@endsection

@section('content')


<div class="container my-4">
    @if(isset($data['notification']))
    <h1>{{ trans('legend.edit').' '.trans('object.notification').' "'.$data['notification']->title.'"' }}   <span class="badge badge-secondary">{{ trans('legend.editing').' '.trans('object.notification') }} </span>  </h1>
        @else
        <h1>{{ trans('object.notification') }} <span class="badge badge-secondary">{{ trans('legend.create').' '.trans('object.notification') }} </span> </h1>
        @endif
   
    <div class="row">
        <div class="col-12">
        <div class="bs-component">
          @include('form.notification')
        </div>
 
    </div>
</div>





@endsection