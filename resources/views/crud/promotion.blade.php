@extends('layouts.public')

@if(isset($data['promotion']))
@section('title', trans('legend.edit').' '.trans('object.promotion').' – '.$data['promotion']->title)
@else
@section('title', trans('legend.create').' '.trans('object.promotion'))
@endif

@section('header')
@parent
@include('script.form')
<script src="{{ asset('dist/js/promotion-min.js') }}"></script>
<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/promotions.css') }}">
@endsection

@section('content')


<div class="container mb-4 mt-4">

    <div class="t-section bg-red mt-4">
        @if(isset($data['promotion']))
        <h4>{{ trans('legend.edit').' '.trans_choice('object.promotion', 1).' "'.$data['promotion']->title.'"' }}    </h4>
        @else
        <h4>{{ trans_choice('object.sales_channel', 1) }}</h4>
        @endif
    </div>

    <div class="row">
        <div class="col-12">
        <div class="bs-component">
          @include('form.promotion')
        </div>
 
    </div>
</div>




@endsection