@extends('layouts.public')

@section('title', trans('object.cart'))

@section('header')
    @parent
    @include('script.form')
    <script src="{{ asset('dist/js/cart-min.js') }}"></script>
    <script src="{{ asset('dist/js/buy-min.js') }}"></script>
    <script src="{{ asset('dist/js/shop-min.js') }}" defer></script>

    <link rel='stylesheet' type='text/css' href="{{ asset('dist/css/product.css') }}">
    <link rel='stylesheet' type='text/css' href="{{ asset('dist/css/cart.css') }}">

@endsection

@section('content')

  <fieldset>

<div class="container mb-4">
    <div class="row ">
        <div class="col-12">

            <div class="t-section bg-red mt-4">
                <h4>@lang('object.cart')</h4>
              </div>
     


<form id="cart-data">
  @if(isset($data['cart']))
  @if(Auth::user()->can('manage') || Auth::user()->can('sell'))
  <input type="hidden" name="manager_id" value="{{ Auth::user()->id }}">
  @endif
  <input type="hidden" name="cart_id" value="{{ $data['cart']->id }}">
  @endif
  <input type="hidden" name="session_id" value="{{ session()->getId() }}">
  <div class="my-4" style="height:1px;"></div>
  <div class="table-responsive">
    <table class="table table-striped cart" id="list">
      <thead>
        <tr>
          <th scope="col">Img </th>
          <th scope="col">Tipo</th>
          <th scope="col">Identificação</th>
          <th scope="col" class="text-center">Qtd</th>
          <th scope="col"> </th>
        </tr>
      </thead>
      <tbody>
        <tr class="item d-none" id="row-model" data-quantity="" data-stock-id="" data-index-id="" data-item-id="" data-status-id="">
          <td class="img">
            <img class="img-fluid img-view" src="">
          </td>
          <td class="type"></td>
          <td class="title"></td>
          <td class="quantity">
          </td>
          <td>
            <div class="alert-low-stock d-none"></div>
            <input type="hidden" class="object_type">
            <input type="hidden" class="object_id">
          </td>
        </tr>
      </tbody>
    </table>
  </div>
  @include('form.buy-customer')
  @include('partials.cart-fixed')
</form>
@include('partials.modal-gallery')
@include('partials.modal-item')


</div>
</div>
</div>
</fieldset>



@endsection