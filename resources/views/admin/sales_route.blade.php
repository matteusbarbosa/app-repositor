@extends('layouts.manage')
@section('title', __('object.sales_route'))
@section('header')
@parent
<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/admin.css') }}">
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-12  a-bottom">
            <ul class="list-group mt-4" id="menu-admin">
                <li class="list-group-item"> <a href="{{ url('admin/sales_route') }}" class="dropdown-item"><i class="fa fa-map-marker fa-2x"></i>  {{ trans_choice('object.sales_route', 2) }}</a></li>
                <li class="list-group-item"> <a href="{{ url('admin/sales_route_report') }}" class="dropdown-item"><i class="fa fa-table fa-2x"></i> Relatórios</a></li>
             </ul>
        </div>
    </div>
</div>
@endsection
