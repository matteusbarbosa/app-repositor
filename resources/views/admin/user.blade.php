@extends('layouts.manage')
@section('title', __('object.admin'))
@section('header')
@parent
<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/admin.css') }}">
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-12  a-bottom">
            <ul class="list-group mt-4" id="menu-admin">
                <li class="list-group-item"> <a href="{{ url('admin/sales_channel') }}" class="dropdown-item"><i class="fa fa-tags fa-2x"></i>  {{ trans_choice('object.sales_channel', 2) }}</a></li>
                <li class="list-group-item"> <a href="{{ url('admin/user') }}" class="dropdown-item"><i class="fa fa-users fa-2x"></i> {{ trans_choice('object.user', 2) }}</a></li>
             </ul>
        </div>
    </div>
</div>
@endsection
