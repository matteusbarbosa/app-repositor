@extends('layouts.manage')
@section('title', __('object.admin'))
@section('header')
@parent
<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/admin.css') }}">
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-12 col-md-4 mt-4">
            <h5>Resumo</h5>
            <div class="card">
                <div class="card-body">
                    <div class="text-muted">Hoje</div>
                    @if($data['buy_count_today']['total'] > 0)
                        <h4>{{ $data['buy_count_today']['total'] }} <small>{{ trans_choice('object.buy', $data['buy_count_today']['total']) }}</small></h4>
                        <hr>
                        @else –
                        @endif
                        <div class="text-muted">Última semana</div>
                        @if($data['buy_count_last_week']['total'] > 0)
                    <h4>{{ $data['buy_count_last_week']['total'] }} <small>{{ trans_choice('object.buy', $data['buy_count_last_week']['total']) }}</small></h4>
                    <hr>
                    @else –
                    @endif
                    <div class="text-muted">Mês passado</div>
                    @if($data['buy_count_last_month']['total'] > 0)
                    <h4>{{ $data['buy_count_last_month']['total'] }} <small>{{ trans_choice('object.buy', $data['buy_count_last_month']['total']) }}</small></h4>
                    @else –
                    @endif
                </div>
            </div>
        </div>
            <div class="col-12 col-md-8">
            <ul class="list-group mt-4" id="menu-admin">
                <li class="list-group-item"> <a href="{{ url('/') }}" class="dropdown-item"><i class="fa fa-cart-arrow-down fa-2x"></i> {{ trans_choice('object.shop', 1) }}</a></li>
                <li class="list-group-item"> <a href="{{ url('admin?products=1') }}" class="dropdown-item"><i class="fa fa-cube fa-2x"></i> Produtos</a></li>
                 <li class="list-group-item"> <a href="{{ url('admin?buys=1') }}" class="dropdown-item"><i class="fa fa-list fa-2x"></i> Pedidos</a></li>
                <li class="list-group-item"> <a href="{{ url('admin?user=1') }}" class="dropdown-item"><i class="fa fa-address-card-o fa-2x"></i> Usuários</a></li>
                <li class="list-group-item"> <a href="{{ url('admin?sales_routes=1') }}" class="dropdown-item"><i class="fa fa-map-marker fa-2x"></i> Rotas de venda</a></li>
                <li class="list-group-item"> <a href="{{ url('admin/report') }}" class="dropdown-item"><i class="fa fa-table fa-2x"></i> Relatórios</a></li>
            </ul>
        </div>
  
    </div>
</div>
@endsection
