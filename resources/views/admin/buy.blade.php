@extends('layouts.manage')
@section('title', __('object.admin'))
@section('header')
@parent
<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/admin.css') }}">
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-12  a-bottom">
            <ul class="list-group mt-4" id="menu-admin">
                <li class="list-group-item"> <a href="{{ url('admin/buy') }}" class="dropdown-item"><i class="fa fa-list fa-2x"></i> Pedidos</a></li>
                <li class="list-group-item"> <a href="{{ url('admin/queue') }}" class="dropdown-item"><i class="fa fa-list fa-2x"></i> Fila por solicitante</a></li>
                <li class="list-group-item"> <a href="{{ url('admin/dispatch') }}" class="dropdown-item"><i class="fa fa-list fa-2x"></i> Fila por despacho de itens</a></li>
            </ul>
        </div>
    </div>
</div>
@endsection
