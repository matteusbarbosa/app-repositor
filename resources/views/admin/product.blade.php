@extends('layouts.manage')
@section('title', __('object.admin'))
@section('header')
@parent
<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/admin.css') }}">
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-12  a-bottom">
            <ul class="list-group mt-4" id="menu-admin">
                <li class="list-group-item"> <a href="{{ url('admin/tag') }}" class="dropdown-item"><i class="fa fa-tags fa-2x"></i> Tags – Categorias</a></li>
                <li class="list-group-item"> <a href="{{ url('admin/stock_type') }}" class="dropdown-item"><i class="fa fa-barcode fa-2x"></i>  {{ trans_choice('product.stock-type', 2) }}</a></li>
                <li class="list-group-item"> <a href="{{ url('admin/product') }}" class="dropdown-item"><i class="fa fa-cube fa-2x"></i>  Produtos e variações</a></li>
                <li class="list-group-item"> <a href="{{ url('admin/field') }}" class="dropdown-item"><i class="fa fa-file-text-o fa-2x"></i> {{ trans_choice('product.stock-field', 2) }}</a></li>
                <li class="list-group-item"><a href="{{ url('admin/promotion') }}" class="dropdown-item"><i class="fa fa-wifi fa-2x"></i> @lang('legend.promotion')</a></li>
             </ul>
        </div>
    </div>
</div>
@endsection
