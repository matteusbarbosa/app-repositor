
<form action="{{ url('/admin/stock_type') }}@if(isset($data['stock_type']))/{{ $data['stock_type']->id }}@endif" method="POST" class="validate" enctype="multipart/form-data">
    @csrf
     <fieldset>
       
       @if(isset($data['stock_type']))
       {{ method_field('PATCH') }}
       @endif
   
         <div class="form-group row">
           <div class="col-md-3 offset-md-9">
           <label for="active" class="form-control">Ativa *
           @if(!isset($data['stock_type']) || (isset($data['stock_type']) && $data['stock_type']->disabled_at == null))
           <input type="checkbox" name="active" id="active" value="1" checked>
           @else
           <input type="checkbox" name="active" id="active" value="1">
           @endif
           </label>
         </div>
         </div>
 
         <div class="form-group">
           
           <label for="parent_id">@lang('product.stock-type-parent')</label>
             <select name="parent_id" id="parent_id" class="form-control custom-select">
               <option value="">– Selecione –</option>
               @foreach($data['stock_type_list'] as $k => $v)
                 <option @if($v->parent_id != null) class="pl-4" @endif value="{{ $v->id }}" @if(isset($data['stock_type']) && !empty($data['stock_type']->parent) && $v->id == $data['stock_type']->parent->id) selected @endif>{{ $v->title }}</option>
               @endforeach
             </select>
           </div>
           
         <div class="form-group">
             <label for="title">{{ __('legend.title') }}</label>
             <input type="text" class="form-control" name="title" value="@if(isset($data['stock_type'])){{ $data['stock_type']->title }}@endif">
           </div>
 
       <div class="form-group">
         <label for="details">Detalhes</label>
         <textarea id="details" class="form-control" name="details">@if(isset($data['stock_type'])){{ $data['stock_type']->details }}@endif</textarea>
       </div>
       
       @if(isset($data['stock_type']))
       <div class="form-group">
         <label for="amount">Data de cadastro</label>
         <input type="text" class="form-control" disabled name="created_at" value="{{ $data['stock_type']->created_at->format('d/m/Y H:i') }}">
       </div>
       <div class="form-group">
         <label for="amount">Última atualização</label>
         <input type="text" class="form-control" disabled name="created_at" value="{{ $data['stock_type']->updated_at->format('d/m/Y H:i')  }}">
       </div>
       
       @endif
       
       @if(isset($data['stock_type']))
       
       <div class="btn-group float-left">
         <button type="button" class="btn btn-danger btn-delete float-right" data-id="{{ $data['stock_type']->id }}">
           Excluir
         </button>
       </div>
       @endif
       
       <div class="btn-group float-right">
         
         <button class="btn btn-success" type="submit">Salvar</button>
       </div>
     </fieldset>
   </form>
