
  <div class="form-group">
      <label for="d-block">@lang('legend.user-type')</label>

    <label class="d-inline-block btn btn-secondary @if(isset($data['user']) && !empty($data['user']->cpf)) d-inline-block @endif" >
        <input type="radio" name="type" value="PF" @if(isset($data['user']) && !empty($data['user']->cpf)) checked @endif>Pessoa física
    </label>
    <label class="d-inline-block btn btn-secondary @if(isset($data['user']) && !empty($data['user']->cnpj)) d-inline-block @endif">   
          <input type="radio" name="type" value="PJ" @if(isset($data['user']) && !empty($data['user']->cnpj)) checked @endif>Pessoa jurídica
      </label>

  </div>
  <div class="form-group w-pj @if(isset($data['user']) && !empty($data['user']->cnpj)) d-block @else d-none @endif">
    <label for="cnpj">CNPJ</label>
    <input type="tel" name="cnpj" class="form-control mask-cnpj" id="i-cnpj" @if(isset($data['user']) && !empty($data['user']->cnpj)) value="{{ $data['user']->cnpj }}" @endif>
    <div>
        <label for="cnpj">Digite o Captcha</label>
     
      <div class="row">
          <div class="col-md-3">
              <div id="receita-pj-captcha" class="my-2">
           
                </div>
          </div>
          <div class="col-md-9">
              <input type="text" name="captcha" class="form-control" id="i-captcha" placeholder="CAPTCHA">
          </div>
    </div>
    <input type="hidden" id="i-cookie" name="cookie">
    <button type="button" class="btn btn-sm btn-secondary my-2" id="query-pj">Consultar</button>
  </div>
</div>
    <div class="form-group w-pf @if(isset($data['user']) && !empty($data['user']->cpf)) d-block @else d-none @endif">
    <label for="cpf">CPF</label>
    <input type="tel" name="cpf" class="form-control mask-cpf">
  </div>
  <div class="form-group">
    <label for="phone_commercial">Telefone/login</label>
    <input type="tel" name="phone" class="form-control f-telefone" @if(isset($data['user']))value="{{ $data['user']->phone }}" readonly @endif>
    <small id="emailHelp" class="form-text text-muted">Identificação primária.</small>
  </div>
  <div class="form-group">
    <label for="phone_whatsapp"><i class="fa fa-whatsapp"></i>WhatsApp</label>
    <input type="tel" name="phone_whatsapp" class="form-control" value="@if(isset($data['user'])){{ $data['user']->phone_whatsapp }}@endif">
    <small id="phone_whatsapp_help" class="form-text text-muted">Contato alternativo</small>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">{{ trans('legend.name') }}</label>
    <input type="text" name="name" class="form-control f-nome_fantasia" value="@if(isset($data['user'])){{ $data['user']->name }}@endif" >
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Email</label>
    <input name="email" type="email" class="form-control f-email" value="@if(isset($data['user'])){{ $data['user']->email }}@endif" >
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Senha</label>
    <input name="password_new" type="password" class="form-control">
    <small id="pwHelp" class="form-text text-muted">Sua senha é armazenada com criptografia.</small>
  </div> 

  
<div class="form-group">
  <label class="my-3">{{ __('legend.bank-data') }}</label>
  <textarea id="f-bank_data" class="form-control f-buy" name="bank_data">@if(isset($data['user']) && !empty($data['user']->bank_data)){{ Crypt::decrypt($data['user']->bank_data) }}@endif</textarea>
  <small class="form-text text-muted">@lang('tip.bank-data')</small>

  </div>

  
<div class="form-group">
  <label class="my-3">{{ trans('legend.details') }}</label>
  <textarea name="details" id="f-details" class="form-control f-buy" name="details" @if(empty($data['cart']->details)) placeholder="{{ trans('legend.details-empty') }}" @endif>@if(isset($data['buy'])){{ $data['buy']->details }}@endif</textarea>
  
  </div>
  @if(isset($data['user']))
  <div class="form-group row">
    <label for="staticEmail" class="col-sm-2 col-form-label">Data do cadastro</label>
    <div class="col-sm-10">
      <input type="text" readonly="" class="form-control-plaintext"  value="{{ $data['user']->created_at->format('d/m/Y H:i:s') }}">
    </div>
  </div>
  <div class="form-group row">
    <label for="staticEmail" class="col-sm-2 col-form-label">Data do último acesso</label>
    <div class="col-sm-10">
      <input type="text" readonly="" class="form-control-plaintext" value="{{ !empty($data['login_last']) ? \Helpers\Format::dateDmy($data['login_last']->created_at) : trans('legend.unknown') }}">
    </div>
  </div>
  @endif
