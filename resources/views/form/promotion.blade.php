<form action="{{ url('/admin/promotion') }}{{ isset($data['promotion']) ? '/'.$data['promotion']->id : null }}" method="POST" class="validate" enctype="multipart/form-data">
    @csrf
    <fieldset>
        @if(isset($data['promotion']))
        <input type="hidden" name="user_id" value="{{ $data['promotion']->user_id  }}">
        <input type="hidden" name="promotion_id" value="{{ $data['promotion']->id }}">
        {{ method_field('PATCH') }}
        @endif
        <div class="form-group row">
            <div class="col-md-3 offset-md-9">
                <label for="active" class="form-control">{{ __('legend.active') }}
                    @if(!isset($data['promotion']) || (isset($data['promotion']) && $data['promotion']->disabled_at == null))
                    <input type="checkbox" name="active" id="active" value="1" checked>
                    @else
                    <input type="checkbox" name="active" id="active" value="1">
                    @endif
                </label>
            </div>
        </div>
        @if(isset($data['sales_channels']))
        <div class="form-group">
            <label>{{ trans_choice('object.sales_channel', 1) }}</label>
            <select class="form-control custom-select" name="sales_channel_id">
                <option value="">– Selecione –</option>
                @if(isset($data['promotion']) && $data['promotion']->sales_channel_id != null)
                <option value="{{ $data['promotion']->sales_channel_id }}" selected>{{ $data['promotion']->sales_channel->title }}</option>
                @endif
                @foreach($data['sales_channels'] as $id => $title)
                <option value="{{ $id }}">{{ $title }}</option>
                @endforeach
            </select>
        </div>
        @endif
        <div class="form-group">
            <label for="title">Título *</label>
            <input name="title" type="text" id="title" class="form-control validate[required]" value="@if(isset($data['promotion'])){{ $data['promotion']->title }}@endif">
        </div>
        <div class="form-group">
            <label for="details">Detalhes</label>
            <textarea id="details" class="form-control" name="details">@if(isset($data['promotion'])){{ $data['promotion']->details }}@endif</textarea>
        </div>
        <div class="form-group">
            <label for="details">Produto</label>
            <select name="stock_id" class="form-control custom-select object-type">
                <option value="">{{ __('legend.select-option') }}</option>
                @if(isset($data['promotion']) && $data['promotion']->object_type == 'App\Stock')
                <option value="{{ $data['promotion']->object_id }}" selected>[@lang('legend.current')] – {{ $data['promotion']->object->type->title  }} – {{ $data['promotion']->object->product->title }} @if( $data['promotion']->object->components->count() > 0)– contém {{ $data['promotion']->object->components->count() }}@endif – {{ $data['promotion']->object->fieldsToString(3) }}</option>
                @endif
                @foreach($data['stocks'] as $k => $s)                
                <option value="{{ $s->id }}">{{ $s->type->title  }} – {{ $s->product->title }}@if($s->components->count() > 0) – contém {{ $s->components->count() }}@endif – {{ $s->fieldsToString(3) }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="discount_percent">{{ __('legend.discount_percent') }}</label>
            <input type="text" name="discount_percent" id="discount_percent" class="form-control validate[required,custom[number]]" value="@if(isset($data['promotion'])){{ $data['promotion']->discount_percent }}@endif">
        </div>
        <div class="form-group">
            <label for="discount_start">{{ __('legend.discount_start') }}</label>
            <input type="text" class="form-control date"  name="discount_start" value="@if(isset($data['promotion'])) {{ $data['promotion']->discount_start->format('d/m/Y H:i') }} @endif">
        </div>
        <div class="form-group">
            <label for="discount_end">{{ __('legend.discount_end') }}</label>
            <input type="text" class="form-control date"  name="discount_end" value="@if(isset($data['promotion'])) {{ $data['promotion']->discount_end->format('d/m/Y H:i') }} @endif">
        </div>
        @if(isset($data['promotion']))
        <div class="form-group">
            <label for="amount">Data de cadastro</label>
            <input type="text" class="form-control" disabled name="created_at" value="{{ $data['promotion']->created_at->format('d/m/Y H:i') }}">
        </div>
        <div class="form-group">
            <label for="amount">Última atualização</label>
            <input type="text" class="form-control" disabled name="created_at" value="{{ $data['promotion']->updated_at->format('d/m/Y H:i')  }}">
        </div>
        @endif
    
        <div class="form-group">
                @if(isset($data['promotion']))
            <div class="btn-group">
                <button type="button" class="btn btn-danger btn-delete float-right"  data-id="{{ $data['promotion']->id }}">
                    Excluir
                </button>
            </div>
            @endif
            <div class="btn-group float-right">
                <button class="btn btn-success" type="submit">Salvar</button>
            </div>
        </div>
    </fieldset>
</form>
@if(isset($data['promotion']))
@php
$o_slug = 'promotion';
$o_endpoint = 'promotion';
@endphp
@include('partials.modal-delete')
@endif