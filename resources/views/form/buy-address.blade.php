<div class="form-group">
<label class="my-3">{{ trans('legend.addresses') }}</label>
<div class="row">        
    <div class="col-md-12" id="address-change">                  
        <ul class="list-group" id="list-address">
            <li id="model-address" class="list-group-item p-0 d-none"><label class="m-0 d-block p-2">
                <input type="checkbox" value="" name="model_address" class="mx-3" checked>
                <span class="t-address"></span>
            </label>
        </li>
        @if(isset($data['addresses_available']))
        @foreach($data['addresses_available'] as $k => $address)
        <li class="list-group-item p-0"><label class="m-0 d-block p-2">
            <input type="checkbox" value="{{ $address->id }}" name="address_id[]" class="mx-3 check-address f-buy" @if(isset($data['addresses']) && in_array($address->id, $data['addresses'])) checked @endif >
            @if(isset($data['address_last']) && in_array($data['address_last']->id, $data['addresses']))
            <span class="badge bg-success">{{ trans('legend.last-used-address') }}</span>
            @endif
            <span class="t-address">{{ $address->street }}, {{ $address->number }} – {{ $address->district }}...</span></label>
            </li>
            @endforeach
            @endif
        </ul>
        <button type="button" class="btn btn-secondary btn-sm my-3 float-right" id="btn-address-add">{{ trans('legend.address-create') }}</button>
        @include('partials.modal-address')
        <div id="form-address-create">
        </div>
    </div>
</div>
</div>
