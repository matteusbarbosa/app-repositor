<form action="{{ path_admin('sales_goal') }}@if(isset($data['sales_goal']))/{{ $data['sales_goal']->id }}@endif" method="POST" class="validate" enctype="multipart/form-data">
  @csrf
  <fieldset>
    @if(isset($data['sales_goal']))
    <input type="hidden" name="user_id" value="{{ $data['sales_goal']->user_id != null ? $data['sales_goal']->user_id : Auth::user()->id }}">
    <input type="hidden" name="sales_goal_id" value="{{ $data['sales_goal']->id }}">
    {{ method_field('PATCH') }}
    @endif
    @if(isset($data['sales_channel']))
    <div class="form-group">
      <label for="title">{{ trans_choice('object.sales_channel', 1) }}</label>
      <input type="text" class="form-control" name="title" value="@if(isset($data['sales_goal'])){{ $data['sales_goal']->title }}@endif">
      <input type="hidden" name="sales_channel_id" value="{{ $data['sales_channel']->id }}">
    </div>
    @endif
    <div class="form-group">
      <label for="title">{{ __('legend.title') }}</label>
      <input type="text" class="form-control" name="title" value="@if(isset($data['sales_goal'])){{ $data['sales_goal']->title }}@endif">
    </div>
    <div class="form-group row">
      <div class="col-md-6">
        <label for="active" class="form-control">{{ __('legend.active') }}
          @if(!isset($data['sales_goal']) || (isset($data['sales_goal']) && $data['sales_goal']->disabled_at == null))
          <input type="checkbox" name="active" id="active" value="1" checked>
          @else
          <input type="checkbox" name="active" id="active" value="1">
          @endif
        </label>
      </div>  
    </div>
    <div class="form-group">
      <label>@lang('legend.goal-amount-min')</label>
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text" id="basic-addon1">R$</span>
        </div>
        <input type="text" class="form-control validate[custom[real]] mask-money" name="goal_amount_min" value="@if(isset($data['sales_goal'])){{ $data['sales_goal']->goal_amount_min }}@endif">
      </div>
    </div>
    <div class="form-group">
      <label>@lang('legend.goal-amount')</label>
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text" id="basic-addon1">R$</span>
        </div>
        <input type="text" class="form-control validate[custom[real]] mask-money" name="goal_amount" value="@if(isset($data['sales_goal'])){{ $data['sales_goal']->goal_amount }}@endif">
      </div>
    </div>
    <div class="form-group">
      <label>@lang('legend.goal-percent-max')</label>
      <div class="input-group">               
        <input type="text" class="form-control mask-percent validate[max[100]]" name="goal_percent_max" value="@if(isset($data['sales_goal'])){{ $data['sales_goal']->goal_percent_max }}@endif">
        <div class="input-group-prepend">
          <span class="input-group-text" id="basic-addon1">%</span>
        </div>
      </div>
    </div>
    @if(isset($data['sales_goal']))
      @if(!empty($data['sales_goal']->start_at))
      <div class="form-group">
        <label>@lang('legend.start_at')</label>
        <input type="text" class="form-control date validate[custom[date]]" name="start_at" value="@if(isset($data['sales_goal']) && !empty($data['sales_goal']->created_at)){{ $data['sales_goal']->start_at->format('d/m/Y H:i') }}@endif">
      </div>
      @endif
      @if(!empty($data['sales_goal']->end_at))
      <div class="form-group">
        <label>@lang('legend.end_at')</label>
        <input type="text" class="form-control date validate[custom[date]]" name="end_at" value="@if(isset($data['sales_goal']) && !empty($data['sales_goal']->end_at)){{ $data['sales_goal']->end_at->format('d/m/Y H:i') }}@endif">
      </div>
      @endif
    @endif
    <div class="form-group">
      <label for="details">@lang('legend.details')</label>
      <textarea id="details" class="form-control" name="details">@if(isset($data['sales_goal'])){{ $data['sales_goal']->details }}@endif</textarea>
    </div>
    @if(isset($data['sales_goal']))
    @if(!empty($data['sales_goal']->created_at))
    <div class="form-group">
      <label>Data de cadastro</label>
      <input type="text" class="form-control" disabled name="created_at" value="{{ $data['sales_goal']->created_at->format('d/m/Y H:i') }}">
    </div>
    @endif
    @if(!empty($data['sales_goal']->created_at))
    <div class="form-group">
      <label>Última atualização</label>
      <input type="text" class="form-control" disabled name="created_at" value="{{ $data['sales_goal']->updated_at->format('d/m/Y H:i')  }}">
    </div>
    @endif
    @endif
    @if(isset($data['sales_goal']))
    <div class="btn-group float-left">
      <button type="button" class="btn btn-danger btn-delete float-right"  data-id="{{ $data['sales_goal']->id }}">
        Excluir
      </button>
    </div>
    @endif
    <div class="btn-group float-right">
      <button class="btn btn-success" type="submit">Salvar</button>
    </div>
  </fieldset>
</form>
@if(isset($data['sales_goal']))
@php
$o_slug = 'sales_goal';
$o_endpoint = 'sales_goal';
@endphp
@include('partials.modal-delete')
@endif