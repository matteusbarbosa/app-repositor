@if(isset($data['product']))
<form id="form-product" action="{{ url('/admin/product/'.$data['product']->id) }}" method="POST" class="validate" enctype="multipart/form-data">
  <input type="hidden" name="product_id" value="{{ $data['product']->id }}">
  {{ method_field('PATCH') }}
  @else
  <form id="form-product" action="{{ url('/admin/product') }}" method="POST" class="validate" enctype="multipart/form-data">
    @endif
    @csrf
    <fieldset>
      @if(isset($data['product']))
      <div class="t-section bg-red mt-4">
        <h4>{{ trans('legend.edit').' '.trans_choice('object.product', 1).' "'.$data['product']->title.'"' }}    </h4>
      </div>
      <div class="text-right my-3">
        <a href="{{ path_admin('product/create') }}" class="btn btn-secondary">{{ __('crud.add', ['object' => trans_choice('object.product', 1)]) }} </a> 
      </div>
      @else
      <div class="t-section bg-red mt-4">
        <h4>{{ trans_choice('object.product', 1) }}  </h4>
      </div>
      @endif
      <div class="form-group d-flex">
        @if(isset($data['product']))
        <div class="col-md-4 p-0">
          <img src="{{ url($data['product']->image->url)  }}" class="img-fluid img m-auto d-block img-thumbnail" style="height:128px;">      
        </div>
        @endif
        <div class="col-md-8 p-0 ">
          <div class="custom-file">
            <input type="file" name="upload" id="image_upload" class="form-control custom-file-input" accept="image/*">
            <label class="custom-file-label" for="image_upload">@lang('legend.image-pick')</label>
          </div>
        </div>
      </div>
      @if(isset($data['product']) && !empty($data['product']->file_id))
      <div class="form-group row">
        <div class="col-md-3 offset-md-6">
          <label for="upload_remove" class="form-control">@lang('legend.image-remove')
            <input type="checkbox" name="upload_remove" id="upload_remove" value="1">
          </label>
        </div>
      </div>
      @endif
      <div class="form-group">
        <label for="title">Título</label>
        <input type="text" class="form-control" id="title" name="title" value="@if(isset($data['product'])){{ $data['product']->title }}@endif">
      </div>
      <div class="form-group row">
        <div class="col-md-3 offset-md-9">
          <label for="active" class="form-control">{{ __('legend.active') }}
            @if(!isset($data['product']) || (isset($data['product']) && $data['product']->disabled_at == null))
            <input type="checkbox" name="active" id="active" value="1" checked>
            @else
            <input type="checkbox" name="active" id="active" value="1">
            @endif
          </label>
        </div>
      </div>
      <div class="form-group">
        <label for="details">Detalhes</label>
        <textarea id="details" class="form-control" name="details">@if(isset($data['product'])){{ $data['product']->details }}@endif</textarea>
      </div>
      @if(isset($data['product']))
      @if($data['product']->created_at != null)
      <div class="form-group">
        <label for="created_at">Data de cadastro</label>
        <input type="text" class="form-control" id="created_at" disabled name="created_at" value="{{ $data['product']->created_at->format('d/m/Y H:i') }}">
      </div>
      @endif
      @if($data['product']->created_at != null)
      <div class="form-group">
        <label for="amount">Última atualização</label>
        <input type="text" class="form-control" disabled name="created_at" value="{{ $data['product']->updated_at->format('d/m/Y H:i') }}">
      </div>
      @endif
      @endif
      @if(isset($data['product']))
      <div class="btn-group float-left">
        <button type="button" class="btn btn-danger btn-delete float-right" data-id="{{ $data['product']->id }}">
          Excluir
        </button>
      </div>
      @endif
      <div class="btn-group float-right">
        <button class="btn btn-success" type="submit">Salvar</button>
      </div>
    </fieldset>
  </form>
  @if(isset($data['product']) && isset($data['stock_list']))
  <div class="t-section bg-red mt-4">
  <h4>@lang('legend.view-stock-options')</h4>
  </div>
  <ul class="list-group">
    @foreach($data['stock_list'] as $k => $s)
    <li class="list-group-item">
      <a href="{{ path_admin('stock/'.$s->id.'/edit') }}">
        @if(!empty($s->type)){{ $s->type->title  }} – @endif{{ $s->product->title }}@if($s->components->count() > 0) – contém {{ $s->components->count() }}@endif – {{ $s->details }} – {{ $s->fieldsToString() }}
      </a>
    </li>
    @endforeach
    <li class="list-group-item">
      <a class="text-success" href="{{ path_admin('stock/create?product_id='.$data['product']->id) }}">@lang('crud.add', ['object' => __('object.stock')])</a>
    </li>
  </ul>
  @endif
  @if(isset($data['product']))
  @php
  $o_slug = 'product';
  $o_endpoint = 'product';
  @endphp
  @include('partials.modal-delete')
  @endif
  