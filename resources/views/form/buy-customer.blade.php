<div class="row">
<div class="form-group col-md-6">
    <label for="f-name">{{ __('legend.name') }}</label>
    @if(Auth::user()->can('manage') || Auth::user()->can('sell'))
    <select id="f-name" class="form-control custom-select" name="user_id">
        <option>@lang('legend.select-option')</option>
        @foreach($data['customer_list'] as $id => $name)
            <option value="{{ $id }}">{{ $name }}</option>
        @endforeach
    </select>
    @else
    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
    <input type="text" value="{{ Auth::user()->name }}" class="form-control">
    @endif
</div>
<div class="form-group col-md-6">
<label for="f-phone">{{ __('legend.phone') }}</label>
<input id="f-phone" type="tel" name="phone" class="form-control phone validate[required,custom[phone],minSize[14]] f-buy" @if(isset($data['customer']))value="{{ $data['customer']->phone }}"@endif>
</div>
</div>

<div class="form-group">
<label class="my-3">{{ trans('legend.details') }}</label>
<textarea name="details"  id="f-details" class="form-control f-buy" name="details" @if(empty($data['cart']->details)) placeholder="{{ trans('legend.details-empty') }}" @endif>@if(isset($data['buy'])){{ $data['buy']->details }}@endif</textarea>

</div>
