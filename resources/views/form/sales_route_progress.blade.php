<form action="{{ path_admin('sales_route_progress') }}@if(isset($data['sales_route_progress']))/{{ $data['sales_route_progress']->id }}@endif" method="POST" class="validate" enctype="multipart/form-data">
    @csrf
    <fieldset>
        @if(isset($data['sales_route_progress']))
        <input type="hidden" name="user_id" value="{{ $data['sales_route_progress']->user_id != null ? $data['sales_route_progress']->user_id : Auth::user()->id }}">
        <input type="hidden" name="sales_route_progress_id" value="{{ $data['sales_route_progress']->id }}">
        {{ method_field('PATCH') }}
        @endif
        @if(isset($data['user_list']) && count($data['user_list']) > 0)
        <div class="form-group">
            <label class="d-block">{{ trans_choice('object.user', 1) }}</label>
            <select class="form-control" name="sales_route_user_id">
                @foreach($data['user_list'] as $id => $name)
                <option value="{{ $id }}"  @if(isset($data['sales_route_progress']) && $data['sales_route_progress']->user->id == $id) selected @endif>{{ $name }}</option>
                @endforeach
            </select>
        </div>
        @endif
        <div class="form-group">
            <label class="d-block">{{ trans_choice('object.sales_route_plan_address', 1) }}</label>
            <select class="form-control" name="sales_route_id">
                @foreach($data['plan_list'] as $id => $title)
                <option value="{{ $id }}"  @if(isset($data['sales_route_progress']) && $data['sales_route_progress']->address->id == $id) selected @endif>{{ $title }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label class="d-block">{{ trans_choice('object.sales_status', 1) }}</label>
            <select class="form-control" name="sales_route_id">
                @foreach($data['status_list'] as $id => $title)
                <option value="{{ $id }}"  @if(isset($data['sales_route_status']) &&  $data['sales_route_status']->id == $id) selected @endif>{{ $title }}</option>
                @endforeach
            </select>
        </div>       
        @if(isset($data['sales_route_progress']))
        @if(!empty($data['sales_route_progress']->created_at))
        <div class="form-group">
            <label>Data de cadastro</label>
            <input type="text" class="form-control" disabled name="created_at" value="{{ $data['sales_route_progress']->created_at->format('d/m/Y H:i') }}">
        </div>
        @endif
        @if(!empty($data['sales_route_progress']->created_at))
        <div class="form-group">
            <label>Última atualização</label>
            <input type="text" class="form-control" disabled name="created_at" value="{{ $data['sales_route_progress']->updated_at->format('d/m/Y H:i')  }}">
        </div>
        @endif
        @endif
        @if(isset($data['sales_route_progress']))
        <div class="btn-group float-left">
            <button type="button" class="btn btn-danger btn-delete float-right"  data-id="{{ $data['sales_route_progress']->id }}">
                Excluir
            </button>
        </div>
        @endif
        <div class="btn-group float-right">
            <button class="btn btn-success" type="submit">Salvar</button>
        </div>
    </fieldset>
</form>
@if(isset($data['sales_route_progress']))
@php
$o_slug = 'sales_route_progress';
$o_endpoint = 'sales_route_progress';
@endphp
@include('partials.modal-delete')
@endif