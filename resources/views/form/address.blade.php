<fieldset>
    <div class="form-group">
        <label for="f-title">{{ trans('legend.address-title') }}</label>
        <input id="f-title" type="tel" class="form-control" value="{{ isset($data['address']) ? $data['address']->title : '' }}" name="title">
        <small class="form-text text-muted"></small>
    </div>
    <div class="form-group">
        <label for="f-zipcode">{{ trans('legend.zipcode') }}</label>
        <input id="f-zipcode" type="tel" class="form-control" value="{{ isset($data['address']) ? $data['address']->zipcode : '' }}"  name="zipcode">
        <small class="form-text text-muted"></small>
    </div>
    <div class="form-group">
        <label for="street">{{ trans('legend.street') }}</label>
        <input type="text" class="form-control" value="{{ isset($data['address']) ? $data['address']->street : '' }}" id="f-street"  name="street">
        <small class="form-text text-muted"></small>
    </div>
    <div class="form-group">
        <label for="f-number">{{ trans('legend.address-number') }}</label>
        <input type="tel" class="form-control" value="{{ isset($data['address']) ? $data['address']->number : '' }}" id="f-number"  name="number">
        <small class="form-text text-muted"></small>
    </div>
    <div class="form-group">
        <label for="f-district">{{ trans('legend.district') }}</label>
        <input type="text" class="form-control" value="{{ isset($data['address']) ? $data['address']->district : '' }}" id="f-district"  name="district">
        <small class="form-text text-muted"></small>
    </div>
    <div class="form-group">
        <label for="f-city">{{ trans('legend.city') }}</label>
        <input type="text" class="form-control" value="{{ isset($data['address']) ? $data['address']->city : '' }}" id="f-city"  name="city">
        <small class="form-text text-muted"></small>
    </div>
    <div class="form-group">
        <label for="f-state">{{ trans('legend.state') }}</label>
        <input type="text" class="form-control" value="{{ isset($data['address']) ? $data['address']->state : '' }}" id="f-state"  name="state">
        <small class="form-text text-muted"></small>
    </div>
    
    @if(isset($data['address']))
    <div class="form-group row">
        <label for="f-created_at" class="col-sm-2 col-form-label">{{ trans('legend.created_at') }}</label>
        <div class="col-sm-10">
            <input type="text" readonly="" class="form-control-plaintext"  value="{{  isset($data['address']) ? $data['address']->created_at->format('d/m/Y H:i:s') : trans('legend.unknown') }}">
        </div>
    </div>
    <div class="form-group row">
        <label for="f-updated_at" class="col-sm-2 col-form-label">{{ trans('legend.updated_at') }}</label>
        <div class="col-sm-10">
            <input type="text" readonly="" class="form-control-plaintext" value="{{ isset($data['address']) ? $data['address']->updated_at->format('d/m/Y H:i:s') : trans('legend.unknown') }}">
        </div>
    </div>
    @endif
    <div class="form-group row">
        <div class="col-12">
            <button type="submit" class="btn btn-primary float-right"><i class="fa fa-floppy-o"></i> Salvar</button>
        </div>
    </div>
</fieldset>