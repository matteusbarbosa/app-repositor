@if(isset($data['stock']))
<form id="form-stock" action="{{ url('/admin/stock') }}@if(isset($data['stock']))/{{ $data['stock']->id }}@endif" method="POST" class="validate" enctype="multipart/form-data">
  <input type="hidden" name="stock_id" value="{{ $data['stock']->id or null }}">
  {{ method_field('PATCH') }}
  @else
  <form id="form-stock" action="{{ url('/admin/stock/') }}" method="POST" class="validate" enctype="multipart/form-data">
    @endif
    @csrf
    <fieldset>
      <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
      <div class="form-group d-flex">
        @if(isset($data['stock']))
        <div class="col-md-4 p-0">
          <img src="{{ url($data['stock']->image->url)  }}" class="img-fluid img m-auto d-block img-thumbnail" style="height:128px;">      
        </div>
        @endif
        <div class="col-md-8 p-0 ">
          <div class="custom-file">
            <input type="file" name="upload" id="image_upload" class="form-control custom-file-input" accept="image/*">
            <label class="custom-file-label" for="image_upload">@lang('legend.image-pick')</label>
          </div>
        </div>
      </div>
      @if(isset($data['stock']) && !empty($data['stock']->file_id))
      <div class="form-group row">
        <div class="col-md-3 offset-md-6">
          <label for="upload_remove" class="form-control">@lang('legend.image-remove')
            <input type="checkbox" name="upload_remove" id="upload_remove" value="1">
          </label>
        </div>
      </div>
      @endif
      <div class="form-group row">
        <div class="col-md-6">
          <label for="active" class="form-control">{{ __('legend.active') }}
            @if(!isset($data['stock']) || (isset($data['stock']) && $data['stock']->disabled_at == null))
            <input type="checkbox" name="active" id="active" value="1" checked>
            @else
            <input type="checkbox" name="active" id="active" value="1">
            @endif
          </label>
        </div>
        <div class="col-md-6">
          <label for="shop_display" class="form-control">{{ __('legend.shop-display') }}
            @if(!isset($data['stock']) || (isset($data['stock']) && $data['stock']->shop_display_at == null))
            <input type="checkbox" name="shop_display" id="shop_display" value="1" >
            @else
            <input type="checkbox" name="shop_display" id="shop_display" value="1" checked>
            @endif
          </label>
        </div>
      </div>
      @if(is_enabled('sku', 'STOCK_CODES'))
      <div class="form-group">
        <label for="f-sku">SKU</label>
        <input id="f-sku" type="tel" class="form-control" name="sku" value="@if(isset($data['stock'])){{ $data['stock']->sku }}@endif">
      </div>
      @endif
      @if(is_enabled('ncm', 'STOCK_CODES'))
      <div class="form-group">
        <label for="f-ncm">NCM</label>
        <input id="f-ncm" type="tel" class="form-control" name="ncm" value="@if(isset($data['stock'])){{ $data['stock']->ncm }}@endif">
      </div>
      @endif
      @if(is_enabled('ean', 'STOCK_CODES'))
      <div class="form-group">
        <label for="f-ean">EAN</label>
        <input id="f-ean" type="tel" class="form-control" name="ean" value="@if(isset($data['stock'])){{ $data['stock']->ean }}@endif">
      </div>
      @endif
      @if(is_enabled('dun', 'STOCK_CODES'))
      <div class="form-group">
        <label for="f-dun">DUN</label>
        <input id="f-dun" type="tel" class="form-control" name="dun" value="@if(isset($data['stock'])){{ $data['stock']->dun }}@endif">
      </div>
      @endif
      @if(is_enabled('tax_class', 'STOCK_CODES'))
      <div class="form-group">
        <label for="f-tax_class">{{ __('product.tax_class') }}</label>
        <input id="f-tax_class" type="text" class="form-control" name="tax_class" value="@if(isset($data['stock'])){{ $data['stock']->tax_class }}@endif">
      </div>
      @endif
      @if(isset($data['stock']) && !empty($data['stock']->user))
      <div class="form-group">
        <label for="recorder">{{ __('object.recorder') }}</label>
        <input type="text" class="form-control" name="recorder" readonly value="{{ $data['stock']->user->name }}">
      </div>
      @endif
      <div class="form-group">
        <label for="type_id">@lang('legend.stock-type')</label>
        <select name="type_id" id="type_id" class="form-control custom-select validate[required]" @if(isset($data['stock'])) readonly @endif>
          @foreach($data['type_list'] as $k => $type)
          <option value="{{ $type->id }}" @if(isset($data['stock']) && $type->id == $data['stock']->type_id) selected @endif>{{ $type->title }}</option>
          @endforeach
        </select> 
      </div>
      <div class="form-group">
        <label for="product_id">{{ trans_choice('object.product', 1) }}</label>
        <select readonly name="product_id" id="product_id" class="form-control custom-select validate[required]">
          @foreach($data['product_list'] as $id => $title)
          <option  value="{{ $id }}" @if(Request::query('product_id') == $id || isset($data['stock']) && $id == $data['stock']->product_id) selected @endif>{{ $id }} – {{ $title }}</option>
          @endforeach
        </select>
        <small class="form-text text-muted">@lang('tip.product-change')</small>
      </div>
      <section class="group-field">
        <div class="form-group">
          <h3>{{ trans_choice('product.stock-field',2) }}</h3>
        </div>
        <div class="form-group wrap-fields">
          @if(isset($data['stock_fields']))
          @foreach($data['stock_fields'] as $k => $sf)
          <div class="field mt-2">
            @includeFirst(['field.'.$sf->type, 'field.default'])
          </div>
          @endforeach
          @endif
          <div class="field-model d-none">
            <div class="row field">
              <div class="col-md-6">
                <label>{{ trans_choice('product.stock-field', 1) }} <button type="button"  class="btn remove btn-sm btn-danger">@lang('crud.delete')</button></label>
                <select name="field_id[]" class="f-field-id form-control">
                  <option value="">@lang('legend.select-option')</option>
                  @foreach($data['field_list'] as $id => $title)
                  <option value="{{ $id }}">{{ $title }}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-md-6">
                <div class="custom-file">
                  <label>{{ trans_choice('product.field-content', 1) }}</label>
                  <input type="text" name="field_content[]" class="f-field-content form-control validate[required]">
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group justify-content-end mb-3 w-add-field d-none">
          <button type="button" class="add-field btn btn-secondary">+ @lang('crud.add', ['object' => trans_choice('product.stock-field', 1)])</button>
        </div>
      </section>
      <section class="group-component">
        <div class="form-group">
          <h3>@lang('legend.stock-components')</h3>
        </div>
        <div class="form-group wrap-components">
          @if(isset($data['stock_components']))
          @foreach($data['stock_components'] as $k => $sc)
          <div class="row component">
            <div class="col-md-9">
              <label>@lang('legend.stock-component')  <button type="button" class="btn remove btn-danger btn-sm">@lang('crud.delete')</button></label>
              <select name="component_id[]" class="f-component-id form-control">
                <option value="">@lang('legend.select-option')</option>
                @foreach($data['stock_list'] as $k => $s)
                <option @if($sc->id == $s->id) selected @endif data-parent-type-id="{{ $s->type->parent_id }}" data-product_id="{{ $s->product_id }}" value="{{ $s->id }}">{{ $s->type->title  }} – {{ $s->product->title }}@if($s->components->count() > 0) – contém {{ $s->components->count() }}@endif – {{ $s->fieldsToString(null, null, true) }}</option>
                @endforeach
              </select>
            </div>
            <div class="col-md-3">
              <label>@lang('legend.stock-quantity')</label>
              <input type="number" name="component_quantity[]" value="{{ $sc->quantity }}" class="f-component-quantity form-control">
            </div>
          </div>
          @endforeach
          @endif
          <div class="component-model d-none">
            <div class="row component">
              <div class="col-md-9">
                <label>@lang('legend.stock-component') <button type="button"  class="btn remove btn-sm btn-danger">@lang('crud.delete')</button></label>
                <select name="component_id[]" class="f-component-id form-control">
                  <option value="">@lang('legend.select-option')</option>
                  @foreach($data['stock_list'] as $k => $s)
                  <option @if(!empty($s->type->parent_id))data-parent-type-id="{{ $s->type->parent_id }}"@endif data-product_id="{{ $s->product_id }}" value="{{ $s->id }}">@if(!empty($s->type)){{ $s->type->title  }} – @endif{{ $s->product->title }}@if($s->components->count() > 0) – contém {{ $s->components->count() }}@endif – {{ $s->fieldsToString(null, explode(',', config('instance.STOCK_FIELD_DEFAULT_DISPLAY'))) }}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-md-3">
                <label>@lang('legend.quantity')</label>
                <input type="number" name="component_quantity[]" value="1" class="f-component-quantity form-control">
              </div>
            </div>
          </div>
        </div>
        <div class="form-group justify-content-end mb-3 w-add-component d-none">
          <button type="button" class="add-component btn btn-secondary">+ @lang('crud.add', ['object' => __('object.stock_component')])</button>
        </div>
      </section>
      <div class="form-group">
        <label for="quantity">{{ __('legend.quantity') }}</label>
        <input type="text" class="form-control" name="quantity" value="@if(isset($data['stock'])){{ $data['stock']->quantity }}@endif">
      </div>
      @if(!empty($data['tag_list']))
      <div class="form-group">
        <label class="d-block">{{ trans_choice('object.tag',1) }}</label>
        <select name="tag_id[]" class="form-control" multiple>
          @foreach($data['tag_list'] as $id => $title)
          <option @if(isset($data['stock']) && in_array($id, $data['stock_tag_list'])) selected @endif value="{{ $id }}">{{ $title }}</option>
          @endforeach
        </select>
      </div>
      @else
      <div class="alert alert-warning">
        @lang('legend.no-tags-available')
      </div>
      @endif
      @if(isset($data['stock']))
      @if($data['stock']->created_at != null)
      <div class="form-group">
        <label for="created_at">Data de cadastro</label>
        <input type="text" class="form-control" id="created_at" disabled name="created_at" value="{{ $data['stock']->created_at->format('d/m/Y H:i') }}">
      </div>
      @endif
      @if($data['stock']->updated_at != null)
      <div class="form-group">
        <label for="amount">Última atualização</label>
        <input type="text" class="form-control" disabled name="created_at" value="{{ $data['stock']->updated_at->format('d/m/Y H:i') }}">
      </div>
      @endif
      @endif
      @if(isset($data['stock']))
      <div class="btn-group float-left">
        <button type="button" class="btn btn-danger float-right btn-delete" data-id="{{ $data['stock']->id }}">
          Excluir
        </button>
      </div>
      @endif
      <div class="btn-group float-right">
        <button class="btn btn-success" type="submit">Salvar</button>
      </div>
    </fieldset>
  </form>
  @if(isset($data['stock']))
  @php
  $o_slug = 'stock';
  $o_endpoint = 'stock';
  @endphp
  @include('partials.modal-delete')
  @endif
  