<form action="{{ path_admin('sales_route') }}@if(isset($data['sales_route']))/{{ $data['sales_route']->id }}@endif" method="POST" class="validate" enctype="multipart/form-data">
    @csrf
    <fieldset>
      @if(isset($data['sales_route']))
      <input type="hidden" name="user_id" value="{{ $data['sales_route']->user_id != null ? $data['sales_route']->user_id : Auth::user()->id }}">
      <input type="hidden" name="sales_route_id" value="{{ $data['sales_route']->id }}">
      {{ method_field('PATCH') }}
      @endif
      @if(isset($data['sales_channel']))
      <div class="form-group">
        <label for="title">{{ trans_choice('object.sales_channel', 1) }}</label>
        <input type="text" class="form-control" name="title" value="@if(isset($data['sales_route'])){{ $data['sales_route']->title }}@endif">
        <input type="hidden" name="sales_channel_id" value="{{ $data['sales_channel']->id }}">
      </div>
      @endif
      <div class="form-group">
        <label for="title">{{ __('legend.title') }}</label>
        <input type="text" class="form-control" name="title" value="@if(isset($data['sales_route'])){{ $data['sales_route']->title }}@endif">
      </div>
      <div class="form-group">
        <label class="d-block">{{ trans_choice('object.sales_route_status', 1) }}</label>
        <select class="form-control" name="sales_route_id">
            @foreach($data['sales_route_statuses'] as $id => $title)
            <option value="{{ $id }}"  @if(isset($data['sales_route']) && $data['sales_route']->status_id == $id) selected @endif>{{ $title }}</option>
            @endforeach
        </select>
    </div>
      <div class="form-group row">
        <div class="col-md-6 offset-md-6">
          <label for="active" class="form-control">{{ __('legend.active') }}
            @if(!isset($data['sales_route']) || (isset($data['sales_route']) && $data['sales_route']->disabled_at == null))
            <input type="checkbox" name="active" id="active" value="1" checked>
            @else
            <input type="checkbox" name="active" id="active" value="1">
            @endif
          </label>
        </div>  
      </div>
      <div class="form-group">
        <label for="details">@lang('legend.details')</label>
        <textarea id="details" class="form-control" name="details">@if(isset($data['sales_route'])){{ $data['sales_route']->details }}@endif</textarea>
      </div>
      @if(isset($data['sales_route']))
      @if(!empty($data['sales_route']->created_at))
      <div class="form-group">
        <label>Data de cadastro</label>
        <input type="text" class="form-control" disabled name="created_at" value="{{ $data['sales_route']->created_at->format('d/m/Y H:i') }}">
      </div>
      @endif
      @if(!empty($data['sales_route']->created_at))
      <div class="form-group">
        <label>Última atualização</label>
        <input type="text" class="form-control" disabled name="created_at" value="{{ $data['sales_route']->updated_at->format('d/m/Y H:i')  }}">
      </div>
      @endif
      @endif
      @if(isset($data['sales_route']))
      <div class="btn-group float-left">
        <button type="button" class="btn btn-danger btn-delete float-right"  data-id="{{ $data['sales_route']->id }}">
          Excluir
        </button>
      </div>
      @endif
      <div class="btn-group float-right">
        <button class="btn btn-success" type="submit">Salvar</button>
      </div>
    </fieldset>
  </form>
  @if(isset($data['sales_route']))
  @php
  $o_slug = 'sales_route';
  $o_endpoint = 'sales_route';
  @endphp
  @include('partials.modal-delete')
  @endif