<form action="{{ path_admin('sales_route_plan') }}@if(isset($data['sales_route_plan']))/{{ $data['sales_route_plan']->id }}@endif" method="POST" class="validate" enctype="multipart/form-data">
    @csrf
    <fieldset>
        @if(isset($data['sales_route_plan']))
        <input type="hidden" name="user_id" value="{{ $data['sales_route_plan']->user_id != null ? $data['sales_route_plan']->user_id : Auth::user()->id }}">
        <input type="hidden" name="sales_route_plan_id" value="{{ $data['sales_route_plan']->id }}">
        {{ method_field('PATCH') }}
        @endif

        <div class="form-group">
                <div class="col-md-6 offset-md-6">
                    <label for="active" class="form-control">{{ __('legend.active') }}
                        @if(!isset($data['sales_route_plan']) || (isset($data['sales_route_plan']) && $data['sales_route_plan']->disabled_at == null))
                        <input type="checkbox" name="active" id="active" value="1" checked>
                        @else
                        <input type="checkbox" name="active" id="active" value="1">
                        @endif
                    </label>
                </div>  
            </div>
        
        <div class="form-group">
            <label class="d-block">{{ trans_choice('object.sales_route', 1) }}</label>
            @if(isset($data['sales_route_plan']))
            <input class="form-control" value="{{ $data['sales_route_plan']->sales_route->title }}" type="text" readonly>
            @else
            @if(isset($data['sales_routes']) && count($data['sales_routes']) > 0)
            <select class="form-control" name="sales_route_id">
                @foreach($data['sales_routes'] as $id => $title)
                <option value="{{ $id }}"  @if(isset($data['sales_routes']) && count($data['sales_routes']) > 0 && in_array($id, $data['sales_routes']) || Request::query('sales_route_id') == $id) selected @endif>{{ $title }}</option>
                @endforeach
            </select>
            @else
            <div class="alert bg-secondary">@lang('tip.insert-rows')</div>
            <div class="text-right">
                <a href="{{ path_admin('sales_routes') }}" class="btn btn-sm btn-secondary">{{  __('crud.add', ['object' => trans_choice('object.sales_route',1)]) }}</a>
            </div>
            @endif      
            @endif     
        </div>

        @if(isset($data['supervisor_list']) && count($data['supervisor_list']) > 0)
        <div class="form-group">
                <label class="d-block">{{ trans_choice('object.supervisor', 1) }}</label>
            
                <select class="form-control" name="supervisor_id">
                    @foreach($data['supervisor_list'] as $id => $name)
                    <option value="{{ $id }}"  @if(isset($data['sales_route_plan']) && $data['sales_route_plan']->supervisor_id) selected @endif>{{ $name }}</option>
                    @endforeach
                </select>
             
        </div>
        @endif

        
        <section class="group-address">
            <div class="form-group">
                <h3>{{ __('legend.route-plan-addresses') }}</h3>
            </div>
            <div class="form-group wrap-addresses">
                @if(isset($data['plan_address_list']))
                @foreach($data['plan_address_list'] as $k => $plan_address)
                <div class="row address">
                    <!-- CLIENTE ID -->
                    <div class="col-md-1 d-flex justify-content-center align-items-center">
                            <input type="hidden" class="number-input" name="queue_position[]">
                        <span class="badge badge-success number">{{ $k+1 }}</span>
                    </div>
                            <div class="col-md-5">
                        <label class="address-title">{{ trans_choice('object.address', 1) }}<button type="button" class="btn remove btn-sm btn-danger ml-3">@lang('crud.delete')</button></label>
                        <select name="client_id[]" class="f-client-id form-control  mt-2">
                            <option value="">@lang('legend.select-option')</option>
                            @foreach($data['client_list'] as $id => $name)
                            <option value="{{ $id }}" @if($plan_address->user_id == $id) selected @endif>{{ $name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <!-- ENDEREÇO -->
                    <div class="col-md-6">
                        <label class="address-title">{{ trans_choice('object.address', 1) }}</label>
                        <select name="address_id[]" class="f-address-id form-control  mt-2" >
                            <option value="">@lang('legend.select-option')</option>
                            @foreach($data['address_list'] as $id => $address)
                            <option data-client_id="{{ $address->user_id }}" value="{{ $address->id }}" @if($address->id == $plan_address->id) selected @else class="d-none" @endif>{{ $address->title.' – '.$address->street.' – '.$address->number }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                @endforeach
                @endif
                <!-- MODEL -->
                    <div class="row address address-model d-none">
                        <!-- CLIENTE ID -->
                        <div class="col-md-1 d-flex justify-content-center align-items-center">
                            <input type="hidden" class="number-input" name="queue_position[]">
                                <span class="badge badge-success number"></span>
                            </div>
                        <div class="col-md-5">
                            <label class="address-title">{{ trans_choice('object.address', 1) }}<button type="button"  class="btn remove btn-sm btn-danger ml-3">@lang('crud.delete')</button></label>
                            <select name="client_id[]" class="f-client-id form-control  mt-2">
                                <option value="">@lang('legend.select-option')</option>
                                @foreach($data['client_list'] as $id => $name)
                                <option value="{{ $id }}" >{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <!-- ENDEREÇO -->
                        <div class="col-md-6">
                                <label class="address-title">{{ trans_choice('object.address', 1) }}</label>
                            <select name="address_id[]" class="f-address-id form-control  mt-2" >
                                <option value="">@lang('legend.select-option')</option>
                                @foreach($data['address_list'] as $id => $address)
                                <option class="d-none" data-client_id="{{ $address->user_id }}" value="{{ $address->id }}" >{{ $address->title.' – '.$address->street.' – '.$address->number }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
            </div>
            <div class="form-group justify-content-end mb-3 w-add-address d-none">
                <button type="button" class="add-address btn btn-secondary">+ @lang('crud.add', ['object' => trans_choice('object.address', 1)])</button>
            </div>
        </section>
        
        
        
        @if(isset($data['sales_route_plan']))
        @if(!empty($data['sales_route_plan']->created_at))
        <div class="form-group">
            <label>Data de cadastro</label>
            <input type="text" class="form-control" disabled name="created_at" value="{{ $data['sales_route_plan']->created_at->format('d/m/Y H:i') }}">
        </div>
        @endif
        @if(!empty($data['sales_route_plan']->created_at))
        <div class="form-group">
            <label>Última atualização</label>
            <input type="text" class="form-control" disabled name="created_at" value="{{ $data['sales_route_plan']->updated_at->format('d/m/Y H:i')  }}">
        </div>
        @endif
        @endif
        @if(isset($data['sales_route_plan']))
        <div class="btn-group float-left">
            <button type="button" class="btn btn-danger btn-delete float-right"  data-id="{{ $data['sales_route_plan']->id }}">
                Excluir
            </button>
        </div>
        @endif
        <div class="btn-group float-right">
            <button class="btn btn-success" type="submit">Salvar</button>
        </div>
    </fieldset>
</form>
@if(isset($data['sales_route_plan']))
@php
$o_slug = 'sales_route_plan';
$o_endpoint = 'sales_route_plan';
@endphp
@include('partials.modal-delete')
@endif