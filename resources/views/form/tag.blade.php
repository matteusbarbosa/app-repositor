<form action="{{ url('/admin/tag') }}@if(isset($data['tag']))/{{ $data['tag']->id }}@endif" method="POST" class="validate" enctype="multipart/form-data">
  @csrf
  <fieldset>
    @if(isset($data['tag']))
    <input type="hidden" name="user_id" value="{{ $data['tag']->user_id != null ? $data['tag']->user_id : Auth::user()->id }}">
    <input type="hidden" name="tag_id" value="{{ $data['tag']->id }}">
    {{ method_field('PATCH') }}
    @endif
    <div class="form-group d-flex">
      @if(isset($data['tag']))
      <div class="col-md-4 p-0">
         <img src="{{ url($data['tag']->image->url)  }}" class="img-fluid img m-auto d-block img-thumbnail" style="height:128px;"> 
      </div>
      @endif
      <div class="col-md-8 p-0 ">
        <div class="custom-file">
          <input type="file" name="upload" id="upload" class="form-control custom-file-input" accept="image/*" >
          <label class="custom-file-label" for="upload">@lang('legend.image-pick')</label>
        </div>
      </div>
    </div>
    @if(isset($data['tag']) && !empty($data['tag']->file_id))
    <div class="form-group row">
      <div class="col-md-3 offset-md-6">
        <label for="upload_remove" class="form-control">@lang('legend.image-remove')
          <input type="checkbox" name="upload_remove" id="upload_remove" value="1">
        </label>
      </div>
    </div>
    @endif
    @if($data['tag_list']->count() > 0)
    <div class="form-group">
      <label for="parent_id">@lang('legend.tag-parent')</label>
      <select name="parent_id" id="parent_id" class="form-control custom-select">
        <option value="">– Selecione –</option>
        @foreach($data['tag_list'] as $k => $v)
        <option @if($v->parent_id != null) class="pl-4" @endif value="{{ $v->id }}" @if(isset($data['tag']) && !empty($data['tag']->parent) && $v->id == $data['tag']->parent->id) selected @endif>{{ $v->title }}</option>
        @endforeach
      </select>
    </div>
    @endif
    <div class="form-group">
      <label for="title">{{ __('legend.title') }}</label>
      <input type="text" class="form-control" name="title" value="@if(isset($data['tag'])){{ $data['tag']->title }}@endif">
    </div>
    <div class="form-group row">
      <div class="col-md-6">
        <label for="active" class="form-control">{{ __('legend.active') }}
          @if(!isset($data['tag']) || (isset($data['tag']) && $data['tag']->disabled_at == null))
          <input type="checkbox" name="active" id="active" value="1" checked>
          @else
          <input type="checkbox" name="active" id="active" value="1">
          @endif
        </label>
      </div>

      <div class="col-md-6">
          <label for="shop_display" class="form-control">{{ __('legend.shop-display') }}
            @if(!isset($data['tag']) || (isset($data['tag']) && $data['tag']->shop_display_at == null))
            <input type="checkbox" name="shop_display" id="shop_display" value="1" >
            @else
            <input type="checkbox" name="shop_display" id="shop_display" value="1" checked>
            @endif
          </label>
        </div>

    </div>
    <div class="form-group">
      <label for="details">@lang('legend.details')</label>
      <textarea id="details" class="form-control" name="details">@if(isset($data['tag'])){{ $data['tag']->details }}@endif</textarea>
    </div>
    @if(isset($data['tag']))
      @if(!empty($data['tag']->created_at))
      <div class="form-group">
        <label for="amount">Data de cadastro</label>
        <input type="text" class="form-control" disabled name="created_at" value="{{ $data['tag']->created_at->format('d/m/Y H:i') }}">
      </div>
      @endif
      @if(!empty($data['tag']->updated_at))
      <div class="form-group">
        <label for="amount">Última atualização</label>
        <input type="text" class="form-control" disabled name="created_at" value="{{ $data['tag']->updated_at->format('d/m/Y H:i')  }}">
      </div>
      @endif
    @endif
    @if(isset($data['tag']))
    <div class="btn-group float-left">
      <button type="button" class="btn btn-danger btn-delete float-right"  data-id="{{ $data['tag']->id }}">
        Excluir
      </button>
    </div>
    @endif
    <div class="btn-group float-right">
      <button class="btn btn-success" type="submit">Salvar</button>
    </div>
  </fieldset>
</form>
@if(isset($data['tag']))
@php
$o_slug = 'tag';
$o_endpoint = 'tag';
@endphp
@include('partials.modal-delete')
@endif