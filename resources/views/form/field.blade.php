<form action="{{ url('/admin/field') }}@if(isset($data['field']))/{{ $data['field']->id }}@endif" method="POST" class="validate" enctype="multipart/form-data">
    @csrf
    <fieldset>
      @if(isset($data['field']))
      <input type="hidden" name="user_id" value="{{ $data['field']->user_id != null ? $data['field']->user_id : Auth::user()->id }}">
      <input type="hidden" name="field_id" value="{{ $data['field']->id }}">
      {{ method_field('PATCH') }}
      @endif


      <div class="form-group row">
        <div class="col-md-6">
          <label for="active" class="form-control">{{ __('legend.active') }}
            @if(!isset($data['field']) || (isset($data['field']) && $data['field']->disabled_at == null))
            <input type="checkbox" name="active" id="active" value="1" checked>
            @else
            <input type="checkbox" name="active" id="active" value="1">
            @endif
          </label>
        </div>
        <div class="col-md-6">
          <label for="shop_display" class="form-control">{{ __('legend.shop-display') }}
            @if(!isset($data['field']) || (isset($data['field']) && $data['field']->shop_display_at == null))
            <input type="checkbox" name="shop_display" id="shop_display" value="1" >
            @else
            <input type="checkbox" name="shop_display" id="shop_display" value="1" checked>
            @endif
          </label>
        </div>
      </div>


      <div class="form-group">
        <label for="type">{{ trans_choice('product.field-type', 1) }}</label>
        <select name="type" id="type" class="form-control custom-select">
          <option value="">– Selecione –</option>
          <option @if(isset($data['field']) && $data['field']->type == 'text') selected @endif value="text">@lang('legend.string')</option>
          <option @if(isset($data['field']) && $data['field']->type == 'double') selected @endif value="double">@lang('legend.double')</option>
          <option @if(isset($data['field']) && $data['field']->type == 'date') selected @endif value="date">@lang('legend.date')</option>
          <option @if(isset($data['field']) && $data['field']->type == 'color') selected @endif value="color">@lang('legend.color')</option>
        </select>
      </div>

      <div class="form-group">
        <label for="title">{{ __('legend.title') }}</label>
        <input type="text" class="form-control" name="title" value="@if(isset($data['field'])){{ $data['field']->title }}@endif">
      </div>

      @if(isset($data['field']))
      <div class="form-group">
        <label for="amount">Data de cadastro</label>
        <input type="text" class="form-control" disabled name="created_at" value="{{ $data['field']->created_at->format('d/m/Y H:i') }}">
      </div>
      <div class="form-group">
        <label for="amount">Última atualização</label>
        <input type="text" class="form-control" disabled name="created_at" value="{{ $data['field']->updated_at->format('d/m/Y H:i')  }}">
      </div>
      @endif
      @if(isset($data['field']))
      <div class="btn-group float-left">
        <button type="button" class="btn btn-danger float-right btn-delete" data-id="{{ $data['field']->id }}">
          Excluir
        </button>
      </div>
      @endif
      <div class="btn-group float-right">
        <button class="btn btn-success" type="submit">Salvar</button>
      </div>
    </fieldset>
  </form>
  @if(isset($data['field']))
  @php
  $o_slug = 'field';
  $o_endpoint = 'field';
  @endphp
  @include('partials.modal-delete')
  @endif