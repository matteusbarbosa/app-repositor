<form action="{{ url('/admin/sales_channel') }}@if(isset($data['sales_channel']))/{{ $data['sales_channel']->id }}@endif" method="POST" class="validate" enctype="multipart/form-data">
    @csrf
    <fieldset>
      @if(isset($data['sales_channel']))
      <input type="hidden" name="user_id" value="{{ $data['sales_channel']->user_id != null ? $data['sales_channel']->user_id : Auth::user()->id }}">
      <input type="hidden" name="sales_channel_id" value="{{ $data['sales_channel']->id }}">
      {{ method_field('PATCH') }}
      @endif

      <div class="form-group">
          <label class="d-block">{{ trans_choice('object.sales_goal', 2) }}</label>
          @if(isset($data['sales_goals']) && count($data['sales_goals']) > 0)
          
          <select class="form-control custom-select" name="sales_goal_id" multiple>
              @foreach($data['sales_goals'] as $id => $title)
              <option value="{{ $id }}"  @if(isset($data['sales_channel']) && count($data['sales_channel_goals']) > 0 && in_array($id, $data['sales_channel_goals'])) selected @endif>{{ $title }}</option>
              @endforeach
          </select>
          @else
          <div class="alert bg-secondary">@lang('tip.insert-rows')</div>
          @endif
          <div class="text-right">
              <a href="{{ path_admin('sales_goal') }}" class="btn btn-sm btn-secondary">@lang('crud.manage', ['object' => trans_choice('object.sales_goal', 2)])</a>
          </div>
      </div>


      <div class="form-group">
        <label for="title">{{ __('legend.title') }}</label>
        <input type="text" class="form-control" name="title" value="@if(isset($data['sales_channel'])){{ $data['sales_channel']->title }}@endif">
      </div>

      <div class="form-group row">
        <div class="col-md-6">
          <label for="active" class="form-control">{{ __('legend.active') }}
            @if(!isset($data['sales_channel']) || (isset($data['sales_channel']) && $data['sales_channel']->disabled_at == null))
            <input type="checkbox" name="active" id="active" value="1" checked>
            @else
            <input type="checkbox" name="active" id="active" value="1">
            @endif
          </label>
        </div>  
      </div>
      
      <div class="form-group">
        <label for="details">@lang('legend.details')</label>
        <textarea id="details" class="form-control" name="details">@if(isset($data['sales_channel'])){{ $data['sales_channel']->details }}@endif</textarea>
      </div>
      @if(isset($data['sales_channel']))
        @if(!empty($data['sales_channel']->created_at))
        <div class="form-group">
          <label>Data de cadastro</label>
          <input type="text" class="form-control" disabled name="created_at" value="{{ $data['sales_channel']->created_at->format('d/m/Y H:i') }}">
        </div>
        @endif
        @if(!empty($data['sales_channel']->created_at))
        <div class="form-group">
          <label>Última atualização</label>
          <input type="text" class="form-control" disabled name="created_at" value="{{ $data['sales_channel']->updated_at->format('d/m/Y H:i')  }}">
        </div>
        @endif
      @endif
      @if(isset($data['sales_channel']))
      <div class="btn-group float-left">
        <button type="button" class="btn btn-danger btn-delete float-right"  data-id="{{ $data['sales_channel']->id }}">
          Excluir
        </button>
      </div>
      @endif
      <div class="btn-group float-right">
        <button class="btn btn-success" type="submit">Salvar</button>
      </div>
    </fieldset>
  </form>
  @if(isset($data['sales_channel']))
  @php
  $o_slug = 'sales_channel';
  $o_endpoint = 'sales_channel';
  @endphp
  @include('partials.modal-delete')
  @endif