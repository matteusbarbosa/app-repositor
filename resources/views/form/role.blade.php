<form action="/cargo{{ isset($data['role']) ? '/'.$data['role']->id : null }}" method="POST" class="validate">
  {{ csrf_field() }}
  <fieldset>
    
    <input type="hidden" name="user_id" value="{{ $data['role']->user_id != null ? $data['role']->user_id : Auth::user()->id }}">

    @if(isset($data['role']))
    <input type="hidden" name="role_id" value="{{ $data['role']->id }}">
    {{ method_field('PATCH') }}
    @endif
    

        <div class="form-group">
      
      <label for="title">Título *</label>
      
      <input name="title" id="title" class="form-control validate[required]" value="{{ $data['role']->title }}">
      
    </div>
    
    <div class="form-group">
      <label for="details">Detalhes</label>
      <textarea id="details" class="form-control" name="details">{{ $data['role']->details }}</textarea>
    </div>

        <div class="form-group">
      <label for="list_permission">Permissões</label>
      <textarea id="list_permission" class="form-control" name="list_permission">{{ $data['role']->list_permission }}</textarea>
    </div>

          <div class="form-group row">
      <div class="col-md-3 offset-md-9">
      <label for="active" class="form-control">Ativa *
      @if(!isset($data['role']) || (isset($data['role']) && $data['role']->disabled_at == null))
      <input type="checkbox" name="active" id="active" value="1" checked>
      @else
      <input type="checkbox" name="active" id="active" value="1">
      @endif
      </label>
    </div>
    </div>
    
    @if(isset($data['role']))
    <div class="form-group">
      <label for="amount">Data de cadastro</label>
      <input type="text" class="form-control" disabled name="created_at" value="{{ \Extra\Helper::dateDmy($data['role']->created_at->timestamp) }}">
    </div>
    <div class="form-group">
      <label for="amount">Última atualização</label>
      <input type="text" class="form-control" disabled name="created_at" value="{{ \Extra\Helper::dateDmy($data['role']->updated_at->timestamp) }}">
    </div>
    
    @endif
    
    @if(isset($data['role']))
    
    <div class="btn-group float-left">
      <button type="button" class="btn btn-danger btn-delete float-right"  data-id="{{ $data['role']->id }}">
        Excluir
      </button>
    </div>
    @endif
    
    <div class="btn-group float-right">
      
      <button class="btn btn-success" type="submit">Salvar</button>
    </div>
  </fieldset>
</form>

@if(isset($data['role']))
@php
$o_slug = 'role';
$o_endpoint = 'role';
@endphp
@include('partials.modal-delete')
@endif
