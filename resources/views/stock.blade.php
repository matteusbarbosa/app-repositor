@extends('layouts.public')
@section('title', $data['stock']->product->title)
@section('description', __('seo.stock-description', ['app_name' => config('app.name'),'title' =>  $data['stock']->product->title, 'fields' => $data['stock']->fieldsToString(3), 'details' => $data['stock']->details]))
@section('header')
@parent
<script src="{{ asset('dist/js/cart-min.js') }}" ></script>
<script src="{{ asset('dist/js/buy-min.js') }}" ></script>
<script src="{{ asset('dist/js/shop-min.js') }}" defer></script>
<script src="{{ asset('dist/js/share-min.js') }}" defer></script>
<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/product.css') }}">
<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/stock.css') }}">
@endsection
@section('content')
<div class="mt-4 container">
  <div class="t-section bg-red">
    <h4>{{ $data['product']->title }}</h4>
  </div>
  <div class="t-section">
    <h4>{{ $data['stock']->type->title }}</h4>
  </div>
  <div class="card main">
    <div class="row card-body">
      <div class="col-md-4">
        <div class="d-block d-md-none my-4"></div>
        <figure class="m-0">
          <img src="@if(!empty($data['stock']->file_id)){{ asset($data['stock']->image->url) }}@else{{ asset($data['stock']->product->image->url) }}@endif" class="img-fluid img mx-auto mb-4 mt-2 d-block img-view w-50"> 
          @if(!empty($data['stock']->component_quantity))
          <figcaption>@lang('product.stock-quantity', ['stock' => $data['stock']->type->title, 'qt' => $data['stock']->component_quantity])</figcaption>
          @endif

          @cannot('manage')
          @php
          $stock = $data['stock'];
          @endphp
            @include('partials.btn-cart-add')
          @endcannot

     
        </figure>
        @include('partials.modal-gallery')
      </div>
      <div class="col-md-8">
        @if(isset($data['stock_tags']))
        <ul class="list-inline">
          @foreach($data['stock_tags'] as $id => $title)
          <li class="list-inline-item"><span class="badge badge-secondary">{{ $title }}</span></li> 
          @endforeach
        </ul>
        @endif    
        @if(!empty($data['product']->details)) 
        <blockquote>{{ $data['product']->details }}</blockquote>
        @endif
        @if(!empty($data['stock']->details)) 
        <hr>
        <blockquote>{{ $data['stock']->details }}</blockquote>
        <ul>
        @foreach($data['stock']->fields as $k => $f)
        <li>{{ $f->title }}: {{ $f->pivot->content }}</li>
        @endforeach
        @endif
      </ul>
      </div> 
    </div>
    <div class="row card-footer text-right">
      <div class="col-12">
        <a class="btn btn-secondary btn-share btn-sm" data-link="{{ url('stock/'.$data['stock']->id) }}" href="#"><i class="fa fa-share-alt-square"></i> @lang('legend.share')</a>
      </div>
    </div>
  </div>
</div>
@endsection