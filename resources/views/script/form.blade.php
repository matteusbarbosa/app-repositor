<link href="{{ asset('dist/css/form.css') }}" rel="stylesheet" async>
<script src="{{ asset('vendor/jQuery-Validation-Engine-master/js/languages/jquery.validationEngine-pt_BR.js') }}" charset="utf-8" defer></script>
<script src="{{ asset('vendor/jQuery-Validation-Engine-master/js/jquery.validationEngine.js') }}" charset="utf-8" defer></script>
<link rel="stylesheet" href="{{ asset('vendor/jQuery-Validation-Engine-master/css/validationEngine.jquery.min.css') }}"/>
<!--<script
  src="https://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>-->
  <script src="{{ asset('vendor/jQuery-Mask-Plugin-master/dist/jquery.mask.min.js') }}"></script>
  <script src="{{ asset('vendor/multiple-select/multiple-select.js') }}"></script>
  <script>
    $(document).ready(function(){
      var ls = localStorage;
      if($("form.validate").length > 0){
        $("form.validate").validationEngine('attach',  { promptPosition: "topLeft" });
      }
      $('.date').mask('00/00/0000');
      $('.mask-money').mask('0.000.000,00', {reverse: true});
      var phone_behavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
      },
      phone_options = {
        onKeyPress: function (val, e, field, phone_options) {
          field.mask(phone_behavior.apply({}, arguments), phone_options);
        }
      };
      $('.phone').mask(phone_behavior, phone_options);
      $('.date').mask('00/00/0000');
      $('.mask-percent').mask('000,00', {reverse: true});
      $('.mask-cnpj').mask('00.000.000/0000-00');
      $('.mask-cpf').mask('000.000.000-00');
      $('.phone').change(function(){
        if($(this).val().length >= 8)
        ls.setItem('phone', $(this).val());
      });
      if(ls.getItem('phone') != null){
        $('.phone').val(ls.getItem('phone'));
      }
      
      $('select[multiple=""]').multipleSelect({
          multiple: true,
          openOnHover: true,
          formatSelectAll: function(){
            return "{{ __('legend.select-all') }}";
          },
          formatAllSelected: function(){
            return "{{ __('legend.all-selected') }}";
          },
          formatCountSelected: function(count,total){
            return count + ' / ' + total + " {{ __('legend.selected') }}";
          },
          formatNoMatchesFound: function(){
            return "{{ __('legend.no-matches-found') }}";
          }
        });

          $('body').on('change', '.custom-file-input', function(){
           $(this).next().text($(this).prop('files')[0].name);
          }); 

    });
  </script>