@extends('layouts.manage')

@section('title', __('report.view_best_selling_set'))

@section('header')
@parent

<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/report.css') }}">

@include('script.datatable')
<script>
    $(document).ready( function () {
        var list = $('table#list').DataTable({
            "language": {!! get_datatable_lang($lang_object) !!},
            order: [[ 1, "desc" ]],
            paging: true,
            searching: true,
            dom: 'Bfrtip',
    buttons: [
        {
            extend: 'print',
            text: 'Imprimir'
        },
        {
            extend: 'excel',
            text: 'Excel .xls'
        },
 'pdf'
    ]
        });
    } );
</script>

@endsection

@section('content')
<div class="container">
<h1>@lang('report.view_best_selling_set')</h1>

@if($data['rows']->count() > 0)
<table id="list">
    <thead>
        <tr>
            <th>{{ trans_choice('product.product', 2) }}</th>
            <th>@lang('product.times-requested')</th>
        </tr>
    </thead>
@foreach($data['combinations'] as $combination => $content)
<tr>
    <td>
        @foreach($content['product_names'] as $k => $title)

        <a href="{{ url('/admin/stock/'.$content['object_ids'][$k].'/edit') }}">{{ $title }}</a>

        @if(isset($content['product_names'][$k+1]))
        +
        @endif
        @endforeach
  
    </td>
    <td>{{ $content['times_requested'] }}</td>
</tr>
@endforeach
</table>
@else
<div class="alert alert-warning">
    @lang('report.empty-report')
</div>
@endif
</div>
@endsection