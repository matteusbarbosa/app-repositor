@extends('layouts.manage')

@section('title', __('report.view_search_empty_result'))

@section('header')
@parent

<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/report.css') }}">

@include('script.datatable')
<script>
    $(document).ready( function () {
        var list = $('table#list').DataTable({
            "language": {!! get_datatable_lang($lang_object) !!},
            order: [[ 0, "desc" ]],
            paging: true,
            searching: true,
            dom: 'Bfrtip',
    buttons: [
        {
            extend: 'print',
            text: 'Imprimir'
        },
        {
            extend: 'excel',
            text: 'Excel .xls'
        },
 'pdf'
    ]
        });
    } );
</script>

@endsection

@section('content')
<div class="container">
<h1>@lang('report.view_search_empty_result')</h1>

@if($data['rows']->count() > 0)
<table id="list">
    <thead>
        <tr>
            <th>Total</th>
            <th>@lang('legend.search-term')</th>
        </tr>
    </thead>
@foreach($data['rows'] as $r)
<tr>
    <td>{{ $r['total'] }}</td>
    <td>{{ $r['term'] }}</td>
</tr>
@endforeach
</table>
@else
<div class="alert alert-warning">
    @lang('report.empty-report')
</div>
@endif
</div>
@endsection