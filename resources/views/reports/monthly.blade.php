@extends('layouts.manage')

@section('title', trans('object.reports'))

@section('header')
@parent
<link rel="stylesheet" href="{{ asset('dist/css/reports.css') }}" >
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-4">
            <div class="card">
                <div class="card-body"></div>
                <div class="card-footer"></div>
            </div>
        </div>
    </div>
</div>

@endsection