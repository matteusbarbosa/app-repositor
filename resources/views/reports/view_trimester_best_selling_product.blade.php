@extends('layouts.manage')

@section('title', __('report.view_trimester_best_selling_product'))

@section('header')
@parent

<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/report.css') }}">

@include('script.datatable')
<script>
    $(document).ready( function () {
        var list = $('table#list').DataTable({
            "language": {!! get_datatable_lang($lang_object) !!},
            order: [[ 2, "desc" ], [ 3, "desc" ], [ 4, "desc" ], [ 5, "desc" ]],
            paging: true,
            searching: true,
            dom: 'Bfrtip',
    buttons: [
        {
            extend: 'print',
            text: 'Imprimir'
        },
        {
            extend: 'excel',
            text: 'Excel .xls'
        },
 'pdf'
    ]
        });
    } );
</script>

@endsection

@section('content')
<div class="container">
<h1>@lang('report.view_trimester_best_selling_product')</h1>

@if($data['rows']->count() > 0)
<table id="list">
    <thead>
        <tr>
            <th>{{ trans_choice('product.product', 1) }}</th>
            <th>@lang('product.details')</th>
            <th>@lang('report.trimester-1')</th>
            <th>@lang('report.trimester-2')</th>
            <th>@lang('report.trimester-3')</th>
            <th>@lang('report.trimester-4')</th>
        </tr>
    </thead>
@foreach($data['rows'] as $k => $content)
<tr>
    <td><a href="{{ url('/admin/stock/'.$content['object_id'].'/edit') }}">{{ $content['product_name'] }}</a></td>
    <td>{{ $content['details'] }}</td>
    <td>{{ $content['trimester-1'] }}</td>
    <td>{{ $content['trimester-2'] }}</td>
    <td>{{ $content['trimester-3'] }}</td>
    <td>{{ $content['trimester-4'] }}</td>
</tr>
@endforeach
</table>
@else
<div class="alert alert-warning">
    @lang('report.empty-report')
</div>
@endif
</div>
@endsection