@extends('layouts.manage')

@section('title', __('report.view_month_year_stock_item_buy_by_tag'))

@section('header')
@parent

<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/report.css') }}">

@include('script.datatable')
<script>
    $(document).ready( function () {
        var list = $('table#list').DataTable({
            "language": {!! get_datatable_lang($lang_object) !!},
            order: [[ 1, "desc" ]],
            paging: true,
            searching: true,
            dom: 'Bfrtip',
    buttons: [
        {
            extend: 'print',
            text: 'Imprimir'
        },
        {
            extend: 'excel',
            text: 'Excel .xls'
        },
 'pdf'
    ]
        });
    } );
</script>

@endsection

@section('content')
<div class="container">
<h1>@lang('report.view_month_year_stock_item_buy_by_tag')</h1>

@if($data['rows']->count() > 0)
<table id="list">
    <thead>
        <tr>
            <th>Slug</th>
            <th>Total</th>
            <th>@lang('calendar.month')</th>
            <th>@lang('calendar.year')</th>
        </tr>
    </thead>
@foreach($data['rows'] as $r)
<tr>
    <td>{{ $r['slug'] }}</td>
    <td>{{ $r['total'] }}</td>
    <td>{{ $r['month'] }}</td>
    <td>{{ $r['year'] }}</td>
</tr>
@endforeach
</table>
@else
<div class="alert alert-warning">
    @lang('report.empty-report')
</div>
@endif
</div>
@endsection