@extends('layouts.manage')

@section('title', __('report.view_stock_never_sold'))

@section('header')
@parent

<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/report.css') }}">

@include('script.datatable')
<script>
    $(document).ready( function () {
        var list = $('table#list').DataTable({
            "language": {!! get_datatable_lang($lang_object) !!},
            order: [[ 1, "desc" ]],
            paging: true,
            searching: true,
            dom: 'Bfrtip',
    buttons: [
        {
            extend: 'print',
            text: 'Imprimir'
        },
        {
            extend: 'excel',
            text: 'Excel .xls'
        },
 'pdf'
    ]
        });
    } );
</script>

@endsection

@section('content')
<div class="container">
<h1>@lang('report.view_stock_never_sold')</h1>

@if($data['rows']->count() > 0)
<table id="list">
    <thead>
        <tr>
            <th>@lang('product.id')</th>
            <th>{{ trans_choice('product.promotion', 2) }}</th>
            <th>@lang('product.title')</th>
            <th>{{ trans_choice('product.sold', 2) }}</th>
            <th>@lang('product.created_at')</th>
        </tr>
    </thead>
@foreach($data['rows'] as $r)
<tr>
    <td>{{ $r['id'] }}</td>
    <td>{{ $r['promo_count'] }}</td>
    <td><a href="{{ url('/admin/stock/'.$r['id'].'/edit') }}">{{ $r['title'] }}</a></td>
    <td>{{ $r['item_count'] }}</td>
    <td>{{ $r['created_at'] }}</td>
</tr>
@endforeach
</table>
@else
<div class="alert alert-warning">
    @lang('report.empty-report')
</div>
@endif
</div>
@endsection