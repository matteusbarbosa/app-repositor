@extends('layouts.manage')

@section('title', __('report.available-reports'))

@section('header')
@parent
<link rel="stylesheet" href="{{ asset('dist/css/reports.css') }}" >
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col">
            <ul class="list-group mt-4">
                <li class="list-group-item"><a href="{{ url('admin/report/vendas-por-categoria') }}">@lang('report.view_month_year_stock_item_buy_by_tag')</a></li>
                <li class="list-group-item"><a href="{{ url('admin/report/vendas-por-status') }}">@lang('report.view_month_year_stock_item_buy_by_status')</a></li>
                <li class="list-group-item"><a href="{{ url('admin/report/usuarios-cadastrados') }}">@lang('report.view_month_year_user_join_count')</a></li>
                <li class="list-group-item"><a href="{{ url('admin/report/produtos-nunca-vendidos') }}">@lang('report.view_stock_never_sold')</a></li>
                <li class="list-group-item"><a href="{{ url('admin/report/atendente-mais-ativo') }}" >@lang('report.view_most_active_attendant')</a></li>
                <li class="list-group-item"><a href="{{ url('admin/report/conjunto-mais-vendido') }}">@lang('report.view_best_selling_set')</a></li>
                <li class="list-group-item"><a href="{{ url('admin/report/product-mais-vendido-por-trimestre') }}">@lang('report.view_trimester_best_selling_product')</a></li>
                <li class="list-group-item"><a href="{{ url('admin/report/product-vendido-em-maior-quantidade-por-trimestre') }}">@lang('report.view_trimester_highest_quantity_selling_product')</a></li>
                <li class="list-group-item"><a href="{{ url('admin/report/pesquisas-sem-resultado') }}">@lang('report.view_search_empty_result')</a></li>
            </ul>
        </div>
    </div>
</div>

@endsection