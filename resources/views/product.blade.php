@extends('layouts.public')

@section('title', $data['product']->title)
@section('description', __('seo.product-description', ['app_name' => config('app.name'),'title' =>  $data['product']->title, 'details' => $data['product']->details]))

@section('header')
@parent

<script src="{{ asset('dist/js/cart-min.js') }}" defer></script>
<script src="{{ asset('dist/js/buy-min.js') }}" defer></script>
<script src="{{ asset('dist/js/shop-min.js') }}" defer></script>

<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/product.css') }}">

@endsection

@section('content')

<div class="mt-4 container">
  <div class="t-section bg-red mt-4">
    <h4>{{ $data['product']->title }}</h4>
  </div>
  
  <div class="card main">
    <div class="row card-body">
      <div class="col-md-6">
        <div class="d-block d-md-none my-4"></div>
        <figure class="m-0">
          <img src="{{ asset($data['product']->image->url) }}" class="img-fluid img m-auto d-block img-view w-50"> 
        </figure>
        @include('partials.modal-gallery')
      </div>
      <div class="col-md-6">
        @if(isset($data['stock_tags']))
        <ul class="list-inline">
          @foreach($data['stock_tags'] as $k => $tag)
          <li class="list-inline-item"><span class="badge badge-secondary">{{ $tag->title }}</span></li> 
          @endforeach
        </ul>
        @endif     
        <blockquote>{{ $data['product']->details }}</blockquote>
      </div>
    </div>

  </div>
</div>





<div class="container">
  @if(isset($data['stocks']) )
  <div class="t-section mt-4">
    <h4>{{ trans('legend.view-stock-options') }}</h4>
  </div>
  
  <div class="row text-center">
    <div class="col-12 carousel-1">
        @foreach($data['stocks'] as $stock)
        <div>
          <div class="card product">
            <div class="card-img-top">
              <div class="caption">{{ __('item.'.$stock->type->slug) }}</div>
             <figure>
               @if(!empty($stock->file_id))
                <img class="img-fluid img mx-auto d-block" data-lazy="{{ asset($stock->image->url) }}" alt="{{ $stock->title }}">
                @else
                <img class="img-fluid img mx-auto d-block" data-lazy="{{ asset($stock->product->image->url) }}" alt="{{ $stock->title }}">
              @endif
              </figure>
              <div class="info-shelf">
  
                @php 
  
                $stock_fields = $stock->fields()->whereNotNull('field.shop_display_at')->get();
  
                @endphp
                @foreach($stock_fields as $f)
                  {!! \Helpers\Visual::displayStockField($f) !!}
                @endforeach
  
              </div>
            </div>
            <div class="card-body">
              <p class="card-title">{{ $stock->product->title }}</p>
            </div>
            <div class="card-footer">
              @cannot('manage')
        
                @include('partials.btn-cart-add')
              @endcannot
         
              @if(!empty($v->product))
               <a href="{{ url('product/'.$v->product_id) }}" class="btn btn-link" title="{{ __('legend.product-show') }}" >+{{ __('legend.product-show') }}</a>
              @endif
  
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div> 
@endif

@include('partials.btn-fixed-share')

</div>

@endsection