@extends('layouts.public')

@section('title', trans('legend.buy-list'))

@section('header')
@parent
<script src="{{ asset('dist/js/buy-min.js') }}" defer></script>
<script src="{{ asset('dist/js/cart-min.js') }}" defer></script>
<script src="{{ asset('dist/js/shop-min.js') }}" defer></script>

@endsection

@section('content')

<div class="container mb-4">
    <div class="row">
        <div class="col-12">
            @if(isset($data['buy_list']))
<div class="table-responsive">
  <table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">Pedido</th>
        <th scope="col" class="">Atendente</th>
        <th scope="col" class="">Data <br>e Hora</th>
        <th scope="col" class="">Status</th>
        <th scope="col" class="">Ações</th>
        <td scope="col" class="d-none"></td>
      </tr>
    </thead>
    <tbody>
      @if(isset($data['buy_list']) && $data['buy_list']->count() > 0)
      @foreach ($data['buy_list'] as $k => $v)
      <tr class="buy">
        <td>#{{ $v->id }}</td>
        <td>@if(!empty($v->getLastStatus()->pivot->attendant)){{ $v->getLastStatus()->pivot->attendant->name }}@endif</td>
        <td>{{ Carbon\Carbon::parse($v->created_at)->format('d/m/Y H:i:s') }}</td>
        <td>@if(!empty($v->getLastStatus())){{ $v->getLastStatus()->title }}@endif</td>
        <td><a href="{{ url('/buy/'.$v->id) }}"><i class="fa fa-eye"></i></a></td>
        <td class="d-none"><input type="hidden" name="id[]" value=""></td>
      </tr>
      @endforeach
      @endif
    </tbody>
  </table>
</div>

@else
<div class="cart-empty text-center">
        <div class="alert alert-info mt-3 ">@lang('legend.results-zero')</div>
        <a href="{{ url('/') }}">@lang('object.catalog')</a>
        </div>
@endif

</div>
</div>

</div>

@endsection
