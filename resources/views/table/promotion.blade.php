@extends('layouts.public')

@section('title', __('crud.manage', ['object' => trans_choice('object.promotion',2)]))

@section('header')
@parent
@include('script.datatable')
<script src="{{ asset('dist/js/cart-min.js') }}"></script>
<script src="{{ asset('dist/js/promotion-min.js') }}"></script>
@can('manage')
<script src="{{ asset('dist/js/notification-min.js') }}"></script>
<script src="{{ asset('dist/js/manage-min.js') }}"></script>
@endcan

<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/promotions.css') }}">

<script>
    $(document).ready( function () {
        $('table#list').DataTable({
            "language": {!! get_datatable_lang($lang_object) !!},
            searching: true
      
        });
    } );
</script>


@endsection

@section('content')

<div class="container mb-4">
        <div class="t-section bg-red mt-4">
                <h4>@lang('crud.manage', ['object' => trans_choice('object.promotion',1)])</h4>
        </div>
        <div class="row mb-3">
                <div class="col-md-12">
                    <div class="btn-group float-right">
                        <a href="{{ url('/admin/promotion/create') }}" class="btn btn-secondary">@lang('crud.add', ['object' => trans_choice('object.promotion', 1)])</a>
                    </div>
                </div>
            </div>
            
        <div class="row">
            <div class="col-12">
                @if($data['promotions']->count() > 0)
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="list">
                        <thead>
                            <tr>
                                <th scope="col" class="d-none d-md-table-cell">Img </th>
                                <th scope="col">Tipo</th>
                                <th scope="col">Conteúdo</th>
                                <th class="text-center" >Ações</th>
                                <th class="d-none">ID</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data['promotions'] as $k => $p)
                            <tr class="promotion">
                                <td class="d-none d-md-table-cell"><img src="{{ $p->object->image != null ? url($p->object->image->url) : asset('img/placeholder.jpg')  }}" class="img-fluid img d-block m-auto" style="height:48px;"> </td>
                                <td>{{ __('item.'.$p->object->type->slug) }}</td>
                                    <td class="title">{{ $p->title }}</td>
                                    <td>
                                        <ul class="list-inline d-flex justify-content-around">
                                            <li class="list-inline-item">
                                                <form method="POST" action="{{ url('api/notifications/promotion') }}">
                                                    @csrf
                                                    <input type="hidden" name="promotion_id" value="{{ $p->id }}">
                                                    @if($p->sales_channel_id != null)
                                                        <input type="hidden" name="sales_channel_id" value="{{ $p->sales_channel_id }}">
                                                    @endif
                                                    <button title="Notificar usuários" type="submit" class="btn btn-warning"><i class="fa fa-wifi"></i></button>
                                                </form>
                                            </li>
                                            <li class="list-inline-item"><a href="{{ url('admin/promotion/'.$p->id.'/edit') }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a></li>
                                            <li class="list-inline-item"><button class="btn btn-sm btn-danger btn-delete" data-id="{{ $p->id }}" type="button"><i class="fa fa-remove"></i></button></li>
                                        </ul>
                                    </td>
                                    <td class="d-none"><input type="hidden" name="id[]" value="{{ $p->id }}"></td>
                                </tr>
                                @endforeach
                                
                            </tbody>
                            
                        </table>
                        
                    </div>
                    @else
                    <div class="alert alert-info mt-2">
                            <p>{{ __('legend.results-zero') }}</p>
                        </div>
                    @endif
                    
                </div>
                
            </div>
   
        <div class="col mb-2">
            <hr>
            
        </div>
    </div>
</div>




@php
$o_slug = 'promotion';
$o_endpoint = 'promotion';
@endphp
@include('partials.modal-delete')

@endsection