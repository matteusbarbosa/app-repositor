@extends('layouts.manage')
@section('title', __('crud.manage', ['object' => trans_choice('product.field', 2)]))
@section('header')
@parent
@include('script.datatable')
<link rel="stylesheet" href="{{ asset('dist/css/field.css') }}" >
<script>
    $(document).ready( function () {
        var list = $('table#list').DataTable({
            "language": {!! get_datatable_lang($lang_object) !!},
            paging: true,
            searching: true,
            "columnDefs": [
            { "orderable": false, "targets": [1,2] }
            ],
            dom: 'Bfrtip',
    buttons: [
        {
            extend: 'print',
            text: 'Imprimir'
        },
        {
            extend: 'excel',
            text: 'Excel .xls'
        },
 'pdf'
    ]
        });
    } );
</script>
@endsection
@section('content')
<div class="container mt-3">
    <div class="t-section bg-red mt-4">
            <h4>@lang('crud.manage', ['object' => trans_choice('product.field', 2)])</h4>
          </div>
    <div class="row mb-3">
        <div class="col-md-12">
            <div class="btn-group float-right">
                <a href="{{ url('/admin/field/create') }}" class="btn btn-secondary">@lang('crud.add', ['object' => trans_choice('product.field', 1)])</a>
            </div>
        </div>
    </div>
    
    <div class="row mb-3">
        <div class="col-md-12">
                @if(isset($data['fields']))
            <div id="export_buttons"></div>
            <table id="list" class="display">
                <thead>
                    <tr>
                        <th scope="col">@lang('legend.identification')</th>
                        <th>
                            Ações
                        </th>  
                        <th class="d-none"> </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data['fields'] as $k => $f)
                    <tr class="field">
                        <td class="title">{{ $f->title.'('.$f->slug.')' }}</td>
                        <td>
                            <ul class="list-inline d-flex justify-content-around">
                                <li class="list-inline-item"><a href="{{ url('admin/field/'.$f->id.'/edit') }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a></li>
                                <li class="list-inline-item"><button class="btn btn-danger btn-sm btn-delete" data-id="{{ $f->id }}" type="button"><i class="fa fa-remove"></i></button></li>
                            </ul>
                        </td>
                        <td class="d-none"><input type="hidden" name="id[]" value="{{ $f->id }}"></td>
                    </tr>
                    @endforeach
                   
                </tbody>
            </table>
            @else
            <div class="alert alert-info mt-2">
                <p>{{ __('legend.results-zero') }}</p>
            </div>
            @endif
        </div>
    </div>
</div>
      

@php
$o_slug = 'field';
$o_endpoint = 'field';
@endphp
@include('partials.modal-delete')


@endsection