<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col" class="">{{ trans('legend.zipcode') }}</th>
                <th scope="col" class="">{{ trans('legend.title') }}</th>
                <th scope="col" class="">{{ trans('legend.street') }}</th>
                <th scope="col" class="">{{ trans('legend.number') }}</th>
                <th scope="col" class="">Ações</th>
                <td scope="col" class="d-none"></td>
            </tr>
        </thead>
        <tbody>
            @if(isset($data['address_list']) && $data['address_list']->count() > 0)
            @foreach ($data['address_list'] as $k => $v)
            <tr class="address">
                <td>{{ $v->zipcode }}</td>
                <td>{{ $v->title }}</td>
                <td>{{ $v->street }}</td>
                <td>{{ $v->number }}</td>
                <td><a href="{{ url('/address/'.$v->id) }}"><i class="fa fa-eye"></i></a></td>
                <td class="d-none"><input type="hidden" name="id[]" value=""></td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>