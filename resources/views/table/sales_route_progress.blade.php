@extends('layouts.manage')
@section('title', __('crud.manage', ['object'=> trans_choice('object.sales_route_progress', 2)]))
@section('header')
@parent
@include('script.datatable')
<link rel="stylesheet" href="{{ asset('dist/css/sales_route_progress.css') }}" >
<script>
    $(document).ready( function () {
        var list = $('table#list').DataTable({
            "language": {!! get_datatable_lang($lang_object) !!},
            paging: true,
            searching: true,
            "columnDefs": [
            { "orderable": false, "targets": [2,3] }
            ],
            dom: 'Bfrtip',
    buttons: [
        {
            extend: 'print',
            text: 'Imprimir'
        },
        {
            extend: 'excel',
            text: 'Excel .xls'
        },
 'pdf'
    ]
        });
    } );
</script>
@endsection
@section('content')
<div class="container mt-3">
    <div class="t-section bg-red mt-4">
            <h4>{{ __('crud.manage', ['object'=> trans_choice('object.sales_route_progress', 2)]) }}</h4>
          </div>
    <div class="row mb-3">
        <div class="col-md-12">
            <div class="btn-group float-right">
                <a href="{{ url('/admin/sales_route_progress/create') }}" class="btn btn-secondary">{{ __('crud.add', ['object'=> trans_choice('object.sales_route_progress', 1)])  }}</a>
            </div>
        </div>
    </div>
    
    <div class="row mb-3">
        <div class="col-md-12">
      
                    @if(isset($data['sales_progress']) && count($data['sales_progress']) > 0)
                    @foreach ($data['sales_progress'] as $k => $srp)
                    @if(!empty($srp->address))
                        <h2>{{ $srp->address->street }}</h2>
                    @endif
                    <table class="table table-bordered">
                        @foreach ($srp->progress as $k => $p)
                        <tr>
                            <td class="title">{{ $p->user_name }}</td>
                            <td class="detail"><span class="badge badge-{{ $p->status->class }}">{{ $p->status->title }}</span></td>
                            <td><a href="{{ path_admin('sales_route_progress/'.$p->id) }}" class="btn btn-info"><i class="fa fa-edit"></i></a></td>
                        </tr>
                        @endforeach
                    </table>
                    @endforeach
                    @else
                    <div class="alert alert-info mt-2">
                        <p>{{ __('legend.results-zero') }}</p>
                    </div>
                    @endif
               
</div>
</div>
</div>
      
@php
$o_slug = 'sales_route_progress';
$o_endpoint = 'sales_route_progress';
@endphp
@include('partials.modal-delete')

@endsection