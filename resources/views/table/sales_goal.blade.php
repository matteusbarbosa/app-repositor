@extends('layouts.manage')
@section('title', __('crud.manage-sales_goal'))
@section('header')
@parent
@include('script.datatable')
<link rel="stylesheet" href="{{ asset('dist/css/sales_goal.css') }}" >
<script>
    $(document).ready( function () {
        var list = $('table#list').DataTable({
            "language": {!! get_datatable_lang($lang_object) !!},
            paging: true,
            searching: true,
            "columnDefs": [
            { "orderable": false, "targets": [2,3] }
            ],
            dom: 'Bfrtip',
    buttons: [
        {
            extend: 'print',
            text: 'Imprimir'
        },
        {
            extend: 'excel',
            text: 'Excel .xls'
        },
 'pdf'
    ]
        });
    } );
</script>
@endsection
@section('content')
<div class="container mt-3">
    <div class="t-section bg-red mt-4">
            <h4>@lang('crud.manage-sales_goal')</h4>
          </div>
    <div class="row mb-3">
        <div class="col-md-12">
            <div class="btn-group float-right">
                <a href="{{ url('/admin/sales_goal/create') }}" class="btn btn-secondary">@lang('crud.add-sales_goal')</a>
            </div>
        </div>
    </div>
    
    <div class="row mb-3">
        <div class="col-md-12">
            <div id="export_buttons"></div>
            <table id="list" class="display">
                <thead>
                    <tr>
                        <th scope="col">@lang('legend.identification')</th>
                        <th scope="col">@lang('legend.details')</th>
                        <th>
                            Ações
                        </th>  
                        <th class="d-none"> </th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($data['sales_goals']))
                    @foreach ($data['sales_goals'] as $k => $t)
                    <tr class="sales_goal">
                        <td class="title">{{ $t->title.'('.$t->slug.')' }}</td>
                        <td class="detail">{{ $t->details }}</td>
                        <td>
                            <ul class="list-inline d-flex justify-content-around">
                                <li class="list-inline-item"><a href="{{ url('admin/sales_goal/'.$t->id.'/edit') }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a></li>
                                <li class="list-inline-item"><button class="btn btn-danger btn-sm btn-delete" data-id="{{ $t->id }}" type="button"><i class="fa fa-remove"></i></button></li>
                            </ul>
                        </td>
                        <td class="d-none"><input type="hidden" name="id[]" value="{{ $t->id }}"></td>
                    </tr>
                    @endforeach
                    @else
                    <div class="alert alert-info mt-2">
                        <p>{{ __('legend.results-zero') }}</p>
                    </div>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
      
@php
$o_slug = 'sales_goal';
$o_endpoint = 'sales_goal';
@endphp
@include('partials.modal-delete')

@endsection