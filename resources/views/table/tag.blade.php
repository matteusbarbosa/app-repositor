@extends('layouts.manage')
@section('title', __('crud.manage', ['object' => trans_choice('object.tag', 2)]))
@section('header')
@parent
@include('script.datatable')
<link rel="stylesheet" href="{{ asset('dist/css/tag.css') }}" >
<script>
    $(document).ready( function () {
        var list = $('table#list').DataTable({
            "language": {!! get_datatable_lang($lang_object) !!},
            paging: true,
            searching: true,
            "columnDefs": [
                { "orderable": false, "targets": [3] }
            ],
            dom: 'Bfrtip',
    buttons: [
        {
            extend: 'print',
            text: 'Imprimir'
        },
        {
            extend: 'excel',
            text: 'Excel .xls'
        },
 'pdf'
    ]
        });
    } );
</script>
@endsection
@section('content')
<div class="container-fluid mt-3">
    <div class="t-section bg-red mt-4">
            <h4>@lang('crud.manage', ['object' => trans_choice('object.tag', 2)])</h4>
          </div>
    <div class="row mb-3">
        <div class="col-md-12">
            <div class="btn-group float-right">
                <a href="{{ path_admin('tag/create') }}" class="btn btn-secondary">@lang('product.tag-add')</a>
            </div>
        </div>
    </div>
    
    <div class="row mb-3">
        <div class="col-md-12">
            <div id="export_buttons"></div>
            <table id="list" class="display">
                <thead>
                    <tr>
                        <th scope="col" class="d-none d-md-table">@lang('legend.image')</th>
                        <th scope="col">@lang('legend.identification')</th>
                        <th scope="col" class="text-center">@lang('legend.items-amount')</th>
                        <th>
                            Ações
                        </th>  
                        <th class="d-none"> </th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($data['tags']))
                    @foreach ($data['tags'] as $k => $t)
                    <tr class="tag">
                        <td class="d-none d-md-table-cell"><img src="{{ $t->image != null ? url($t->image->url) : asset('img/item-default.png') }}" class="img-fluid img m-auto d-block" style="height:48px;"> </td>
                        <td class="title">{{ $t->title.'('.$t->slug.')' }}</td>
                        <td>{{ $t->stocks->count() }}</td>
                        <td>
                            <ul class="list-inline d-flex justify-content-around">
                                <li class="list-inline-item"><a href="{{ url('admin/tag/'.$t->id.'/edit') }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a></li>
                                <li class="list-inline-item"><button class="btn btn-danger btn-sm btn-delete" data-id="{{ $t->id }}" type="button"><i class="fa fa-remove"></i></button></li>
                            </ul>
                        </td>
                        <td class="d-none"><input type="hidden" name="id[]" value="{{ $t->id }}"></td>
                    </tr>
                    @endforeach
                    @else
                    <div class="alert alert-info mt-2">
                        <p>{{ __('legend.results-zero') }}</p>
                    </div>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
      
@php
$o_slug = 'tag';
$o_endpoint = 'tag';
@endphp
@include('partials.modal-delete')

@endsection