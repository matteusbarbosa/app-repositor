@extends('layouts.manage')
@section('title', __('crud.manage', ['object' => trans_choice('object.stock', 2)]))
@section('header')
@parent
@include('script.datatable')
<link rel="stylesheet" href="{{ asset('dist/css/stock.css') }}" >
<script>
    $(document).ready( function () {
        var list = $('table#list').DataTable({
            "language": {!! get_datatable_lang($lang_object) !!},
            paging: true,
            searching: true,
            "columnDefs": [
            { "orderable": false, "targets": [4] }
            ],
            dom: 'Bfrtip',
    buttons: [
        {
            extend: 'print',
            text: 'Imprimir'
        },
        {
            extend: 'excel',
            text: 'Excel .xls'
        },
 'pdf'
    ]
        });
   
    } );
</script>
@endsection
@section('content')
<div class="container mt-3">
        <div class="t-section bg-red mt-4">
                <h4>@lang('crud.manage', ['object' => __('object.stock')])</h4>
              </div>
              <div class="alert alert-info">@lang('tip.stock-create')</div>
    
    <div class="row mb-3">
        <div class="col-md-12">
            <div id="export_buttons"></div>
            <table id="list" class="display">
                <thead>
                    <tr>
                        <th scope="col" class="d-none d-md-table-cell">Imagem </th>
                        <th scope="col">Identificação</th>
                        <th scope="col" class="text-center">@lang('product.details')</th>
                        <th scope="col" class="text-center">Qtd</th>
                        <th>
                            Ações
                        </th>  
                        <th class="d-none"> </th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($data['stocks']))
                    @foreach ($data['stocks'] as $k => $s)
                    <tr class="stock">
                        <td class="d-none d-md-table-cell"><img src="{{ $s->image != null ? url($s->image->url) : asset('img/item-default.png') }}" class="img-fluid img m-auto d-block img-view" style="height:48px;"> </td>
                        <td class="title">{{ $s->product['title'] }}</td>
                        <td>{{ $s->fieldsToString(3) }}</td>
                        <td>{{ $s->quantity }}</td>
                        <td>
                            <ul class="list-inline d-flex justify-content-around">
                                <li class="list-inline-item"><a href="{{ url('admin/stock/'.$s->id.'/edit') }}" class="btn btn-info"><i class="fa fa-edit"></i></a></li>
                                <li class="list-inline-item"><button class="btn btn-danger btn-delete" data-id="{{ $s->id }}" type="button"><i class="fa fa-remove"></i></button></li>
                            </ul>
                        </td>
                        <td class="d-none"><input type="hidden" name="id[]" value="{{ $s->id }}"></td>
                    </tr>
                    @endforeach
                    @else
                    <div class="alert alert-info mt-2">
                        <p>{{ __('legend.results-zero') }}</p>
                    </div>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

@php
$o_slug = 'stock';
$o_endpoint = 'stock';
@endphp
@include('partials.modal-delete')
@include('partials.modal-gallery')

@endsection