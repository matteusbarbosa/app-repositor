@extends('layouts.manage')
@section('title', __('crud.manage', ['object' => trans_choice('object.user', 2)]))
@section('header')
@parent
@include('script.datatable')
<link rel="stylesheet" href="{{ asset('dist/css/user.css') }}" >
<script>
    $(document).ready( function () {
        var list = $('table#list').DataTable({
            "language": {!! get_datatable_lang($lang_object) !!},
            paging: true,
            searching: true,
            "columnDefs": [
            { "orderable": false, "targets": [2,3] }
            ],
            dom: 'Bfrtip',
    buttons: [
        {
            extend: 'print',
            text: 'Imprimir'
        },
        {
            extend: 'excel',
            text: 'Excel .xls'
        },
 'pdf'
    ]
        });
    } );
</script>
@endsection
@section('content')
<div class="container mt-3">
        <div class="t-section bg-red mt-4">
                <h4>@lang('crud.manage', ['object' => trans_choice('object.user', 2)])</h4>
              </div>

    <div class="row mb-3">
        <div class="col-md-12">
            <div class="btn-group float-right">
                <a href="{{ url('/admin/user/create') }}" class="btn btn-secondary">@lang('crud.add', ['object' => trans_choice('object.user', 1)])</a>
            </div>
        </div>
    </div>
    
    <div class="row mb-3">
        <div class="col-md-12">
            <div id="export_buttons"></div>
            <table id="list" class="display">
                <thead>
                    <tr>
                        <th scope="col" class="d-none d-md-table-cell">@lang('legend.phone')</th>
                        <th scope="col">@lang('legend.last-meaningful-activity')</th>
                        <th>
                            Ações
                        </th>  
                        <th class="d-none"> </th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($data['users']))
                    @foreach ($data['users'] as $k => $u)
                    <tr class="user">
                        <td class="title">{{ $u->name }}</td>
                        <td>@if(!empty($u->last_meaningful_activity->created_at)){{ $u->last_meaningful_activity->created_at->format('d/m/Y H:i') }}@endif</td>
                        <td>
                            <ul class="list-inline d-flex justify-content-around">
                                <li class="list-inline-item"><a href="{{ url('admin/user/'.$u->id.'/edit') }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a></li>
                                <li class="list-inline-item"><button class="btn btn-danger btn-sm btn-delete" data-id="{{ $u->id }}" type="button"><i class="fa fa-remove"></i></button></li>
                            </ul>
                        </td>
                        <td class="d-none"><input type="hidden" name="id[]" value="{{ $u->id }}"></td>
                    </tr>
                    @endforeach
                    @else
                    <div class="alert alert-info mt-2">
                        <p>{{ __('legend.results-zero') }}</p>
                    </div>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
      
@php
$o_slug = 'user';
$o_endpoint = 'user';
@endphp
@include('partials.modal-delete')

@endsection