@extends('layouts.public')

@section('title', __('crud.manage', ['object' => trans_choice('object.product',2)]))

@section('header')
@parent
@include('script.datatable')
<link rel="stylesheet" href="{{ asset('dist/css/product.css') }}" >
<script>
    $(document).ready( function () {
        var list = $('table#list').DataTable({
            "language": {!! get_datatable_lang($lang_object) !!},
            paging: true,
            searching: true,
            "columnDefs": [
            { "orderable": false, "targets": [4] }
            ],
            dom: 'Bfrtip',
    buttons: [
        {
            extend: 'print',
            text: 'Imprimir'
        },
        {
            extend: 'excel',
            text: 'Excel .xls'
        },
 'pdf'
    ]
        });
   
    } );
</script>


@can('manage')
<script src="{{ asset('dist/js/manage-min.js') }}"></script>
@endcan
@endsection

@section('content')

<div class="container mb-4">
        <div class="t-section bg-red mt-4">
                <h4>@lang('crud.manage', ['object' => trans_choice('object.product',1)])</h4>
              </div>
  
    <div class="row mb-3">
        <div class="col-md-12">
            <div class="btn-group float-right">
                <a href="{{ url('/admin/product/create') }}" class="btn btn-secondary">@lang('crud.add', ['object' => trans_choice('object.product', 1)])</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="table-responsive">
                <table class="table table-striped cart" id="list">
                    <thead>
                        <tr>
                            <th scope="col" class="d-none d-md-table-cell">Img </th>
                            <th scope="col">ID</th>
                            <th scope="col">Título</th>
                            <th scope="col">Ações</th>
                            <th class="d-none"> </th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach ($data['product_list'] as $k => $p)
                        <tr class="product">
                            <td class="d-none d-md-table-cell"><img src="{{ $p->image != null ? url($p->image->url) : asset('img/placeholder.jpg')  }}" class="img-fluid img m-auto d-block" style="height:48px;"> </td>
                            <td>{{ $p->id }}</td>
                            <td>{{ $p->title }}</td>
                    
                            <td>
                                <ul class="list-inline">
                                    <li class="list-inline-item"><a href="{{ url('admin/product/'.$p->id.'/edit') }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a></li>

                                    <li class="list-inline-item"><button class="btn btn-sm btn-danger btn-delete" data-id="{{ $p->id }}" type="button"><i class="fa fa-remove"></i></button></li>



                                </ul>
                            </td>
                            <td class="d-none"><input type="hidden" name="id[]" value="{{ $p->id }}"></td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                        <tfoot>
                            <tr>
                                <th scope="col" class="d-none d-md-table-cell">Img </th>
                                <th scope="col">ID</th>
                                <th scope="col">Título</th>
                                <th scope="col">Ações</th>
                                <th class="d-none"> </th>
                            </tr>
                        </tfoot>
                    </table>
                    
                </div>
            </div>
        </div>
        <div class="col mb-2">
            <hr>
            
        </div>
    </div>



@php
$o_slug = 'product';
$o_endpoint = 'product';
@endphp
@include('partials.modal-delete')
      

@endsection