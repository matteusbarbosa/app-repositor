@if(!empty($data['field_list']))
<div class="row">
        <div class="col-md-6">
            <label class="field-title" for="f-{{ $sf->type }}">{{ $sf->title }} <button type="button"  class="btn remove btn-sm btn-danger">@lang('crud.delete')</button></label>
          <select name="field_id[]" class="f-field-id form-control">
            <option value="">@lang('legend.select-option')</option>
            @foreach($data['field_list'] as $id => $title)
            <option value="{{ $id }}" @if($id == $sf->id) selected @endif>{{ $title }}</option>
            @endforeach
          </select>
        </div>
        <div class="col-md-6">
          <label>{{ trans_choice('product.field-content', 1) }}</label>
          <input type="text" name="field_content[]" value="{{ $sf->pivot->content }}" class="f-field-content form-control">
        </div>
      </div>
@endif
