<div class="row form-group">
    <div class="col-md-6">
        <label class="field-title" for="f-{{ $sf->type }}">{{ $sf->title }} <button type="button"  class="btn remove btn-sm btn-danger">@lang('crud.delete')</button></label>
      <select name="field_id[]" class="f-field-id form-control">
        <option value="color">@lang('product.color')</option>
      </select>
    </div>
  <div class="col">
    <select id="f-color" name="field_content[]" class="custom-select form-control mt-4">
      <option value="">@lang('legend.select-option')</option>
      @foreach(config('instance-colors') as $rgb => $color)
      <option @if(isset($data['stock']) && !empty($data['stock']->getContent('color')) && $data['stock']->getContent('color') == $color) selected @endif>{{ $color }}</option>
      @endforeach
    </select>
  </div>
</div>