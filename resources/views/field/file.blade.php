<div class="row">
    <div class="col-md-4 p-0 ">
            <label class="field-title" for="f-{{ $sf->type }}">{{ $sf->title }} <button type="button"  class="btn remove btn-sm btn-danger">@lang('crud.delete')</button></label>
    </div>  
  <div class="col-md-8 p-0">
      @if(!empty($sf->pivot->content))
        @php
        $file = \App\File::find($sf->pivot->content);
        @endphp
            @if(!empty($file))
            <a class="btn btn-info" href="{{ url($file->url) }}" target="_blank">Download</a> 
            @endif
        @endif
    </div> 
</div>