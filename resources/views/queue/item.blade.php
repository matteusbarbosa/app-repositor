@extends('layouts.public')

@section('title', trans('object.queue') )

@section('header')
    @parent
    <script src="{{ asset('dist/js/manage-min.js') }}"></script>
    <script src="{{ asset('dist/js/dispatch-min.js') }}" async></script>
 
    <link rel="stylesheet" href="{{ asset('dist/css/queue.css') }}">

    <link href="{{ url('vendor/EasyAutocomplete/dist/easy-autocomplete.min.css') }}" rel="stylesheet">
    <link href="{{ url('vendor/EasyAutocomplete/dist/easy-autocomplete.themes.min.css') }}" rel="stylesheet">
    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="{{ url('vendor/EasyAutocomplete/dist/jquery.easy-autocomplete.min.js') }}"></script>
    
    
@endsection

@section('content')

@include('search.buy')

<div class="container my-4">
        <div class="row">
            <div class="col-md-6">
              <div class="t-section bg-red">
                <h4>Expedição</h4>
              </div>
            </div>
                <div class="col-md-6 d-flex justify-content-end">
                   <button type="button" class="play-queue btn btn-disabled" disabled><img src="{{ asset('img/loading_2.gif') }}" class="img-fluid loading" style="width:15px; height: 15px;"> <i class="fa fa-play d-none "></i>  <span class="text d-none">Retomar</span></button>
                   <button type="button" class="stop-queue btn btn-secondary"><i class="fa fa-stop"></i> Parar</button>
                </div>
            </div>
    <div class="row">
        <div class="col-12">
         
          <!-- QUEUE -->
          <div class="table-responsive">
              <table class="table table-striped" id="queue">
                <thead>
                  <tr>
                    <th scope="col">Produto</th>
                    <th scope="col">@lang('legend.requested_by')</th>
                    <th scope="col">@lang('product.quantity')</th>
                    <th scope="col">@lang('product.details')</th>
                    <th scope="col">Estoque <br>disponível</th>
                    <th scope="col" class="">Data <br>e Hora</th>
                    <th scope="col" class="">Status</th>
                  </tr>
                </thead>
                <tbody>
            
                </tbody>
              </table>
              
              
              <!-- Modal -->
              <div id="modal-status-change" class="modal fade" role="dialog">
                <div class="modal-dialog">
                  
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">@lang('legend.dispatch')</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <p>@lang('legend.dispatch-info')</p>
                      <div class="status" data-id="">
                        <div class="form-group">
                          <label>Status</label>
                            <select id="status-id" name="status" class="form-control custom-select" data-stock-id="" data-status-id="">
                                @if(!empty($data['statuses']))
                                @foreach($data['statuses'] as $k => $v)
                                <option value="{{ $k }}">{{ $v }}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label>@lang('legend.details')</label>
                          <textarea id="status-details" name="status-details" class="form-control"></textarea>
                      </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button class="btn btn-primary" id="status-update" data-dismiss="modal" type="button">@lang('legend.confirm')</button>
                      
                    </div>
                  </div>
                  
                </div>
              </div>
              
          <!-- QUEUE -->

        </div>
    </div>
</div>
@endsection