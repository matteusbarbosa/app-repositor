@extends('layouts.public')

@section('title', trans('object.queue')  )

@section('header')
@parent


<script src="{{ asset('dist/js/queue-min.js') }}"></script>
<script src="{{ asset('dist/js/manage-min.js') }}"></script>

<link rel="stylesheet" href="{{ asset('dist/css/queue.css') }}">

<link href="{{ url('vendor/EasyAutocomplete/dist/easy-autocomplete.min.css') }}" rel="stylesheet">
<link href="{{ url('vendor/EasyAutocomplete/dist/easy-autocomplete.themes.min.css') }}" rel="stylesheet">
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="{{ url('vendor/EasyAutocomplete/dist/jquery.easy-autocomplete.min.js') }}"></script>

@endsection

@section('content')

@include('search.buy')

<div class="container my-4">
  <div class="row">
    <div class="col-md-6">
      <div class="t-section bg-red">
        <h4>@lang('legend.buy-queue')</h4>
      </div>
    </div>
    <div class="col-md-6 d-flex justify-content-end">
      <button type="button" class="play-queue btn btn-disabled" disabled><img src="{{ asset('img/loading_2.gif') }}" class="img-fluid loading" style="width:15px; height: 15px;"> <i class="fa fa-play d-none "></i>  <span class="text d-none">Retomar</span></button>
      <button type="button" class="stop-queue btn btn-secondary"><i class="fa fa-stop"></i> Parar</button>
    </div>
  </div>
  <div class="row">
    <div class="col-12">
      <div class="table-responsive">
        <table class="table table-striped" id="queue">
          <thead>
            <tr>
              <th scope="col">N#</th>
              <th scope="col">Cliente</th>
              <th scope="col">Atendente</th>
              <th scope="col" class="">Data <br>e Hora</th>
            </tr>
          </thead>
          <tbody>
              <tr id="all-updated"></tr>
            
          </tbody>
        </table>                
      </div>
    </div>
  </div>
  @endsection