<div class="bs-component fixed-top navbar top">
    <div class="container-fluid p-0">
  <ul class="nav d-flex p-0">
        <li class="nav-title nav-item">
            <a href="{{ url('/') }}" class="nav-link p-0">
            <img src="{{ asset('images/launcher-icon-1x.png') }}" class="img-fluid">
            </a>
        </li>    
        @include('partials.breadcrumb')
  </ul> 
</div>
</div> 

<div class="nav-placeholder top"></div>