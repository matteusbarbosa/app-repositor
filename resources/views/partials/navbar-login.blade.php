<!-- The Modal -->
<div class="modal" id="modal-form-join">
        <div class="modal-dialog">
          <div class="modal-content">      
            <!-- Modal body -->
            <div class="modal-body">
                <?php echo file_get_contents('https://repositor.com.br/form-join.html'); ?>
            </div>
          </div>
        </div>
      </div>

<div class="bs-component fixed-bottom navbar bottom p-0">
    <div class="container-fluid">
        <ul class="row d-flex p-0">
            <li class="col-4 nav-item p-0">
                <a href="https://repositor.com.br/wiki" class="btn">
                    <i class="fa fa-question-circle"></i> <span class="d-inline-block ml-2">FAQ/Wiki</span>
                </a>
            </li>
            <li class="col-4 nav-item p-0">
                <a href="https://www.desenvolvedormatteus.com.br/suporte/" class="btn">
                    <i class="fa fa-commenting"></i> <span class="d-inline-block ml-2">Suporte</span>
                </a>
            </li>
            <li class="col-4 nav-item p-0">
                    <a href="https://repositor.com.br/wiki" class="btn"  data-toggle="modal" data-target="#modal-form-join">
                        <i class="fa fa-plus-square-o"></i> <span class="d-inline-block ml-2">Contratar</span>
                    </a>
                </li>
        </ul>
    </div>
</div>
<div class="nav-placeholder bottom"></div>