<!-- Modal -->
<div id="img-view" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
      
            <div class="modal-body p-0">
             <button type="button" class="close btn btn-secondary" data-dismiss="modal">
                 @lang('legend.close')
             </button>
             <img class="img-fluid d-block m-auto">
            </div>
        
          </div>
        </div>
      </div>