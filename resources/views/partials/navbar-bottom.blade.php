@include('partials.btn-fixed-dock')

<div class="bs-component fixed-bottom navbar bottom p-0">
  
  <div class="w-100 collapse" id="box-search">
    <form class="form-inline align-self-center d-flex" action="{{ url('search') }}" method="GET">
      <div class="input-group wrap-search mt-4 mb-2 mx-auto" style="min-width: 280px;">
        <input class="form-control " type="search" name="term" placeholder="busca de produtos">
        <button class="btn btn-secondary" type="submit"><i class="fa fa-search"></i></button>
      </div>
    </form>
  </div>
  <div class="container-fluid">
    <ul class="row d-flex justify-content-around p-0">
      
      @if(Auth::check() && Auth::user()->cannot('manage'))
      <li class="col-6 nav-item buy-status" id="buy-status" data-id="0">
        
        <div class="null">
          <a href="{{ url('buy') }}" class="btn">@lang('legend.buy-list-ok')</a>
        </div>
        

        <a href="{{ url('buy') }}" class="btn buy-link p-0">
            <div class="loading d-none float-left">
                <img src="{{ asset('img/loading_2.gif') }}" class="img-fluid">
              </div>
              <span class="buy-id d-none"></span>
              <span class="status d-none"></span>
        </a>
        

  
      </li>
      
      @endif
      
      
      @cannot('manage')
      
      @if(Request::segment(1) != 'cart')
      <li class="col-3 nav-item nav-cart p-0">
      <a href="{{ url('cart') }}" class="btn d-flex cart-checkout"><i class="fa fa-cart-arrow-down"></i> 
        
      
        <span class="badge badge-secondary rounded quantity ml-2 d-none">{{ Auth::check() ? Auth::user()->cart()->items->count() : '...' }}</span>
    
      </a>
    </li>
    @endif
    
    @endcan

    <li class="col-3 nav-menu @if(Request::segment(1) == 'cart') d-none @endif">
       
      <div class="btn-group dropup">
        <button type="button" class="btn btn-menu dropdown-toggle @if(Auth::check() && Auth::user()->cannot('manage') && Request::segment(1) == 'cart') d-none @endif" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fa fa-caret-square-o-up"></i>
        </button>
        <div class="dropdown-menu dropdown-menu-right">
          @if(!empty(Request::segment(1)))
          <a href="{{ url('/') }}" class="dropdown-item">Home</a>
          @endif

          @can('manage')
          
          <a href="{{ url('admin') }}" class="dropdown-item">Admin</a>
          
          @endcan
          
         <!-- <a href="{{ url('catalogo') }}" class="dropdopwn-item">Catálogo</a> -->
          <a href="{{ url('promotion') }}" class="dropdown-item">@lang('legend.promotion')</a>
          
          <a href="#" class="dropdown-item toggle-search">Pesquisa</a>
        
          
          @if(Auth::check())
          
          @cannot('manage')
          <div class="dropdown-divider"></div>
          
          <a href="{{ url('buy') }}" class="dropdown-item">Pedidos</a>

          <a href="{{ url('cart') }}" class="dropdown-item">Carrinho</a>
          
          <div class="dropdown-divider"></div>
          
          @endcannot
          
          <a href="{{ url('user/'.Auth::user()->id.'/edit') }}" class="dropdown-item">@lang('legend.account-preferences')</a>

          <a href="{{ url('logout') }}" class="dropdown-item">Sair (deslogar)</a>
          @else
          <a href="{{ url('login') }}" class="dropdown-item">Login</a>
          @endif
        </div>
      </div>
    </li>
   
    
  </ul>
</div>
</div>

<div class="nav-placeholder bottom"></div>