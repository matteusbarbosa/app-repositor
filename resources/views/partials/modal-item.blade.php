 <!-- Modal -->
 <div id="modal-item" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">@lang('product.fields-input')</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form id="item-fields">

                
                                <h4 class="product-title"></h4>
                     

                    <input type="hidden" name="stamp">
                    <input type="hidden" name="item_id">
                    @can('manage')
                    <div class="status" data-id="">
                        <div class="form-group">
                            <label>Status</label>
                            <select id="item-status-id" name="status_id" class="form-control custom-select" data-item-id="" data-status-id="">
                                @if(!empty($data['statuses']))
                                @foreach($data['statuses'] as $k => $v)
                                <option value="{{ $k }}">{{ $v }}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                        @endcan

                        <!-- stocks -->
                        <div class="form-group stock d-none">
                            <label>@lang('legend.view-stock-options')</label>
                            <select name="object_id"  class="form-control custom-select">
                                <option value="">@lang('legend.select-option')</option>
                            </select>
                        </div>
                        <!-- stocks --> 

                        <div class="form-group">
                            <label for="quantity">{{ __('legend.quantity') }}</label>
                            <input type="text" class="form-control quantity validate[custom[integer]] f-buy" name="quantity">
                        </div>
                        <div class="form-group">
                            <label>@lang('legend.details')</label>
                            <textarea name="details" class="form-control"></textarea>
                        </div>
                    </div>
                    </form>
             
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-danger remove" data-stamp="" data-item-id=""><i class="fa fa-times"></i> </button> 
                    <button class="btn btn-primary" id="btn-item-update" type="button">@lang('legend.confirm')</button>
                </div>
            </div>
            </div>
        </div>
    </div>
    <!-- end modal -->