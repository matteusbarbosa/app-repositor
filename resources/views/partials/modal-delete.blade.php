<!-- require $o_slug $o_endpoint -->
<div class="modal" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content"> 
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">{{ __('legend.delete') }} {{ trans_choice('object.'.$o_slug, 1) }}</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <form method="POST" action="{{ url('/admin/'.$o_endpoint) }}" >
                    {{ csrf_field()  }}
                    {{method_field('DELETE')}}
                    <input type="hidden" name="id">
                    <input type="hidden" value="{{  url('/admin/'.$o_endpoint) }}" name="action">
                    <div class="btn-group m-auto" style="width: 200px;display: block;">
                        <button type="button" type="button" data-dismiss="modal" class="btn btn-secondary">
                            Cancelar
                        </button>
                        <button class="btn btn-danger " type="submit">Confirmar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>