@php
$data = isset($data) ? current($data) : null;

$breadcrumbs = get_breadcrumbs($data);
@endphp
@foreach ($breadcrumbs as $breadcrumb)
<li class="nav-item separator">
    <i class="fa fa-angle-double-right"></i>
</li>
@if($breadcrumb->url && !$loop->last)
<li><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
@else
<li><a class="active">{{ $breadcrumb->title }}</a></li>
@endif
@endforeach