<div class="alert notify mt-2 alert-{{ session('flash-class') }} @if(session('flash')) d-block @endif">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true" style="font-size:20px">×</span>
    </button><strong class="text-prefix">{{ session('flash-prefix') }}</strong> <span class="text">{{ session('flash-text') }}</span>
</div>
<div class="alert mt-2 status-update alert-info">
    <div class="row">
        <div class="col-4">
            {{ trans('legend.status-updated') }}
        </div>
        <div class="col-8">
            <p class="text"></p>
        </div>
    </div>
</div>
<div class="alert mt-2 cart-added notify alert-success">
    <div class="row">
        <div class="col-4">
            <figure class="ml-2">
                <img class="img d-block img-fluid m-auto">
            </figure>
            <div class="added"><img src="{{ asset('img/checked-30.png') }}" class="img-fluid m-auto d-block"> {{ trans('legend.added') }}</div>
        </div>
        <div class="col-8">
            <p class="text"></p>
            <p class="quantity"></p>
        </div>
    </div>
</div>
<div class="alert mt-2 cart-remove alert-danger">
    <div class="row">
        <div class="col-3">
            <figure>
                <img class="img d-block img-fluid m-auto">
            </figure>
            <div class="added"><img src="{{ asset('img/checked-30.png') }}" class="img-fluid m-auto d-block"> {{ trans('legend.removed') }}</div>
        </div>
        <div class="col-9">
            <p class="text"></p>
            <p class="quantity"></p>
        </div>
    </div>
</div>

<div class="alert buy-sync alert-success">@lang('legend.sync-success')</div>
<div class="alert share-copy alert-success">@lang('legend.copy-success')</div>
@if(!empty(Request::query('cart-empty')))
<div class="alert alert-info cart-empty">@lang('legend.cart-empty')</div>
@endif
<div class="alert alert-info offline">@lang('legend.working-offline')</div>
