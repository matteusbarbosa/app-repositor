
<div class="container-fluid fixed-top buy-actions options navbar d-block">
  <div class="row">
    
    <div class="col-6 p-0">
      
      <button class="btn btn-block btn-secondary text-uppercase m-auto btn-delete" @if(!empty($data['buy']->getLastStatus()) && $data['buy']->getLastStatus()->allow_user_cancel) data-id="{{ $data['buy']->id }}" @else disabled @endif><i class="fa fa-arrow-left"></i> Cancelar #{{ $data['buy']->id }}</button>
  
 
    </div>
    
    
    <div class="col-6 p-0">
      @can('manage')
      <a class="btn btn-block btn-success text-uppercase m-auto"  href="https://web.whatsapp.com/send?phone=55{{ $data['buy']->user->phone }}&text=Ola,%20Cliente" target="_blank"><i class="fa fa-whatsapp"></i> Falar com o Cliente</a>
      @endcan
      
      @if(Auth::check() && Auth::user()->cannot('manage'))
      <a class="btn btn-block btn-success text-uppercase m-auto" href="https://web.whatsapp.com/send?phone=55{{ env('SUPPORT_NUMBER') }}&text=Sobre%20o%20buy%20numero%20#{{ $data['buy']->id }}%20Pelo%20Cliente%20{{ Auth::user()->phone }}." target="_blank"><i class="fa fa-whatsapp"></i> Falar com atendente</a>
      @endif
    </div>
  </div>
  
</div>
<div class="navbar-placeholder container" style="height: 52px"></div>


@if(isset($data['buy']))
  @php
  $o_slug = 'buy';
  $o_endpoint = 'buy';
  @endphp
  @include('partials.modal-delete')
@endif