<div class="btn-fixed-dock">
        <a class="btn btn-block btn-dark btn-shop" href="{{ url('shop') }}">
                <i class="fa fa-angle-left"></i>
            </a>
        <button type="button" class="btn btn-block btn-success text-uppercase btn-buy" href="#" id="buy-confirm">
            <i class="fa fa-check-circle-o"></i>
        </button>
 
    </div>

<div class="navbar-placeholder container" style="height: 48px"></div>

