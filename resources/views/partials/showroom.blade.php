    @foreach($data['results'] as $tag_item)
    <div class="t-section mt-4">
      <h4>{{ $tag_item->tag['title'] }}</h4>
    </div>
    <div class="row text-center">
      <div class="col-12 carousel-1">
        @foreach($tag_item->items as $k => $v)
        <div>
          <div class="card product">
            <div class="card-img-top">
              @if(!empty($v->type))
              <div class="caption">{{ __('item.'.$v->type['slug']) }}</div>
              @endif
              <figure>
                @if(!empty($v->file_id))
                <img class="img-fluid img mx-auto d-block" data-lazy="{{ asset($v->image->url) }}" alt="{{ $v->title }}">
                @else
                <img class="img-fluid img mx-auto d-block" data-lazy="{{ asset($v->product->image->url) }}" alt="{{ $v->title }}">
                @endif
              </figure>
              <div class="info-shelf">
                @php
                $stock_fields = $v->fields()->whereNotNull('field.shop_display_at')->get();
                @endphp
                @foreach($stock_fields as $f)
                {!! \Helpers\Visual::displayStockField($f) !!}
                @endforeach
              </div>
            </div>
            <div class="card-body">
              <p class="card-title">{{ $v->product->title }}</p>
            </div>
            <div class="card-footer">
              @cannot('manage')
              @php
              $stock = $v;
              @endphp
              @include('partials.btn-cart-add')
              @endcannot
              @if(!empty($v->product))
              <a href="{{ url('product/'.$v->product_id) }}" class="btn btn-link" title="{{ __('legend.product-show') }}" >+{{ __('legend.product-show') }}</a>
              @endif
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </div> 
    @endforeach