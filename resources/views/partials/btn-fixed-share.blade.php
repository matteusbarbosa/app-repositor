<script src="{{ asset('dist/js/share-min.js') }}" async></script>

<div class="btn-fixed-share btn-fixed-dock">
    <a class="btn btn-secondary btn-share btn-sm" data-link="{{ url()->current() }}" href="#">
        <i class="fa fa-share-square-o"></i>
    </a>

    <!-- The Modal -->
</div>