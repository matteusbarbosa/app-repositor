<!-- The Modal -->
<div class="modal" id="modal-address">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">{{ trans('legend.address-create') }}</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            
            <!-- Modal body -->
            <div class="modal-body">
                
                <div class="form-group">
                    <label for="f-title">{{ trans('legend.address-title') }}</label>
                    <input id="f-title" type="text" class="form-control" name="title">
                    <small class="form-text text-muted"></small>
                </div>
                <div class="form-group">
                    <label for="f-zipcode">{{ trans('legend.zipcode') }}</label>
                    <input id="f-zipcode" type="tel" class="form-control" name="zipcode">
                    <small class="form-text text-muted"></small>
                </div>
                <div class="form-group">
                    <label for="street">{{ trans('legend.street') }}</label>
                    <input type="text" class="form-control validate[required]" id="f-street" name="street">
                    <small class="form-text text-muted"></small>
                </div>
                <div class="form-group">
                    <label for="f-number">{{ trans('legend.address-number') }}</label>
                    <input type="tel" class="form-control validate[required]" id="f-number" name="number">
                    <small class="form-text text-muted"></small>
                </div>
                <div class="form-group">
                    <label for="f-district">{{ trans('legend.district') }}</label>
                    <input type="text" class="form-control validate[required]" id="f-district" name="district">
                    <small class="form-text text-muted"></small>
                </div>
                <div class="form-group">
                    <label for="f-city">{{ trans('legend.city') }}</label>
                    <input type="text" class="form-control validate[required]" id="f-city" name="city">
                    <small class="form-text text-muted"></small>
                </div>
                <div class="form-group">
                    <label for="f-state">{{ trans('legend.state') }}</label>
                    <input type="text" class="form-control" id="f-state" name="state">
                    <small class="form-text text-muted"></small>
                </div>
                
            </div>
            
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('legend.back') }}</button>
                <button type="button" class="btn btn-primary" id="btn-address-save">{{ trans('legend.save') }}</button>
            </div>
            
        </div>
    </div>
</div>