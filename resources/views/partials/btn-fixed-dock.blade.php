@if(isset($data['btn_dock']))
<div class="btn-fixed-dock">
    <a class="btn btn-add" href="{{ $data['btn_dock']->link }}/create" data-toggle="tooltip" title="@lang('crud.add', ['object' => ''])" >
        <i class="fa fa-plus"></i>
    </a>
    <a class="btn btn-edit" href="{{ $data['btn_dock']->link.'/'.$data['btn_dock']->id }}/edit" data-toggle="tooltip" title="@lang('crud.edit', ['object' => ''])" >
        <i class="fa fa-edit"></i>
    </a>
    <form method="POST" action="{{ $data['btn_dock']->link.'/'.$data['btn_dock']->id }}"  >
        @csrf
        {{ method_field('DELETE') }}
        <button type="submit" class="btn btn-delete" data-toggle="tooltip" title="@lang('crud.delete')"  data-id="{{ $data['btn_dock']->id }}">
            <i class="fa fa-trash"></i>
        </button>
    </form>
    <!-- The Modal -->
</div>
@if(isset($data['btn_dock']->link))
<div class="modal" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Excluir</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <form method="POST"  >
                    @csrf
                    {{ method_field('DELETE') }}
                    <input type="hidden" name="id">
                    <input type="hidden" value="{{  $data['btn_dock']->link }}" name="action">
                    <div class="btn-group m-auto" style="width: 200px;display: block;">
                        <button type="button" type="button" data-dismiss="modal" class="btn btn-secondary">
                            Cancelar
                        </button>
                        <button class="btn btn-danger " type="submit">Confirmar</button>
                    </div>
                </form>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
@endif
@endif