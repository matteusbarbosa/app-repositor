<style id="parameters">
body *{
    font-family: {{ str_replace('+',' ', config('instance.APP_FONT_FAMILY')) }};
}
.navbar.top{
    background-color: {{ config('instance.APP_BG_COLOR') }} !important;
}
.navbar.bottom{
    background-color: {{ config('instance.APP_BG_COLOR') }} !important;
}
</style>