<button type="none" class="btn btn-cart btn-success cart-add m-auto d-block" 
data-img="@if(!empty($stock->file_id)){{ asset($stock->image->url) }}@else{{ asset($stock->product->image->url) }}@endif" 
data-quantity="@if(!empty($stock->getContent('item_quantity_min'))){{ $stock->getContent('item_quantity_min') }}@else{{ '1' }}@endif" 
data-item_quantity_min="{{ $stock->item_quantity_min }}"
data-item_quantity_max="{{ $stock->item_quantity_max }}"
data-title="{{ $stock->product->title }}" 
data-content="{{ $stock->fieldsToString(null, null, true) }}" 
@if(!empty($stock->type))
data-type="{{ __('item.'.$stock->type['slug']) }}"
@endif
data-object_type="{{ get_class($stock) }}"
data-object_id="{{ $stock->id }}"
><i class="fa fa-cart-plus fa-2x"></i>
</button>
