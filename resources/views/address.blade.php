@extends('layouts.public')

@section('title', trans('legend.addresses'))


@section('header')
@parent

<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/addresses.css') }}">

@endsection


@section('content')

<div class="container my-4">
    <div class="row">
        <div class="col-12 col-md-6">
                <div class="t-section bg-red mt-4">
            <h4>{{ trans_choice('user.address',2) }}</h4>
        </div>
    </div>
        <div class="col-12 col-md-6">
            <a href="{{ url('address/create') }}" class="btn btn-success float-right my-2">{{ __('legend.address-create') }}</a>
        </div>
    </div>
    @if($data['address_list']->count() > 0)
    <div class="row">
        <div class="col-12">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col"  class="d-none">Título </th>
                            <th scope="col">Endereço</th>
                            <th scope="col">Ações</th>
                            <th class="d-none"> </th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach ($data['address_list'] as $k => $add)
                        <tr class="address">
                            <td class="d-none">{{ $add->title }}</td>
                            <td>{{ $add->zipcode }} – {{ $add->street }}, Nº {{ $add->number }}, {{ $add->district }}</td>
                            
                            <td>
                                <ul class="list-inline">
                                    <li class="list-inline-item"><a href="{{ url('address/'.$add->id.'/edit') }}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a></li>
                                    <li class="list-inline-item"><button class="btn btn-danger btn-delete btn-sm" data-id="{{ $add->id }}" type="button"><i class="fa fa-remove"></i></button></li>
                                </ul>
                            </td>
                            <td class="d-none"><input type="hidden" name="id[]" value="{{ $add->id }}"></td>
                        </tr>
                        @endforeach
                        
                    </tbody>
                </table>
            </div>
        </div>
        @else
        <div class="alert bg-light my-4" role="alert">
            {{ trans('legend.empty-dataset') }}
        </div>
        
        @endif
    </div>
    <div class="col mb-2">
        <hr>
        
    </div>
</div>
</div>


@php
$o_slug = 'address';
$o_endpoint = 'address';
@endphp
@include('partials.modal-delete')

@endsection