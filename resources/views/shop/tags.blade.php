@extends('layouts.public')
@section('title', trans_choice('object.tag', 2))
@section('description', trans('seo.tags-description'))
@section('header')
@parent 
<script src="{{ asset('dist/js/cart-min.js') }}" defer></script>
<script src="{{ asset('dist/js/buy-min.js') }}" defer></script>
<script src="{{ asset('dist/js/shop-min.js') }}" defer></script>
<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/shop.css') }}">
@endsection
@section('content')
<div class="container mt-4 mb-4">
    @if(count($data['results']) == 0)
    <div class="alert bg-light my-2">
        @lang('legend.empty-product-list')
    </div>
    @can('manage')
    <div class="text-center">
        <a href="{{ path_admin('product') }}">{{ __('crud.manage', ['object' => trans_choice('object.product', 2)]) }}</a>
    </div>
    @endcan
    @else
    <div class="w-100" id="box-search">
        <form class="form-inline align-self-center d-flex" action="{{ asset('/search') }}" method="GET">
            <div class="input-group wrap-search mt-4 mb-2 mx-auto" style="min-width: 280px;">
                <input class="form-control " type="search" name="term" placeholder="@lang('tip.product-search')">
            </div>
        </form>
    </div>
    @endif
    @if(count($data['results']) > 0)
    <div class="row">
        @foreach($data['results'] as $tag_item)
        <div class="col-6 col-md-4 mb-2">
            <a href="{{ url('search?tag[]='.$tag_item->tag['slug']) }}">
                <div class="card card-tag">
                    <div class="card-body p-0">
                        <div class="card-image w-100">
                            <figure>
                                <img class="img-fluid img mx-auto d-block" src="{{ asset($tag_item->tag->image->url) }}" alt="{{ $tag_item->tag->title }}">
                            </figure>
                            <span class="badge badge-info count">{{ count($tag_item->items) }}</span>
                        </div>
                        <div class="card-caption">
                            <h4>{{ $tag_item->tag['title'] }}</h4>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        @endforeach
    </div>
    @endif
</div>
@endsection