@extends('layouts.public')
@section('title', trans('legend.promotion'))
@section('header')
@parent
<script src="{{ asset('dist/js/cart-min.js') }}" defer></script>
<script src="{{ asset('dist/js/buy-min.js') }}" defer></script>
<script src="{{ asset('dist/js/shop-min.js') }}" defer></script>
<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/promotions.css') }}">
@endsection
@section('content')
<div class="container mt-4 mb-4">
  <div class="container">

    @foreach($data['results'] as $tag_item)
    <div class="t-section">
      <h4>{{ $tag_item->tag['title'] }}</h4>
    </div>
    <div class="row text-center">
      <div class="col-12 carousel-1">
        @foreach($tag_item->items as $k => $v)
        <div>
          <div class="card product">
            <div class="card-img-top">
              <div class="caption">{{ __('item.'.$v->type->slug) }}</div>
              <figure> 
                @if(!empty($v->file_id))
                <img class="img-fluid img mx-auto d-block" src="{{ asset($v->image->url) }}" alt="{{ $v->title }}">
                @else
                <img class="img-fluid img mx-auto d-block" src="{{ asset($v->product->image->url) }}" alt="{{ $v->title }}">
                @endif
              </figure>
              <div class="info-shelf">
                  @if(!empty($v->getContent('weight')))
                <span class="stock-option">
                  {{ $v->weightDisplay() }}
                </span>
                @endif
                @if($v->discount_percent != null)
                <span class="discount"> - {{ $v->discount_percent }}%</span>
                @endif    
              </div>
            </div>
            <div class="card-body">
              <p class="card-title">{{ $v->product->title }}</p>
            </div>
            <div class="card-footer">
              @cannot('manage')
              @php
              $stock = $v;
              @endphp
                @include('partials.btn-cart-add')
              @endcannot
              @if($v->type['slug'] != 'bundle' && !empty($v->product))
              <a href="{{ url('product/'.$v->product_id) }}" class="btn btn-link" title="{{ __('legend.product-show') }}" >+{{ __('legend.product-show') }}</a>
              @endif
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </div> 
    @endforeach
  </div>
  @endsection