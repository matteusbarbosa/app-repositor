@extends('layouts.public')
@section('title', trans('legend.products-featured'))
@section('description', trans('legend.products-featured'))
@section('header')
@parent 
<script src="{{ asset('dist/js/cart-min.js') }}" defer></script>
<script src="{{ asset('dist/js/buy-min.js') }}" defer></script>
<script src="{{ asset('dist/js/shop-min.js') }}" defer></script>
<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/shop.css') }}">
@endsection
@section('content')



<div class="container mt-4 mb-4">
    @if(count($data['results']) == 0)
    <div class="alert bg-light my-2">
      @lang('legend.empty-product-list')
    </div>
    @can('manage')
    <div class="text-center">
      <a href="{{ path_admin('product') }}">{{ __('crud.manage', ['object' => trans_choice('object.product', 2)]) }}</a>
    </div>
    @endcan
    @else
  <div class="w-100" id="box-search">
    <form class="form-inline align-self-center d-flex" action="{{ asset('/search') }}" method="GET">
      <div class="input-group wrap-search mt-4 mb-2 mx-auto" style="min-width: 280px;">
        <input class="form-control " type="search" name="term" placeholder="@lang('tip.product-search')">
      </div>
    </form>
  </div>
  
  @endif
  
  @if(count($data['results']) > 0)
    @include('partials.showroom')
  @endif

  </div>
  @endsection