<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <META HTTP-EQUIV="refresh" CONTENT="9990">
        <title>
        @if($breadcrumb = Breadcrumbs::current())
        {{  $breadcrumb->title }} | {{ config('app.name') }}
        @else
        @yield('title')
        @endif
    </title>
        <meta charset='utf-8'>
        <meta http-equiv='X-UA-Compatible' content='IE=edge'>
        <meta name='viewport' content='width=device-width, initial-scale=1.0'>
        <meta name='description' content='{{ config('app.name', 'Pedidos') }}'>
        <meta name='author' content='Matteus Barbosa'>
        <meta name='robot' content='noindex, nofollow' />
        <meta name='theme-color' content='{{ config('instance.APP_BG_COLOR') }}'>
        <!-- Add to home screen for Safari on iOS --> 
        <meta name='apple-mobile-web-app-capable' content='yes'>
        <meta name='apple-mobile-web-app-status-bar-style' content='black'>
        <meta name='apple-mobile-web-app-title' content="{{ config('app.name', 'Pedidos') }}">
        <link rel='apple-touch-icon' href="{{ asset('images/launcher-icon-3x.png') }}">
        <meta name='msapplication-TileImage' content="{{ asset('images/launcher-icon-3x.png') }}">
        <meta name='msapplication-TileColor' content='{{ config('instance.APP_BG_COLOR') }}'>
        <link rel="manifest" href="{{ asset('manifest.webmanifest') }}">
        <link rel="stylesheet" href="{{ asset('font-awesome-4.7.0/css/font-awesome.min.css') }}">
        <script src="{{ asset('vendor/js/jquery-3.3.1.min.js') }}"></script>
        <script src="{{ asset('vendor/js/bootstrap.bundle.min.js') }}"></script>
        <link rel='stylesheet' type='text/css' href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">
        <link rel='stylesheet' type='text/css' href="{{ asset('dist/css/menus.css') }}">
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.css"/>
        <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" type="image/x-icon">
        <link rel="icon" href="{{ asset('images/launcher-icon-1x.png') }}" type="image/x-icon">
        <script src="{{ asset('vendor/jQuery-Mask-Plugin-master/dist/jquery.mask.min.js') }}"></script>
        <script src="{{ asset('dist/js/public-min.js') }}"></script>
        <script src="{{ asset('dist/js/manage-min.js') }}"></script>
        <!-- custom meta -->
        @if(Auth::check())
        <meta name="user_id" content="{{ Auth::user()->id }}" />
        @if(isset($data['buy_last']))
        <meta name="buy_last_id" content="{{ $data['buy_last']->id }}" />
        <meta name="buy_last_status_id" content="{{ $data['buy_last']->status_id }}" />
        @endif
        @endif
        <meta name="app_name" content="{{ config('app.name') }}"/>
        <meta name="app_url" content="{{ config('app.url') }}"/>
        <meta name="api_url" content="{{ config('app.url_api') }}"/>
        <meta name="queue_length" content="{{ config('instance.QUEUE_LENGTH') }}"/>
        <link href="https://fonts.googleapis.com/css?family={{ config('instance.APP_FONT_FAMILY') }}" rel="stylesheet">         
        <!-- Scripts -->
        <script>
            window.Laravel = {!! json_encode([
                'user' => Auth::user(),
                'csrfToken' => csrf_token(),
                'vapidPublicKey' => config('webpush.vapid.public_key'),
                'pusher' => [
                'key' => config('broadcasting.connections.pusher.key'),
                'cluster' => config('broadcasting.connections.pusher.options.cluster'),
                ],
                ]) !!};
            </script>
            @include('partials.parameters')
            @section('header')          
            @show
        </head>
        <body class="layout-public notify-enabled user-{{ Auth::check() ? Auth::user()->name : 'guest' }} {{ Request::segment(1) != null ? 'page-'.Request::segment(1) : 'page-home' }}" id="{{ Request::segment(1) != null ? 'page-'.Request::segment(1) : 'page-home' }}">
            <script src="{{ asset('dist/js/swconfig-min.js') }}"></script>

            @include('partials.navbar-top')
        </div>
    </div>
    <main>
        <div class="container">
            <div class="row">
                <div class="col">
                    @include('partials.notify')
                </div>
                @yield('content')
            </main>
            @include('partials.navbar-bottom')
            <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        </body>
        </html>