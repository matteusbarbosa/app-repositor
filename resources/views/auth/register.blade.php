@extends('layouts.public')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
<div class="row">
        <div class="col-12 mt-4 mb-4">
    
            <figure class="d-block m-auto" width="200">
                <img src="{{ asset('img/lin.jpg') }}" class="img-fluid img-thumbnail m-auto d-block float-left" width="100">
                <figcaption class="float-left ml-3"><h1>{{ config('app.name') }}</h1></figcaption>
            </figure>
        </div>
    </div>
            <div class="card">
                <div class="card-header">{{ __('legend.pre-register') }}</div>
                
                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf                    
                        
                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">{{ trans('legend.phone') }}</label>
                            
                            <div class="col-md-6">
                                <input id="phone" placeholder="Ex.: 319999-8888" type="tel" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" required>
                                
                                @if($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ trans($errors->first('phone')) }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('legend.register-name') }}</label>
                            
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                                @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        
                 

                        
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary ml-auto">
                                    {{ __('legend.register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card-footer">
                        <a href="{{ url('/login') }}">{{ __('legend.login') }}</a>
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection
