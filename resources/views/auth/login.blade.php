@extends('layouts.public')
@section('title', trans('legend.login'))
@section('header')
@parent
@include('script.form')
<link rel='stylesheet' type='text/css' href="{{ asset('dist/css/login.css') }}">
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center align-middle">
        <div class="col-md-4 md-offset-4">
            <img src="{{ asset('img/logo.png') }}" class="mx-auto my-4 d-block img-fluid w-25">
            <div class="card mt-4">
                <div class="card-header">{{ __('legend.phone-login') }}</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}" class="validate">
                        @csrf
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="phone" type="text" placeholder="" class="form-control phone validate[required,custom[phone]] {{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" required autofocus>
                                @if ($errors->has('phone'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        @if(Request::query('admin'))
                        <input type="hidden" name="admin" value="true">
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="password" type="password" placeholder="Insira senha" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{ old('password') }}" required autofocus>
                                @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary float-right">Login</button>
                            </div>
                        </div>
                        @endif
                    </form>
                </div>
            </div>
            <form method="POST" action="{{ route('login') }}" class="text-center mt-4">
                @csrf
                <input type="hidden" name="phone" value="99999999999">

                <input type="hidden" value="1" name="password" >

                <button type="submit" class="btn btn-link btn-sm float-right">
                    <small>Entre como Admin</small>
                </button>
            </form>
        </div>
    </div>
</div>

@endsection
