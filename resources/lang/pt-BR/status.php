<?php

return [
    'route-undefined' => 'Indefinida',
    'route-approved' => 'Aprovada',
    'route-locked' => 'Travada',
    'route-canceled' => 'Cancelada',
    'route-done' => 'Finalizada',
    'sales-visit-done' => 'Visita Realizada',
    'sales-visit-postponed' => 'Visita Adiada',
    'sales-visit-canceled' => 'Visita Cancelada',
    'sales-denied-user' => 'Venda Negada pelo Cliente',
    'sales-denied-vendor' => 'Venda Negada pelo Fornecedor',
    'sales-done-partial' => 'Venda Realizada Parcialmente',
    'sales-done' => 'Venda Realizada Integralmente'
];