<?php

return [
    'tags-description' => 'Categorias do catálogo',
    'stock-description' => 'Na :app_name você encontra :title. :fields. :details',
    'product-description' => 'Na :app_name você encontra :title. :details'
];