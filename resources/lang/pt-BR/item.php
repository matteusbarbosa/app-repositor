<?php

return [
    "bulk" => 'Granel',
    "unit" => 'Unidade',
    "bundle" => 'Malão',
    "package" => 'Fardo',
    "product" => 'Unidade',
    "stock" => 'Granel',
    "box" => 'Caixa'
];