<?php
return [
    'view_month_year_stock_item_buy_by_tag' => 'itens vendidos por tag ano-mês',
    'view_month_year_stock_item_buy_by_status' => 'itens em vendidos por status ano-mês',
    'view_month_year_cart_abandon_count' => 'carrinhos abandonados',
    'view_month_year_user_join_count' => 'usuários cadastrados',
    'view_stock_never_sold' => 'produtos nunca vendidos',
    'view_most_active_attendant' => 'vendedor mais ativo',
    'view_products_sold_together' => 'produtos mais vendidos em conjunto',
    'view_products_sold_trimester' => 'produtos mais vendidos por trimestre',
    'view_products_sold_more_quantity' => 'produtos vendidos em maior quantidade',
    'view_best_selling_set' => 'conjunto mais vendido',
    'view_trimester_best_selling_product' => 'produto mais vendido por trimestre',
    'view_trimester_highest_quantity_selling_product' => 'produto vendido em maior quantidade por trimestre',
    'view_search_empty_result' => 'pesquisas sem resultado',
    'available-reports' => 'Relatórios disponíveis',
    'empty-report' => 'Relatório vazio ou sem dados suficientes para exibição',
    'trimester-1' => '1° trimestre (Jan-Mar)',
    'trimester-2' => '2° trimestre (Abr-Jun)',
    'trimester-3' => '3° trimestre (Jul-Set)',
    'trimester-4' => '4° trimestre (Out-Dez)'
];