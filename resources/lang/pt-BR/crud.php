<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'add' => 'Novo(a) :object',
    'edit' => 'Alterar ":object"',
    'update' => 'Atualizar',
    'delete' => 'Excluir',
    'list' => 'Listar :object',
    'manage' => 'Gerenciar :object',
    'manage-sales_channel' => 'Gerenciar Canais de Venda',
    'add-sales_goal' => 'Cadastrar Meta de Venda',
    'add-sales_channel' => 'Cadastrar Canal de Venda',
    'edit-sales_channel' => 'Alterar Canal de Venda',
    'manage-sales_goal' => 'Gerenciar Metas de Venda',
    'manage-addresses' => 'Gerenciar Endereços'
];
