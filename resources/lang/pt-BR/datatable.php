<?php

return [
    "sEmptyTable" => "Nenhum(a) :lang_object encontrado(a)",
    "sInfo" => "Mostrando de _START_ até _END_ de _TOTAL_ :lang_object",
    "sInfoEmpty" => "Mostrando 0 até 0 de 0 :lang_object",
    "sInfoFiltered" => "(Filtrados(as) de _MAX_ :lang_object)",
    "sInfoPostFix" => "",
    "sInfoThousands" => ".",
    "sLengthMenu" => "_MENU_ :lang_object por página",
    "sLoadingRecords" => "Carregando...",
    "sProcessing" => "Processando...",
    "sZeroRecords" => "Nenhum(a) :lang_object encontrado",
    "sSearch" => "Pesquisar",
    "sNext" => "Próximo",
    "sPrevious" => "Anterior",
    "sFirst" => "Primeiro",
    "sLast" => "Último",
    "sSortAscending" => "Ordenar colunas de forma ascendente",
    "sSortDescending" => "Ordenar colunas de forma descendente"
];