<?php

return [
    'access-denied' => 'Acesso negado',
    'not-found' => 'Objeto não encontrado',
    'session-expired' => 'Sessão expirada',
    'page-not-found' => 'Página não encontrada',
    'invalid-password' => 'Senha incorreta'
];