<?php

return [
    'stock-create' => 'Para criar stock, primeiro acesse o menu de "Produtos", escolha um produto e gerencie o stock relacionado a cada um',
    'product-change' => 'Cuidado: alterar o produto de uma variação que já consta em vendas pode influir nos relatórios.',
    'color-choose' => 'Escolha uma cor na roda de cores',
    'buy-search' => 'ID, Nome, Telefone ou Detalhes ',
    'product-search' => 'Busca por produto',
    'bank-data' => 'Os dados são armazenados criptografados para garantir a segurança',
    'insert-rows' => 'Nada encontrado. Por favor, insira registros.'
];