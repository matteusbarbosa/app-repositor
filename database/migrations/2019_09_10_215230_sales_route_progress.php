<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SalesRouteProgress extends Migration
{
        /**
     * Run the migrations.
     *
    * @return void
     */
    public function up()
    {
        Schema::create('sales_route_progress', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sales_route_user_id');
            $table->unsignedBigInteger('sales_route_plan_address_id');
            $table->unsignedBigInteger('sales_status_id');
            $table->timestamps();
        });

        Schema::table('sales_route_progress', function (Blueprint $table) {

            $table->foreign('sales_route_user_id')
            ->references('id')->on('sales_route_user')
            ->onDelete('cascade');

            $table->foreign('sales_route_plan_address_id')
            ->references('id')->on('sales_route_plan_address')
            ->onDelete('cascade');

            $table->foreign('sales_status_id')
            ->references('id')->on('sales_status')
            ->onDelete('cascade');

    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_route_progress');
    }
}
