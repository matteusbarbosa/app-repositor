<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Promotion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotion', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('file_id')->nullable();
            $table->unsignedBigInteger('sales_channel_id')->nullable();
            $table->string('title')->nullable(true);
            $table->string('object_type');
            $table->unsignedBigInteger('object_id');
            $table->double('discount_percent', 4, 2)->nullable(true);
            $table->timestamp('discount_start')->nullable(true);
            $table->timestamp('discount_end')->nullable(true);
            $table->timestamp('disabled_at')->nullable(true);
            $table->text('details')->nullable(true);
            $table->timestamp('notified_at')->nullable(true);;
            $table->timestamp('end_at')->nullable(true);;
            $table->timestamps();

            $table->index(['object_type', 'object_id']);

  

        });

        Schema::table('promotion', function (Blueprint $table) { 

            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('set null');

            $table->foreign('file_id')
            ->references('id')->on('file')
            ->onDelete('set null');

            $table->foreign('sales_channel_id')
            ->references('id')->on('sales_channel')
            ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotion');
    }
}
