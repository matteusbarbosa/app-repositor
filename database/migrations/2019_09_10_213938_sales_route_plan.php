<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SalesRoutePlan extends Migration
{

      /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_route_plan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('supervisor_id')->nullable();
            $table->unsignedBigInteger('sales_route_id');
            $table->timestamp('disabled_at')->nullable();
            $table->timestamps();
        });

        Schema::table('sales_route_plan', function (Blueprint $table) {

            $table->foreign('supervisor_id')
            ->references('id')->on('users')
            ->onDelete('cascade');

            $table->foreign('sales_route_id')
            ->references('id')->on('sales_route')
            ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_route_plan');
    }
}
