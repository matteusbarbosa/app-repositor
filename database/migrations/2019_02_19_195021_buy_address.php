<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuyAddress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buy_address', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('buy_id');
            $table->unsignedBigInteger('address_id');
            $table->timestamps();


            
        });

        Schema::table('buy_address', function (Blueprint $table) { 

            $table->foreign('buy_id')
            ->references('id')->on('buy')
            ->onDelete('cascade');

            $table->foreign('address_id')
            ->references('id')->on('address')
            ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buy_address');
    }
}
