<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ItemField extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_field', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('item_id');
            $table->unsignedBigInteger('field_id');
            $table->unsignedBigInteger('stock_field_id');
            $table->integer('sort_position')->nullable(true);
            $table->timestamps();
        });

        Schema::table('item_field', function (Blueprint $table) {

            $table->foreign('item_id')
            ->references('id')->on('item')
            ->onDelete('cascade');

            $table->foreign('field_id')
            ->references('id')->on('field')
            ->onDelete('cascade');

            $table->foreign('stock_field_id')
            ->references('id')->on('stock_field')
            ->onDelete('cascade');

        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_field');
    }
}
