<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SalesStatus extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_status', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('slug');
            $table->integer('progress_percent');
            $table->enum('class', [
                'success',
                'warning',
                'danger',
                'danger',
                'info',
                'success',
                'success'
            ]);
            $table->text('details')->nullable();
            $table->timestamps();
        });

        $this->insertRouteStatuses();

    }

    private function insertRouteStatuses(){

        $faker = Faker\Factory::create();

        $statuses = [
            'sales-visit-done',
            'sales-visit-postponed',
            'sales-visit-canceled',
            'sales-denied-user',
            'sales-denied-vendor',
            'sales-done-partial',
            'sales-done'
        ];

        $percent = [
            100,
            25,
            100,
            100,
            100,
            75,
            100
        ];

        $class = [
            'success',
            'warning',
            'danger',
            'danger',
            'info',
            'success',
            'success'
        ];

        foreach($statuses as $k => $s){
            DB::table('sales_status')->insert([
                'title' => __('status.'.$s),
                'slug' => \Helpers\Format::urlFriendly(__('status.'.$s)),
                'progress_percent' => $percent[$k],
                'class' => $class[$k],
                'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
            ]);
            
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_status');
    }
}
