<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Status extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::create('status', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->unsignedBigInteger('file_id')->nullable(true);
            $table->string('title');
            $table->string('slug');
            $table->string('time_deadline')->nullable(true);
            $table->tinyInteger('in_queue')->nullable(true);
            $table->tinyInteger('allow_user_cancel')->nullable(true);
            $table->char('stock_quantity_operator', 1)->nullable(true);// +/-
            $table->timestamp('stock_updated_at')->nullable(true);
            $table->text('details')->nullable(true); 
            $table->timestamp('disabled_at')->nullable(true);
            $table->timestamps();
        });
        
        $this->insertStatuses();
        $this->insertStatusImages();
    }

        
    private function insertStatusImages(){
        $faker = Faker\Factory::create();
        
        $data = [
                [
                    'user_id' => 1,
                    'label' => 'Solicitado',
                    'url' => '/img/status/2.png',
                ],
                [
                    'user_id' => 1,
                    'label' => 'Estoque insuficiente',
                    'url' => '/img/status/3.png',
                ],
                [
                    'user_id' => 1,
                    'label' => 'Confirmado',
                    'url' => '/img/status/4.png'
                ],
                [
                    'user_id' => 1,
                    'label' => 'Aguardando pagamento',
                    'url' => '/img/status/5.png'
               ],
                [
                    'user_id' => 1,
                    'label' => 'Em separação',
                    'url' => '/img/status/6.png'
              ],
                [
                    'user_id' => 1,
                    'label' => 'Sem previsão para entrega',
                    'url' => '/img/status/7.png',
               ],
                [
                    'user_id' => 1,
                    'label' => 'Pronto para entrega',
                    'url' => '/img/status/8.png'
                     ],
                [
                    'user_id' => 1,
                    'label' => 'A caminho do destino',
                    'url' => '/img/status/9.png'
                   ],
                [
                    'user_id' => 1,
                    'label' => 'Parcialmente Entregue',
                    'url' => '/img/status/10.png'
                ],
                [
                    'user_id' => 1,
                    'label' => 'Entregue',
                    'url' => '/img/status/11.png'
                ],
                [
                    'user_id' => 1,
                    'label' => 'Devolvido',
                    'url' => '/img/status/12.png'
                ],
                [
                    'user_id' => 1,
                    'label' => 'Cancelado pelo cliente',
                    'url' => '/img/status/13.png'
               ],
                [
                    'user_id' => 1,
                    'label' => 'Cancelado pelo fornecedor',
                    'url' => '/img/status/14.png'
                 ],
            ];
            
            DB::table('file')->insert($data);
        }
        
    
    private function insertStatuses(){
        
        $faker = Faker\Factory::create();
        
        $data = [
            [
                'file_id' => 2,
                'title' => 'Solicitado',
                'slug' => 'solicitado',
                'time_deadline' => '3',
                'in_queue' => 1,
                'allow_user_cancel' => 1,
                'stock_quantity_operator' => null,
                'stock_updated_at' => null,
                'details' => str_replace(",","", $faker->text(150)),
                'updated_at' => $faker->dateTime()
            ],
            [
                'file_id' => 3,
                'title' => 'Estoque insuficiente',
                'slug' => 'stock-insuficiente',
                'time_deadline' => '3',
                'in_queue' => 1,
                'allow_user_cancel' => 1,
                'stock_quantity_operator' => null,
                'stock_updated_at' => null,
                'details' => str_replace(",","", $faker->text(150)),
                'updated_at' => $faker->dateTime()
            ],
            [
                'file_id' => 4,
                'title' => 'Confirmado',
                'slug' => 'confirmado',
                'time_deadline' => '3',
                'in_queue' => 1,
                'allow_user_cancel' => 1,
                'stock_quantity_operator' => '-',
                'stock_updated_at' => $faker->randomElement([$faker->dateTimeBetween('-3 days', 'now')]),
    
                'details' => str_replace(",","", $faker->text(150)),
                'updated_at' => $faker->dateTime()
            ],
            [
                'file_id' => 5,
                'title' => 'Aguardando pagamento',
                'slug' => 'aguardando-pagamento',
                'time_deadline' => '3',
                'in_queue' => 1,
                'allow_user_cancel' => 1,
                'stock_quantity_operator' => '-',
                'stock_updated_at' => $faker->randomElement([$faker->dateTimeBetween('-3 days', 'now')]),
                'details' => str_replace(",","", $faker->text(150)),
                'updated_at' => $faker->dateTime()
            ],
            [
                'file_id' => 6,
                'title' => 'Em separação',
                'slug' => 'em-separacao',
                'time_deadline' => '3',
                'in_queue' => 1,
                'allow_user_cancel' => 1,
                'stock_quantity_operator' => '-',
                'stock_updated_at' => $faker->randomElement([$faker->dateTimeBetween('-3 days', 'now')]),
                'details' => str_replace(",","", $faker->text(150)),
                'updated_at' => $faker->dateTime()
            ],
            [
                'file_id' => 7,
                'title' => 'Sem previsão para entrega',
                'slug' => 'sem-previsao-entrega',
                'time_deadline' => '3',
                'in_queue' => 1,
                'allow_user_cancel' => 1,
                'stock_quantity_operator' => '-',
                'stock_updated_at' => $faker->randomElement([$faker->dateTimeBetween('-3 days', 'now')]),
                'details' => str_replace(",","", $faker->text(150)),
                'updated_at' => $faker->dateTime()
            ],
            [
                'file_id' => 8,
                'title' => 'Pronto para entrega',
                'slug' => 'pronto-para-entrega',
                'time_deadline' => '3',
                'in_queue' => 1,
                'allow_user_cancel' => null,
                'stock_quantity_operator' => '-',
                'stock_updated_at' => $faker->randomElement([$faker->dateTimeBetween('-3 days', 'now')]),
                'details' => str_replace(",","", $faker->text(150)),
                'updated_at' => $faker->dateTime()
            ],
            [
                'file_id' => 9,
                'title' => 'A caminho do destino',
                'slug' => 'a-caminho-do-destino',
                'time_deadline' => '3',
                'in_queue' => null,
                'allow_user_cancel' => null,
                'stock_quantity_operator' => '-',
                'stock_updated_at' => $faker->randomElement([$faker->dateTimeBetween('-3 days', 'now')]),
                'details' => str_replace(",","", $faker->text(150)),
                'updated_at' => $faker->dateTime()
            ],
            [
                'file_id' => 10,
                'title' => 'Parcialmente Entregue',
                'slug' => 'parcial-entregue',
                'time_deadline' => '3',
                'in_queue' => 1,
                'allow_user_cancel' => 1,
                'stock_quantity_operator' => '-',
                'stock_updated_at' => $faker->randomElement([$faker->dateTimeBetween('-3 days', 'now')]),
                'details' => str_replace(",","", $faker->text(150)),
                'updated_at' => $faker->dateTime()
            ],
            [
                'file_id' => 11,
                'title' => 'Entregue',
                'slug' => 'entregue',
                'time_deadline' => '3',
                'in_queue' => null,
                'allow_user_cancel' => null,
                'stock_quantity_operator' => '-',
                'stock_updated_at' => $faker->randomElement([$faker->dateTimeBetween('-3 days', 'now')]),
                'details' => str_replace(",","", $faker->text(150)),
                'updated_at' => $faker->dateTime()
            ],
            [
                'file_id' => 12,
                'title' => 'Devolvido',
                'slug' => 'devolvido',
                'time_deadline' => '3',
                'in_queue' => null,
                'allow_user_cancel' => null,
                'stock_quantity_operator' => '+',
                'stock_updated_at' => $faker->randomElement([$faker->dateTimeBetween('-3 days', 'now')]),
                'details' => str_replace(",","", $faker->text(150)),
                'updated_at' => $faker->dateTime()
            ],
            [
                'file_id' => 13,
                'title' => 'Cancelado pelo cliente',
                'slug' => 'cancelado-cliente',
                'time_deadline' => 0,
                'in_queue' => null,
                'allow_user_cancel' => null,
                'stock_quantity_operator' => '+',
                'stock_updated_at' => $faker->randomElement([$faker->dateTimeBetween('-3 days', 'now')]),
                'details' => str_replace(",","", $faker->text(150)),
                'updated_at' => $faker->dateTime()
            ],
            [
                'file_id' => 14,
                'title' => 'Cancelado pelo fornecedor',
                'slug' => 'cancelado-fornecedor',
                'time_deadline' => 0,
                'in_queue' => null,
                'allow_user_cancel' => null,
                'stock_quantity_operator' => '+',
                'stock_updated_at' => $faker->randomElement([$faker->dateTimeBetween('-3 days', 'now')]),
                'details' => str_replace(",","", $faker->text(150)),
                'updated_at' => $faker->dateTime()
            ],
        ];
        
        DB::table('status')->insert($data);
    }
    
    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::dropIfExists('status');
    }
}
