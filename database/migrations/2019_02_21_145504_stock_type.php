<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StockType extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::create('stock_type', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('parent_id')->nullable(true);
            $table->string('title');
            $table->string('slug');
            $table->text('details')->nullable(true);
            $table->timestamp('disabled_at')->nullable(true);
            $table->timestamps();
            
            $table->index('slug');
        });
        $this->insertStockTypes();
        
    }
    
    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::dropIfExists('stock_type');
    }
    
    private function insertStockTypes(){
        $faker = Faker\Factory::create();
        DB::table('stock_type')->insert([
            'title' => __('object.unit'),
            'parent_id' => 2,
            'slug' => 'unit',
            'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
            'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
            ]);
            
            DB::table('stock_type')->insert([
                'title' => __('object.package'),
                'parent_id' => 3,
            'slug' => 'package',
            'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
            'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
                ]);

                DB::table('stock_type')->insert([
                    'title' => __('object.bundle'),
                    'slug' => 'bundle',
                    'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                    'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
                    ]);

                    DB::table('stock_type')->insert([
                        'title' => __('object.box'),
                        'slug' => 'box',
                        'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                        'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
                        ]);

                        DB::table('stock_type')->insert([
                            'title' => __('product.bulk'),
                            'slug' => 'bulk',
                            'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                            'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
                            ]);
                }
            }
            