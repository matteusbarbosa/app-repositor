<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SalesChannelGoal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_channel_goal', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sales_channel_id'); 
            $table->unsignedBigInteger('sales_goal_id');
            $table->timestamps();
        });

        Schema::table('sales_channel_goal', function (Blueprint $table) {
            $table->foreign('sales_channel_id')
            ->references('id')->on('sales_channel')
            ->onDelete('cascade');

            $table->foreign('sales_goal_id')
            ->references('id')->on('sales_goal')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_channel_goal');
    }
}
