<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SalesGoal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_goal', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable(true);
            $table->string('slug');
            $table->double('goal_amount_min', 8, 2)->nullable(true);
            $table->double('goal_amount', 8, 2)->nullable(true);
            $table->double('goal_percent_max', 3, 2)->nullable(true);
            $table->text('details')->nullable(true);
            $table->timestamp('start_at')->nullable(true);
            $table->timestamp('end_at')->nullable(true);
            $table->timestamp('disabled_at')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_goal');
    }
}
