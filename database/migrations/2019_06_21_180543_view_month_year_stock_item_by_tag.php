<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/* 
Informação sobre quais itens estão sendo mais procurados
*/ 
class ViewMonthYearStockItemByTag extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        DB::statement("CREATE VIEW ".DB::getTablePrefix()."view_month_year_stock_item_by_tag AS (select `app_tag`.`slug`,`app_tag`.`title`, count(`app_item`.`id`) AS total ,
        month(`app_item`.`created_at`) as `month`, year(`app_item`.`created_at`) as `year`
        from `app_tag` inner join `app_tag_stock` on `app_tag_stock`.`tag_id` = `app_tag`.`id` 
        inner join `app_stock` on `app_stock`.`id` = `app_tag_stock`.`stock_id` 
        inner join `app_item` on `app_item`.`object_id` = `app_stock`.`id` 
        and `app_item`.`object_type` = '".addslashes('App\Stock')."' 
        group by `year`,`month`, `app_tag`.`slug`, `app_tag`.`title`)");
        
    }
    
    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        DB::statement("DROP VIEW ".DB::getTablePrefix()."view_month_year_stock_item_by_tag");
    }
}
