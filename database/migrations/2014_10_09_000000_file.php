<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class File extends Migration
{
    /**
     * Run the migrations.
     * 
     * @return void
     */
    public function up()
    {
        Schema::create('file', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('label');
            $table->text('url');
            //caso esteja pagando uma saída
            $table->text('details')->nullable(true);
            $table->timestamps();

        });

        $this->insertDefaultUserImage();

      
    }

    private function insertDefaultUserImage(){

        $faker = Faker\Factory::create();
        
        $b_id = DB::table('file')->insertGetId([
            'label' => 'user-default',
            'url' => 'img/user-default.png',
            'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
            'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
        ]);
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file');
    }
}
