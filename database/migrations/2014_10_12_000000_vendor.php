<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Vendor extends Migration
{
      /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('file_id')->nullable(true);
            $table->string('title')->nullable(true);
            $table->string('slug');
            $table->text('details')->nullable(true);
            $table->timestamp('disabled_at')->nullable(true);
            $table->timestamps();

        });

        Schema::table('vendor', function (Blueprint $table) { 

            $table->foreign('file_id')
            ->references('id')->on('file')
            ->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor');
    }
}
