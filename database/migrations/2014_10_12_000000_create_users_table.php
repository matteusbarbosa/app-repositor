<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id'); 
            $table->unsignedBigInteger('file_id')->nullable();
            $table->unsignedBigInteger('role_id')->nullable();
            $table->unsignedBigInteger('cpf')->nullable();
            $table->unsignedBigInteger('cnpj')->nullable();
            $table->text('name')->nullable(true);
            $table->text('bank_data')->nullable(true);
            $table->text('details')->nullable(true);
            $table->string('email')->nullable(true);
            $table->string('phone')->unique()->nullable(true);
            $table->string('phone_whatsapp')->nullable(true);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->index('role_id');

        });

        Schema::table('users', function (Blueprint $table) { 

            $table->foreign('file_id')
            ->references('id')->on('file')
            ->onDelete('set null');

            $table->foreign('role_id')
            ->references('id')->on('role')
            ->onDelete('set null');

  
        });

        $this->insertRealUsers();
    }

    private function insertRealUsers(){
        $faker = Faker\Factory::create();

        DB::table('users')->insert([
            'cnpj' => $faker->randomNumber(7).$faker->randomNumber(7),
            'name' => 'Atendente Maria',
            'file_id' => 1,
            'role_id' => 2,
            'email' => 'atende@lin.com.br',
            'phone' => '31994098816',
            'phone_whatsapp' => '31994098816',
            'password' => '$2y$10$XOA1DjuGK.xaMBGM/kOeeOnwo/MCMuRjPjkk.H7TnHlAnxUSl0qVW', // 220101
            'remember_token' => str_random(10),
            'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
            'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
        ]);

        DB::table('users')->insert([
            'cnpj' => $faker->randomNumber(7).$faker->randomNumber(7),
            'name' => 'Cliente Roberto',
            'file_id' => 1,
            'role_id' => 4,
            'email' => 'cliente@lin.com.br',
            'phone' => '31994098815',
            'phone_whatsapp' => '31994098815',
            'password' => '$2y$10$XOA1DjuGK.xaMBGM/kOeeOnwo/MCMuRjPjkk.H7TnHlAnxUSl0qVW', // 220101
            'remember_token' => str_random(10),
            'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
            'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
        ]);


        DB::table('users')->insert([
            'cnpj' => $faker->randomNumber(7).$faker->randomNumber(7),
            'name' => 'ADMIN TEST',
            'file_id' => 1,
            'role_id' => 1,
            'email' => 'contato@desenvolvedormatteus.com.br',
            'phone' => '99999999999',
            'phone_whatsapp' => '99999999999',
            'password' => Hash::make(1),
            'remember_token' => str_random(10),
            'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
            'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
        ]);

        DB::table('users')->insert([
            'cnpj' => $faker->randomNumber(7).$faker->randomNumber(7),
            'name' => 'Matteus',
            'file_id' => 1,
            'role_id' => 1,
            'email' => 'contato@desenvolvedormatteus.com.br',
            'phone' => '31994098814',
            'phone_whatsapp' => '31994098814',
            'password' => '$2y$10$XOA1DjuGK.xaMBGM/kOeeOnwo/MCMuRjPjkk.H7TnHlAnxUSl0qVW', // 220101
            'remember_token' => str_random(10),
            'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
            'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
        ]);
        

            $this->insertAdmins();

    }

    private function insertAdmins(){
        $faker = Faker\Factory::create();
        DB::table('users')->insert([
            'cnpj' => $faker->randomNumber(7).$faker->randomNumber(7),
            'name' => 'Irlei',
            'file_id' => 1,
            'role_id' => 1,
            'email' => 'lin@repositor.com.br',
            'phone' => '31994771188',
            'phone_whatsapp' => '31994771188',
            'password' => Hash::make(220269), // 220101
            'remember_token' => str_random(10),
            'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
            'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
        ]);

        DB::table('users')->insert([
            'cnpj' => $faker->randomNumber(7).$faker->randomNumber(7),
            'name' => 'Luiz',
            'file_id' => 1,
            'role_id' => 1,
            'email' => 'princesmoda@repositor.com.br',
            'phone' => '31994145557',
            'phone_whatsapp' => '31994145557',
            'password' => Hash::make(5557), // 220101
            'remember_token' => str_random(10),
            'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
            'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
