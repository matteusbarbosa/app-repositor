<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ItemStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_status', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('item_id');
            $table->unsignedBigInteger('attendant_id')->nullable();
            $table->unsignedTinyInteger('status_id');
            $table->text('details')->nullable(true);
            $table->timestamp('notified_at')->nullable(true);
            $table->timestamps();

            $table->index('item_id');
            $table->index('status_id');

        });

        Schema::table('item_status', function (Blueprint $table) { 

            $table->foreign('item_id')
            ->references('id')->on('item')
            ->onDelete('cascade');

            $table->foreign('attendant_id')
            ->references('id')->on('users')
            ->onDelete('set null');

            $table->foreign('status_id')
            ->references('id')->on('status')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_status');
    }
}
