<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Role extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('title');
            $table->string('slug');
            //caso esteja pagando uma saída
            $table->text('details')->nullable(true);
            $table->timestamp('disabled_at')->nullable(true);
            $table->timestamps();

            $table->index('slug');

        });

        $this->insertDefaultRoles();
    }

      /**
     * Run the database seeds.
     *
     * @return void
     */
    public function insertDefaultRoles()
    {
        $faker = Faker\Factory::create();

        $dt = $faker->dateTimeBetween('-5 years', 'now');

        $now = $dt->format('Y-m-d H:i:s');
        
            DB::table('role')->insert([
                'title' => 'Administrador',
                'slug' => 'admin',
                'created_at' => $now,
                'updated_at' => $now
            ]);

            DB::table('role')->insert([
                'title' => 'Supervisor',
                'slug' => 'supervisor',
                'created_at' => $now,
                'updated_at' => $now
            ]);

            DB::table('role')->insert([
                'title' => 'Atendente',
                'slug' => 'attendant',
                'created_at' => $now,
                'updated_at' => $now
            ]);

            DB::table('role')->insert([
                'title' => 'Promotor',
                'slug' => 'promoter',
                'created_at' => $now,
                'updated_at' => $now
            ]);

            DB::table('role')->insert([
                'title' => 'Vendedor',
                'slug' => 'seller',
                'created_at' => $now,
                'updated_at' => $now
            ]);

            DB::table('role')->insert([
                'title' => 'Expedidor',
                'slug' => 'dispatcher',
                'created_at' => $now,
                'updated_at' => $now
            ]);

            DB::table('role')->insert([
                'title' => 'Entregador',
                'slug' => 'deliver',
                'created_at' => $now,
                'updated_at' => $now
            ]);

            DB::table('role')->insert([
                'title' => 'Cliente',
                'slug' => 'customer',
                'created_at' => $now,
                'updated_at' => $now
            ]);

            DB::table('role')->insert([
                'title' => 'Visitante',
                'slug' => 'guest',
                'created_at' => $now,
                'updated_at' => $now
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role');
    }
}
