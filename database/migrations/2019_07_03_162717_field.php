<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Field extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('field', function (Blueprint $table) {
            
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id'); 
            $table->string('slug');
            $table->text('title');
            $table->json('options')->nullable(true);
            $table->string('type');
            $table->timestamp('shop_display_at')->nullable(true);
            $table->timestamp('disabled_at')->nullable(true);
            $table->timestamps();
        });

        
        Schema::table('field', function (Blueprint $table) { 

            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');

        });

        
        $this->insertDefaultFields();

    }

    private function insertDefaultFields(){
    
        $default_fields = [
            ['user_id' => 1, 'title' => __('product.file'),  'slug' => 'file', 'type' => 'file', 'shop_display_at' => null,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['user_id' => 1, 'title' => __('product.size'),  'slug' => 'size', 'type' => 'size', 'shop_display_at' => date('Y-m-d H:i:s'),'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['user_id' => 1, 'title' => __('product.color'), 'slug' => 'color', 'type' => 'color', 'shop_display_at' => date('Y-m-d H:i:s'),'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['user_id' => 1, 'title' => __('product.weight'),  'slug' => 'weight', 'type' => 'weight', 'shop_display_at' => date('Y-m-d H:i:s'),'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['user_id' => 1, 'title' => __('product.price-unit'), 'slug' => 'price_unit', 'type' => 'price','shop_display_at' => null,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['user_id' => 1, 'title' => __('product.price-total'),  'slug' => 'price_total', 'type' => 'price','shop_display_at' => date('Y-m-d H:i:s'),'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['user_id' => 1, 'title' => __('product.discount-percent'), 'slug' => 'discount_percent', 'type' => 'double', 'shop_display_at' => null,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['user_id' => 1, 'title' => __('product.item-quantity-min'), 'slug' => 'item_quantity_min', 'type' => 'integer', 'shop_display_at' => null,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['user_id' => 1, 'title' => __('product.item-quantity-max'), 'slug' => 'item_quantity_max', 'type' => 'integer', 'shop_display_at' => null,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['user_id' => 1, 'title' => __('product.flavor'), 'slug' => 'flavor', 'type' => 'select', 'shop_display_at' => null,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['user_id' => 1, 'title' => __('product.wrapper'),  'slug' => 'wrapper', 'type' => 'select', 'shop_display_at' => null,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['user_id' => 1, 'title' => __('product.cover-flavor'), 'slug' => 'cover-flavor', 'type' => 'text', 'shop_display_at' => null,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['user_id' => 1, 'title' => __('product.composition'),  'slug' => 'select-multiple', 'type' => 'text', 'shop_display_at' => null,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['user_id' => 1, 'title' => __('product.fat'), 'slug' => 'fat', 'type' => 'select', 'shop_display_at' => null,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
           ['user_id' => 1, 'title' => __('product.filling'),  'slug' => 'filling', 'type' => 'select', 'shop_display_at' => null,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]
        ];

        DB::table('field')->insert($default_fields);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('field');
    }
}
