<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SalesRoute extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_route', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('status_id')->nullable();
            $table->string('title');
            $table->string('slug');
            $table->text('details');
            $table->timestamp('disabled_at')->nullable();
            $table->timestamps();
        });

        Schema::table('sales_route', function (Blueprint $table) {

            $table->foreign('status_id')
            ->references('id')->on('sales_route_status')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_route');
    }
}
