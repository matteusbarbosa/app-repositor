<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class ViewStockNeverSold extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        DB::statement("CREATE VIEW ".DB::getTablePrefix().sprintf("view_stock_never_sold AS (select sum(`app_promotion`. `id`) as `promo_count`,`app_stock`.`id`, `app_product`.`title`, `app_stock`.`created_at`, count(`app_item`.`id`) as `item_count`  from `app_product`
        inner join `app_stock` on `app_stock`.`product_id` = `app_product`.`id`
        left join `app_item` on `app_item`.`object_id` = `app_stock`.`id` and `app_item`.`object_type` = '%s'
        left join `app_promotion` on `app_promotion`.`object_id` = `app_stock`.`id` and `app_promotion`.`object_type` = '%s' 
        where `app_item`.`id` is null 
        group by `app_stock`.`id`,`app_product`.`title`,`app_stock`.`created_at`
        having count(`app_item`.`id`) <= %d order by `app_stock`.`created_at`)", addslashes('App\Stock'),addslashes('App\Stock'), config('instance.ITEM_LOW_SELL_AMOUNT')));
    }
    
    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        DB::statement("DROP VIEW ".DB::getTablePrefix()."view_stock_never_sold");
    }
}
