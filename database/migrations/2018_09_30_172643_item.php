<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class Item extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('cart_id');
            $table->string('object_type');
            $table->unsignedBigInteger('object_id');
            $table->double('price_unit', 8, 2)->nullable(true);
            $table->double('price_total', 8, 2)->nullable(true);
            $table->double('discount_percent', 4, 2)->nullable(true);
            $table->integer('quantity')->default(1);
            $table->text('details')->nullable(true);
            $table->timestamp('stock_updated_at')->nullable(true);
            $table->char('stock_quantity_operator', 1)->nullable(true);// +/-
            $table->timestamp('in_attendance_at')->nullable(true);
            $table->timestamps();
            $table->index(['object_type', 'object_id']);
        });
        Schema::table('item', function (Blueprint $table) { 
            $table->foreign('cart_id')
            ->references('id')->on('cart')
            ->onDelete('cascade');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item');
    }
}
