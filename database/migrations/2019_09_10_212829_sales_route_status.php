<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SalesRouteStatus extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_route_status', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('slug');
            $table->text('details')->nullable();
            $table->timestamps();
        });

        $this->insertRouteStatuses();

    }

    private function insertRouteStatuses(){

        $faker = Faker\Factory::create();

        DB::table('sales_route_status')->insert([
            'title' => __('status.route-undefined'),
            'slug' => \Helpers\Format::urlFriendly(__('status.route-undefined')),
            'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
            'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
        ]);
        
        DB::table('sales_route_status')->insert([
            'title' => __('status.route-approved'),
            'slug' => \Helpers\Format::urlFriendly(__('status.route-approved')),
            'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
            'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
        ]);

        DB::table('sales_route_status')->insert([
            'title' => __('status.route-locked'),
            'slug' => \Helpers\Format::urlFriendly(__('status.route-locked')),
            'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
            'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
        ]);

        DB::table('sales_route_status')->insert([
            'title' => __('status.route-canceled'),
            'slug' => \Helpers\Format::urlFriendly(__('status.route-canceled')),
            'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
            'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
        ]);

        DB::table('sales_route_status')->insert([
            'title' => __('status.route-done'),
            'slug' => \Helpers\Format::urlFriendly(__('status.route-done')),
            'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
            'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
        ]);



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_route_status');
    }
}
