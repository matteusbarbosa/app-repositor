<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Stock;
use App\Field;

class StockField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_field', function (Blueprint $table) {
            
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('stock_id');
            $table->unsignedBigInteger('field_id');
            $table->text('content');
         

        });

        Schema::table('stock_field', function (Blueprint $table) { 

            
            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');


            $table->foreign('stock_id')
            ->references('id')->on('stock')
            ->onDelete('cascade');

            $table->foreign('field_id')
            ->references('id')->on('field')
            ->onDelete('cascade');

        });


    
    }

    
     

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_field');
    }
}
