<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ViewTrimesterBestSellingProduct extends Migration
{
          /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        DB::statement("CREATE VIEW ".DB::getTablePrefix().sprintf("view_trimester_best_selling_product AS (/* mais vendido por trimestre  */
            select `i1`.`object_id`,
            /* trimester 1*/ 
            (
            select count(`app_item`.`id`)
            from `app_item`
            where 
            `app_item`.`object_id` = `i1`.`object_id` and
            month(`app_item`.`created_at`) <= 3 and year(`app_item`.`created_at`) = %d
            group by `app_item`.`object_id`
            ) as `trimester-1`,
            /* trimester 2*/ 
            (
            select count(`app_item`.`id`)
            from `app_item`
            where 
            `app_item`.`object_id` = `i1`.`object_id` and
            month(`app_item`.`created_at`) > 3 and month(`app_item`.`created_at`) <=6 and year(`app_item`.`created_at`) = %d
            group by `app_item`.`object_id`
            ) as `trimester-2`,
            /* trimester 3*/ 
            (
            select count(`app_item`.`id`)
            from `app_item`
            where 
            `app_item`.`object_id` = `i1`.`object_id` and
            month(`app_item`.`created_at`) > 6 and month(`app_item`.`created_at`) <=9 and year(`app_item`.`created_at`) = %d
            group by `app_item`.`object_id`
            ) as `trimester-3`,
            /* trimester 4*/ 
            (
            select count(`app_item`.`id`)
            from `app_item`
            where 
            `app_item`.`object_id` = `i1`.`object_id` and
            month(`app_item`.`created_at`) > 9 and month(`app_item`.`created_at`) <=12 and year(`app_item`.`created_at`) = %d
            group by `app_item`.`object_id`
            ) as `trimester-4`  
            from `app_item` as `i1`
            group by `i1`.`object_id`
            order by `trimester-1` desc, `trimester-2` desc, `trimester-3` desc, `trimester-4` desc
            )", date('Y'), date('Y'), date('Y'), date('Y')));
    }
     
    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        DB::statement("DROP VIEW ".DB::getTablePrefix()."view_trimester_best_selling_product");
    }
}
