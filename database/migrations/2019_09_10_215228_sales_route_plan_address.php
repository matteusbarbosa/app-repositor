<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SalesRoutePlanAddress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_route_plan_address', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sales_route_plan_id');
            $table->unsignedBigInteger('address_id');
            $table->integer('queue_position');
            $table->timestamps();
        });

        Schema::table('sales_route_plan_address', function (Blueprint $table) {

            $table->foreign('sales_route_plan_id')
            ->references('id')->on('sales_route_plan')
            ->onDelete('cascade');

            $table->foreign('address_id')
            ->references('id')->on('address')
            ->onDelete('cascade');

    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_route_plan_address');

    }
}
