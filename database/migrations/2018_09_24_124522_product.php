<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Product extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable(true);
            $table->unsignedBigInteger('vendor_id')->nullable(true);
            $table->unsignedBigInteger('file_id')->nullable(true);
            $table->string('title');
            $table->string('slug');
            $table->text('details')->nullable(true);
            $table->timestamp('disabled_at')->nullable(true);
            $table->timestamps();       
            
            $table->index('user_id');
            $table->index('slug');
        });

        Schema::table('product', function (Blueprint $table) { 

            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('set null');

            $table->foreign('vendor_id')
            ->references('id')->on('vendor')
            ->onDelete('set null');

            $table->foreign('file_id')
            ->references('id')->on('file')
            ->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
