<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ViewBestSellingSet extends Migration
{
        /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        DB::statement("CREATE VIEW ".DB::getTablePrefix()."view_best_selling_set AS (select group_concat(`app_item`.`object_id`) as `combination`  
        from `app_item`
        inner join `app_buy` on `app_buy`.`cart_id` = `app_item`.`cart_id`  
        group by `app_item`.`cart_id`
        having locate(',', `combination` ) != 0
        )");
    }
     
    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        DB::statement("DROP VIEW ".DB::getTablePrefix()."view_best_selling_set");
    }
}
