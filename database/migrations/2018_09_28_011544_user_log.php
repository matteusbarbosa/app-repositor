<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_log', function (Blueprint $table) {
            $table->increments('id');
            $table->string('session_id');
            $table->unsignedBigInteger('user_id')->nullable(true);
            $table->string('action');
            $table->string('object_type')->nullable(true);
            $table->integer('object_id')->nullable(true);
            $table->string('path')->nullable(true);
            $table->timestamp('created_at');

            $table->index('user_id');



        });

        Schema::table('user_log', function (Blueprint $table) { 

            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_log');
    }
}
