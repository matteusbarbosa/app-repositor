<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ViewMonthYearUserJoinCount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE VIEW ".DB::getTablePrefix()."view_month_year_user_join_count AS (select month(`created_at`) as `month`, year(`created_at`) as `year`, 
        count(`id`) as total FROM `app_users` group by `month`, `year`)");
    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW ".DB::getTablePrefix()."view_month_year_user_join_count");
    }
}
