<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Stock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sku')->nullable();
            $table->string('ncm')->nullable();
            $table->string('ean')->nullable();
            $table->string('dun')->nullable();
            $table->string('tax_class')->nullable();
            $table->unsignedBigInteger('file_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('type_id');
            $table->unsignedBigInteger('product_id');
            $table->integer('quantity')->nullable(true);
            $table->text('details')->nullable(true);
            $table->timestamp('shop_display_at')->nullable(true);
            $table->timestamp('disabled_at')->nullable(true);
            
            $table->timestamps();

            $table->index('user_id');
            $table->index('type_id');
            $table->index('product_id');
        });

        Schema::table('stock', function (Blueprint $table) { 

            
            $table->foreign('file_id')
            ->references('id')->on('file')
            ->onDelete('set null');

            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('set null');

            $table->foreign('type_id')
            ->references('id')->on('stock_type')
            ->onDelete('cascade');

            $table->foreign('product_id')
            ->references('id')->on('product')
            ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock');
    }
}
