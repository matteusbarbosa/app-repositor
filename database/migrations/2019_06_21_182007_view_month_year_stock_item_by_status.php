<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ViewMonthYearStockItemByStatus extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        DB::statement("CREATE VIEW ".DB::getTablePrefix()."view_month_year_stock_item_by_status AS (select `app_status`.`title` AS `title`,count(`app_item`.`id`) AS `total`,month(`app_item`.`created_at`) AS `month`,year(`app_item`.`created_at`) AS `year`
        from (((`app_status` join `app_item_status` on((`app_item_status`.`item_id` = `app_status`.`id`))) 
        join `app_item` on((`app_item`.`id` = `app_item_status`.`item_id`))) 
        join `app_stock` on(((`app_stock`.`id` = `app_item`.`object_id`) 
        and (`app_item`.`object_type` = '".addslashes('App\Stock')."' )))) 
        group by `year`,`month`,`app_status`.`title`)");
        
    }
    
    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        DB::statement("DROP VIEW ".DB::getTablePrefix()."view_month_year_stock_item_by_status");
    }
}
