<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class ViewMostActiveAttendant extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        DB::statement("CREATE VIEW ".DB::getTablePrefix()."view_most_active_attendant AS (select `app_users`.`name`,count(`app_item_status`.`attendant_id`) as `total`
        from `app_item_status` inner join `app_users` on `app_users`.`id` = `app_item_status`.`attendant_id`
        group by `app_users`.`name`,`app_item_status`.`attendant_id` order by `total` desc)");
    }
    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        DB::statement("DROP VIEW ".DB::getTablePrefix()."view_most_active_attendant");
    }
}
