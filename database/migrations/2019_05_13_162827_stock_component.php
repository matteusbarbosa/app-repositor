<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StockComponent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_component', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('parent_id');
            $table->unsignedBigInteger('stock_id');
            $table->integer('quantity');
            $table->timestamps();

        });

        Schema::table('stock_component', function (Blueprint $table) { 

            $table->foreign('parent_id')
            ->references('id')->on('stock')
            ->onDelete('cascade');

            $table->foreign('stock_id')
            ->references('id')->on('stock')
            ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_component');
    }
}
