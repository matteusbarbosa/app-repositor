<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ViewSearchEmptyResult extends Migration
{
        /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        DB::statement("CREATE VIEW ".DB::getTablePrefix()."view_search_empty_result AS (
            select `app_search`.`term`, sum(`app_search`.`id`) as `total` from `app_search`
where `result_count` IS NULL or `result_count` = 0 
group by `app_search`.`term`
        )");
    }
     
    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        DB::statement("DROP VIEW ".DB::getTablePrefix()."view_search_empty_result");
    }
}
