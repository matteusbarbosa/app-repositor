<?php

use Illuminate\Database\Seeder;

class AddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($c = 0; $c < 30; $c++){

            $title = $faker->catchPhrase;

            $pb_id = DB::table('address')->insertGetId([
                'user_id' => rand(1,30),
                'title' => $title,
                'zipcode' => $faker->postcode,
                'street' => $faker->streetName,
                'number' => rand(1,30),
                'district' => $faker->cityPrefix,
                'city' => $faker->city,
                'state' => $faker->state,
                'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
            ]);
        }
        
    }
}
