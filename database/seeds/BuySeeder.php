<?php

use Illuminate\Database\Seeder;

class BuySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($c = 0; $c < 30; $c++){

            $title = $faker->catchPhrase;

            $b_id = DB::table('buy')->insertGetId([
                'user_id' => rand(1,30),
                'manager_id' => rand(1,30),
                'cart_id' => rand(1,30),
                'details'=> $faker->realText,
                'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
            ]);       


        }
    }
}
