<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();


        for($c = 0; $c < 30; $c++){
            DB::table('users')->insert([
            'cnpj' => $faker->randomNumber(7).$faker->randomNumber(7),
            'name' => $faker->name,
            'details' => $faker->realText,
            'file_id' => 1,
            'role_id' => rand(1,4),
            'email' => $faker->unique()->safeEmail,
            'phone' => $faker->e164PhoneNumber,
            'phone_whatsapp' => $faker->e164PhoneNumber,
            'password' => '$2y$10$XOA1DjuGK.xaMBGM/kOeeOnwo/MCMuRjPjkk.H7TnHlAnxUSl0qVW', // 220101
            'remember_token' => str_random(10),
            'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
            'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
        ]);
    }

    $this->user_sales_goal();
    }

    public function user_sales_goal(){
        $faker = Faker\Factory::create();
        for($c = 1; $c <= 30 ; $c++){
            DB::table('user_sales_goal')->insert([
            'user_id' => $c,
            'sales_goal_id' => rand(1,30),
            'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
            'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
        ]);
        }
    }
}
