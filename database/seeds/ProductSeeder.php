<?php
use Illuminate\Database\Seeder;
use App\User;
use App\Stock;
use App\Customer;
use App\Withdraw;
use App\Product;
class ProductSeeder extends Seeder
{
    private $list_weight = [50,100,3000];
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $this->insertRealProducts();
    }
    private function insertRealProducts(){
        $faker = Faker\Factory::create();
        //polvilho
        $products = DatabaseSeeder::getRealProducts();
        $title = $faker->catchPhrase;
        foreach($products as $p){
            $p_id = DB::table('product')->insertGetId(
                array_merge(
                    $p,
                    [
                        'title' => $products[rand(0,3)]['title'],
                        'user_id' => rand(1,30),
                        'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                        'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
                        ]));
                    }
                }
            }
