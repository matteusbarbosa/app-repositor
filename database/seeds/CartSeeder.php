<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Customer;
use App\Withdraw;

class CartSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($c = 0; $c < 30; $c++){

            $b_id = DB::table('cart')->insertGetId([
                'session_id' => $faker->md5,
                'user_id' => rand(1,30),
                'details'=> $faker->realText,
                'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
            ]);
        }
    }
}
