<?php

use Illuminate\Database\Seeder;

class SalesGoalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $dt = $faker->dateTimeBetween('-5 years', 'now');

        $now = $dt->format('Y-m-d H:i:s');

        for($c = 1; $c <= 30; $c++){
            $title = $faker->name;
            DB::table('sales_goal')->insertGetId([
                'title' => $title,
                'slug' => \Helpers\Format::urlFriendly($title),
                'goal_amount_min' => $faker->randomElement([0,5.00,15.00, 35.00]),
                'goal_amount' => $faker->randomElement([0,50.00,1500.00, 3500.00]),
                'goal_percent_max' => $faker->randomElement([0,5.00,15.00, 35.00]),
                'details' => $faker->realText,
                'end_at' => $faker->randomElement([null, $faker->dateTimeBetween('now', '+1 years')]),
                'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
            ]);
        }   
    }
}
