<?php

use Illuminate\Database\Seeder;

class PromotionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $products = DatabaseSeeder::getRealProducts();

        for($c = 0; $c < 5; $c++){
            foreach($products as $p){
                $pc_id = DB::table('promotion')->insertGetId([
                    'user_id' => rand(1,30),
                    'file_id' => rand(9,31),
                    'sales_channel_id' => rand(1,4),
                    'title' => $p['title'],
                    'object_type' => $faker->randomElement(['App\Stock']),
                    'object_id' => rand(1,30),
                    'discount_percent' => $faker->randomElement([0,5.00,15.00]),
                    'discount_start' => $faker->dateTimeBetween('-1 years', 'now'),
                    'discount_end' => $faker->dateTimeBetween('+1 years', '+2 years'),
                    'details' => $faker->realText,
                    'end_at' => $faker->randomElement([null, $faker->dateTimeBetween('now', '+1 years')]),
                    'notified_at' => $faker->randomElement([null, $faker->dateTimeBetween('-1 years', 'now')]),
                    'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                    'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
                ]);
            }
        }
     

    }
}
