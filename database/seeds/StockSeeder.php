<?php

use Illuminate\Database\Seeder;
use App\Stock;
use App\StockType;
use App\Product;
use App\Field;

class StockSeeder extends Seeder
{
    public $list_weight = [100,300,500,1000,2000,3000];
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $products = Product::all();
        $types = StockType::pluck('title', 'id');
        foreach($products as $p){
            foreach($types as $type_id => $title){
                $this->insert($p->id, $type_id);
            }
        }
    }
    
    private function insert($p_id, $type_id){
        $faker = Faker\Factory::create();
        foreach($this->list_weight as $qtd){
            $title = $faker->catchPhrase;
            $stock = [
                'user_id' => rand(1,30),
                'file_id' => rand(15,44),
                'type_id' => $type_id,
                'product_id' => $p_id,
                'quantity' => $qtd,
                'shop_display_at' => $faker->randomElement([null, $faker->dateTimeBetween('-1 years', 'now')]),
                'disabled_at' => $faker->randomElement([null, $faker->dateTimeBetween('-1 years', 'now')]),
                'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
            ];
            $s_id = DB::table('stock')->insertGetId($stock);
     
            $components = $this->insertComponents($s_id, $type_id-1);
            $this->insertStockFields($s_id);
        }
    }
    
    private function insertComponents($s_id, $type_id){
        $faker = Faker\Factory::create();
        $components = [];
        $items = Stock::where('type_id', $type_id)->get();
        
        foreach($items as $k => $v){
            $components[] = DB::table('stock_component')->insertGetId([
                'parent_id' => $s_id,
                'stock_id' => $v->id,
                'quantity' => rand(5, DatabaseSeeder::QUOTA),
                'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
                ]);
            }
            return $components;
        }
        
        
        private function insertStockFields($s_id){
            
            $faker = Faker\Factory::create();
            
            $fields = Field::all();
            
            $inserts = [];
            
            foreach($fields as $k => $f){
                if($f->slug == 'item_quantity_min' ||  $f->slug == 'item_quantity_max' ||
                    $f->slug == 'weight' || $f->slug == 'price_unit' ||
                 $f->slug == 'price_total' || $f->slug == 'size')
                $content = $faker->randomNumber(2);
                else
                $content = $faker->sentence;
                
                $inserts[] = [
                    'user_id' => 1,
                    'field_id' => $f->id,
                    'stock_id' => $s_id,
                    'content' => $content
                ];
                
            }
            
            
            DB::table('stock_field')->insert($inserts);
            
            
        }
    }
    