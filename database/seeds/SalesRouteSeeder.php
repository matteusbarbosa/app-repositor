<?php

use Illuminate\Database\Seeder;

class SalesRouteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $dt = $faker->dateTimeBetween('-5 years', 'now');

        $now = $dt->format('Y-m-d H:i:s');

        for($c = 1; $c <= 30; $c++){
            $title = $faker->name;

            $sr_id = DB::table('sales_route')->insertGetId([
                'title' => $title,
                'status_id' => rand(1,4),
                'slug' => \Helpers\Format::urlFriendly($title),
                'details' => $faker->realText,
                'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
            ]);

            $srpn_id = DB::table('sales_route_plan')->insertGetId([
                'supervisor_id' => rand(1,6),
                'sales_route_id' => $sr_id,
                'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
            ]);

            $srpn_add_id = DB::table('sales_route_plan_address')->insertGetId([
                'sales_route_plan_id' => $srpn_id,
                'address_id' => $c,
                'queue_position' => 1,
                'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
            ]);

            $srur_id = DB::table('sales_route_user')->insertGetId([
                'sales_route_id' => $sr_id,
                'user_id' => rand(1,6),
                'start_at' => $faker->dateTimeBetween('-1 months', 'now'),
                'end_at' => $faker->dateTimeBetween('now', '+1 years'),
                'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
            ]);

            //usuário faz progresso em cada endereço do plano de rota
            $srpg_id = DB::table('sales_route_progress')->insertGetId([
                'sales_route_user_id' => $srur_id,
                'sales_route_plan_address_id' => $srpn_add_id,
                'sales_status_id' => rand(1,4),
                'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
            ]);
  
        }   
    }
}
