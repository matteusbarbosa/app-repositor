<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\Customer;
use App\Withdraw;

class FileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        /* for($c = 0; $c < 30; $c++){
            $b_id = DB::table('file')->insertGetId([
                'user_id' => rand(1,30),
                'label' => $faker->randomElement(['Ruffles', 'Doritos', 'Chita', 'Pop', 'Cebolitos', 'Cheetos', 'Fandangos', 'Doce de Leite']),
                'url' => $faker->imageUrl(),
                'details'=> $faker->realText,
                'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
            ]);
        } */


        $this->realProducts();
    }



    public function realProducts(){

        $products = $this->getProducts();

        $faker = Faker\Factory::create();
        for($c = 0; $c < 30; $c++){
            $b_id = DB::table('file')->insertGetId([
                'user_id' => rand(1,5),
                'label' => $products[rand(0,2)]['label'],
                'url' => $products[rand(0,2)]['url'],
                'details'=> $faker->realText,
                'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
            ]);
        }
        
    }

    public function getProducts(){

        return [
            [ 'label' => 'Biscoito Polvilho 50g', 'url' => 'storage/products/biscoito-polvilho/biscoito-polvilho-50g.jpeg'],
            [ 'label' => 'Salgadinho de Milho/Bacon 70g', 'url' => 'storage/products/salgadinhos/milho-bacon-70g.jpeg'],
            [ 'label' => 'Suspiro de Leite condensado 50g', 'url' => 'storage/products/suspiro/leite-condensado-50g.jpeg'],
        ];
    }
}
