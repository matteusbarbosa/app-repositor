<?php

use Illuminate\Database\Seeder;

class TagStockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($c = 0; $c < 30; $c++){

            $title = $faker->catchPhrase;

            $pc_id = DB::table('tag_stock')->insertGetId([
                'tag_id' => rand(1,4), 
                'stock_id' => rand(1,60),
                'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
            ]);
        }
    }
}
