<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Customer;
use App\Withdraw;

class ItemStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($c = 0; $c < 30; $c++){

            $b_id = DB::table('item_status')->insertGetId([
                'item_id' => rand(1,20),
                'status_id' => rand(1,8),
                'attendant_id' => rand(1,8),
                'details'=> $faker->realText,
                'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
            ]);
        }
    }
}
