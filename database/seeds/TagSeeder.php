<?php

use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

            DB::table('tag')->insertGetId([
                'title' => 'Salgadinho',
                'slug' => 'salgadinho',
                'details'=> $faker->realText,
                'shop_display_at' => $faker->dateTimeBetween('-5 years', 'now'),
                'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
            ]);

            
            DB::table('tag')->insertGetId([
                'title' => 'Doce',
                'slug' => 'doce',
                'details'=> $faker->realText,
                'shop_display_at' => $faker->dateTimeBetween('-5 years', 'now'),
                'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
            ]);

            DB::table('tag')->insertGetId([
                'title' => 'Biscoito',
                'slug' => 'biscoito',
                'details'=> $faker->realText,
                'shop_display_at' => $faker->dateTimeBetween('-5 years', 'now'),
                'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
            ]);

            DB::table('tag')->insertGetId([
                'title' => 'Bala',
                'slug' => 'bala',
                'details'=> $faker->realText,
                'shop_display_at' => $faker->dateTimeBetween('-5 years', 'now'),
                'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
            ]);

    }
}
