<?php

use Illuminate\Database\Seeder;

class SalesChannelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $dt = $faker->dateTimeBetween('-5 years', 'now');

        $now = $dt->format('Y-m-d H:i:s');

            DB::table('sales_channel')->insert([
                'title' => 'Padaria',
                'slug' => 'padaria'
            ]);

            DB::table('sales_channel')->insert([
                'title' => 'Hipermercado',
                'slug' => 'hipermercado'
            ]);

            DB::table('sales_channel')->insert([
                'title' => 'Mercearia',
                'slug' => 'mercearia'
            ]);

            DB::table('sales_channel')->insert([
                'title' => 'Rede própria',
                'slug' => 'rede-propria'
            ]);

            $this->sales_channel_goals();
    }

    public function sales_channel_goals(){
        $faker = Faker\Factory::create();
        for($c = 1; $c <= 4 ; $c++){
            DB::table('sales_channel_goal')->insert([
                'sales_channel_id' => $c,
                'sales_goal_id' => rand(1,30),
                'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
            ]);
        }
    }


 
}
