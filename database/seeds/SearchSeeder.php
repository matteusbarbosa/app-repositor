<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Customer;
use App\Withdraw;

class SearchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($c = 0; $c < 30; $c++){

            $title = $faker->catchPhrase;

            $pb_id = DB::table('search')->insertGetId([
                'session_id' => $faker->swiftBicNumber,
                'term' => $title,
                'created_at' => $faker->dateTimeBetween('-5 years', 'now')
            ]);
        }
    }
}
