<?php

use Illuminate\Database\Seeder;

class UserLogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
 
        for($c = 0; $c < 30; $c++){
            DB::table('user_log')->insert([
            'session_id' => $faker->md5,
            'user_id' => rand(1,30),
            'action' => $faker->randomElement([ 'login', 'logout', 'create', 'update', 'view',  'delete' ]),
            'object_id' => rand(1,30),
            'object_type' => 'App\User',
            'created_at' => $faker->dateTimeBetween('-5 years', 'now')
        ]);
    }

    for($c = 0; $c < 30; $c++){
        DB::table('user_log')->insert([
            'session_id' => $faker->md5,
        'user_id' => rand(1,30),
        'action' => 'search',
        'object_id' => rand(1,30),
        'object_type' => 'App\Search',
        'created_at' => $faker->dateTimeBetween('-5 years', 'now')
    ]);
}

    }
}
