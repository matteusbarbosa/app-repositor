<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public const QUOTA = 15;
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        date_default_timezone_set('America/Sao_Paulo');
         $this->call(TagSeeder::class);
         $this->call(SalesGoalSeeder::class);
         $this->call(SalesChannelSeeder::class);
         $this->call(UsersSeeder::class);
         $this->call(AddressSeeder::class);
         $this->call(SearchSeeder::class);
         $this->call(FileSeeder::class);
         $this->call(ProductSeeder::class);
         $this->call(StockSeeder::class);
         $this->call(TagStockSeeder::class);
         $this->call(PromotionSeeder::class);
         $this->call(CartSeeder::class);
         $this->call(BuySeeder::class);
         $this->call(BuyAddressSeeder::class);
         $this->call(UserLogSeeder::class);
         $this->call(ItemSeeder::class);
         $this->call(ItemStatusSeeder::class);
         $this->call(SalesRouteSeeder::class);
    }

    public static function getRealProducts(){
        return [
            [
                
                'file_id' => 31,
                'title' => 'Biscoito de Polvilho',
                'slug' => 'biscoito-de-polvilho-1',
                'details'=>  'Biscoitos assados de polvilho.',
            ],
            [
                
                'file_id' => 32,
                'title' => 'Salgadinho',
                'slug' => 'salgadinho-1',
                'details'=> 'Mistura de trigo temperada.',
            ],
            [
                
                'file_id' => 33,
                'title' => 'Suspiro de leite condensado',
                'slug' => 'suspiro-leite-condensado',
                'details'=> 'Sabores variados de suspiro.',
            ],
            [
                
                'file_id' => 34,
                'title' => 'Suspiro de morango',
                'slug' => 'suspiro-morango',
                'details'=> 'Sabores variados de suspiro.',
            ],
        ];
        
    }
}
