<?php

use Illuminate\Database\Seeder;
use App\Item;
use App\Stock;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($c = 0; $c < 20; $c++){
            $stock_id = rand(1,30);
            $stock_quantity_operator = $faker->randomElement([null, '+', '-']);
            if($stock_quantity_operator != null)
            $stock_updated_at = $faker->randomElement([$faker->dateTimeBetween('-3 days', 'now')]);
            else
            $stock_updated_at = null;

            $in_active_status = $faker->randomElement([null, true]);
            $i_id = DB::table('item')->insertGetId([
                'cart_id' => rand(1,30),
                'object_type' => $faker->randomElement([ 'App\Stock' ]),
                'object_id' => $stock_id,
                'details' => $faker->realText,
                'stock_quantity_operator' => $stock_quantity_operator,
                'stock_updated_at' => $stock_updated_at,
                'in_attendance_at' => $faker->randomElement([null, $faker->dateTimeBetween('-3 days', 'now')]),
                'quantity' => rand(1,30),
                'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
            ]);

            $this->insertItemFields($i_id);
           
        }
    }

    private function insertItemFields($i_id){
            
        $faker = Faker\Factory::create();
        
        $item = Item::find($i_id); 
        $stock_fields = $item->object->fields;
        
        $inserts = [];
        
        foreach($stock_fields as $k => $f){
      
            $inserts[] = [
                'item_id' => $i_id,
                'field_id' => $f->id,
                'stock_field_id' => $f->pivot->id,
                'sort_position' => rand(1,3),
                'created_at' => $faker->dateTimeBetween('-5 years', 'now'),
                'updated_at' => $faker->dateTimeBetween('-5 years', 'now')
            ];
            
        }
        
        
        DB::table('item_field')->insert($inserts);
        
        
    }
}
