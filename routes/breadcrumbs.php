<?php

Breadcrumbs::before(function ($trail) {
    if(request()->route()->getPrefix() == 'admin' && last_segment() != 'admin'){
        $trail->parent('admin');
    }
});


// home
Breadcrumbs::for('/', function ($trail) {
    $trail->push(__('legend.home'), url('/'));
});

// Login
Breadcrumbs::for('login', function ($trail) {
    $trail->push(__('legend.login'), url('/login'));
});

// Admin
Breadcrumbs::for('admin', function ($trail) {
    $trail->push(trans_choice('object.admin',1), path_admin());
});

Breadcrumbs::for('dispatch', function ($trail) {
        $trail->push(__('legend.dispatch'));
});

Breadcrumbs::for('queue', function ($trail) {
    $trail->push(__('legend.dispatch'));
});

Breadcrumbs::for('buy', function ($trail) {
    $trail->push(trans_choice('object.buy', 2));
});

Breadcrumbs::for('search.index', function ($trail) {
    $trail->push(__('legend.search'));
});

Breadcrumbs::for('shop.index', function ($trail) {
    $trail->push(__('object.shop'));
});

Breadcrumbs::for("report", function ($trail){
    $trail->push(trans_choice('object.report', 2));
});

Breadcrumbs::for("report/{kind}", function ($trail){
    $trail->push(trans_choice('object.report', 2));
});

Breadcrumbs::for("user/{user_id}/enderecos", function ($trail){
    $trail->push(trans_choice('object.address', 2));
});

Breadcrumbs::macro('resource', function ($name, $title) {
    //can use title
    Breadcrumbs::for("$name.index", function ($trail) use ($name) {
        $trail->push(trans_choice('object.'.$name,2), route("$name.index"));
    });

    // Home > Blog > New
    Breadcrumbs::for("$name.create", function ($trail) use ($name) {
        $trail->parent("$name.index");
        $trail->push(__('crud.add', ['object' => trans_choice('object.'.$name, 1)]), route("$name.create"));
    });

    // Home > Blog > Post 123
    Breadcrumbs::for("$name.show", function ($trail, $model) use ($name) {
        
        if(!Schema::hasTable($name))
        return;
        
        $obj = DB::table($name)->find($model);

        $title_or_name = trans_choice('object.'.$name,1);
   
        if($obj != null){
            if(isset($obj->title)){
                $title_or_name = $obj->title;
            }
            if(isset($obj->name)){
                $title_or_name = $obj->name;
            }

            $trail->push($title_or_name, route("$name.show", $model));
        }
    });

    // Home > Blog > Post 123 > Edit
    Breadcrumbs::for("$name.edit", function ($trail, $model) use ($name) {
        $trail->parent("$name.index");

        if(!Schema::hasTable($name))
        return;
        
        $obj = DB::table($name)->find($model);

        $title_or_name = trans_choice('object.'.$name,1);

        if($obj != null){
            if(isset($obj->title)){
                $title_or_name = $obj->title;
            }
            if(isset($obj->name)){
                $title_or_name = $obj->name;
            }
            $trail->push(__('crud.edit', ['object' => $title_or_name]), route("$name.show", $model));
        }
    });
});


Breadcrumbs::resource('user', trans_choice('object.user', 2));
Breadcrumbs::resource('address', trans_choice('object.address', 2));
Breadcrumbs::resource('buy', trans_choice('object.buy', 2));
Breadcrumbs::resource('tag', trans_choice('object.tag', 2));
Breadcrumbs::resource('sales_channel', trans_choice('object.sales_channel', 2));
Breadcrumbs::resource('sales_goal', trans_choice('object.sales_goal', 2));
Breadcrumbs::resource('sales_route', trans_choice('object.sales_route', 2));
Breadcrumbs::resource('sales_route_plan', trans_choice('object.sales_route_plan', 2));
Breadcrumbs::resource('field', trans_choice('object.field', 2));
Breadcrumbs::resource('product', trans_choice('object.product', 2));
Breadcrumbs::resource('stock', trans_choice('object.stock', 2));
Breadcrumbs::resource('cart', trans_choice('object.cart', 1));
Breadcrumbs::resource('promotion', trans_choice('object.promotion', 2));
Breadcrumbs::resource('stock_type', trans_choice('object.stock_type', 2));