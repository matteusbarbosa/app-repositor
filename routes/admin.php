<?php
Route::get('queue', 'BuyController@queue')->name('queue');
Route::get('buy', 'BuyController@index')->name('buy');
Route::get('dispatch', 'ItemController@index')->name('dispatch');
Route::get('destaque/{id}/notificacoes', 'PromotionController@notificationIndex');
Route::get('report', 'ReportController@index')->name('report');
Route::get('report/{kind}', 'ReportController@show')->name('report/{kind}');
Route::get('sales_goal/create-channel/{channel_id}', 'SalesGoalController@createForChannel');

Route::resources([
    'sales_channel' => 'SalesChannelController',
    'sales_goal' => 'SalesGoalController',
    'sales_route' => 'SalesRouteController',
    'sales_route_plan' => 'SalesRoutePlanController',
    'sales_route_progress' => 'SalesRouteProgressController',
    'sales_route_user' => 'SalesRouteUserController',
    'user' => 'UserController',
    'promotion' => 'PromotionController',
    'stock' => 'StockController',
    'stock_type' => 'StockTypeController',
    'field' => 'FieldController',
    'product' => 'ProductController',
    'tag' => 'TagController',
    'notification' => 'NotificationController'
]);

Route::get('/', 'AdminController@index')->name('admin');