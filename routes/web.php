<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('logout', 'Auth\LoginController@getLogout');
Route::post('login', 'Auth\LoginController@login');

Route::group(['middleware' => ['auth', 'log']], function () {
    
    Route::get('stock/{id}', 'StockController@show');
    Route::get('buy/repetir/{buy_id}', 'BuyController@repeat');
    Route::get('buy/{id}/notify', 'BuyController@notify');
Route::get('user/{user_id}/enderecos', 'AddressController@showFromUser')->name('user/{user_id}/enderecos');
    
        Route::resources([
            'user' => 'UserController',
            'address' => 'AddressController',  
            'buy' => 'BuyController',
            'product' => 'ProductController',
            'shop' => 'ShopController',
            'promotion' => 'PromotionController',
            'cart' => 'CartController',
            'search' => 'SearchController' 
        ]);      
            
        Route::get('/', 'ShopController@index')->name('/');
});
        
Auth::routes();