<?php

Route::post('buy-price-inform', 'NotificationController@buyPriceInform');
Route::post('promotion', 'NotificationController@promotion');
Route::put('stock/{stock_id}/status', 'NotificationController@stockDispatchStatusUpdate');
Route::put('buy/{buy_id}/status', 'NotificationController@buyStatusUpdate');
Route::put('item/{id}', 'NotificationController@itemUpdate');
Route::post('status', 'NotificationController@status');
Route::patch('{id}/read', 'NotificationController@markAsRead');
Route::post('mark-all-read', 'NotificationController@markAllRead');
Route::post('{id}/dismiss', 'NotificationController@dismiss');
Route::post('/', 'NotificationController@store');
Route::get('/', 'NotificationController@index');