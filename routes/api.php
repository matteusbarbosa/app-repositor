<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('address/buy-unbind', 'AddressController@unbindFromBuy');
Route::post('address/buy-bind', 'AddressController@bindToBuy');
Route::post('address', 'AddressController@store');
Route::get('queue', 'BuyController@queueUpdate');
Route::get('stock/availability-check', 'StockController@availabilityCheck');
Route::get('stock/{id}/alternatives', 'StockController@getAlternatives');
//status de buy
Route::get('buy-status/{id}', 'BuyController@broadcastStatus');
//status dos itens do buy
Route::get('buy-items-status/{buy_id}', 'BuyController@dispatchUpdate');
//status de um item do buy
Route::get('item-status/{item_id}', 'ItemStatusController@broadcastStatus');

//status de itens de mesmo tipo na expedição
Route::get('item/dispatch', 'ItemStatusController@queueUpdate');

//mudança do status de itens da expedição
Route::put('stock/{stock_id}/dispatch', 'ItemStatusController@stockItemsDispatch');

//mudança do status de itens do buy
Route::put('buy/{buy_id}/dispatch', 'ItemStatusController@buyItemsDispatch');

//localiza buy pela query
Route::get('buy-search/{term}', 'BuyController@searchByTerm');

Route::get('product-options/{promotion_id}', 'PromotionController@findProductOptions');
Route::get('user/buy-list', 'UserController@getBuyList');
Route::post('buy-sync', 'BuyController@sync'); 
Route::resource('buy', 'BuyController');
Route::get('item/{id}', 'ItemController@show');
Route::put('item/{id}', 'ItemController@update');
Route::delete('item/{id}', 'ItemController@destroy');

// Push Subscriptions
Route::post('subscriptions', 'PushSubscriptionController@update');
Route::delete('subscriptions/{key}', 'PushSubscriptionController@destroy');

Route::get('user/receita-pj/captcha', 'UserController@getPJCaptcha');
Route::post('user/receita-pj/info', 'UserController@getPJInfo');

/*
Route::middleware('auth')->get('/user', function(Request $request) {
    return $request->user();
}); */
