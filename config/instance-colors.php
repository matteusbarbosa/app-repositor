<?php

return [
  '#000000' => 'Preto',
  '#FFFFFF' => 'Branco',
  '#FF0000' => 'Vermelho',
  '#00FF00' => 'Cal',
  '#0000FF' => 'Azul',
  '#FFFF00' => 'Amarelo',
  '#00FFFF' => 'Ciano / Aqua',
  '#FF00FF' => 'Magenta / Fuchsia',
  '#C0C0C0' => 'Prata',
  '#808080' => 'Cinza',
  '#800000' => 'Marrom',
  '#808000' => 'Azeitona',
  '#008000' => 'Verde',
  '#800080' => 'Roxo',
  '#008080' => 'Cerceta',
  '#000080' => 'Marinha'
];