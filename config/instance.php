<?php

return [
    'APP_DESCRIPTION' => env('APP_DESCRIPTION', true),
    'APP_BG_COLOR' => env('APP_BG_COLOR','#B22222'),
    'APP_FONT_COLOR' => env('APP_FONT_COLOR','#444'), 
    'APP_FONT_FAMILY' => env('APP_FONT_FAMILY', 'Livvic'),
    'USER_REGISTER' => env('USER_REGISTER', true),
    //opts: dispatch, buy
    'QUEUE_LENGTH' => env('QUEUE_LENGTH', 15),
    'QUEUE_BUY_SORT' => env('QUEUE_BUY_SORT', 'status_id'),
    'QUEUE_ITEM_SORT' => env('QUEUE_ITEM_SORT', 'updated_at'),
    'SEARCH_RESULTS_LENGTH' => env('SEARCH_RESULTS_LENGTH', 15),
    'SSE_SLEEP' => env('SSE_SLEEP', 800000),
    'WHOLESALE_ONLY' => env('WHOLESALE_ONLY', false),
    //disabled, numeric
    'STOCK_CODES' => env('STOCK_CODES', 'sku,ncm,ean,dun,tax_class'),
    'STOCK_WEIGHT_MODE' => env('STOCK_WEIGHT_MODE', 'numeric'),
    //disabled, char, numeric
    'STOCK_SIZE_MODE' => env('STOCK_SIZE_MODE', 'numeric'),
    'STOCK_PRICE_DISPLAY' => env('STOCK_PRICE_DISPLAY', false),
    'BUY_ITEM_INITIAL_STATUS' => env('BUY_ITEM_INITIAL_STATUS', 1),
    'ITEM_MULTIPLE_CONTENT_IMPLODE_GLUE' => ',',
    'STOCK_FIELD_DEFAULT_DISPLAY' => env('STOCK_FIELD_DEFAULT_DISPLAY', 'size,weight,price_total,color'),
    'ITEM_FIELD_INPUT' => env('ITEM_FIELD_INPUT','price_total,size,color'),
    'ITEM_LOW_SELL_AMOUNT' => 10,
    'LOW_STOCK_LIMIT' => env('LOW_STOCK_LIMIT', 10),
    'SEARCH_FILTER_IGNORE' => env('SEARCH_FILTER_IGNORE', 'item_quantity_min,item_quantity_max,file')
];