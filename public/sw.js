// This is the service worker code that lives at the root (sw.js)

// You have to supply a name for your cache, this will
// allow us to remove an old one to avoid hitting disk
// space limits and displaying old resources
var cacheName = 'v1';

//var domain = 'https://www.desenvolvedormatteus.com.br/industrial/buys/public';
var domain = 'https://192.168.100.185';

// Assets to cache
var assetsToCache = [ 
       domain+'/manifest.webmanifest',
       domain+'/sw.js',
       domain+'/vendor/js/jquery-3.3.1.min.js',
       domain+'/favicon.ico',
       domain+'/css/sw.css',
       domain+'/shop',
       domain+'/cart',
       domain+'/buy',
       domain+'/index.php',
       domain+'/images/launcher-icon-1x.png',
       domain+'/images/launcher-icon-2x.png',
       domain+'/images/launcher-icon-3x.png',
       domain+'/images/launcher-icon-4x.png',
       domain+'/images/launcher-icon-256.png',
       domain+'/images/launcher-icon-384.png',
       domain+'/images/launcher-icon-512.png',
       domain+'/images/logo-black.png',
       domain+'/images/logo.svg'
];

self.addEventListener('sync', function(event) {
  if (event.registration.tag == "oneTimeSync") {
      console.dir(self.registration);
      console.log("One Time Sync Fired");

      registration.showNotification('Sync OK', {
        body: '2 new Messages!',
        tag: 'id1',
        renotify: true
      });
      
  }
});

self.addEventListener('install', function(event) {
// waitUntil() ensures that the Service Worker will not
// install until the code inside has successfully occurred
event.waitUntil(
  // Create cache with the name supplied above and
  // return a promise for it
  caches.open(cacheName).then(function(cache) {
      // Important to `return` the promise here to have `skipWaiting()`
      // fire after the cache has been updated.
      return cache.addAll(assetsToCache);
  }).then(function() {
    // `skipWaiting()` forces the waiting ServiceWorker to become the
    // active ServiceWorker, triggering the `onactivate` event.
    // Together with `Clients.claim()` this allows a worker to take effect
    // immediately in the client(s).
    return self.skipWaiting();
  })
);
});

// Activate event
// Be sure to call self.clients.claim()
self.addEventListener('activate', function(event) {
  // `claim()` sets this worker as the active worker for all clients that
  // match the workers scope and triggers an `oncontrollerchange` event for
  // the clients.
  return self.clients.claim();
});

self.addEventListener('fetch', function(event) {
// Ignore non-get request like when accessing the admin panel
if (event.request.method !== 'GET') { return; }
// Don't try to handle non-secure assets because fetch will fail
if (/http:/.test(event.request.url)) { return; }

// Here's where we cache all the things!
event.respondWith(
  // Open the cache created when install
  caches.open(cacheName).then(function(cache) {
    // Go to the network to ask for that resource
    return fetch(event.request).then(function(networkResponse) {
      // Add a copy of the response to the cache (updating the old version)
      cache.put(event.request, networkResponse.clone());
      // Respond with it
      return networkResponse;
    }).catch(function() {
      // If there is no internet connection, try to match the request
      // to some of our cached resources
      return cache.match(event.request);
    })
  })
);
});

self.addEventListener('beforeinstallprompt', function(e)
{
e.userChoice.then(function(choiceResult)
{
  if(choiceResult.outcome == 'dismissed')
  {
    alert('User cancelled home screen install');
  }
  else
  {

    registration.showNotification('Aplicativo Instalado', {
      body: 'Abra diretamente da sua lista de apps',
      tag: 'inst1',
      renotify: true
    });

    alert('Adicionado à tela principal');
  }
});
});

self.addEventListener('notificationclose', function(e) {
var notification = e.notification;
var primaryKey = notification.data.primaryKey;

console.log('Closed notification: ' + primaryKey);
});

self.addEventListener('notificationclick', function(e) {
var notification = e.notification;
var primaryKey = notification.data.primaryKey;
var action = e.action;

if (action === 'close') {
  notification.close();
} else {
  clients.openWindow(domain);
  notification.close();
}
});

(() => {
  'use strict'

  const WebPush = {
    init () {
      self.addEventListener('push', this.notificationPush.bind(this))
      self.addEventListener('notificationclick', this.notificationClick.bind(this))
      self.addEventListener('notificationclose', this.notificationClose.bind(this))
    },

    /**
     * Handle notification push event.
     *
     * https://developer.mozilla.org/en-US/docs/Web/Events/push
     *
     * @param {NotificationEvent} event
     */
    notificationPush (event) {
      if (!(self.Notification && self.Notification.permission === 'granted')) {
        return
      }

      // https://developer.mozilla.org/en-US/docs/Web/API/PushMessageData
      if (event.data) {
        event.waitUntil(
          this.sendNotification(event.data.json())
        )
      }
    },

    /**
     * Handle notification click event.
     *
     * https://developer.mozilla.org/en-US/docs/Web/Events/notificationclick
     *
     * @param {NotificationEvent} event
     */
    notificationClick (event) {
      // console.log(event.notification)

      if (event.action === 'item-status') {
        self.clients.openWindow('/buy/'+event.buy_id)
      } 
      
      if (event.action === 'promotion') {
        self.clients.openWindow('/promotion/'+event.id)
      } 

        self.clients.openWindow('/')
      
    },

    /**
     * Handle notification close event (Chrome 50+, Firefox 55+).
     *
     * https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerGlobalScope/onnotificationclose
     *
     * @param {NotificationEvent} event
     */
    notificationClose (event) {
      self.registration.pushManager.getSubscription().then(subscription => {
        if (subscription) {
          this.dismissNotification(event, subscription)
        }
      })
    },

    /**
     * Send notification to the user.
     *
     * https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerRegistration/showNotification
     *
     * @param {PushMessageData|Object} data
     */
    sendNotification (data) {
      return self.registration.showNotification(data.title, data)
    },

    /**
     * Send request to server to dismiss a notification.
     *
     * @param  {NotificationEvent} event
     * @param  {String} subscription.endpoint
     * @return {Response}
     */
    dismissNotification ({ notification }, { endpoint }) {
      if (!notification.data || !notification.data.id) {
        return
      }

      const data = new FormData()
      data.append('endpoint', endpoint)

      // Send a request to the server to mark the notification as read.
      fetch('/api/notifications/${notification.data.id}/dismiss', {
        method: 'POST',
        body: data
      })
    }
  }

  WebPush.init()
})()
