var app_url = $("meta[name=app_url]").attr("content");
var api_url = $("meta[name=api_url]").attr("content");
var user_id = $("meta[name=user_id]").attr("content");
var session_id = $("meta[name=session_id]").attr("content");
var cart_quantity = localStorage.getItem('cart-quantity');
var queue_length = $("meta[name=queue_length]").attr("content");
var buy_last_id = $('meta[name="buy_last_id"]').attr("content");
var buy_last_status_id = $('meta[name="buy_last_status_id"]').attr("content");
var low_stock_limit = $('meta[name="low_stock_limit"]').attr("content");
$(document).ready(function(){
  if (!navigator.onLine){
    $("body").addClass('offline');
    $('.alert.cart-added').fadeIn("slow");
    $('.alert.cart-added').fadeOut(5000);
    $('.offline-disabled').attr('disabled', 'disabled');
    //remove all events
    $(".offline-disabled").unbind();
  }

  $(window).resize(function() {
    $(".nav-placeholder.top").css("height", $(".navbar.fixed-top").height());
    $(".nav-placeholder.bottom").css("height", $(".navbar.fixed-bottom").height());
  });

  if($('[data-toggle="tooltip"]').length > 0){
    $('[data-toggle="tooltip"]').tooltip(); 
  }
  if($('[data-toggle="tooltip-show"]').length > 0){
    $('[data-toggle="tooltip-show"]').tooltip('show'); 
  }
  $('.toggle-search').click(function(){
    $('#box-search').collapse('toggle');
  });
  $('body').on('click', '.tooltip', function(){
    $(this).remove();
  });
  $('body').on('click', '.alert-low-stock', function(){
    $(this).remove();
  });
  $('.btn-delete').click(function(e){
    e.preventDefault();
    var id = $(this).data('id');
    var action = $('#modal-delete').find('input[name="action"]').val();
    $('#modal-delete').find('input[name="id"]').val(id);
    $('#modal-delete').find('form').attr('action', action+'/'+id);
    $('#modal-delete').modal('show');
  });
  $('body').on('click', '.img-view', function(e){
    e.preventDefault();
    let url = $(this).attr('src');
    $('.modal#img-view').find('.modal-body img').attr('src', url.replace('\/240\/', '\/1280\/'));
    $('.modal#img-view').modal('show');
  });



});
