var buy_listeners = {};
var buy_items_status_listeners = {};
//start buy
function tableFill(buy_items){

  if(buy_items.length == 0)
  location.href = app_url;
  
  $.each(buy_items, function( index, item ) {
    var row_new = $('#row-model').clone();
    row_new.removeAttr('id').removeClass('d-none');
    row_new.attr('data-item-id', item.id);
    row_new.attr('data-stock-id', item.stock_id);
    row_new.attr('data-index-id', index);
    row_new.attr('data-status-id', item.status_id);
    row_new.attr('data-stamp', item.stamp);
    
    $(row_new).find('.img img').attr('src', app_url+'/'+item.file_url); 
    $(row_new).children('.title').text(item.title);
    if(item.details != ""){
      let collapse = '<button class="btn btn-link btn-sm" type="button" data-toggle="collapse" data-target="#t-details-'+index+'" aria-expanded="false" aria-controls="t-details-'+index+'">+ detalhes</button>'
      +'<div class="collapse" id="t-details-'+index+'">'+item.details+'</div>';
      $(row_new).children('.title').append(collapse);
    }
    $(row_new).children('.quantity').children('input').val(item.quantity);
    $(row_new).children('.quantity').children('input').addClass('validate[custom[min['+item.item_quantity_min+'], max['+item.item_quantity_max+']]]')
    $(row_new).children('.status').text(item.status_title); 

    $(row_new).find('.remove').attr('data-stamp', item.stamp);
    $(row_new).find('.remove').attr('data-img', item.img);
    $(row_new).find('.remove').attr('data-title', item.title);

    var row_find = $('table.buy > tbody').find('tr[data-item-id="'+item.id+'"]');
    if(row_find.length > 0){
      $(row_find).replaceWith(row_new).slideDown();
    }        
    else{
      /* if($('tr.status-'+stock.status_id).length > 0)
      $(row_new).insertBefore($('tr.status-'+stock.status_id+':first'));
      else */
      $('table.buy > tbody').prepend(row_new);
    }
  });
  if($('table.buy > tbody tr').length >= queue_length)
  $('table.buy > tbody tr:last-child').remove();
}
//Visualização da tabela
function rtBuyItemsStatus(buy_id){
  
  if(typeof buy_items_status_listeners.buy_id == "undefined"){
    buy_items_status_listeners.buy_id =  new EventSource(api_url+'/buy-items-status/'+buy_id);
  }

  var source_bis = buy_items_status_listeners.buy_id;

    source_bis.onmessage = function(event) {
      var data_j = JSON.parse(event.data);
      var d = new Date();
      if(data_j.length == 0){
        $('#all-updated').removeClass('d-none').replaceWith('<tr id="all-updated"><td class="text-center" colspan="4" id="all-updated">Nenhuma atualização de status desde '+d.getHours() +':'+ d.getMinutes()+':'+ d.getSeconds()+' </td></tr>').slideDown();
      } else {
        $('#all-updated').addClass('d-none');
      }
      if(typeof data_j.queue != 'undefined')
      tableFill(data_j.queue);
      else
      console.log('no data received');
    };

}
function unbindAddressFromBuy(buy_id, address_id){
  var request = $.ajax({
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    },  
    data: {
      buy_id: buy_id,
      address_id: address_id
    },
    url: api_url+"/address/buy-unbind",
    method: "POST"
  });
  request.done(function( msg ) {
  });
  request.fail(function( jqXHR, textStatus ) {
    alert( "Não foi possível cadastrar o endereço: " + textStatus );
  });
}
function bindAddressToBuy(buy_id, address_id){
  var request = $.ajax({
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    },  
    data: {
      buy_id: buy_id,
      address_id: address_id
    },
    url: api_url+"/address/buy-bind",
    method: "POST"
  });
  request.done(function( msg ) {
  });
  request.fail(function( jqXHR, textStatus ) {
    alert( "Não foi possível cadastrar o endereço: " + textStatus );
  });
}
function asyncSaveAddress(){
  var request = $.ajax({
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    },  
    data: {
      user_id: $('input[name="user_id"]').val(),
      title: $('input[name="title"]').val(),
      zipcode: $('input[name="zipcode"]').val(),
      street: $('input[name="street"]').val(),
      number: $('input[name="number"]').val(),
      district: $('input[name="district"]').val(),
      city: $('input[name="city"]').val(),
      state: $('input[name="state"]').val()
    },
    url: api_url+"/address",
    method: "POST"
  });
  request.done(function( data ) {
    var address_new = $('#model-address').clone();
    address_new.removeAttr('id');
    address_new.addClass('check-address');
    address_new.find('input[name="model_address"]').attr('name', 'address_new').val('true');
    address_new.find('.t-address').text($('#f-title').val()+', '+$('#f-number').val()+' – '+$('#f-district').val()+'...');
    $('#address-validate').attr('checked','checked');
    $('#list-address').append(address_new.html());
    var buy_id = $('input[name="buy_id"]').val();
    if(buy_id !== undefined){
      bindAddressToBuy(buy_id, data.address_id);
    }
    $('#modal-address').modal('hide');
  });
  request.fail(function( jqXHR, textStatus ) {
    alert( "Não foi possível cadastrar o endereço: " + textStatus );
  });
}
function rtBuyStatus(buy_id, dom_target){

    if(typeof buy_listeners.buy_id == "undefined"){
      buy_listeners.buy_id =  new EventSource(api_url+'/buy-status/'+buy_id);
    }

    let source_bs = buy_listeners.buy_id;

    source_bs.onmessage = function(event) {
      var data_j = JSON.parse(event.data); 
       
        $(dom_target).find('.buy-link').attr('href', app_url+'/buy/'+buy_id);
        $(dom_target).find('.buy-id').removeClass('d-none');
        $(dom_target).find('.buy-id').html("#"+buy_id);
        $(dom_target).find('.status').removeClass('d-none');
        $(dom_target).find('.updated_at').removeClass('d-none');
        $(dom_target).find('.updated_at').html(data_j.updated_at);
        $(dom_target).find('.loading').removeClass('d-none');
        $(dom_target).find('img').attr('src', data_j.file_url); 
        $(dom_target).find('.null').addClass('d-none');
        $(dom_target).find('.status').text(data_j.status_title);
        $("meta[name=buy_last_status_id]").attr("content", data_j.status_id);   
        $(dom_target).find('#status-id').children('option[value="'+data_j.status_id+'"]').attr('selected', 'selected');
        $(dom_target).find('.display').text(data_j.title);
        $("#status-id").attr('data-current-status-id', data_j.status_id);

    };
}
function notifyItemUpdate(item_id, status_id){
  var request = $.ajax({
    url: api_url+"/notifications/item/"+item_id+"/status",
    method: "PUT",
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    },
    data: { 
      status_id : status_id
    },
    dataType: "json"
  });
  request.done(function( response ) {
    console.log('Atendentes notificados');
    console.log('Usuários notificados');
  });
  request.fail(function( jqXHR, textStatus ) {
    console.log( "Request failed: " + textStatus );
  });
}

function notifyBuyStatusUpdate(buy_id, status_id){
  var request = $.ajax({
    url: api_url+"/notifications/buy/"+buy_id+"/status",
    method: "PUT",
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    },
    data: { 
      status_id : status_id
    },
    dataType: "json"
  });
  request.done(function( response ) {
    console.log('Atendentes notificados');
    console.log('Usuários notificados');
  });
  request.fail(function( jqXHR, textStatus ) {
    console.log( "Request failed: " + textStatus );
  });
}
function changeBuyStatus(buy_id, status_id){
  var attendant_id = user_id;
  var request = $.ajax({
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    },  
    url: api_url+"/buy/"+buy_id+'/dispatch',
    method: "PUT",
    dataType: "json",
    data: {
      attendant_id: attendant_id,
      status_id: status_id
    }
  });
  request.done(function( response ) {
    notifyBuyStatusUpdate(buy_id, status_id);
  });
  request.fail(function( jqXHR, textStatus ) {
    console.log( "Request failed: " + textStatus );
  });
}
//end buy
function cartTableFill(){
  const cart = JSON.parse(localStorage.getItem( "cart" ));
  $.each(cart.items, function( index, item ) {
    let row_new = $('#row-model').clone();
    row_new.removeAttr('id').removeClass('d-none');
    row_new.attr('data-stamp', item.stamp);
    row_new.attr('data-stock-id', item.object_id);
    $(row_new).find('.img img').attr('src', item.img); 
    $(row_new).children('.type').text(item.type);
    $(row_new).children('.title').text(item.title);
    $(row_new).children('.content').text(item.content);
    $(row_new).children('.quantity').text(item.quantity);
    $(row_new).children('.hidden').children('.object_id').val(item.object_id).attr('name', 'object_id['+index+']');
    $(row_new).children('.hidden').children('.object_type').val(item.object_type).attr('name', 'object_type['+index+']');

    $('table.cart > tbody').prepend(row_new);
  });
}
function displayNotification(title, body) {
  if (Notification.permission == 'granted') {
    navigator.serviceWorker.getRegistration().then(function(reg) {
      var options = {
        body: body,
        icon: app_url+'/img/checked.png',
        vibrate: [100, 50, 100], 
        data: {
          dateOfArrival: Date.now(),    
          primaryKey: 1
        },  
        actions: [
          {action: 'explore', title: 'Explore this new world',
          icon: app_url+'/images/checkmark.png'},
          {action: 'close', title: 'Close notification', 
          icon: app_url+'/images/xmark.png'},
        ]
      };
    });
  }
}

function rtStockAvailabilityCheck(){
  const source = new EventSource(api_url+'/stock/availability-check', {withCredentials: true});
  if(typeof(EventSource) !== "undefined") {
    var msg = null;
    source.onmessage = function(event) {
      var data_j = JSON.parse(event.data);

      $('#list .item').addClass('available');
      $('#list .item').removeClass('unavailable');
      $('#list .item').find('.alert-low-stock').addClass('d-none');

      $.each(data_j, function( stock_id, quantity ) { 
        var item_row = $('.item[data-stock-id="'+stock_id+'"]');
  
        if(quantity <= low_stock_limit){
          item_row.removeClass('available');
          item_row.addClass('unavailable');
          item_row.find('.alert-low-stock').removeClass('d-none').html('<strong>O estoque está baixo. Restam apenas '+quantity+'</strong>');
        } else {
          item_row.addClass('available');
          item_row.removeClass('unavailable');
          item_row.find('.alert-low-stock').addClass('d-none');
        }
      });
    };
  } else {
    msg = "Sorry, your browser does not support server-sent events...";
    console.log(msg);
  }
}
function splashLoading(){
  $('#layer-loading').fadeIn(500);
  (function(){ 
    $(document).ready(function(){
      $('#layer-loading').fadeOut(1000);
    });
})();
}
function loadCarousel(){

  if($('.carousel-1').length > 0){
    splashLoading(); 
    $('.carousel-1').slick({
      lazyLoad: 'ondemand',
      infinite: true,
      slidesToShow: 4,
      slidesToScroll: 3,
      autoplay: true,
      arrows: false,
      adaptiveHeight: true,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 3
          }
        },
        {
          breakpoint: 480,
          settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 1
          }
        }
      ]
    });
  }
}

function loadAlternativeStocks(stock_id){
  var request = $.ajax({
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    },  
    url: api_url+"/stock/"+stock_id+"/alternatives",
    method: "GET",
    dataType: "json"
  });
  request.done(function( response ) {
    if(response.length > 0){
      $('#item-fields .stock').removeClass('d-none');
      $('select[name="object_id"]').html('');
      $.each(response, function(i,v){
        $('select[name="object_id"]').append('<option value="'+i+'">'+v+'</option>');
      });
    } else {
      $('#item-fields .stock').addClass('d-none');
    }
  });
  request.fail(function( jqXHR, textStatus ) {
    console.log( "Request failed: " + textStatus );
  });
}

function loadItemModal(item_dom, item_data){
  $('#modal-item').modal('show');

  var status_id = item_dom.attr('data-status-id');

  $('#modal-item .product-title').text(item_data.title); 
  $('#modal-item .quantity').val(item_data.quantity);
  $('#modal-item textarea[name="details"]').val(item_data.details);
  $('#modal-item input[name="item_id"]').val(item_data.id);
  $('#item-status-id').val(status_id);
  $('#item-status-id').children('option[value="'+status_id+'"]').attr('selected', 'selected');

  $('#modal-item .remove').attr('data-img', item_data.img);
  $('#modal-item .remove').attr('data-title', item_data.title);
  $('#modal-item .remove').attr('data-item-id', item_data.id);

  var stock_id = item_dom.attr('data-stock-id');
    
  loadAlternativeStocks(stock_id);
 
  var source_bis = buy_items_status_listeners.buy_id;

  if(typeof source_bis !== 'undefined')
  source_bis.close();

  if(typeof item_data.item_quantity_min !== "undefined"){
    var min_quantity = '';
    var max_quantity = '';
  
    if(parseInt(item_data.item_quantity_min) > 0)
    min_quantity += 'min['+item_data.item_quantity_min+']';
    if(parseInt(item_data.item_quantity_max) > 0)
    max_quantity += ', max['+item.item_quantity_max+']'; 
    if(min_quantity != '' || max_quantity != '')
    $('#modal-item .quantity').addClass('validate[custom['+min_quantity+' '+max_quantity+']]');
  }

}

$(document).ready(function(){

  loadCarousel();

    if($('.cart-empty').length > 0){
      $('.cart-empty').fadeIn(1000);
      $('.cart-empty').fadeOut(3000);
    }
    //sync if connected
    window.buy.server.sync();

    window.cart.browser.updateCartDOMQuantity();
   
    if( $('table.cart').length > 0){
      if(!window.cart.browser.isEmpty()){
        splashLoading();
        cartTableFill();
        rtStockAvailabilityCheck();
        $('.cart-empty').removeClass('d-none');
      } else {
        location.href=app_url+'?cart-empty=1';
      }
    }
  
  $('body').on('click','.btn-cart', function(e){
    //send button ref to browser cart
 
    if($(this).hasClass('cart-add')){
      let stamp = window.cart.browser.storeItem($(this));
      $(this).attr('data-stamp', stamp);
      window.cart.browser.displayAlert($(this));
    } else {
      window.cart.browser.removeItem($(this));
      window.cart.browser.displayRemovalAlert($(this));
    }

    window.cart.browser.updateButton($(this));
  });

  if($('#buy-status').length > 0 && buy_last_id != null){
    rtBuyStatus(buy_last_id, '#buy-status');
  }

  $( "#buy-confirm" ).on( "click", function(e) {
    e.preventDefault();
    if($("form.validate").validationEngine('validate')){
      window.buy.browser.store($("form#cart-data").serializeArray());
      window.cart.browser.release();
      //sync if connected
      setTimeout(function(){
        location.href=app_url;
      },5000);
    } else {
      console.log('Form inválido');
    }
  });
  $('#btn-address-add').on("click", function(e){
    e.preventDefault();
    $('#modal-address').modal('show');
  });
  $('#address-change').on("click", 'input[type="checkbox"]', function(e){
    var address_id = $(e.target).val();
    var buy_id = $('input[name="buy_id"]').val();
    if(!$(e.target).is(':checked')){
      unbindAddressFromBuy(buy_id, address_id);
    } else {
      bindAddressToBuy(buy_id, address_id);
    }
  });
  if($('table.buy').length > 0){
    splashLoading();
    rtBuyStatus($('table.buy').attr('data-id'), '#buy-status-specific');
    rtBuyItemsStatus($('table.buy').data('id'));
  }
  $('#btn-address-save').on("click", function(e){
    e.preventDefault();
    asyncSaveAddress();
  });
  $('#list').on('click', '.item', function(){
    var item_dom = $(this);  
    $('.item').removeClass('active');
    $(this).addClass('active');

    var stamp = $(this).attr('data-stamp');
    if(stamp != ""){

      var item = window.cart.browser.getItem(stamp);
      $('#modal-item input[name="stamp"]').val(stamp);
      $('#modal-item .remove').attr('data-stamp', stamp);
      $( '#btn-item-update' ).attr('data-stamp', stamp);

      loadItemModal(item_dom, item);
    } else {

      var item_id = $(this).attr('data-item-id');
   
      var request = $.ajax({
        headers: {
          'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },  
        url: api_url+"/item/"+item_id,
        method: "GET",
        dataType: "json"
      });
      request.done(function( response ) {
 
          loadItemModal(item_dom, response);
        
      });
      request.fail(function( jqXHR, textStatus ) {
        console.log( "Request failed: " + textStatus );
      });
    }

  });

  $('.remove').on('click', function(){
    window.cart.browser.removeItem($(this));
    $('#modal-item').modal('hide');
  });
  $('#list').on('click', '.edit', function(){
    window.cart.browser.editItem($(this));

  });
  $( '#status-id' ).on( "change", function(e) {
    var buy_id = $("#status-id").attr('data-buy-id');
    var status_id = $("#status-id").children('option:selected').val();
    changeBuyStatus(buy_id, status_id);
  });
  $(document).on('click','#btn-item-remove',function() {
    let stamp = $(this).attr('data-stamp');
    let row = $(`tr[data-stamp="${stamp}"]`);
    $('#modal-item').modal('hide');
    row.fadeOut(1000);  
    row.remove();

    window.cart.browser.removeItem($(this));
  });
  $(document).on('click','#btn-item-update',function() {
    let stamp = $(this).attr('data-stamp');
    let row = $('tr[data-stamp="'+stamp+'"]');
    window.cart.browser.updateItem($('#item-fields').serializeArray());
    row.find('.quantity').text($('input[name="quantity"]').val());
    $('#modal-item').modal('hide');
  }); 
 
  $('#modal-item').on('hidden.bs.modal', function () {
    var source_bis = buy_items_status_listeners.buy_id;
    
    if(typeof source_bis !== "undefined")
    source_bis.start();

    $('.item').removeClass('active');
  });

  $('.btn-cart').each(function(){
      cart.browser.updateButton($(this));  
  });

  // window.cart.server.loadCartFromDatabase();
});