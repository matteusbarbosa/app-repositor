var cart = {
    browser: {
        isEmpty: function(){
            if(this.get() == null)
            return true;
            else{
                if(this.get().items == null || this.get().items.length == 0)
                return true;
            }               
        },
        displayAlert: function(e_source){
            $('.alert.cart-added .img').attr('src', e_source.attr('data-img'));
            $('.alert.cart-added .text').text(e_source.attr('data-title'));
            $('.alert.cart-added .quantity').removeClass('d-none');
            $('.alert.cart-added .quantity').text(e_source.attr('data-quantity'));
            if(e_source.attr('data-details') != ""){
                $('.alert.cart-added .details').removeClass('d-none');
                $('.alert.cart-added .details').text(e_source.attr('data-details'));    
            }
            $('.alert.cart-added').fadeIn(1500);
            $('.alert.cart-added').fadeOut(3000);
        },
        get: function(){
            return JSON.parse(localStorage.getItem("cart"));
        },
        updateCartDOMQuantity: function(){
            let length = this.get() != null && typeof this.get().items !== "undefined" ? this.get().items.length : 0; 
            $("meta[name=cart_quantity]").attr("content", length);
            $('.cart-checkout .quantity').text(length);    
            if(length > 0)
            $('.cart-checkout .quantity').removeClass('d-none');
        },
        formToArray: function(form){
            var data = {};
            for(let c = 0; c < form.length; c++){
                data[form[c].name] = form[c].value;
            }
            return data;
        },
        updateButton: function(button_source){
            const item = this.getItemById($(button_source).data('object_id'), $(button_source).data('object_type'));
            if(item != null){
                button_source.removeClass('cart-add btn-success');
                button_source.addClass('cart-remove btn-danger');
                button_source.attr('data-stamp', item.stamp);
                button_source.children('.fa').removeClass('fa-cart-plus');
                button_source.children('.fa').addClass('fa-reply-all');
            } else {
                button_source.removeClass('cart-remove btn-danger');
                button_source.addClass('cart-add btn-success');
                button_source.children('.fa').removeClass('fa-reply-all');
                button_source.children('.fa').addClass('fa-cart-plus');
            }  
        },
        updateItem: function (form){
     
            var data = this.formToArray(form);
            console.log(data);

            var cart = this.get(); 
            if(cart == null || cart.items.length == 0){
                return;
            } else {
                for(let c = 0; c < cart.items.length; c++){
                    if(cart.items[c].stamp == data.stamp){
                        Object.assign(cart.items[c], data);
                    }
            } 
            var jsonStr = JSON.stringify( cart );
            localStorage.setItem( "cart", jsonStr );
            }

        },
        storeItem: function (stock){
            var cart = this.get();

            if(cart == null || cart.items == null){
                cart = {};
                cart.items = new Array();
                cart_quantity = 0;
            }

            let stamp = + new Date();

            var stock_obj = {
                stamp: stamp,
                img: stock.attr('data-img'),
                type: stock.attr('data-type'),
                content: stock.attr('data-content'),
                title: stock.attr('data-title'),
                object_type: stock.attr('data-object_type'),
                object_id: stock.attr('data-object_id'),
                quantity: stock.attr('data-quantity')
            };

            if(stock.find('.field-input').length > 0){
                stock_obj.fields = stock.find('.field-input').map(function() {
                    return { name: $(this).attr('name'), value: $(this).val() };
                  });
            }
        
            cart.items.push(stock_obj);

            var jsonStr = JSON.stringify( cart );
            localStorage.setItem( "cart", jsonStr );

            $(stock).parents('.item').fadeIn(2000);   

            this.updateCartDOMQuantity();
            /* if (navigator.onLine){
                window.cart.server.storeItem( object_type, object_id);
            } else {
                console.log('Adicionado ao cart local');
            } */
            return stamp;
        },

        displayRemovalAlert: function(e_source){
            $('.alert.cart-remove').addClass('d-block');
            $('.alert.cart-remove .img').attr('src', e_source.attr('data-img'));
            $('.alert.cart-remove .text').text(e_source.attr('data-title'));
            setTimeout(function(){
                $('.alert.cart-remove').slideUp( "slow", function() {
                    $('.alert.cart-remove').removeClass('d-block'); 
                    if($('.item').length == 0){
                        $('#cart').addClass('d-none');
                        $('.cart-empty').removeClass('d-none');
                    }
                });
            }, 2000);
        },
        //only after submit to server
        release(){ 
            localStorage.removeItem('cart-quantity');
            localStorage.removeItem('cart');
        },
        getItemById: function(object_id, object_type){
            var cart = this.get();
                if(cart == null || cart.items.length == 0){
                    return null;
                } else {
                    for(let c = 0; c < cart.items.length; c++){
                        if(cart.items[c].object_id == object_id &&
                        cart.items[c].object_type == object_type){
                           return cart.items[c];
                        }
                    }

                    return null;
                }
        },
        getItem: function(stamp){
            var cart = this.get();
                if(cart == null || cart.items.length == 0){
                    return;
                } else {
                    for(let c = 0; c < cart.items.length; c++){
                        if(cart.items[c].stamp == stamp){
                           return cart.items[c];
                        }
                    } 
                }
        },
        editItem: function(e_source = null){
            $(e_source).parents('.item').find('.remove').addClass('d-inline-block');
            $(e_source).parents('.item').find('.save').addClass('d-inline-block');
            $(e_source).parents('.item').find('.edit').addClass('d-none');
            $(e_source).parents('.item').find('.field-input').addClass('d-inline-block');
            $(e_source).parents('.item').find('.field-text').addClass('d-none');
        },
        removeItem: function(e_source = null){
            var cart = this.get();
            var stamp = $(e_source).attr('data-stamp');

            if(stamp != ""){

                if(cart == null || cart.items.length == 0){
                    return;
                } else {
                    for(let c = 0; c < cart.items.length; c++){
                        if(cart.items[c].stamp == stamp){
                           cart.items.splice(c, 1);
                        }
                    } 
                }
    
                localStorage.removeItem( "cart" );
                var jsonStr = JSON.stringify( cart );
                localStorage.setItem( "cart", jsonStr );
    
                if($(e_source).parents('.item').length > 0)
                $(e_source).parents('.item').fadeOut(2000);   
                
            }

            //server remove? when on buy context
            if($(e_source).attr('data-item-id') != null){

                var row_find = $('table.buy > tbody').find('tr[data-item-id="'+$(e_source).attr('data-item-id')+'"]');
               
                if(row_find.length > 0){
                  $(row_find).fadeOut(3000);
                }      

                window.buy.server.removeItem($(e_source).attr('data-item-id'), $(e_source));
            }

            this.displayRemovalAlert(e_source);

            this.updateCartDOMQuantity();

            setTimeout(function(){
                if($("meta[name=cart_quantity]").attr('content') == "0"){
                    /*    $('.cart-empty').removeClass('d-none');
                    $('.navbar.checkout').addClass('d-none');
                    $('.navbar-placeholder').addClass('d-none');
                    $('table.cart').addClass('d-none'); */
                    location.href = app_url;
                }
            }, 2000);
        }
    }
};