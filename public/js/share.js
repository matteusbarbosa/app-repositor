$(document).ready(function(){
    $('.btn-share').on('click', function(e){
        e.preventDefault();
            var text = $(this).attr('data-link');
            navigator.clipboard.writeText(text).then(function() {
            $('.alert.share-copy').fadeIn("slow");
            $('.alert.share-copy').fadeOut(5000);
            }, function(err) {
                console.error('Async: Could not copy text: ', err);
            });
    }); 
})