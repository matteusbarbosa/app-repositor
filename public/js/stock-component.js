$(document).ready(function(){ 
    function checkComponentsAvailable(){
        var type_id = $('#type_id').children('option:selected').val();
        var component_count = $('.f-component-id').children('option[data-parent-type-id="'+type_id+'"]').length;
        if(component_count == 0)
        $('.group-component').addClass('d-none');
        else
        $('.group-component').removeClass('d-none');
    }
    
    function removeComponentInput(c){
        c.remove();
    }
    
    function addComponentInput(){
        var cm = $('.component-model').clone();
        cm.removeClass('d-none component-model');
        
        var type_id = $('#type_id').children('option:selected').val();

        var component_count = $('.f-component-id').children('option[data-parent-type-id="'+type_id+'"]').length;
        
        if(component_count > 0)
        $('.wrap-components').append(cm);
    }
    $('.add-component').on('click', function(){
        addComponentInput();
    });
    
    $('.wrap-components').on('click', '.remove', function(){
        var c = $(this).parents('.component');
        removeComponentInput(c);
    });
    function filterComponentStocks(){
        var type_id = $('#type_id').children('option:selected').val();
        var component_options = $('.f-component-id').children('option[data-parent-type-id="'+type_id+'"]');
        $('.f-component-id').children('option').not($('option[data-parent-type-id="'+type_id+'"]')).addClass('d-none');
        component_options.removeClass('d-none');
        
        if(component_options.length > 0)
        $('.w-add-component').removeClass('d-none');
    }
    
    checkComponentsAvailable();
    filterComponentStocks();
    
    $('#type_id').on('change', function(){
        checkComponentsAvailable();
        filterComponentStocks();
    });
});