var source = new EventSource(api_url+'/item/dispatch');

function tableFill(queue){
    $('table#queue tbody tr').remove();
    
    $.each(queue, function( index, stock ) {

        stock.details = stock.details == null ? '–' : stock.details;

        var row_new = '<tr data-id="'+stock.id+'">'
        + '<td class="title"><a href="'+app_url+'/stock/'+stock.id+'" target="_blank">'+stock.title+'</a></td>'
        + '<td class="users_name">'+stock.users_name+'</td>'
        + '<td class="item-quantity">'+stock.item_quantity+'</td>'
        + '<td class="details">'+stock.details+'</td>'
        + '<td class="quantity">'+stock.stock_quantity+'</td>'
        + '<td class="updated_at">'+stock.created_at_formatted+'</td>'
        + '<td><button type="button" class="status-change btn btn-info btn-sm" data-status-id="1" data-stock-id="'+stock.id+'"><i class="fa fa-edit"></i></button></td>'
        + '<td></td>' 
        + '</tr>';
        var row_find = $('table#queue tbody').find('tr[data-id="'+stock.id+'"]');
        if(row_find.length > 0){
            $(row_find).replaceWith(row_new).slideDown();
        }        
        else{
            /* if($('tr.status-'+stock.status_id).length > 0)
            $(row_new).insertBefore($('tr.status-'+stock.status_id+':first'));
            else */
            $('table#queue tbody').prepend(row_new);
        }
    });

    if($('table#queue tbody tr').length >= queue_length)
    $('table#queue tbody tr:last-child').remove();
}
function rtDispatchUpdate(source){

    if(typeof(EventSource) !== "undefined") {
        var msg = null;
        source.onmessage = function(event) {
            var data_j = JSON.parse(event.data);
            var d = new Date();
            if(data_j.length == 0){
                $('#all-updated').removeClass('d-none').replaceWith('<tr id="all-updated"><td class="text-center" colspan="4" id="all-updated">Nenhuma atualização de status desde '+d.getHours() +':'+ d.getMinutes()+':'+ d.getSeconds()+' </td></tr>').slideDown();
            } else {
                $('#all-updated').addClass('d-none');
            }
            if(typeof data_j.queue != 'undefined')
            tableFill(data_j.queue);
    
        };
    } else {
        msg = "Sorry, your browser does not support server-sent events...";
    }
}

function notifyStockDispatchStatusUpdate(stock_id, status_id){
    var request = $.ajax({
          url: api_url+"/notifications/stock/"+stock_id+"/status",
          method: "PUT",
          headers: {
              'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
          },
          data: { 
            status_id : status_id,
          },
          dataType: "json"
        });
         
        request.done(function( response ) {
          console.log('Atendentes notificados');
          console.log('Usuários notificados');
        });
         
        request.fail(function( jqXHR, textStatus ) {
          console.log( "Request failed: " + textStatus );
        });
  }
  
  function changeStockDispatchStatus(stock_id, status_id, details = null){
    var attendant_id = user_id;
    var request = $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },  
        url: api_url+"/stock/"+stock_id+'/dispatch',
        method: "PUT",
        dataType: "json",
        data: {
            attendant_id: attendant_id,
            status_id: status_id,
            details: details
        }
      });
       
      request.done(function( response ) {

        console.log(response);
        
        notifyStockDispatchStatusUpdate(stock_id, status_id);
  
      });
       
      request.fail(function( jqXHR, textStatus ) {
        console.log( "Request failed: " + textStatus );
      });
  
  }
  
$(document).ready(function(){
    rtDispatchUpdate(source);

    $( '#status-update' ).on( "click", function(e) {
        e.preventDefault();
        var stock_id = $("#status-id").attr('data-stock-id');
        var status_id = $("#status-id").children('option:selected').val();
        var details = $('#status-details').val();
        changeStockDispatchStatus(stock_id, status_id, details);
    });

    function playQueue(source){
        $('.play-queue').removeClass('btn-success');
        $('.play-queue').addClass('btn-disabled');
        $('.play-queue').find('.fa-play').addClass('d-none');
        $('.play-queue').find('.loading').removeClass('d-none');
        $('.play-queue').find('.text').addClass('d-none');
        $('.stop-queue').removeClass('btn-disabled');
        $('.stop-queue').addClass('btn-secondary');
        $('.stop-queue').removeAttr('disabled');
        rtDispatchUpdate(source);
    }
    $('.play-queue').on('click', function(){
        playQueue(source);
    });
    function stopQueue(source){
        source.close();
        $('.play-queue').addClass('btn-success');
        $('.play-queue').removeClass('btn-disabled');
        $('.play-queue').removeAttr('disabled');
        $('.play-queue').find('.fa-play').removeClass('d-none');
        $('.play-queue').find('.loading').addClass('d-none');
        $('.play-queue').find('.text').removeClass('d-none');
        $('.stop-queue').removeClass('btn-secondary');
        $('.stop-queue').addClass('btn-disabled');
        $('.stop-queue').attr('disabled', 'disabled');
    }
    $('.stop-queue').on('click', function(){
      stopQueue(source);
    });
    $('#queue').on('click', '.status-change', function(){
        var stock_id = $(this).attr('data-stock-id');
        var status_id = $(this).attr('data-status-id');
        $('#status-id').attr('data-stock-id', stock_id);
        $('#status-id').children('option[value="'+status_id+'"]').attr('selected', 'selected');
        stopQueue(source);
        $('#modal-status-change').modal('show');
    });
    $('#queue').on('change', '#status-id', function(){
        rtDispatchUpdate(source);
    });

    $('#modal-status-change').on('hidden.bs.modal', function (e) {
        playQueue(source);
    });
});
