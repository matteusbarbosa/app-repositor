$(document).ready(function(){

    function onColorChange(color, changes) {
    $('#f-color').val(color.hexString);
  }

if($("#color-picker-container").length > 0){
    var colorPicker = new iro.ColorPicker("#color-picker-container", {
        // Set the size of the color picker
        width: 192,
        // Set the initial color to pure red
        color: "#f00"
      });

  colorPicker.on('color:change', onColorChange);
     
}

}); 