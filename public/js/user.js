$(document).ready(function(){
    $('.test-notification').on('click', function(){
        if(notification !== undefined){
            notification.send($(this));        
        }    else {
            console.log('Impossível testar no momento');
        }
    });
    setTimeout(function(){
        updateButtonStatus($('.toggle-push'));
    }, 2000       
    );
    $('.toggle-push').on('click', function(){
        toggleNotificationButton($(this));
    });
    function updateButtonStatus(button){
        if(notification !== undefined){
            if(notification.isPushEnabled == true){
                button.html('<i class="fa fa-bell-o"></i> Desativar notificações');
                button.addClass('btn-danger');
                button.removeClass('btn-secondary');
            } else {
                button.html('<i class="fa fa-bell-o"></i> Ativar notificações');
                button.addClass('btn-secondary');
                button.removeClass('btn-danger');
            }
        }
    }
    function toggleNotificationButton(button){
        notification.togglePush();
        setTimeout(function(){
        updateButtonStatus(button);
        }, 2000 );
    }

    $('input[name="type"]').change(function(){
        if($(this).val() == 'PF'){
            $('.w-pf').removeClass('d-none');
            $('.w-pj').addClass('d-none');
        } else {
            $('.w-pj').removeClass('d-none');
            $('.w-pf').addClass('d-none');
        }
    });

    function loadCaptcha(){
        var request = $.ajax({
            url: api_url+"/user/receita-pj/captcha",
            method: "GET",
            headers: {
              'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            }
          });
          request.done(function( response ) {
              let html = '<img class="img-thumbnail" src="data:image/png;base64, '+response.captchaBase64+'" alt="" />';
            $('#receita-pj-captcha').html(html);
            $('#i-cookie').val(response.cookie);
          });
          request.fail(function( jqXHR, textStatus ) {
            console.log( "Request failed: " + textStatus );
          });
    }

    loadCaptcha();

    function loadInfoPJ(){
        var request = $.ajax({
            url: api_url+"/user/receita-pj/info",
            method: "POST",
            data: {
                cookie: $('#i-cookie').val(),
                captcha: $('#i-captcha').val(),
                cnpj: $('#i-cnpj').val()
            },
            headers: {
              'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            }
          });
          request.done(function( response ) {
 
              localStorage.setItem('user-data', JSON.stringify(response));

                let entries = Object.entries(response);

              $(entries).each(function(k, v){
                $('#data-preview').append('<li>'+v[0]+': '+v[1]+'</li>');
              });

              $('#modal-preview').modal('show');

          });
          request.fail(function( jqXHR, textStatus ) {
            alert('CNPJ Inválido. Confira novamente.');
            console.log( "Request failed: " + textStatus );
          });
    }

    function asyncSaveAddress(){

        let user_data = JSON.parse(localStorage.getItem('user-data'));

        var request = $.ajax({
          headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
          },  
          data: {
            user_id: $('input[name="user_id"]').val(),
            title: '(RECEITA) '+user_data.logradouro+' '+user_data.numero+' '+user_data.complemento,
            zipcode: user_data.cep,
            street: user_data.logradouro,
            number: user_data.numero,
            district: user_data.bairro,
            city: user_data.cidade,
            state: user_data.uf
          },
          url: api_url+"/address",
          method: "POST"
        });
        request.done(function( data ) {
            console.log('Endereço cadastrado');
        });
        request.fail(function( jqXHR, textStatus ) {
          alert( "Não foi possível cadastrar o endereço: " + textStatus );
        });
      }

    $('#query-pj').click(function(){
        loadInfoPJ();
    });

    $('body').on('click', '#data-confirm', function(){
        
        let entries = Object.entries(JSON.parse(localStorage.getItem('user-data')));
        
        $(entries).each(function(k, v){
            if($('.f-'+v[0]).length > 0)
            $('.f-'+v[0]).val(v[1]);
        });

        asyncSaveAddress();

        $('#modal-preview').modal('hide');
    });
});