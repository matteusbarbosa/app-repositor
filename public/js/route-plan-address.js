$(document).ready(function(){ 
    var address_available_count = $('.address-model .f-address-id').children('option').length;

    function checkFieldsAvailable(){

        if(address_available_count == 0)
        $('.group-address').addClass('d-none');
        else
        $('.group-address').removeClass('d-none');
    }
    
    function removeFieldInput(c){
        c.remove();
        updateNumbers();
    }
    function updateNumbers(){
        var c = 1;
        $('.address:not(.address-model)').each(function(){
            $(this).find('.number-input').val(c);
            $(this).find('.number').text(c);
            c++;
        })
    }
    function addFieldInput(){
        var cm = $('.address-model').clone();
        cm.removeClass('d-none address-model');
        
        if(address_available_count > 0){
            $('.wrap-addresses').append(cm);
            updateNumbers();
        }

    }
    $('.add-address').on('click', function(){
        addFieldInput();
    });
    
    $('.wrap-addresses').on('click', '.remove', function(){
        var c = $(this).parents('.address');
        removeFieldInput(c);
    });
    function filterFieldStocks(){
        
        if(address_available_count > 0)
        $('.w-add-address').removeClass('d-none');
    }

    function filter_address(client){
        let client_id = client.children('option:selected').val();
        let address_list = client.parents('.address').find('.f-address-id');
        address_list.children('option').addClass('d-none');
        address_list.children('option[data-client_id="'+client_id+'"]').removeClass('d-none');
    }

    $('body').on('change', '.f-client-id', function(){
        filter_address($(this));
    });
    
    checkFieldsAvailable();
    filterFieldStocks();
    
});