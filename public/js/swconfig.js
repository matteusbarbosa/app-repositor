/* SERVICE WORKER - REQUIRED */ 
if ('serviceWorker' in navigator)
{
	navigator.serviceWorker
	.register('/sw.js')
	.then(function(reg) {
  
    console.log("ServiceWorker registered ◕‿◕", reg);
    reg.update();
	})
	.catch(function(error) {
		console.log("Failed to register ServiceWorker ಠ_ಠ", error);
	});
}

function registerOneTimeSync() {
	if (navigator.serviceWorker.controller) {
		navigator.serviceWorker.ready.then(function(reg) {
			if (reg.sync) {
				reg.sync.register({
					tag: 'oneTimeSync'
				})
				.then(function(event) {
					console.log('Sync registration successful', event);
				})
				.catch(function(error) {
					console.log('Sync registration failed', error);
				});
			} else {
				console.log("Onw time Sync not supported");
			}
		});
	} else {
		console.log("No active ServiceWorker");
	}
}

/* OFFLINE BANNER */
function updateOnlineStatus()
{
	var d = document.body;
	d.className = d.className.replace(/\ offline\b/,'');
	
	if (!navigator.onLine)
	{
    d.className += " offline";
    console.log('Status:offline');
    //Usage:
    //notifyMe("APP Repositor", "Trabalhando offline");

	}
}

updateOnlineStatus();
window.addEventListener
(
	'load',
	function()
	{
		
		window.addEventListener('online',  updateOnlineStatus);
		window.addEventListener('offline', updateOnlineStatus);
	}
);

/* CHANGE PAGE TITLE BASED ON PAGE VISIBILITY */
function handleVisibilityChange()
{
	if (document.visibilityState == "hidden")
	{
		document.title = $('meta[name="app_name"]').attr('content');
	}
	else
	{
		document.title = original_title;
	}
}
var original_title = document.title;
document.addEventListener('visibilitychange', handleVisibilityChange, false);

/* NOTIFICATIONS */
window.addEventListener('load', function ()
{
	// At first, let's check if we have permission for notification
	// If not, let's ask for it
	if (window.Notification && Notification.permission !== "granted")
	{
		Notification.requestPermission(function (status)
		{
			if (Notification.permission !== status)
			{
        Notification.permission = status;
        console.log('Notificações:'+status);
			}
		});
	} else {
    console.log('Notificações ativas');
    notification.subscribe()
  }
});
function notifyMe(alert_title, alert_body)
{
	var options =
	{
		body: alert_body,
		icon: 'images/launcher-icon-4x.png'
	};
	
	// Let's check if the browser supports notifications
	if (!("Notification" in window))
	{
		alert("Para receber notificações, utilize navegador Chrome ou Firefox.");
		return false;
	}
	
	// Let's check whether notification permissions have already been granted
	else if (Notification.permission === "granted")
	{
		// If it's okay let's create a notification
		var notification = new Notification(alert_title,options);
		return true;
	}
	
	// Otherwise, we need to ask the user for permission
	else if (Notification.permission !== 'denied')
	{
		Notification.requestPermission(function (permission)
		{
			// If the user accepts, let's create a notification
			if (permission === "granted")
			{
				var notification = new Notification(alert_title,options);
				return true;
			}
		});
	}
	
	// Finally, if the user has denied notifications and you 
	// want to be respectful there is no need to bother them any more.
	console.log("Notifications denied");
	return false;
}

var notification = {
  loading: false,
  isPushEnabled: false,
  pushButtonDisabled: true,

    initialiseNotifications () {

      if (!('showNotification' in ServiceWorkerRegistration.prototype)) {
        console.log('Notifications aren\'t supported.')
        return
      }

      if (Notification.permission === 'denied') {
        console.log('The user has blocked notifications.')
        return
      }

      if (!('PushManager' in window)) {
        console.log('Push messaging isn\'t supported.')
        return
      }

      navigator.serviceWorker.ready.then(registration => {
        registration.pushManager.getSubscription()
          .then(subscription => {
            this.pushButtonDisabled = false;

            if (!subscription) {
              return
            }

			this.updateSubscription(subscription);
			
			this.isPushEnabled = true;
			
          })
          .catch(e => {
            console.log('Error during getSubscription()', e)
          })
      })
    },

    /**
     * Subscribe for push notifications.
     */
    subscribe () {
      navigator.serviceWorker.ready.then(registration => {
        const options = { userVisibleOnly: true }
        const vapidPublicKey = window.Laravel.vapidPublicKey

        if (vapidPublicKey) {
          options.applicationServerKey = this.urlBase64ToUint8Array(vapidPublicKey)
        }

        registration.pushManager.subscribe(options)
          .then(subscription => {
            this.isPushEnabled = true
            this.pushButtonDisabled = false

            this.updateSubscription(subscription)
          })
          .catch(e => {
            if (Notification.permission === 'denied') {
              console.log('Permission for Notifications was denied')
              this.pushButtonDisabled = true
            } else {
              console.log('Unable to subscribe to push.', e)
              this.pushButtonDisabled = false
            }
          })
      })
    },

    /**
     * Unsubscribe from push notifications.
     */
    unsubscribe () {
      navigator.serviceWorker.ready.then(registration => {
        registration.pushManager.getSubscription().then(subscription => {
          if (!subscription) {
            this.isPushEnabled = false
            this.pushButtonDisabled = false
            return
          }

          subscription.unsubscribe().then(() => {
            this.deleteSubscription(subscription)

            this.isPushEnabled = false;
            this.pushButtonDisabled = false;
          }).catch(e => {
            console.log('Unsubscription error: ', e)
            this.pushButtonDisabled = false;
          })
        }).catch(e => {
          console.log('Error thrown while unsubscribing.', e)
        })
      })
    },

    /**
     * Toggle push notifications subscription.
     */
    togglePush () {
      if (this.isPushEnabled) {
        this.unsubscribe();
      } else {
        this.subscribe();
      }
    },

    /**
     * Send a request to the server to update user's subscription.
     *
     * @param {PushSubscription} subscription
     */
    updateSubscription (subscription) {
      const key = subscription.getKey('p256dh')
	    const token = subscription.getKey('auth')

      const data = {
        endpoint: subscription.endpoint,
        key: key ? btoa(String.fromCharCode.apply(null, new Uint8Array(key))) : null,
        token: token ? btoa(String.fromCharCode.apply(null, new Uint8Array(token))) : null,
        user_id: user_id
      }

      this.loading = true;

      if(user_id != null){
        var request = $.ajax({
          url: api_url+"/subscriptions",
          method: "POST",
          headers: {
              'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
          },
          data: data
        });
         
        request.done(function( response ) {
          this.loading = false;
          this.isPushEnabled = true;
      console.log('Notificações ativas');
  
        });
         
        request.fail(function( jqXHR, textStatus ) {
          console.log( "Request failed: " + textStatus );
        });
      }
    
    },

    /**
     * Send a requst to the server to delete user's subscription.
     *
     * @param {PushSubscription} subscription
     */
    deleteSubscription (subscription) {
      const key = subscription.getKey('p256dh');
      console.log(btoa(String.fromCharCode.apply(null, new Uint8Array(key))));
      
      this.loading = true;
      
        var request = $.ajax({
          url: api_url+"/subscriptions/"+$('meta[name="user_id"]').attr('content'),
          method: "DELETE",
          headers: {
              'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
          },
          data: {
            key: btoa(String.fromCharCode.apply(null, new Uint8Array(key))),
            user_id: user_id,
            endpoint: subscription.endpoint 
          }
        });
         
        request.done(function( response ) {
          this.loading = false;
          this.isPushEnabled = false;
          alert('Notificações desabilitadas');
        });
         
        request.fail(function( jqXHR, textStatus ) {
          console.log( "Request failed: " + textStatus );
        });
  
      },

    /**
     * Send a request to the server for a push notification.
     */
    sendNotification () {
	  this.loading = true;

      var request = $.ajax({
        url: api_url+"/notifications",
        method: "POST",
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        }
      });
       
      request.done(function( response ) {
        this.loading = false;
        console.log('Notificação enviada com sucesso');
      });
       
      request.fail(function( jqXHR, textStatus ) {
        console.log( "Request failed: " + textStatus );
      });
    },

    /**
     * https://github.com/Minishlink/physbook/blob/02a0d5d7ca0d5d2cc6d308a3a9b81244c63b3f14/app/Resources/public/js/app.js#L177
     *
     * @param  {String} base64String
     * @return {Uint8Array}
     */
    urlBase64ToUint8Array (base64String) {
        const padding = '='.repeat((4 - base64String.length % 4) % 4);
        const base64 = (base64String + padding)
          .replace(/\-/g, '+')
          .replace(/_/g, '/') 

        const rawData = window.atob(base64)
        const outputArray = new Uint8Array(rawData.length)

        for (let i = 0; i < rawData.length; ++i) {
            outputArray[i] = rawData.charCodeAt(i)
        }

        return outputArray
    }
};

notification.initialiseNotifications();