$(document).ready(function(){
    if($('.filter-field .check:checked').length > 0){
        $('#filters-enabled').removeClass('d-none');
        if($('.filter-field .check:checked').length > 0){
            $('.filter-field .check:checked').each(function(i, el){
                let header = $(el).parents('.card').children('.card-header').text();
                let title = $(el).parent('.filter-field').text();
                $('#filters-enabled').append('<li class="list-inline-item"><span class="badge badge-secondary">+'+header+':'+title+'</span></li>'); 
            }); 
        }
    } 
    function search(){
        let url = '/search?';
        var params = {};
        params['term'] = $('#search-filter').val();
        if($('.filter-field .check:checked').length > 0){
            $('.filter-field .check:checked').each(function(i, el){
                let name = $(el).attr('name');
                if(typeof params[name] === "undefined" && $('input[name="'+name+'"]').length > 1){
                    params[name] = new Array();          
                }
                if($('input[name="'+name+'"]').length > 1){
                    params[name].push($(el).attr('value'));
                } else{
                    params[name] = $(el).attr('value');
                }
            });
        }
        var query_params = $.param(params);
        console.log(query_params);
        location.href=url+String(query_params);
    }
    $('.filter-field').on('click', function(e){
        search();
    });
    $('#search-filter').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            search();
        }
    });
});
