var buy = {
    browser: {
        getList: function(){
            return JSON.parse(localStorage.getItem("buy-list"));
        },
        parseForm: function(form){
            var result = {};
            for(let c = 0; c < form.length; c++){
                result[form[c].name] = form[c].value;
            }
            return result;
        },
        //when user submits the cart
        store: function(form){
            //sign pending buy
            const stamp = + new Date();
            localStorage.setItem('buy-sync', stamp);

            var cart = window.cart.browser.get();
            var buy_list = window.buy.browser.getList();

            if(buy_list == null)
            buy_list = new Array();

            //merge cart and form
            Object.assign(cart, this.parseForm(form));

            //add new buy
            buy_list.push(cart);
            var jsonStr = JSON.stringify( buy_list );
            localStorage.setItem('buy-list', jsonStr);
        
        },
        release: function(){
            localStorage.removeItem('buy-sync');
            localStorage.removeItem('buy-list');
        }
    },
    server: {
        removeItem(item_id, e_source){
            var request = $.ajax({
                headers: {
                    'x-csrf-token': $('meta[name="csrf-token"]').attr('content')
                },  
                url: api_url+"/item/"+item_id,
                method: "DELETE",    
                dataType: "html"
            });
            request.done(function( response ) {  
                
                window.cart.browser.displayRemovalAlert(e_source);

            });
            request.fail(function( jqXHR, textStatus ) {
                console.log( "Item remove Request failed: " + textStatus );
            });
        },
        sync: function(){
            //sync routine
   
                if (navigator.onLine && localStorage.getItem('buy-sync') != null){
                window.buy.server.storeAll();
                }
        },
        storeAll: function(){
            var request = $.ajax({
                headers: {
                    'x-csrf-token': $('meta[name="csrf-token"]').attr('content')
                },  
                url: api_url+"/buy-sync",
                method: "POST",
                data: {
                    session_id:  $('meta[name="session_id"]').attr('content'),
                    buy_list: window.buy.browser.getList()        
                },      
                dataType: "html"
            });
            request.done(function( response ) {      
                
                $('.alert.buy-sync').fadeIn(1500);
                $('.alert.buy-sync').fadeOut(3000); 
                $('.alert.buy-sync').remove();

                window.buy.browser.release();

            });
            request.fail(function( jqXHR, textStatus ) {
                console.log( "BUY-SYNC Request failed: " + textStatus );
            });
        },  
    }
};
//# sourceMappingURL=maps/buy.js.map
