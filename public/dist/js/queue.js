var source = new EventSource(api_url+'/queue');

function tableFill(queue){

    $('table#queue tbody tr').remove(); 
    
    $.each(queue, function( index, buy ) {
        var row_new = '<tr data-id="'+buy.id+'" class="success status-'+buy.status_id+'">'
        + '<td><a href="'+app_url+'/buy/'+buy.id+'">#'+buy.id+'</a></td>'
        + '<td>'+buy.customer+'</td>'
        + '<td>'+buy.attendant+'</td>'
        + '<td>'+buy.created_at_formatted+'</td>'
        + '</tr>';
        var row_find = $('table#queue tbody').find('tr[data-id="'+buy.id+'"]');
        if(row_find.length > 0){
            $(row_find).replaceWith(row_new).slideDown();
        }        
        else{
            if($('tr.status-'+buy.status_id).length > 0)
            $(row_new).insertBefore($('tr.status-'+buy.status_id+':first'));
            else
            $('table#queue tbody').prepend(row_new);
        }
    });

    if($('table#queue tbody tr').length >= queue_length)
    $('table#queue tbody tr:last-child').remove();
}
function rtQueueUpdate(source){
    console.log(source);
    
    if(source.readyState == 2)
    var source = new EventSource(api_url+'/queue');
    if(typeof(EventSource) !== "undefined") {
        var msg = null;
        source.onmessage = function(event) {
            var data_j = JSON.parse(event.data);
            var d = new Date();
            if(data_j.length == 0){
                $('#all-updated').removeClass('d-none').replaceWith('<tr id="all-updated"><td class="text-center" colspan="4" id="all-updated">Nenhuma atualização de status desde '+d.getHours() +':'+ d.getMinutes()+':'+ d.getSeconds()+' </td></tr>').slideDown();
            } else {
                $('#all-updated').addClass('d-none');
            }
            /* if(typeof data_j.queue != 'undefined') */
            tableFill(data_j.queue);
 
        };
    } else {
        msg = "Sorry, your browser does not support server-sent events...";
        console.log(msg);
    }
}

$(document).ready(function(){
    rtQueueUpdate(source);

    function playQueue(source){
        $('.play-queue').removeClass('btn-success');
        $('.play-queue').addClass('btn-disabled');
        $('.play-queue').find('.fa-play').addClass('d-none');
        $('.play-queue').find('.loading').removeClass('d-none');
        $('.play-queue').find('.text').addClass('d-none');
        $('.stop-queue').removeClass('btn-disabled');
        $('.stop-queue').addClass('btn-secondary');
        $('.stop-queue').removeAttr('disabled');
        rtQueueUpdate(source);
    }
    $('.play-queue').on('click', function(){
        playQueue(source);
    });
    function stopQueue(source){
        source.close();
        $('.play-queue').addClass('btn-success');
        $('.play-queue').removeClass('btn-disabled');
        $('.play-queue').removeAttr('disabled');
        $('.play-queue').find('.fa-play').removeClass('d-none');
        $('.play-queue').find('.loading').addClass('d-none');
        $('.play-queue').find('.text').removeClass('d-none');
        $('.stop-queue').removeClass('btn-secondary');
        $('.stop-queue').addClass('btn-disabled');
        $('.stop-queue').attr('disabled', 'disabled');
    }
    $('.stop-queue').on('click', function(){
      stopQueue(source);
    });
    $('#queue').on('click', '.status-change', function(){
        var buy_id = $(this).attr('data-buy-id');
        var status_id = $(this).attr('data-status-id');
        $('#status').attr('data-buy-id', buy_id);
        $('#status').attr('data-status-id', status_id);
        $('#status').val(status_id);
        stopQueue(source);
        $('#modal-status-change').modal('show');
    });
    $('#queue').on('change', '#status', function(){
        rtQueueUpdate(source);
    });

    $('#modal-status-change').on('hidden.bs.modal', function (e) {
        playQueue(source);
    });

});

//# sourceMappingURL=maps/queue.js.map
