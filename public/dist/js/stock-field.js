$(document).ready(function(){ 
    var field_available_count = $('.field-model .f-field-id').children('option').length;

    function checkFieldsAvailable(){

        if(field_available_count == 0)
        $('.group-field').addClass('d-none');
        else
        $('.group-field').removeClass('d-none');
    }
    
    function removeFieldInput(c){
        c.remove();
    }
    
    function addFieldInput(){
        var cm = $('.field-model').clone();
        cm.removeClass('d-none field-model').addClass('mt-3');
        
        if(field_available_count > 0)
        $('.wrap-fields').append(cm);
    }
    $('.add-field').on('click', function(){
        addFieldInput();
    });
    
    $('.wrap-fields').on('click', '.remove', function(){
        var c = $(this).parents('.field');
        removeFieldInput(c);
    });
    function filterFieldStocks(){
        
        if(field_available_count > 0)
        $('.w-add-field').removeClass('d-none');
    }

    function file_field(field){
        var content_input = field.parents('.field').find('.f-field-content');
        if(field.children('option:selected').val() == "1"){
            content_input.attr('type', 'file');
            content_input.addClass('custom-file-input');
            content_input.attr('accept', 'image/*,audio/*,video/*,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document');
            content_input.after('<label class="custom-file-label">Upload</label>');
        } else {
            content_input.siblings('.custom-file-label').remove();
            content_input.removeClass('custom-file-input');
            content_input.attr('type', 'text');
      
        }
    }

    function color_field(field){
        
    }

    $('body').on('change', '.f-field-id', function(){
        //file type
        //check for file
        file_field($(this));
        color_field($(this)); 
    });
    
    checkFieldsAvailable();
    filterFieldStocks();
    
});
//# sourceMappingURL=maps/stock-field.js.map
