'use strict';
const gulp = require('gulp');
const sass = require('gulp-sass');
const changed = require('gulp-changed');
const gutil = require('gulp-util');
var ftp = require( 'vinyl-ftp' );
const sourcemaps = require('gulp-sourcemaps');
const errorHandler = require('gulp-error-handle');
const logError = function(err) {
  gutil.log(err);
  //this.emit('end');
};
const minify = require('gulp-minify');
const { watch } = require('gulp');
function upload(){ 
  const project_globs = ['config/**', 'public/vendor/**/*','public/dist/**/*', 'resources/**/*', 'app/**/*', 'routes/**', 'database/**'];
  watch(project_globs, function(cb) {
    var conn = ftp.create( {
      host:     '185.28.21.171',
      user:     'u670721390',
      password: '@nadev-0403',
      parallel: 5,
      log:      gutil.log
    } );
    var globs = project_globs; 
    // using base = '.' will transfer everything to /public_html correctly
    // turn off buffering in gulp.src for best performance
    up_lin(conn, globs);
    up_nutreplus(conn, globs); 
    up_arezzo(conn, globs); 
    up_demo(conn, globs); 
    up_kely(conn, globs); 
    up_princesmoda(conn, globs);
    cb();
  });
}

function up_demo(conn, globs){ 
  return gulp.src( globs, { base: '.', buffer: false } )
  .pipe(errorHandler(logError))
  .pipe( conn.newerOrDifferentSize( '/public_html/demo/' ) ) // only upload newer files
  .pipe( conn.dest( '/public_html/demo/' ) );
}

function up_kely(conn, globs){ 
  return gulp.src( globs, { base: '.', buffer: false } )
  .pipe(errorHandler(logError))
  .pipe( conn.newerOrDifferentSize( '/public_html/domains/kely/' ) ) // only upload newer files
  .pipe( conn.dest( '/public_html/domains/kely/' ) );
}

function up_nutreplus(conn, globs){
  return gulp.src( globs, { base: '.', buffer: false } )
  .pipe(errorHandler(logError))
  .pipe( conn.newerOrDifferentSize( '/public_html/domains/nutreplus/' ) ) // only upload newer files
  .pipe( conn.dest( '/public_html/domains/nutreplus/' ) );
}
function up_arezzo(conn, globs){ 
  return gulp.src( globs, { base: '.', buffer: false } )
  .pipe(errorHandler(logError))
  .pipe( conn.newerOrDifferentSize( '/public_html/domains/arezzo' ) ) // only upload newer files
  .pipe( conn.dest( '/public_html/domains/arezzo' ) );
}
function up_princesmoda(conn, globs){
  return gulp.src( globs, { base: '.', buffer: false } )
  .pipe(errorHandler(logError))
  .pipe( conn.newerOrDifferentSize( '/public_html/domains/princesmoda/' ) ) // only upload newer files
  .pipe( conn.dest( '/public_html/domains/princesmoda/' ) );
}
function up_lin(conn, globs){
  return gulp.src( globs, { base: '.', buffer: false } )
  .pipe(errorHandler(logError))
  .pipe( conn.newerOrDifferentSize( '/public_html/domains/lin/' ) ) // only upload newer files
  .pipe( conn.dest( '/public_html/domains/lin/' ) );
}
function watch_assets(){
  watch(['public/css/sass/*.scss'], function(cb) {
    // body omitted
    gulp.src('public/css/sass/*.scss')
    .pipe(errorHandler(logError))
    .pipe(changed('public/dist/css'))
    .pipe(sass({outputStyle: 'compressed'})
    .on('error', sass.logError))
    .pipe(sourcemaps.init())
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest('public/dist/css'));
    cb();
  });
  watch(['public/js/*.js'], function(cb) {
    gulp.src(['public/js/*.js'])
    .pipe(errorHandler(logError))
    .pipe(changed('public/dist/js'))
    .pipe(minify())
    .pipe(sourcemaps.init())
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest('public/dist/js'))
    cb();
  });
}
function w(cb) {
  watch_assets(); 
  upload();
  cb();
}
exports.w = w;
